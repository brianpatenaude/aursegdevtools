/*
 * run_test.cpp
 *
 *  Created on: May 11, 2016
 *      Author: brian
 */
#include "AsrImage3D/AsrImage3D.h"
#include "aursurface/aursurface.h"

// 3rd party includes
#include <boost/filesystem.hpp>

using namespace std;
using namespace auris_segtools;
using namespace boost::filesystem;
void Usage()
{
    cout << "\n asrSurface-vtk2obj <SyntehticMesh.x> <NIFTI image> <ouput file>" << endl;
    cout << "\n Converts SuperD synthetic meshes to vtk in LPS \n" << endl;
}

int main(int argc, char* argv[])
{

    if(argc <= 2) {
        Usage();
        exit(EXIT_FAILURE);
    }

    path file(argv[1]);
    if(!exists(file)) {
        cerr << "\n File does not exists : " << file << "\n" << endl;
        return 1;
    } else if(!is_regular_file(file)) {
        cerr << "\n"
             << file << " is not a file"
             << "\n"
             << endl;
        return 1;
    }

    AsrImage3D<short> imRef;
    cout<<"Reading image...";
    imRef.readImage(string(argv[2]));
   cout<<"done."<<endl;
 string file_out(argv[3]);

    aursurface<float> surfSuperD;
    surfSuperD.readSuperDMesh(file.string(), imRef.GetSize(), imRef.getPixDim());
    vec3<float> origin{ imRef.getOrigin() };
    cout << "origin " << origin.to_string() << endl;
cout<<"done surface write "<<endl;
    imRef = 0;
    surfSuperD.drawSurface(imRef, static_cast<short>(1000));
    imRef.writeImage(file_out + ".nii.gz");


    //need to translate after the draw command, or else vertices may go iout of range
    surfSuperD.translate( vec3<float>(abs(origin.x) *-1,abs(origin.y) *-1,abs(origin.z) *-1));
    surfSuperD.write(file_out + ".vtk", aursurface<float>::VTK);
    return 0;
}
