//========================== ===================================================
// Auris Surgical Robotics, Inc.
// (C) Copyright 2016. All Rights Reserved.
//
//
//! \file       segmentation_utils.cpp
//! \brief
//! Segmentation specific methods
//!
//! \date       July 2016
//!
//! \author     Brian Patenaude
//!
//=============================================================================


#ifndef ASR_TIMER_H
#define ASR_TIMER_H

//STL includes 
#include <iostream>
#include <fstream>
#include <list>
#include <tuple>
namespace auris_segtools{
    class asrTimer{
    
    public: 
        asrTimer();
        void addTimeStamp(const std::string & label);
        std::string to_string();
    protected:
        
        std::list< std::tuple<clock_t,std::string> > m_timeStamps;
        
    };
}

#endif