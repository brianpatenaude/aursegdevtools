/*
 * run_test.cpp
 *
 *  Created on: May 11, 2016
 *      Author: brian
 */
#include "AsrImage3D/AsrImage3D.h"
#include "asrtreemodel/AsrTreeModel.h"
#include "aursurface/aursurface.h"
// 3rd party includes
#include <boost/filesystem.hpp>

using namespace std;
using namespace auris_segtools;
using namespace boost::filesystem;
void Usage()
{
    cout << "\n asrSurface-vtk2obj <image.nii.gz> <input_base_name> <ouput file>" << endl;
    cout << "\n Converts vida skeleton and model to LPS space \n" << endl;
}

int main(int argc, char* argv[])
{

    if(argc <= 2) {
        Usage();
        exit(EXIT_FAILURE);
    }

    AsrImage3D<short> imbase(argv[1]);
    vec3<float> origin{ imbase.getOrigin() };
    cout << "origin " << origin.to_string() << endl;

    string base(argv[2]);
    string baseout(argv[3]);

    path file_pts(base + "_cp.csv");
    if(!exists(file_pts)) {
        cerr << "\n File does not exists : " << file_pts << "\n" << endl;
        return 1;
    } else if(!is_regular_file(file_pts)) {
        cerr << "\n"
             << file_pts << " is not a file"
             << "\n"
             << endl;
        return 1;
    }

    AsrTreeModel treeModel;
    treeModel.readTreePoints(file_pts.generic_string());
    treeModel.scale(vec3<float>(1, -1, -1));
    treeModel.translate(vec3<float>(0, 0, imbase.zdim() * imbase.zsize()));
    treeModel.translate(vec3<float>(abs(origin.x) * -1, abs(origin.y) * -1, abs(origin.z) * -1));

    path file_model(base + "-model.vtk");
    if(!exists(file_model)) {
        cerr << "\n File does not exists : " << file_model << "\n" << endl;
        return 1;
    }

    aursurface<float> surf;
    surf.readVTK(file_model.generic_string());
    surf.scale(vec3<float>(1, -1, -1));
    surf.translate(vec3<float>(0, 0, imbase.zdim() * imbase.zsize()));
    surf.translate(vec3<float>(abs(origin.x) * -1, abs(origin.y) * -1, abs(origin.z) * -1));
    path file_modelOut(baseout + "-model.vtk");
    if(!exists(file_modelOut)) {
        surf.write(file_modelOut.generic_string(), aursurface<float>::WRITE_FORMAT::VTK);
    } else {
        cout << "ERROR: file already exists : " << file_modelOut.generic_string() << endl;
    }

    path file_ptsOut(baseout + "_cp.csv");
    path file_ptsSurfOut(baseout + "_cp.spheres.vtk");
    if(!exists(file_ptsOut)) {
        //        treePoints.writeSkeletonCSV(file_ptsOut.generic_string());
        treeModel.writeTreePoints(file_ptsOut.generic_string());
        treeModel.writeTreePointsAsSurface(file_ptsSurfOut.generic_string(), 1);
    } else {
        cout << "ERROR: file already exists : " << file_ptsOut.generic_string() << endl;
    }

    path file_airmeas(base + "_vida-airmeas.csv");
    if(!exists(file_airmeas)) {
        cerr << "\n File does not exists : " << file_airmeas << "\n" << endl;
        return 1;
    }
    path file_airmeasOut(baseout + "_vida-airmeas.csv");
    if(!exists(file_airmeasOut)) {
        //        treePoints.writeSkeletonCSV(file_ptsOut.generic_string());
        copy_file(file_airmeas, file_airmeasOut, copy_option::overwrite_if_exists);
    } else {
        cout << "ERROR: file already exists : " << file_airmeasOut.generic_string() << endl;
    }
    //
    //    aursurface<float> surfVTK(file.string());
    //    surfVTK.write(file_out,aursurface<float>::OBJ);

    return 0;
}
