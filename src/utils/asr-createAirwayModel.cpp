/*
 * run_test.cpp
 *
 *  Created on: May 11, 2016
 *      Author: brian
 */

// STL includes
#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <vector>
// 3rd party includes
//#include <boost/filesystem.hpp>

using namespace std;
//using namespace boost::filesystem;
void Usage()
{
    cout << "\n asr-createAirwayNodel <list of geometry files? " << endl;
    cout << "\n This program is used to format a generational tree model for purpose of the segment \n" << endl;
}

int main(int argc, char* argv[])
{
    if(argc < 2) {
        Usage();
        exit(EXIT_FAILURE);
    }

    ifstream fin(argv[1]);
    if(fin.is_open()) {
        string line;
        // thow out first line
        getline(fin, line);
        cout << "header " << line << endl;
        map<unsigned int, float> gen2maxV;
        while(getline(fin, line)) {
            unsigned int gen, N;
            float maxV, meanV, varV;
            float maxL, meanL, varL;
            stringstream ss(line);
            ss >> gen >> N >> maxV >> meanV >> varV >> maxL >> meanL >> meanL;
            gen2maxV[gen] = maxV;
        }
        cout<<"MaxVolume by generation"<<endl;
        auto i_g2v = gen2maxV.begin();
        cout<<"{ "<<(i_g2v++)->second;
        for ( ; i_g2v != gen2maxV.end(); ++i_g2v ){
//            cout<<g2v.first<<" "<<g2v.second<<endl;
            cout<<" , "<<i_g2v->second;
        }
        cout<<" }"<<endl;
    }

    return 0;
}
