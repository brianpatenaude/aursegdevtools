/*
 * run_test.cpp
 *
 *  Created on: May 11, 2016
 *      Author: brian
 */
#include "aursurface/aursurface.h"

#include "AsrImage3D/AsrImage3D.h"
#include "aurcommon/miscfunctions.h"
#include "aurcommon/aurstructs_functions.hpp"
#include <glm/gtx/string_cast.hpp>
//STL includes
#include <iostream>
#include <vector>
#include <fstream>
#include <map>
//3rd party includes
#include <boost/filesystem.hpp>

using namespace std;
using namespace auris_segtools;
using namespace boost::filesystem;
void Usage(){
  cout<<"\n asrim-getLargestCC <nifit file> <ouput file>"<<endl;
  cout<<"\n This program is used  to extract the largest connected component from an image \n"<<endl;

}
  


int main(int argc, char*argv[])
{

    if (argc <= 2)
    {
        Usage();
        exit (EXIT_FAILURE);
    }
 
  path file(argv[0]);
  if (!exists(file))
    {
      cerr<<"\n File does not exists : "<<file<<"\n"<<endl;
      return 1;
    }else if (!is_regular_file(file))
    {
      cerr<<"\n"<<file<<" is not a file"<<"\n"<<endl;
      return 1;
    }

  string filename(argv[1]);
  string filename_out(argv[2]);


  //the air image should preserve more of the wall between lungs and trachea
  AsrImage3D<short> imMask;
    imMask.readImage(filename);

    imMask.connectedComponents(imMask,1);
    imMask.writeImage(filename_out);
    return 0;

}

