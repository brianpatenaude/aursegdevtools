
#ifndef ASR_ITKREADWRITE_H
#define ASR_ITKREADWRITE_H

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkNiftiImageIOFactory.h"
#include <itkNiftiImageIO.h>
#include <itkImageToImageFilter.h>

//template<class FilterType, class TOutputImageType>
//void writeNIFTI( typename FilterType::Pointer filterIn,  const std::string & filename )
template <class TOutputImageType> 
void writeNIFTI( const typename TOutputImageType::Pointer imageIn,  const std::string & filename )
{
    typedef itk::ImageFileWriter<TOutputImageType> WriterType;
    itk::NiftiImageIO::Pointer nifti_io = itk::NiftiImageIO::New();

    typename WriterType::Pointer writer = WriterType::New();
    writer->SetImageIO(nifti_io);
    writer->SetFileName(filename);
    writer->SetInput(imageIn);
//    writer->SetInput(filterIn->GetOutput());
    //    writer->SetInput(distanceMapImageFilter->GetOutput());
    writer->Update();
}

#endif