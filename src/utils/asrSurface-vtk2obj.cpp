/*
 * run_test.cpp
 *
 *  Created on: May 11, 2016
 *      Author: brian
 */
#include "aursurface/aursurface.h"

// 3rd party includes
#include <boost/filesystem.hpp>

using namespace std;
using namespace auris_segtools;
using namespace boost::filesystem;
void Usage() {
  cout << "\n asrSurface-vtk2obj <nifit file> <ouput file>" << endl;
  cout << "\n Converts VTK format surfaces to wavefront obj file, assumes basic surface (only support ASCII at the moment) \n" << endl;
}

int main(int argc, char *argv[]) {

  if (argc <= 2) {
    Usage();
    exit(EXIT_FAILURE);
  }

  path file(argv[1]);
  if (!exists(file)) {
    cerr << "\n File does not exists : " << file << "\n" << endl;
    return 1;
  } else if (!is_regular_file(file)) {
    cerr << "\n"
         << file << " is not a file"
         << "\n"
         << endl;
    return 1;
  }

  string file_out(argv[2]);

    aursurface<float> surfVTK(file.string());
    surfVTK.write(file_out,aursurface<float>::OBJ);


  return 0;
}
