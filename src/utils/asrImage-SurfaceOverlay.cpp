/*
 * run_test.cpp
 *
 *  Created on: May 11, 2016
 *      Author: brian
 */
#include "aursurface/aursurface.h"
#include "aursurface/aurcontour.h"

#include "aurcommon/aurstructs_functions.hpp"
#include "aurcommon/miscfunctions.h"
#include "AsrImage3D/AsrImage3D.h"
#include "asr-filehandling.hpp"

// STL includes
#include <fstream>
#include <iostream>
#include <map>
#include <vector>

#include <QtCore/QDir>
using namespace std;
using namespace auris_segtools;
using namespace boost::filesystem;
void Usage() {
  cout << "\n asrImage-SurfaceOverlay <--itkWorldCoordinates> <CT image> <Surfac file> <output>" << endl;
  cout << "\n Draw surface onto base image\n" << endl;
}


int main(int argc, char *argv[]) {

  if (argc <= 3) {
    Usage();
    exit(EXIT_FAILURE);
  }
 bool useITKworld{false};
  int argIndex =1; 
  if ( argv[argIndex] == "--itkWorldCoordinates")
  {
      argIndex++;
      useITKworld=true;
  } 
   
   
//read input 
  string CTfilename(argv[argIndex++]);
  if (!checkFileExists(CTfilename))  return 1; 
  string surfaceFilename(argv[argIndex++]);
  string filename_out(argv[argIndex++]);
    
  AsrImage3D<short> imCT(CTfilename);//read CT image 
  vec3<float> vox2mmShift = imCT.getVox2mmShift();
  cout<<"shift and scale "<<vox2mmShift.to_string()<<endl;
  vec3<float> vox2mmScale = imCT.getVox2mmScale();
    vox2mmShift.x*=(vox2mmScale.x<0)? -1 : 1 ;
    vox2mmShift.y*=(vox2mmScale.y<0)? -1 : 1 ;
    vox2mmShift.z*=(vox2mmScale.z<0)? -1 : 1 ;
  cout<<"shift and scale "<<vox2mmShift.to_string()<<endl;
  vox2mmShift*=-1;
  
  
  cout<<"shift and scale "<<vox2mmShift.to_string()<<endl;
  cout<<"shift and scale "<<vox2mmScale.to_string()<<endl;
  AsrImage3D<short> imSurface = imCT;
  imSurface = 0; 

    aursurface<float> surf(surfaceFilename);
    if (useITKworld){
        surf.translate(vox2mmShift);
    }
    surf.drawSurface(imSurface,static_cast<short>(1));

    imSurface.writeImage(filename_out);
  
  return 0;
}
