/*
 * run_test.cpp
 *
 *  Created on: May 11, 2016
 *      Author: brian
 */
#include "aursurface/aursurface.h"
#include "aursurface/aurcontour.h"

#include "aurcommon/aurstructs_functions.hpp"
#include "aurcommon/miscfunctions.h"
#include "AsrImage3D/AsrImage3D.h"
#include "asr-filehandling.hpp"

// STL includes
#include <fstream>
#include <iostream>
#include <map>
#include <vector>

#include <QtCore/QDir>
using namespace std;
using namespace auris_segtools;
using namespace boost::filesystem;
void Usage() {
  cout << "\n asrImage-EMoverlay <CT image> <EM directory> " << endl;
  cout << "\n Overlay EM trace on CT image \n" << endl;
}


int main(int argc, char *argv[]) {

  if (argc <= 2) {
    Usage();
    exit(EXIT_FAILURE);
  }
 
  int argIndex =1; 
   
//read input 
  string CTfilename(argv[argIndex++]);
//  if (!checkFileExists(CTfilename))  return 1; 
  QString EMdirname(argv[argIndex++]);
//  string filename_out(argv[argIndex++]);
    
    //get EM data from text files
    QDir EMdir(EMdirname);
    EMdir.setNameFilters(QStringList{"emData_registered_?????_*.txt"});
    QStringList filesScope = EMdir.entryList();
    EMdir.setNameFilters(QStringList{"emData_registered_GW_?????_*.txt"});
    QStringList filesGW = EMdir.entryList();
    
    
  AsrImage3D<short> imCT(CTfilename);//read CT image 
  vec3<float> vox2mmShift = imCT.getVox2mmShift();
  cout<<"shift and scale "<<vox2mmShift.to_string()<<endl;
  vec3<float> vox2mmScale = imCT.getVox2mmScale();
    vox2mmShift.x*=(vox2mmScale.x<0)? -1 : 1 ;
    vox2mmShift.y*=(vox2mmScale.y<0)? -1 : 1 ;
    vox2mmShift.z*=(vox2mmScale.z<0)? -1 : 1 ;
  cout<<"shift and scale "<<vox2mmShift.to_string()<<endl;
  vox2mmShift*=-1;
  
  
  cout<<"shift and scale "<<vox2mmShift.to_string()<<endl;
  cout<<"shift and scale "<<vox2mmScale.to_string()<<endl;
  AsrImage3D<short> imScope = imCT;
  AsrImage3D<short> imGuideWire = imCT;
  
  //process scope
    for ( auto em_segment = filesScope.begin(); em_segment != filesScope.end(); ++em_segment ){
        imScope=0;
        cout<<"Files "<<em_segment->toStdString()<<endl;
//        QStringList fileByParts = em_segment->split("_");
//        QString OutputFile{*(fileByParts.begin()+3)};
//        for (auto i_fpart = fileByParts.begin() +4; i_fpart != fileByParts.end(); ++i_fpart )
//        {
//            OutputFile.append(QString("_") + *i_fpart);
//        }
        
        QString OutputFile{*em_segment};
        OutputFile.replace(".txt",".nii.gz");
        cout<<"output "<<OutputFile.toStdString()<<endl;
        aurcontour<float> em_polyline; 
        cout<<"read "<<(EMdir.path() + "/"+ *em_segment).toStdString()<<endl;
        em_polyline.readEMcsv( (EMdir.path() + "/"+ *em_segment).toStdString());
//                        em_polyline.printVertices();
cout<<"-----------------"<<endl;
        em_polyline.translate(vox2mmShift);
//        em_polyline.scale((vox2mmScale.x<0)? -1 : 1 , (vox2mmScale.y<0)? -1 : 1, (vox2mmScale.z<0)? -1 : 1 );
//        em_polyline.printVertices();
        
        
        em_polyline.drawOpenContour(imScope,static_cast<short>(1));
        imScope.writeImage(EMdirname.toStdString() + "/" + OutputFile.toStdString());
    }
  
  //process guidewire
    for ( auto em_segment = filesGW.begin(); em_segment != filesGW.end(); ++em_segment ){
        imGuideWire=0;
        cout<<"Files "<<em_segment->toStdString()<<endl;
        QString OutputFile{*em_segment};
        OutputFile.replace(".txt",".nii.gz");
        
        
        aurcontour<float> em_polyline; 
        em_polyline.readEMcsv( (EMdir.path() + "/"+ *em_segment).toStdString());
        em_polyline.translate(vox2mmShift);
        em_polyline.drawOpenContour(imGuideWire,static_cast<short>(1));
    imGuideWire.writeImage(EMdirname.toStdString() + "/" + OutputFile.toStdString());
    }
  
  
  
    
  
  return 0;
}
