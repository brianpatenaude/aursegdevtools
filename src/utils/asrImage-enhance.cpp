/*
 * run_test.cpp
 *
 *  Created on: May 11, 2016
 *      Author: brian
 */
#include "asr-timer.h"
#include "aursurface/aursurface.h"
// itk
#include "aurcommon/aurstructs_functions.hpp"
#include "aurcommon/miscfunctions.h"
#include "aurimage/AsrImage3D.h"
#include "itkCurvatureAnisotropicDiffusionImageFilter.h"
#include "itkGradientMagnitudeRecursiveGaussianImageFilter.h"
#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkNiftiImageIOFactory.h"
#include "itkRecursiveGaussianImageFilter.h"
#include <itkDiscreteGaussianImageFilter.h>
#include <itkHessian3DToVesselnessMeasureImageFilter.h>
#include <itkHessianRecursiveGaussianImageFilter.h>

#include <itkMeanImageFilter.h>
#include <itkMedianImageFilter.h>
#include <itkNiftiImageIO.h>
// STL includes
#include <fstream>
#include <iostream>
#include <map>
#include <time.h>
#include <vector>
// 3rd party includes
#include <boost/filesystem.hpp>

using namespace std;
using namespace auris_segtools;
using namespace boost::filesystem;
void Usage()
{
    cout << "\n asrImage-enhance [-vesselness,-curvADiffusion,-RecursiveGaussian <varmm>,-mean] <nifit file> <ouput file>" << endl;
    cout << "\n Reduce noise in image. \n" << endl;
}

typedef itk::Image<short, 3> ShortImageType;
typedef itk::Image<float, 3> FloatImageType;
typedef itk::ImageFileReader<ShortImageType> ReaderType;
typedef itk::ImageFileWriter<FloatImageType> FloatWriterType;
typedef itk::ImageFileWriter<ShortImageType> ShortWriterType;
typedef itk::CurvatureAnisotropicDiffusionImageFilter<ShortImageType, FloatImageType> curvADiffusion;
// using medianFilter = itk::MedianImageFilter<ShortImageType, ShortImageType>;
using medianFilter = itk::MedianImageFilter<ShortImageType, FloatImageType>;
using meanFilter = itk::MeanImageFilter<ShortImageType, FloatImageType>;
using discreteGaussianFilter = itk::DiscreteGaussianImageFilter<ShortImageType, FloatImageType>;
using recursiveGaussianFilterType = itk::RecursiveGaussianImageFilter<ShortImageType, FloatImageType>;
using filterType = itk::ImageToImageFilter<ShortImageType, FloatImageType>;

int main(int argc, char* argv[])
{
    asrTimer asrTimeLog;

    itk::NiftiImageIOFactory::RegisterOneFactory();

    enum FILTER_TYPE { MEDIAN, MEAN, CURV_ANISOTROPIC_DIFFUSION, DGAUSSIAN, RGAUSSIAN, VESSELNESS };
    // default value
    FILTER_TYPE ftype{ MEDIAN };

    if(argc <= 2) {
        Usage();
        exit(EXIT_FAILURE);
    }

    double VARIANCE{ 1.0 };

    double HESSIAN_SIGMA{ 1.0 };
    double HESSIAN_ALPHA1{ 1.0 };
    double HESSIAN_ALPHA2{ 1.0 };
    unsigned int Niterations{ 0 };
    string maskname;
    // parse input
    int argIndex = 1;
    if(string(argv[argIndex]) == "-vesselness") {
        ftype = VESSELNESS;
        HESSIAN_SIGMA = atof(argv[++argIndex]);
        HESSIAN_ALPHA1 = atof(argv[++argIndex]);
        HESSIAN_ALPHA2 = atof(argv[++argIndex]);
        maskname = argv[++argIndex];
        argIndex++;
    } else if(string(argv[argIndex]) == "-RecursiveGaussian") {
        ftype = RGAUSSIAN;
        VARIANCE = atof(argv[++argIndex]);
        ++argIndex;
    } else if(string(argv[argIndex]) == "-curvADiffusion") {
        ftype = CURV_ANISOTROPIC_DIFFUSION;
        Niterations = atoi(argv[++argIndex]);
        ++argIndex;
    } else if(string(argv[argIndex]) == "-mean") {
        ftype = MEAN;
        ++argIndex;
    }

    path file(argv[argIndex]);
    if(!exists(file)) {
        cerr << "\n File does not exists : " << file << "\n" << endl;
        return 1;
    } else if(!is_regular_file(file)) {
        cerr << "\n"
             << file << " is not a file"
             << "\n"
             << endl;
        return 1;
    }
    cout << argIndex << " " << argv[argIndex] << endl;

    string filename(argv[argIndex++]);
    string filename_out(string(argv[argIndex]) + ".nii.gz");

    ShortImageType::Pointer image;
    ReaderType::Pointer reader = ReaderType::New();
    reader->SetFileName(filename);
    reader->Update();
    image = reader->GetOutput();

    asrTimeLog.addTimeStamp("strating smoothing");
    itk::NiftiImageIO::Pointer nifti_io = itk::NiftiImageIO::New();
    clock_t t = clock();

    filterType::Pointer imFilter;

    if(ftype == MEDIAN) {
        medianFilter::Pointer medianFilter = medianFilter::New();
//        imFilter = medianFilter::New();
        medianFilter->SetRadius(3);
//        imFilter->SetRadius(3);
        imFilter = medianFilter;
    } else if(ftype == VESSELNESS) {
        //    typedef double     InputPixelType;
        //    typedef float      OutputPixelType;
        //  typedef itk::Image< InputPixelType, Dimension >  InputImageType;
        //  typedef itk::Image< OutputPixelType, Dimension > OutputImageType;
        //  FloatImageType::RegionType desiredRegion(start,patchSize);
        cout << "Do Vesselness " << endl;
        ReaderType::Pointer maskReader = ReaderType::New();
        maskReader->SetFileName(maskname);
        maskReader->Update();
        FloatImageType::RegionType largestRegion = maskReader->GetOutput()->GetLargestPossibleRegion();

        typedef itk::HessianRecursiveGaussianImageFilter<ShortImageType> HessianFilterType;
        typedef itk::Hessian3DToVesselnessMeasureImageFilter<float> VesselnessMeasureFilterType;
        HessianFilterType::Pointer hessianFilter = HessianFilterType::New();
        hessianFilter->SetInput(reader->GetOutput());
        hessianFilter->GetOutput()->SetRequestedRegion(largestRegion);
        hessianFilter->SetSigma(HESSIAN_SIGMA);

        cout << "Done Hessian " << endl;
        VesselnessMeasureFilterType::Pointer vesselnessFilter = VesselnessMeasureFilterType::New();
        vesselnessFilter->GetOutput()->SetRequestedRegion(largestRegion);
        vesselnessFilter->SetInput(hessianFilter->GetOutput());

        FloatWriterType::Pointer writer = FloatWriterType::New();
        cout << "Done Vesselness " << endl;

        writer->SetImageIO(nifti_io);

        writer->SetInput(vesselnessFilter->GetOutput());
        writer->SetFileName(filename_out);
        vesselnessFilter->SetAlpha1(HESSIAN_ALPHA1);
        vesselnessFilter->SetAlpha2(HESSIAN_ALPHA2);
        writer->Update();
    } else if(ftype == CURV_ANISOTROPIC_DIFFUSION) {
        cout << "do anisotropic diffusion" << endl;
        curvADiffusion::Pointer imFilter = curvADiffusion::New();
        imFilter->SetInput(reader->GetOutput());
        imFilter->SetNumberOfIterations(Niterations);
        imFilter->SetTimeStep(0.02);
    } else if(ftype == RGAUSSIAN) {
        cout<<"do recursive gaussian filter"<<endl;
        recursiveGaussianFilterType::Pointer rGaussFilter = recursiveGaussianFilterType::New();
        rGaussFilter->SetInput(reader->GetOutput());
        rGaussFilter->SetSigma(sqrt(VARIANCE));
            imFilter = rGaussFilter;
//    imFilter = recursiveGaussianFilterType::New();
//    imFilter->SetSigma(sqrt(VARIANCE));

    } else if(ftype == MEAN) {
        cout<<"new mean "<<endl;
        imFilter = meanFilter::New();
    }
    if(ftype != VESSELNESS) {
            imFilter->SetInput(reader->GetOutput());
        FloatWriterType::Pointer writer = FloatWriterType::New();
        writer->SetImageIO(nifti_io);

        writer->SetFileName(filename_out);
        writer->SetInput(imFilter->GetOutput());
        writer->Update();
    }
    asrTimeLog.addTimeStamp("done smoothing");
    t = clock() - t;
    cout << "It took " << t / CLOCKS_PER_SEC << " seconds to compute the filter and write the image." << endl;
    ofstream ftime(filename_out + "time.log");
    if(ftime.is_open()) {
        ftime << asrTimeLog.to_string() << endl;
        ftime.close();
    }

    return 0;
}
