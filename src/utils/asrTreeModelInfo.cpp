/*
 * run_test.cpp
 *
 *  Created on: May 11, 2016
 *      Author: brian
 */
#include "asrtreemodel/AsrTreeModel.h"

// 3rd party includes
#include <boost/filesystem.hpp>

using namespace std;
using namespace auris_segtools;
using namespace boost::filesystem;
void Usage() {
  cout << "\n asrTreeModelInfo <tree_structure.csv> <tree_points.csv>" << endl;
  cout << "\n Analyzes airway trees resulting from skeletonization \n" << endl;
}

int main(int argc, char *argv[]) {

  if (argc <= 2) {
    Usage();
    exit(EXIT_FAILURE);
  }

  string fileTreeStructure(argv[1]);
  string filePoints(argv[2]);
    
    AsrTreeModel tree;
    tree.readTreeStructure(fileTreeStructure);
    tree.readTreePoints(filePoints);
    tree.leafStats();

  return 0;
}
