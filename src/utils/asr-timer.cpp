//========================== ===================================================
// Auris Surgical Robotics, Inc.
// (C) Copyright 2016. All Rights Reserved.
//
//
//! \file       asr-timer.cpp
//! \brief
//! Object used to keep tracked of timestamp
//!
//! \date       July 2016
//!
//! \author     Brian Patenaude
//!
//=============================================================================

#include <asr-timer.h>

#include <sstream>
using namespace std;

namespace auris_segtools
{
asrTimer::asrTimer()
{
    m_timeStamps.push_back(make_pair(clock(), "initial"));
}
void asrTimer::addTimeStamp(const std::string& label)
{
    m_timeStamps.push_back(make_pair(clock(), label));
}
std::string asrTimer::to_string()
{
    
    if(m_timeStamps.empty())
        return "";

    auto i_prev = m_timeStamps.begin();
    auto i_cur = m_timeStamps.begin();
    ++i_cur;

    clock_t initialTime{ get<0>(*i_prev) };
    stringstream ss;
    ss << "[ time stamp ] : [ time since last stamp] : stamp label" << endl;
    ss << "[ 0 seconds ] : [ 0 seconds] : starting timer" << endl;
    
    for(; i_cur != m_timeStamps.end(); ++i_cur, ++i_prev) {
        ss << "[ " << (get<0>(*i_cur) - initialTime) / CLOCKS_PER_SEC << " seconds ] : [ "
           << static_cast<float>(get<0>(*i_cur) - get<0>(*i_prev)) / CLOCKS_PER_SEC << " seconds ] : " << get<1>(*i_cur) << endl;
    }

    return ss.str();
}
}