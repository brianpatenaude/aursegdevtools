#ifndef ASRFILEHANDLING_H
#define ASRFILEHANDLING_H
// 3rd party includes
#include <boost/filesystem.hpp>
#include <list>

namespace auris_segtools{

bool checkFileExists( const boost::filesystem::path & filename )
{
    if (!exists(filename)) {
        std::cerr << "\n File does not exist : " << filename<< "\n" << std::endl;
        return 0;
    } else if (!is_regular_file(filename)) {
        std::cerr << "\n"
             << filename << " is not a file"
             << "\n"
             << std::endl;
        return 0;
    }

    return 1;
} 
}

#endif