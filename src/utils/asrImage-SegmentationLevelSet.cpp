/*
 * run_test.cpp
 *
 *  Created on: May 11, 2016
 *      Author: brian
 */
#include "aursurface/aursurface.h"
// itk
#include "itkImage.h"
#include "itkImageIterator.h"

#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkNiftiImageIOFactory.h"
#include <itkDiscreteGaussianImageFilter.h>
#include <itkMedianImageFilter.h>
#include <itkNiftiImageIO.h>
#include <itkHessian3DToVesselnessMeasureImageFilter.h>
#include <itkHessianRecursiveGaussianImageFilter.h>
#include <itkSobelEdgeDetectionImageFilter.h>
#include <itkCurvesLevelSetImageFilter.h>
#include "aurcommon/aurstructs_functions.hpp"
#include "aurcommon/miscfunctions.h"
#include "aurimage/AsrImage3D.h"
// STL includes
#include <fstream>
#include <iostream>
#include <map>
#include <vector>
// 3rd party includes
#include <boost/filesystem.hpp>

using namespace std;
using namespace auris_segtools;
using namespace boost::filesystem;
void Usage() {
  cout << "\n asrImage-enhance <nifit file> <ouput file>" << endl;
  cout << "\n Reduce noise in image. \n" << endl;
}

typedef itk::Image<short, 3> ShortImageType;
typedef itk::Image<float, 3> FloatImageType;
typedef itk::ImageFileReader<ShortImageType> ReaderType;
typedef itk::ImageFileReader<FloatImageType> FloatReaderType;
typedef itk::ImageFileWriter<FloatImageType> FloatWriterType;
typedef itk::ImageFileWriter<ShortImageType> ShortWriterType;

using medianFilter = itk::MedianImageFilter<ShortImageType, ShortImageType>;
using gaussianFilter = itk::DiscreteGaussianImageFilter<ShortImageType, FloatImageType>;

int main(int argc, char *argv[]) {
  itk::NiftiImageIOFactory::RegisterOneFactory();

  enum FILTER_TYPE { MEDIAN, MEAN, VESSELNESS };
  // default value
  FILTER_TYPE ftype{MEDIAN};

  if (argc <= 2) {
    Usage();
    exit(EXIT_FAILURE);
  }

    double HESSIAN_SIGMA{1.0};
    double HESSIAN_ALPHA1{1.0};
    double HESSIAN_ALPHA2{1.0};

    //parse input
    int argIndex = 1; 
//    if ( string(argv[argIndex]) == "-vesselness" )
//    {
//        ftype = VESSELNESS;
//        HESSIAN_SIGMA = atof(argv[++argIndex]);
//        HESSIAN_ALPHA1 = atof(argv[++argIndex]);
//        HESSIAN_ALPHA2 = atof(argv[++argIndex]);
//        argIndex++;
//    }



  path file(argv[argIndex]);
  if (!exists(file)) {
    cerr << "\n File does not exists : " << file << "\n" << endl;
    return 1;
  } else if (!is_regular_file(file)) {
    cerr << "\n"
         << file << " is not a file"
         << "\n"
         << endl;
    return 1;
  }

    
  string filename(argv[argIndex++]);
  string zeroLevelSetname(argv[argIndex++]);
  string filename_out(argv[argIndex]);

//  ShortImageType::Pointer image;
//  ReaderType::Pointer reader = ReaderType::New();
  FloatImageType::Pointer image;
  FloatReaderType::Pointer reader = FloatReaderType::New();
  reader->SetFileName(filename);
  reader->Update();
  image = reader->GetOutput();
  cout<<"read zero level set "<<zeroLevelSetname<<endl;
  
    FloatImageType::Pointer imageZeroLevelSet;
  FloatReaderType::Pointer readerLevelSet = FloatReaderType::New();
  readerLevelSet->SetFileName(zeroLevelSetname);
  readerLevelSet->Update();
  imageZeroLevelSet = readerLevelSet->GetOutput();
  
  

  itk::NiftiImageIO::Pointer nifti_io = itk::NiftiImageIO::New();
    typedef itk::SobelEdgeDetectionImageFilter<FloatImageType,FloatImageType> edgeDectorType;
//    typedef itk::SobelEdgeDetectionImageFilter<ShortImageType,FloatImageType> edgeDectorType;
    edgeDectorType::Pointer edgeDetector = edgeDectorType::New();
//    edgeDetector->SetRadius(3);
    edgeDetector->SetInput(image);
edgeDetector->Update();
FloatImageType::Pointer imEdge = edgeDetector->GetOutput();
  itk::ImageRegionIterator<FloatImageType>  it( imEdge,imEdge->GetRequestedRegion() );
//    for (it = it.Begin(); !it.IsAtEnd(); ++it)
//    {
//    it.Set( 1 );
//    }
 
  while(!it.IsAtEnd())
    {
    // Get the value of the current pixel
    //unsigned char val = imageIterator.Get();
    //std::cout << (int)val << std::endl;
 
    // Set the current pixel to white
    it.Set(1.0/(it.Get()+1));
 
    ++it;
    }
 

    FloatWriterType::Pointer writer = FloatWriterType::New();
    writer->SetImageIO(nifti_io);
  writer->SetFileName(filename_out + "_edgePotential.nii.gz");
    writer->SetInput(edgeDetector->GetOutput());
    writer->Update();
    cout<<"run segmentation"<<endl;
     typedef itk::CurvesLevelSetImageFilter< FloatImageType,FloatImageType , float> levelSetType;
     levelSetType::Pointer airwaySeg = levelSetType::New(); 
// airwaySeg->SetPropagationScaling( propagationScaling );
  airwaySeg->SetCurvatureScaling( 1.0 );
  airwaySeg->SetAdvectionScaling( 1.0 );


  airwaySeg->SetMaximumRMSError( 0.02 );
  airwaySeg->SetNumberOfIterations( 800 );
    airwaySeg->SetInitialImage(imageZeroLevelSet);
    airwaySeg->SetFeatureImage(imEdge);
    cout<<"run segmentation "<<endl;
    airwaySeg->Update();
    cout<<"run segmentation done "<<endl;
    FloatWriterType::Pointer segwriter = FloatWriterType::New();
    segwriter->SetImageIO(nifti_io);
  segwriter->SetFileName(filename_out + "_curvesLevelSet.nii.gz");
    segwriter->SetInput(airwaySeg->GetOutput());
    segwriter->Update();
  return 0;
}
