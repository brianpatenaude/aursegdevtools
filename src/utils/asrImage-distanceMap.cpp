/*
 * run_test.cpp
 *
 *  Created on: May 11, 2016
 *      Author: brian
 */
#include "aursurface/aursurface.h"
//itk 
#include <itkSignedMaurerDistanceMapImageFilter.h>
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkImage.h"
#include "itkNiftiImageIOFactory.h"
#include <itkNiftiImageIO.h>

#include "aurcommon/aurstructs_functions.hpp"
#include "aurcommon/miscfunctions.h"
#include "aurimage/AsrImage3D.h"
// STL includes
#include <fstream>
#include <iostream>
#include <map>
#include <vector>
// 3rd party includes
#include <boost/filesystem.hpp>
#include "utils/asr-timer.h"

using namespace std;
using namespace auris_segtools;
using namespace boost::filesystem;
void Usage() {
  cout << "\n asrim-distanceMap <nifit file> <ouput file>" << endl;
  cout << "\n This program is used  to calculate a distance map to the outeredge of a mask an image \n" << endl;
}

typedef itk::Image<int,3> ImageType;
typedef itk::Image<float,3> FloatImageType;
typedef itk::ImageFileReader<ImageType> ReaderType;

int main(int argc, char *argv[]) {
  itk::NiftiImageIOFactory::RegisterOneFactory();

  if (argc <= 2) {
    Usage();
    exit(EXIT_FAILURE);
  }

  path file(argv[0]);
  if (!exists(file)) {
    cerr << "\n File does not exists : " << file << "\n" << endl;
    return 1;
  } else if (!is_regular_file(file)) {
    cerr << "\n"
         << file << " is not a file"
         << "\n"
         << endl;
    return 1;
  }

  string filename(argv[1]);
  string filename_out(argv[2]);
  
    ImageType::Pointer image;
    ReaderType::Pointer reader = ReaderType::New();
    reader->SetFileName(argv[1]);
     reader->Update();
    image = reader->GetOutput();
         asrTimer algoTimer;

  typedef  itk::SignedMaurerDistanceMapImageFilter< ImageType, FloatImageType  > SignedMaurerDistanceMapImageFilterType;
  SignedMaurerDistanceMapImageFilterType::Pointer distanceMapImageFilter = SignedMaurerDistanceMapImageFilterType::New();
  distanceMapImageFilter->SetUseImageSpacing(true);
  distanceMapImageFilter->SetSquaredDistance(false);
  distanceMapImageFilter->SetInsideIsPositive(true);
  distanceMapImageFilter->SetInput(image);
  
  cout<<"calculate distance ITK "<<endl;
    distanceMapImageFilter->Update();
  cout<<"done calculate distance ITK "<<algoTimer.to_string()<<endl;
  
      typedef  itk::ImageFileWriter< FloatImageType  > WriterType;
        itk::NiftiImageIO::Pointer nifti_io = itk::NiftiImageIO::New();

  WriterType::Pointer writer = WriterType::New();
    writer->SetImageIO(nifti_io);
  writer->SetFileName(filename_out);
    writer->SetInput(distanceMapImageFilter->GetOutput());
  writer->Update();

  return 0;
}
