/*
 * run_test.cpp
 *
 *  Created on: May 11, 2016
 *      Author: brian
 */

#include "aursurface/aursurface.h"
// contour after surface
#include "asrtreemodel/AsrTreeModel.h"
#include "aursurface/aurcontour.h"

#include "aurcommon/aurstructs_functions.hpp"
#include "aurcommon/miscfunctions.h"
#include "AsrImage3D/AsrImage3D.h"
#include "AsrImage3D/AsrImage3Ditk.h"
#include "utils/asr-timer.h"
// STL includes
#include <algorithm>
#include <fstream>
#include <iostream>
#include <map>
#include <unordered_set>
#include <utility>
#include <vector>
// 3rd party includes
#include <boost/filesystem.hpp>

using namespace std;
using namespace auris_segtools;
using namespace boost::filesystem;
void Usage()
{
    cout << "\n asrImage-skeleton [-gen <Maximum Number of Generations] \n \t [--csvToSurface <skeleton.csv> "
            "<surface.vtk>] \n \t [<nifit file> <ouput file> "
            "<seed_x> "
            "<seed_y> <seed_z> ]"
         << endl;
    cout << "\n This program is used  to calculate a distance map to the outeredge of a mask an image \n" << endl;
}

void skeletonizeImage(const string& filename,
    const string& filename_out,
    const int& seedX,
    const int& seedY,
    const int& seedZ,
    const float& width,
    const int& maxGen)
{
    asrTimer algoTimer;
    AsrTreeModel airwayTree;

    AsrImage3D<float> imAirways, distMap;

    imAirways.readImage(filename);
    imAirways.writeImage("airways.nii.gz");
    cout<<"copy im airways "<<endl;
    distMap = imAirways;
    // vec3<int> seed(seedX,seedY,seedZ);
    cout<<"find seed"<<endl;//
    vec3<int> seed = imAirways.findSeedInZPlane();
    cout << "Seed(voxels) : " << seed.to_string() << endl;
    distFromSeed(imAirways, distMap, seed);
    algoTimer.addTimeStamp("Create Skeleton");
     unordered_set<unsigned int>  seg_voxels;
    airwayTree.CreateSkeleton(distMap, seed,seg_voxels);
    algoTimer.addTimeStamp("WriteData");
    airwayTree.writeTreePoints(filename_out + ".skeleton.csv");
        airwayTree.writeSkeletonImage(filename_out + ".skeleton.nii.gz");
    airwayTree.writeTreePointsAsSurface(filename_out + ".skeleton.vtk", 1.0);
    airwayTree.writeTreeStructure(filename_out + ".tree.csv");
    airwayTree.writeSegmentVectorsAsSurface(filename_out + ".segment.vtk");
    airwayTree.writePointTensorsAsSurface(filename_out + ".tensors.vtk");
airwayTree.writePerGenerationsStats(filename_out + ".genStat.txt");
    cout << algoTimer.to_string() << endl;
}

void csvToSurface(const string& filename, const string& imagename, const string& filename_out, const float& radius)
{

    ifstream fcsv(filename.c_str());
    if(fcsv.is_open()) {
        aurcontour<float> skeleton;
        string line;
        getline(fcsv, line); // header
        while(getline(fcsv, line)) {
            //            stringstream ss(line);
            float x{ 0 }, y{ 0 }, z{ 0 }, id{ 0 };
            cout << "line " << line << endl;
            size_t pos = 0;
            // While the the string from point pos contains the delimiter
            for(int i = 0; i < 4; ++i) {
                size_t len = line.substr(pos).find(',');
                cout << "line " << pos << " " << line.substr(pos, len) << endl;
                if(i == 0) {
                    id = stof(line.substr(pos, len));
                } else if(i == 1) {
                    x = stof(line.substr(pos, len));
                } else if(i == 2) {
                    y = stof(line.substr(pos, len));
                } else if(i == 3) {
                    z = stof(line.substr(pos, len));
                }

                pos += len + 1; // 1 is for the deliminter size
            }
            skeleton.push_back(vertex<float>(x, y, z, 0, 0, 1, id));
        }
        fcsv.close();
        cout << "number of vertices " << skeleton.size() << endl;
        skeleton.getVerticesAsSpheres(radius).write(filename_out, aursurface<float>::VTK);
    }
}

int main(int argc, char* argv[])
{
    if(argc < 2) {
        Usage();
        exit(EXIT_FAILURE);
    }

    asrTimer algoTimer;
    int argIndex{ 1 };
    int maxGen{ 0 };
    cout << string(argv[argIndex]) << endl;
    if(string(argv[argIndex]) == "-gen") {
        maxGen = atoi(argv[++argIndex]);
        ++argIndex;
        cout << "Set maximum number of generations to : " << maxGen << endl;
    }

    if(string(argv[argIndex]) == "--csvToSurface") { // convert csv skeleton to surface of spheres
        argIndex++;
        cout << "convert skeleton csv file to a surface of spheres" << endl;
        if(argc < 4) {
            Usage();
            exit(EXIT_FAILURE);
        }

        path file(argv[argIndex]);
        if(!exists(file)) {
            cerr << "\n File does not exists : " << file << "\n" << endl;
            return 1;
        } else if(!is_regular_file(file)) {
            cerr << "\n"
                 << file << " is not a file"
                 << "\n"
                 << endl;
            return 1;
        }
        //        float radius{ atof(argv[3]) };
        //       cout << "radius " << radius << endl;
        // filename, filename_out, radius
        csvToSurface(
            string(argv[argIndex]), string(argv[argIndex + 1]), string(argv[argIndex + 2]), atof(argv[argIndex + 3]));
        return 0;
    } else { // default option skeletonize image

        constexpr float width{ 1.5 }; // width of boundary in distance from seed
        // main skeletonization algorithm

        path file(argv[argIndex]);
        if(!exists(file)) {
            cerr << "\n File does not exists : " << file << "\n" << endl;
            return 1;
        } else if(!is_regular_file(file)) {
            cerr << "\n"
                 << file << " is not a file"
                 << "\n"
                 << endl;
            return 1;
        }

        if(argc <= 5) {
            Usage();
            exit(EXIT_FAILURE);
        }

        string filename(argv[argIndex++]);
        string filename_out(argv[argIndex++]);
        skeletonizeImage(filename, filename_out, atoi(argv[argIndex]), atoi(argv[argIndex + 1]),
            atoi(argv[argIndex + 2]), width, maxGen);
    }
    cout << algoTimer.to_string() << endl;
    return 0;
}
