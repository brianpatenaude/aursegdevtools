/*
 * run_test.cpp
 *
 *  Created on: May 11, 2016
 *      Author: brian
 */
#include "aursurface/aursurface.h"

#include "aurcommon/aurstructs_functions.hpp"
#include "aurcommon/miscfunctions.h"
#include "AsrImage3D/AsrImage3D.h"
#include "segmentation_utils.h"
#include <glm/gtx/string_cast.hpp>
// STL includes
#include <fstream>
#include <iostream>
#include <map>
#include <vector>
// 3rd party includes
#include <boost/filesystem.hpp>

using namespace std;
using namespace auris_segtools;
using namespace boost::filesystem;
void Usage()
{
    cout << "\n estimate_lung <nifit file> <ouput base> <x_voxel> <y_voxel> <z_voxel> <deformation_parameter_file>" << endl;
    cout << "\n This program is used  to estimate the lung boundary. It requires an initial estimate of the lung mask (inside cleaned \n" << endl;
    cout << "\n Generically this can be used to inflate a sphere in a mas.k\n" << endl;
}

int main(int argc, char* argv[])
{
    //	cout<<"\nEstimating tissue thresholds for Fat and Lung from
    // image..."<<endl;
    bool useCT{false};
    int currentArg = 1;

    if (argc < 6) {
        Usage();
        return 0;
    }
    string argname{argv[currentArg]};

    path file(argv[currentArg]);
    if   (!exists(file)) {
        cerr << "\n File does not exists : " << file << "\n" << endl;
        return 1;
    } else if (!is_regular_file(file)) {
        cerr << "\n"
             << file << " is not a file"
             << "\n"
             << endl;
        return 1;
    }

    string filename(argv[currentArg++]);
    string filename_out(argv[currentArg++]);
    vec3<short> coord_vox;
    vec3<float> coord_mm;
    coord_vox.x = atoi(argv[currentArg++]);
    coord_vox.y = atoi(argv[currentArg++]);
    coord_vox.z = atoi(argv[currentArg++]);


    AsrImage3D<short> imLung;
    imLung.readImage(filename_out + "_lungs.nii.gz");
    coord_mm.x = coord_vox.x * imLung.xdim();
    coord_mm.y = coord_vox.y * imLung.ydim();
    coord_mm.z = coord_vox.z * imLung.zdim();
    vector<int> bounds{
        0, 0, 0, imLung.xsize() - 1, imLung.ysize() - 1, imLung.zsize() - 1};

    aursurface<float> surfLung;
    
    surfLung.createSphere<float>(coord_mm,5.0, 20 ,20 );
    surfLung.InitDeform();

    deformationParameters<float> params;
    params.splitThreshold = 20;
    params.alpha_area = 0.0;
    params.alpha_sn = 0.01;
    params.alpha_st = 0.10;
    params.max_step_size = 0.5;
    cout<<"Deforming..."<<endl;
    for (int i = 0 ; i < 20 ; ++i )
    {
        cout<<"i "<<i<<" "<<surfLung.getNumberOfVertices()<<endl;
        surfLung.deform(imLung,200,params, aursurface<float>::SPEEDFUNC::SAMPLE);
        cout<<"does the surface self-intersect : "<<surfLung.selfIntersects()<<endl;
        surfLung.write(filename_out + "_lungs.vtk", aursurface<float>::WRITE_FORMAT::VTK);
    }
}
