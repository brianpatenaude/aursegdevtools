//========================== ===================================================
// Auris Surgical Robotics, Inc.
// (C) Copyright 2016. All Rights Reserved.
//
//
//! \file       segmentation_utils.cpp
//! \brief
//! Segmentation specific methods
//!
//! \date       July 2016
//!
//! \author     Brian Patenaude
//!
//=============================================================================

#include <segmentation_utils.h>

#include <aurcommon/aurstructs_functions.hpp>

#include <map>

using namespace std;

namespace auris_segtools
{

// enforces  a smoothness in the changing normal
// TODO
//        map<float ,vec3<float> > estimateCutPlane(aursurface<float> &
//        surfTrachea, const vec3<float> & cutAxis, vertex<float> &
//        cutPlanePoint, const vec3<float> & prevNormal )
//    {//estimate cut plane
//                map<float ,vec3<float> > areaToNormal;
//                float angleThresh = 45.0/180.0 *M_PI;
//                float angleIncrement = M_PI/40;
//                for ( int i = 0; i <= 40 ; ++i )
//                {
//                    cout<<"i ----"<<i<<endl;
//                    vec3<float> planeNormal = rotatePlaneAboutAxis(  cutAxis,
//                    i*angleIncrement);
//                    float angRads = angle(planeNormal,prevNormal) ;
//                    if (angRads < angleThresh)
//                    {
//                    float area = getContour(
//                    surfTrachea.cutSurface(cutPlanePoint,planeNormal),cutPlanePoint,planeNormal).area(planeNormal);
//                   cout<<"area "<<area<<" / "<<angRads<<" /
//                   "<<surfTrachea.cutSurface(cutPlanePoint,planeNormal).size()<<endl;
//                    if (area != 0 )//0 is the null conditions , couldn't find
//                    contour
//                        areaToNormal[ abs(getContour(
//                        surfTrachea.cutSurface(cutPlanePoint,planeNormal),cutPlanePoint,planeNormal).area(planeNormal)
//                        ) ] = planeNormal;
//
//                    }
//                }
//
//                return     areaToNormal;
//    }
bool successfulSegment(const std::array<vertex<float>, 2>& verts)
{
    return (!(verts[0].isOrigin() || verts[1].isOrigin()));
}

//
array<aursurface<float>, 3> segmentAndCutSegment(AsrImage3D<short>& imSpeedMap,
    aursurface<float>& surfAirway,
    const vertex<float>& initPoint,
    const deformationParameters<float>& deformParams,
    aurcontour<float>& centreLine,
    array<vertex<float>, 2>& cutPoints,
    aursurface<float>::SPEEDFUNC speedFuncType)
{
    array<aursurface<float>, 3> surfAirwaysAB;
    cutPoints = segmentAirwaySegment(imSpeedMap, surfAirway, surfAirwaysAB[0], surfAirwaysAB[1], surfAirwaysAB[2],
        initPoint, deformParams, centreLine, speedFuncType);
    //        surfAirway.trimSurface();
    cout << "number of vertices in the cut " << surfAirwaysAB[0].getNumberOfVertices() << " "
         << surfAirwaysAB[1].getNumberOfVertices() << endl;
    return surfAirwaysAB;
}

array<vertex<float>, 2> segmentAirwaySegment(AsrImage3D<short>& imSpeedMap,
    aursurface<float>& surfAirway,
    aursurface<float>& surfAirwayA,
    aursurface<float>& surfAirwayB,
    aursurface<float>& surfAirwayC,
    const vertex<float>& initPoint,
    const deformationParameters<float>& params,
    aurcontour<float>& centreLine,
    const aursurface<float>::SPEEDFUNC& speedFuncType)
{

    array<vertex<float>, 2> bifurcationCutPoints{ vertex<float>(0, 0, 0), vertex<float>(0, 0, 0) };
    if(surfAirway.empty())
        return bifurcationCutPoints;

    cout << "segment airways " << normal(initPoint).to_string() << endl;

    // need to search for bifurcation before further deformation
    // actually use the trinag les for cutting
    array<array<unsigned int, 2>, 2> bifurcationTriangles;
    vertex<float> normalAtBifurcation;

    //  Check to see if an bifurcation exists
    int bifurcationScanResults = scanForBifurcationWithNormalTracking(
        surfAirway, imSpeedMap, initPoint, normal(initPoint), bifurcationTriangles, normalAtBifurcation, centreLine);

    //  if there is no bifurcation, deform until it is reached
    if(bifurcationScanResults > 0) {
        cout << "Upsampling the surface " << endl;
        //                        surfAirway.splitTriangles(params.splitThreshold);//resample surface to higher before
        //                        deforming
        // deform to Birfurcation incoroprates self-intersection test
        // return 0 if succesfull, 1 if failure
        if(deformToBifurcation(surfAirway, imSpeedMap, initPoint, normal(initPoint), params, speedFuncType) > 0)
            return bifurcationCutPoints;
    }
    cout << "done deforming " << endl;
    surfAirwayC = surfAirway; // save uncut surface
    // need to grab the proper bifurcation triangles
    // deform to bifurcation's stop criteria guarantees that it will exist
    scanForBifurcationWithNormalTracking(
        surfAirway, imSpeedMap, initPoint, normal(initPoint), bifurcationTriangles, normalAtBifurcation, centreLine);
    //  Find the cut planes on the surface to divide into parent  and children.
    //  This  deforms the surface until the cuts are stable
    //        array<vec3<float>, 2> cutPlaneNormals =
    //            cutAirway(imSpeedMap, surfAirway, bifurcationTriangles, bifurcationCutPoints, normalAtBifurcation,
    //            params);
    surfAirway.setScalars(0);
    surfAirway.colorTriangle(bifurcationTriangles[0][0], 1);
    surfAirway.colorTriangle(bifurcationTriangles[0][1], 1);
    surfAirway.colorTriangle(bifurcationTriangles[1][0], 1);
    surfAirway.colorTriangle(bifurcationTriangles[1][1], 1);
    surfAirway.colorTriangle(surfAirway.getClosestTriangle(bifurcationTriangles[0][0]), 2);
    surfAirway.colorTriangle(surfAirway.getClosestTriangle(bifurcationTriangles[1][0]), 2);
    surfAirway.write("origCutPoint.vtk", aursurface<float>::WRITE_FORMAT::VTK);
    //        scanForBifurcationWithNormalTracking(surfAirway, imSpeedMap, initPoint, normal(initPoint),
    //        bifurcationTriangles, normalAtBifurcation,centreLine);
    //        cutPlaneNormals = cutAirway(imSpeedMap, surfAirway, bifurcationTriangles, bifurcationCutPoints,
    //        normalAtBifurcation, params);
    // Estimate the airway direction in the children

    surfAirway.InitDeform();
    surfAirway.computeMinimumNormalToTriangleIntersection();

    array<vec3<float>, 2> cutPlaneNormals{ normal(normalAtBifurcation), normal(normalAtBifurcation) };

    //        list< aurcontour<float> >  all_contours = surfAirway.cutSurface(normalAtBifurcation,
    //        normal(normalAtBifurcation));
    //        bifurcationCutPoints = all_contours.front().nearestVertex(all_contours.back(), bifurcationTriangles);
    //
    bifurcationCutPoints[0] = surfAirway.getTriangleCentroid(bifurcationTriangles[0][0]);
    bifurcationCutPoints[1] = surfAirway.getTriangleCentroid(bifurcationTriangles[1][0]);
    cout << "bifPoint " << bifurcationCutPoints[0].to_string() << " " << bifurcationCutPoints[1].to_string() << endl;

    vertex<float> v00 = surfAirway.getTriangleCentroid(bifurcationTriangles[0][0]);
    vertex<float> v01 = surfAirway.getTriangleCentroid(surfAirway.getClosestTriangle(bifurcationTriangles[0][0]));
    vertex<float> v10 = surfAirway.getTriangleCentroid(bifurcationTriangles[1][0]);
    vertex<float> v11 = surfAirway.getTriangleCentroid(surfAirway.getClosestTriangle(bifurcationTriangles[1][0]));

    cutPlaneNormals[0] = estimateCutPlane(surfAirway, v01 - v00, bifurcationCutPoints[0]);
    cutPlaneNormals[1] = estimateCutPlane(surfAirway, v11 - v10, bifurcationCutPoints[1]);
    reorientNormal(cutPlaneNormals[0], normal(normalAtBifurcation));
    reorientNormal(cutPlaneNormals[1], normal(normalAtBifurcation));
    cout << "number of contours in cut0 " << surfAirway.cutSurface(bifurcationCutPoints[0], cutPlaneNormals[0]).size()
         << endl;
    cout << "number of contours in cut1 " << surfAirway.cutSurface(bifurcationCutPoints[1], cutPlaneNormals[1]).size()
         << endl;

    bifurcationCutPoints[0] =
        aurcontour<float>::getContour(surfAirway.cutSurface(bifurcationCutPoints[0], cutPlaneNormals[0]),
            bifurcationCutPoints[0], cutPlaneNormals[0])
            .centreOfGravity();
    bifurcationCutPoints[1] =
        aurcontour<float>::getContour(surfAirway.cutSurface(bifurcationCutPoints[1], cutPlaneNormals[1]),
            bifurcationCutPoints[1], cutPlaneNormals[1])
            .centreOfGravity();
    cout << "bifPoint2 " << bifurcationCutPoints[0].to_string() << " " << bifurcationCutPoints[1].to_string() << endl;
    // This actually does the surface splitting
    surfAirway.splitSurface(surfAirwayB, bifurcationTriangles[1][0], bifurcationCutPoints[1], cutPlaneNormals[1]);
    surfAirway.splitSurface(surfAirwayA, bifurcationTriangles[0][0], bifurcationCutPoints[0], cutPlaneNormals[0]);
    // Combine the normals  into the vertices, as such the vertices entirely define the plane
    setNormal(bifurcationCutPoints[0], cutPlaneNormals[0]);
    setNormal(bifurcationCutPoints[1], cutPlaneNormals[1]);
    surfAirway.setScalars(0);
    surfAirway.colorTriangle(bifurcationTriangles[0][0], 1);
    surfAirway.colorTriangle(bifurcationTriangles[0][1], 1);
    surfAirway.colorTriangle(bifurcationTriangles[1][0], 2);
    surfAirway.colorTriangle(bifurcationTriangles[1][1], 2);
    surfAirway.colorTriangle(surfAirway.getClosestTriangle(bifurcationTriangles[0][0]), 3);
    surfAirway.colorTriangle(surfAirway.getClosestTriangle(bifurcationTriangles[1][0]), 4);

    return bifurcationCutPoints;
}

//    map<float ,vec3<float> >
vec3<float>
estimateCutPlane(aursurface<float>& surfTrachea, const vec3<float>& cutAxis, const vertex<float>& cutPlanePoint)
{ // estimate cut plane
    map<float, pair<vec3<float>, vertex<float> > > areaToNormal;
    int Nsteps = 360;
    float angleIncrement = M_PI / Nsteps;
    for(int i = 0; i <= Nsteps; ++i) {
        //                    cout<<"i ----"<<i<<endl;
        vec3<float> planeNormal = rotatePlaneAboutAxis(cutAxis, i * angleIncrement);
        aurcontour<float> contour = aurcontour<float>::getContour(
            surfTrachea.cutSurface(cutPlanePoint, planeNormal), cutPlanePoint, planeNormal);
        float area = contour.area(planeNormal);
        //                  cout<<"area "<<area<<"
        //                  "<<surfTrachea.cutSurface(cutPlanePoint,planeNormal).size()<<"
        //                  "<<planeNormal.to_string()<<endl;
        //
        if(area != 0) // 0 is the null conditions , couldn't find contour
            areaToNormal[abs(aurcontour<float>::getContour(
                                 surfTrachea.cutSurface(cutPlanePoint, planeNormal), cutPlanePoint, planeNormal)
                                 .area(planeNormal))] =
                pair<vec3<float>, vertex<float> >(planeNormal, contour.centreOfGravity());
    }
    //                cutPlanePoint = areaToNormal.begin()->second.second;
    if(areaToNormal.empty()) // conditioon where no closed cut plane is found
        return vec3<float>(0.0, 0.0, 0.0);

    return areaToNormal.begin()->second.first;
}

//    map<float ,vec3<float> >
vec3<float> estimateCutPlane(aursurface<float>& surfTrachea,
    array<unsigned int, 2>& carinaTriangles,
    const vertex<float>& cutPlanePoint)
{ // estimate cut plane
    map<float, vec3<float> > areaToNormal;

    //                float angleIncrement = M_PI/40;
    vec3<float> cutAxis = surfTrachea.getTriangleCentroid(surfTrachea.getClosestTriangle(carinaTriangles[0])) -
        surfTrachea.getTriangleCentroid(carinaTriangles[0]);

    return estimateCutPlane(surfTrachea, cutAxis, cutPlanePoint);
}

array<vec3<float>, 2> cutAirway(AsrImage3D<short>& imSpeedMap,
    aursurface<float>& surfTrachea,
    array<array<unsigned int, 2>, 2>& carinaTriangles,
    array<vertex<float>, 2>& cutPlanePoints,
    const vec3<float>& prevNormal,
    const deformationParameters<float>& params)
{
    cout << "cutairway " << endl;
    // we want to deform entire surface for left and right to ensure contour
    // matching criterai
    // achor the cut trinagles so that the cut plane stays valid
    for(auto& array_tris : carinaTriangles) {
        cout << "looping over triangles  " << endl;
        for(auto& tris : array_tris) {
            cout << "tri " << tris << endl;
            surfTrachea.anchorTriangle(tris);
        }
    }
    //        cout<<"computer surface stats "<<endl;
    surfTrachea.computeMinimumNormalToTriangleIntersection();
    surfTrachea.computeRadiusAtVertex();
    //                cout<<"computer surface stats done "<<endl;

    array<vec3<float>, 2> cutPlaneNormals;
    // vInPlane0,vInPlane1,cutPlaneNormal;
    // TODO UPDATE THE PREVIOUS NORMAL
    /// normals is poiinting in the direction going deeper into the lungs
    //	vec3<float> prevNormal(0.0,0.0,-1.0);
    for(int branch = 0; branch <= 1; ++branch) {
        //            cout<<"BRANCH "<<branch<<"
        //            "<<carinaTriangles[branch][0]<<endl;
        cutPlanePoints[branch] = surfTrachea.getTriangleCentroid(carinaTriangles[branch][0]);
        //           .z<<")"<<endl;

        //            map<float ,vec3<float> > areaToNormal  =
        //            estimateCutPlane(surfTrachea,carinaTriangles[branch],
        //            cutPlanePoints[branch]);
        //            cout<<"minimum area "<<areaToNormal.begin()->first<<endl;
        //            cutPlaneNormals[branch] = areaToNormal.begin()->second;
        cutPlaneNormals[branch] = estimateCutPlane(surfTrachea, carinaTriangles[branch], cutPlanePoints[branch]);

        normalize(cutPlaneNormals[branch]);

        cout << "cutPlaneNromal " << cutPlaneNormals[branch].to_string() << " " << prevNormal.to_string() << endl;

        //        Need to make sure normal is in correct direction, may need
        //        something more sophisticated.
        reorientNormal(cutPlaneNormals[branch], prevNormal);
        cout << "cutPlaneNromal2  " << branch << " " << cutPlaneNormals[branch].x << " " << cutPlaneNormals[branch].y
             << " " << cutPlaneNormals[branch].z << endl;

        // initiial edge
        aurcontour<float> bronchusEdge_prev =
            aurcontour<float>::getContour(surfTrachea.cutSurface(cutPlanePoints[branch], cutPlaneNormals[branch]),
                cutPlanePoints[branch], cutPlaneNormals[branch]);

        int iter = 0;
        bool matched{ false };
        do { // deform surface until we get a stable estimate of the bronchus cut
            //        break;
            cout << "ideform " << iter << surfTrachea.getNumberOfVertices() << endl;
            surfTrachea.deform(imSpeedMap, 1, params.alpha_sn, params.alpha_st, params.alpha_area, params.alpha_im,
                params.max_step_size, params.splitThreshold, aursurface<float>::NEG_GRAD);
            // stop deforming if we've hit a self-intersection
            //                        if (iter % selfIntersectionInterval == 0) {
            if(surfTrachea.selfIntersects())
                break;
            //                        }

            aurcontour<float> bronchusEdge =
                aurcontour<float>::getContour(surfTrachea.cutSurface(cutPlanePoints[branch], cutPlaneNormals[branch]),
                    cutPlanePoints[branch], cutPlaneNormals[branch]);
            //            cout<<"Is cut empty ? "<<bronchusEdge.empty()<<endl;
            cout << "cutPoint ( " << cutPlanePoints[branch].x << " " << cutPlanePoints[branch].y << " "
                 << cutPlanePoints[branch].z << ")" << endl;

            // by virtue of the method, there must be at least 1 contour
            float dist = bronchusEdge_prev.maxDistanceSquared(bronchusEdge);
            if(dist < contourThresh) {
                matched = true;
                break;
            }
            cout << "maxdist " << dist << " " << bronchusEdge.size() << endl;

            bronchusEdge_prev = bronchusEdge;
            ++iter;
        } while(!matched);

        /// a final restimation
        surfTrachea.computeMinimumNormalToTriangleIntersection();
        surfTrachea.computeRadiusAtVertex();
        //            areaToNormal  =
        //            estimateCutPlane(surfTrachea,carinaTriangles[branch],
        //            cutPlanePoints[branch]);
        //
        //            cout<<"minimum area
        //            "<<areaToNormal.begin()->first<<cutPlaneNormals[branch].to_string()<<"
        //            "<<endl;
        //            cutPlaneNormals[branch] = areaToNormal.begin()->second;
        cutPlaneNormals[branch] = estimateCutPlane(surfTrachea, carinaTriangles[branch], cutPlanePoints[branch]);
        normalize(cutPlaneNormals[branch]);

        reorientNormal(cutPlaneNormals[branch], prevNormal);

        cutPlanePoints[branch] =
            aurcontour<float>::getContour(surfTrachea.cutSurface(cutPlanePoints[branch], cutPlaneNormals[branch]),
                cutPlanePoints[branch], cutPlaneNormals[branch])
                .centreOfGravity();
        //            cutPlanePoints[branch] = surfTrachea.cutSurface()
        //                  if (dotProduct(prevNormal,cutPlaneNormals[branch]) < 0 )
        //             {
        //                 cout<<"FLIP NORMAL "<<cutPlaneNormals.size()<<"
        //                 "<<branch<<endl;
        //                 cutPlaneNormals[branch]  *= -1;
        //             } 

        //              aursurface<float> surfLeftBronchus;
        //              surfTrachea.splitSurface(surfLeftBronchus,
        //              carinaTriangles[branch][0],     cutPlanePoints[branch],
        //              cutPlaneNormals[branch]);
        //            surfLeftBronchus.write("grot_tracheaCut_"+to_string(branch)+".vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
    }

    return cutPlaneNormals;
}

int findTrachea(AsrImage3D<short>& imAir,
    vec3<float>& tracheaSeed,
    const float& areaThreshLungs,
    const float& areaThreshTrachea,
    const float& searchStep_dzmm)
{
    vector<int> bounds{ 0, 0, 0, imAir.xsize() - 1, imAir.ysize() - 1, imAir.zsize() - 1 };
    float voxel_area = imAir.xdim() * imAir.ydim();
    // if (!userDefinedSeed)
    {
        // scan through image to look for the trachea
        // note that _air and _lungs image are same size
        int dz = static_cast<int>(searchStep_dzmm / imAir.zdim());
        int z = imAir.zsize() - 1;
        for(; z >= 0; z -= dz) {

            cout << "Searching for trachea at z = " << z << ", dz = " <<dz<<endl;
            vector<int> bounds_roi{ bounds[0], bounds[1], z, bounds[3], bounds[4], z };
            sizeCog l_sizes_cog = imAir.connectedComponents(imAir, bounds_roi, 0);
            int possibleTracheaCount{ 0 }, possibleLungsCount{ 0 };
            sizeCog validCogs;
            //                    cout<<"-------"<<endl;

            for(auto& iter : l_sizes_cog) {
                float area = get<0>(iter) * voxel_area;
                if(area > areaThreshLungs) {
                    validCogs.push_back(iter);
                    ++possibleLungsCount;
                } else if(area > areaThreshTrachea) {
                    validCogs.push_back(iter);

                    ++possibleTracheaCount;
                }
            }

            cout << "check " << possibleTracheaCount << " " << possibleLungsCount << " " << validCogs.size() << endl;
            // 3 and 2 because lungs will double count
            // only for more than one trachea possibility, assumption is that
            // trachea is
            // largest amongst noise
            //            if((possibleTracheaCount == 1) && (possibleLungsCount == 2)) {
            if((possibleTracheaCount >= 1) && (possibleLungsCount == 2)) {

                auto riter = validCogs.rbegin();
                array<float, 3> cog_x;
                for(int i = 0; i < 3; ++i, ++riter) {
                    cog_x[i] = get<1>(*riter).x;
                }

                // find the middle in terms of x position
                //
                riter = validCogs.rbegin();
                if(((cog_x[0] > cog_x[1]) && (cog_x[0] < cog_x[2])) ||
                    ((cog_x[0] < cog_x[1]) && (cog_x[0] > cog_x[2]))) {

                    tracheaSeed = get<1>(*riter);
                } else if(((cog_x[1] > cog_x[0]) && (cog_x[1] < cog_x[2])) ||
                    ((cog_x[1] < cog_x[0]) && (cog_x[1] > cog_x[2]))) {

                    tracheaSeed = get<1>(*(++riter));
                } else {
                    ++riter;
                    tracheaSeed = get<1>(*(++riter));
                }

                break;
            }
        }
        if(z <= 0)
            return 1;
    }
    return 0;
}

int deformUntilAllAnchored(aursurface<float>& airwaySurface,
    AsrImage3D<short>& imSpeedMap,
    const deformationParameters<float>& params,
    const aursurface<float>::SPEEDFUNC& speedFuncType)
{
    if(airwaySurface.empty()) {
        cout << "WARNING : surface has no vertices " << endl;
        return 1;
    }
    deformationParameters<float> params_local = params;
    //        float centroidValue = imSpeedMap.interpolate( to_vertex(airwaySurface.centroid()) );
    // set intensity refernece for deforming to value at centroid
    airwaySurface.setReferenceIntensity(imSpeedMap.interpolate(to_vertex(airwaySurface.centroid())));

    short i = 1;

    unsigned int same_count{ 0 };
    unsigned int NVertices_prev{ 0 };
    aursurface<float> airwaySurface_prev = airwaySurface;
    while(airwaySurface.getNumberOfVertices() > airwaySurface.getNumberOfAnchoredVertices()) {
        cout << "Deform Until All anchored iterations " << i * iStep << " / " << iStep * Niters << " "
             << airwaySurface.getNumberOfVertices() << endl;
        cout << "get mean and standard deviation " << endl;
        //                array<float, 2> meanAndStd = airwaySurface.getInsideMeanAndStandardDeviation();
        cout << params_local.to_string() << endl;
        //                airwaySurface.deform(imSpeedMap, iStep, params_local.alpha_sn, params_local.alpha_st,
        //                params_local.alpha_area, params_local.alpha_im, params_local.max_step_size,
        //                                     params_local.splitThreshold, aursurface<float>::SAMPLE);
        //                        airwaySurface.splitTriangles(params_local.splitThreshold);
        airwaySurface.deform(imSpeedMap, iStep, params_local.alpha_sn, params_local.alpha_st, params_local.alpha_area,
            params_local.alpha_im, params_local.max_step_size, params_local.splitThreshold, speedFuncType);
        cout << "resample surface " << endl;
        cout << "stop resample surface " << endl;
        //                     airwaySurface.splitTriangles(params_local.splitThreshold);
        // TODO Re-estimate surface normal ?
        // moving down the trachea
        //                if ( (i*iStep) % selfIntersectionInterval == 0) {
        //                if ( (i*iStep) % selfIntersectionInterval == 0) {
        cout << "do sefl-intersection test " << endl;
        if(airwaySurface.selfIntersects()) {
            cout << "!!!FOUND  self intersectiion!!!!" << endl;
            //                                params_local.alpha_area += 0.01;
            airwaySurface.write("sintersect.vtk", aursurface<float>::VTK);
            // JUST RETURN IF SELF_intersects
            return 1;
            if(((params_local.alpha_st + ALPHA_ST_STEP) > MAX_ALPHA_ST) &&
                ((params_local.splitThreshold - SPLIT_THRESH_STEP) < SPLIT_THRESH_MIN))
                return 1;

            if((params_local.alpha_st + ALPHA_ST_STEP) <= MAX_ALPHA_ST) {
                params_local.alpha_st += ALPHA_ST_STEP;
            }
            if((params_local.splitThreshold - SPLIT_THRESH_STEP) >= SPLIT_THRESH_STEP) {
                params_local.splitThreshold -= SPLIT_THRESH_STEP;
            }

            airwaySurface = airwaySurface_prev;
            continue;
            //                                return 1;
            // bifurcationCutPoints;
        } else {
            airwaySurface_prev = airwaySurface;
        }
        cout << "done sefl-intersection test " << endl;
        //                }

        array<float, 6> surfBounds = airwaySurface.bounds();
        cout << "airwayBounds " << surfBounds[0] << " " << surfBounds[1] << " " << surfBounds[2] << " " << surfBounds[3]
             << " " << surfBounds[4] << " " << surfBounds[5] << endl;
        unsigned int NVertices = airwaySurface.getNumberOfVertices();
        if(NVertices_prev == NVertices) {
            same_count++;
        } else {
            NVertices_prev = NVertices;
        }
        if(same_count == SAME_COUNT_THRESH) {
            params_local.splitThreshold -= 0.5;
            //                    params_local.alpha_im+=0.1;
            cout << "DISABLED - STOPPING DUE TO LACK OF NEW triangles " << endl;
            same_count = 0;
            //                    return 1;
        }
        //                    scanForBifurcationWithNormalTracking(airwaySurface, imSpeedMap, normalAtBifurcation,
        //                    normal, bifurcationTriangles, normalAtBifurcation_new,centreLine);
        //                    normalAtBifurcation = normalAtBifurcation_new;
        cout << "Iterations/Z-cut/Number of Contours : " << i * iStep << " / " << iStep * Niters << "    "
             << "    " << endl;
        airwaySurface.copyAnchoredVerticesToScalars();
        //                     airwaySurface.copyDisplacementToScalars();
        airwaySurface.write("surfStep" + to_string(i) + ".vtk", auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
        ++i; // for moniroting iterations
    }
    return 0;
}
int deformToBifurcation(aursurface<float>& airwaySurface,
    AsrImage3D<short>& imSpeedMap,
    const vertex<float>& cutPoint,
    const deformationParameters<float>& params,
    const aursurface<float>::SPEEDFUNC& speedFuncType)
{
    return deformToBifurcation(
        airwaySurface, imSpeedMap, cutPoint, vec3<float>(cutPoint.nx, cutPoint.ny, cutPoint.nz), params, speedFuncType);
}
int deformToBifurcation(aursurface<float>& airwaySurface,
    AsrImage3D<short>& imSpeedMap,
    const vertex<float>& cutPoint,
    const vec3<float>& normal,
    const deformationParameters<float>& params,
    const aursurface<float>::SPEEDFUNC& speedFuncType)
{
    if(airwaySurface.empty()) {
        cout << "WARNING : surface has no vertices " << endl;
        return 1;
    }
    deformationParameters<float> params_local = params;
    //        float centroidValue = imSpeedMap.interpolate( to_vertex(airwaySurface.centroid()) );
    // set intensity refernece for deforming to value at centroid
    airwaySurface.setReferenceIntensity(imSpeedMap.interpolate(to_vertex(airwaySurface.centroid())));

    short i = 1;
    vertex<float> normalAtBifurcation;
    array<array<unsigned int, 2>, 2> bifurcationTriangles; // actually use the trinag les for cutting
    aurcontour<float> centreLine;
    int results = scanForBifurcationWithNormalTracking(
        airwaySurface, imSpeedMap, cutPoint, normal, bifurcationTriangles, normalAtBifurcation, centreLine);
    normalAtBifurcation = cutPoint;

    unsigned int same_count{ 0 };
    unsigned int NVertices_prev{ 0 };
    aursurface<float> airwaySurface_prev = airwaySurface;
    while(results > 0) {
        cout << "Deform to Bifurcation iterations " << i * iStep << " / " << iStep * Niters << " "
             << airwaySurface.getNumberOfVertices() << endl;
        cout << "get mean and standard deviation " << endl;
        //                array<float, 2> meanAndStd = airwaySurface.getInsideMeanAndStandardDeviation();
        cout << params_local.to_string() << endl;
        //                airwaySurface.deform(imSpeedMap, iStep, params_local.alpha_sn, params_local.alpha_st,
        //                params_local.alpha_area, params_local.alpha_im, params_local.max_step_size,
        //                                     params_local.splitThreshold, aursurface<float>::SAMPLE);
        //                        airwaySurface.splitTriangles(params_local.splitThreshold);
        airwaySurface.deform(imSpeedMap, iStep, params_local.alpha_sn, params_local.alpha_st, params_local.alpha_area,
            params_local.alpha_im, params_local.max_step_size, params_local.splitThreshold, speedFuncType);
        cout << "resample surface " << endl;
        cout << "stop resample surface " << endl;

        // TODO Re-estimate surface normal ?
        // moving down the trachea
        //                if ( (i*iStep) % selfIntersectionInterval == 0) {
        //                if ( (i*iStep) % selfIntersectionInterval == 0) {
        cout << "do sefl-intersection test " << endl;
        if(airwaySurface.selfIntersects()) {
            cout << "!!!FOUND  self intersectiion!!!! " << endl;
            //                                params_local.alpha_area += 0.01;
            return 1;
            if(((params_local.alpha_st + ALPHA_ST_STEP) > MAX_ALPHA_ST) &&
                ((params_local.splitThreshold - SPLIT_THRESH_STEP) < SPLIT_THRESH_MIN))
                return 1;

            if((params_local.alpha_st + ALPHA_ST_STEP) <= MAX_ALPHA_ST) {
                params_local.alpha_st += ALPHA_ST_STEP;
            }
            if((params_local.splitThreshold - SPLIT_THRESH_STEP) >= SPLIT_THRESH_STEP) {
                params_local.splitThreshold -= SPLIT_THRESH_STEP;
            }

            airwaySurface = airwaySurface_prev;
            continue;
            //                                return 1;
            // bifurcationCutPoints;
        } else {
            airwaySurface_prev = airwaySurface;
        }
        cout << "done sefl-intersection test " << endl;
        //                }

        array<float, 6> surfBounds = airwaySurface.bounds();
        cout << "airwayBounds " << surfBounds[0] << " " << surfBounds[1] << " " << surfBounds[2] << " " << surfBounds[3]
             << " " << surfBounds[4] << " " << surfBounds[5] << endl;
        unsigned int NVertices = airwaySurface.getNumberOfVertices();
        if(NVertices_prev == NVertices) {
            same_count++;
        } else {
            NVertices_prev = NVertices;
        }
        if(same_count == SAME_COUNT_THRESH) {
            params_local.splitThreshold -= 0.5;
            //                    params_local.alpha_im+=0.1;
            cout << "DISABLED - STOPPING DUE TO LACK OF NEW triangles " << endl;
            same_count = 0;
            //                    return 1;
        }
        //                vertex<float> normalAtBifurcation_new;

        results = scanForBifurcationWithNormalTracking(
            airwaySurface, imSpeedMap, cutPoint, normal, bifurcationTriangles, normalAtBifurcation, centreLine);
        //                    scanForBifurcationWithNormalTracking(airwaySurface, imSpeedMap, normalAtBifurcation,
        //                    normal, bifurcationTriangles, normalAtBifurcation_new,centreLine);
        //                    normalAtBifurcation = normalAtBifurcation_new;
        cout << "Iterations/Z-cut/Number of Contours : " << i * iStep << " / " << iStep * Niters << "    "
             << "    " << endl;
        airwaySurface.copyAnchoredVerticesToScalars();
        //                     airwaySurface.copyDisplacementToScalars();
        airwaySurface.write("surfStep" + to_string(i) + ".vtk", auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
        ++i; // for moniroting iterations
    }
    return 0;
}
// this is old verison that precedes the front by X=5 mm
// void deformToBifurcation( aursurface<float> & airwaySurface, AsrImage3D<short>
// & imSpeedMap, const vertex<float> & cutPoint, const vec3<float> & normal,
// const deformationParameters<float> & params )
//    {
//        if (airwaySurface.empty())
//        {
//            cout<<"WARNING : surface has no vertices "<<endl;
//            return;
//        }
//        short i = 1;
//        vertex<float> slicePoint = cutPoint+ normal*zres ;
//        vector< aurcontour<float> > all_contours;
//        do {
//            cout<<"Deform to Bifurcation iterations "<<i*iStep<<" /
//            "<<iStep*Niters<<endl;
//            airwaySurface.deform(imSpeedMap,iStep,params.alpha_sn,
//            params.alpha_st,params.alpha_area,params.alpha_im,params.max_step_size,params.splitThreshold);
//            array<float,2> bounds = airwaySurface.zbounds(cutPoint,normal);
//            //deform until the surface spans greater than the resolution at
//            whihc we're checking for bifurcations
//            if ( (bounds[1]-bounds[0]) < zres )
//                continue;
//            //TODO Re-estimate surface normal ?
//    //moving down the trachea
//            slicePoint = cutPoint + normal*(bounds[1] - zres);
//            all_contours =  airwaySurface.cutSurface(slicePoint, normal);
//
//            if (all_contours.empty()) break;
//
//            cout<<"Iterations/Z-cut/Number of Contours : "<<i*iStep<<" /
//            "<<iStep*Niters<<"    "<<"    "<<all_contours.size()<<endl;
//            ++i;// for moniroting iterations
//        }while (all_contours.size()<2); //less than is there for strat cases
//
//    }

array<vertex<float>, 2> scanForBifurcation(aursurface<float>& airwaySurface,
    AsrImage3D<short>& imSpeedMap,
    const vertex<float>& cutPoint,
    array<array<unsigned int, 2>, 2>& bifurcationTriangles)
{
    return scanForBifurcation(
        airwaySurface, imSpeedMap, cutPoint, vec3<float>(cutPoint.nx, cutPoint.ny, cutPoint.nz), bifurcationTriangles);
}

//     array< vertex<float>, 2>
int scanForBifurcationWithNormalTracking(aursurface<float>& airwaySurface,
    AsrImage3D<short>& imSpeedMap,
    const vertex<float>& cutPoint,
    const vec3<float>& normal,
    array<array<unsigned int, 2>, 2>& bifurcationTriangles,
    vertex<float>& finalVertex,
    aurcontour<float>& centreLine)
{

    cout << "scanForBifurcationWithNormalTracking- begin " << endl;
    centreLine.clear();
    // TODO - minor - move this out of here, or put flag to skip if not needed
    // TODO create start point to that is not right at the beginning when doing multiple runs
    cout << "do initDeform " << endl;

    airwaySurface.InitDeform();
    cout << "done initDeform " << endl;
    airwaySurface.computeMinimumNormalToTriangleIntersection();

    AsrImage3D<short> im_slices = imSpeedMap;
    im_slices = 0;
    AsrImage3D<short> im_cog = im_slices;
    AsrImage3D<short> im_cutaxis = im_slices;

    array<vertex<float>, 2> cutPoints{ vertex<float>(0, 0, 0), vertex<float>(0, 0, 0) };
    //        cout << "scan for bifurcation " << endl;
    // bounds will change with changing normal
    array<float, 2> bounds = airwaySurface.zbounds(cutPoint, normal); // get bounds to scans
    //        cout << ":bounds " << bounds[0] << " " << bounds[1] << " " << airwaySurface.getNumberOfVertices() << endl;

    // the cutSurcace can pose problem if it cut through distant airway
    vertex<float> slicePoint = cutPoint + normal * zresBifurcation;
    airwaySurface.drawSurface(im_slices, short(1));
    list<aurcontour<float> > all_contours = airwaySurface.cutSurface(slicePoint, normal);
    { // draw initial contour for debugging
        for(auto& c : all_contours)
            c.drawClosedContour(im_slices, short(2));
        im_slices.value(
            slicePoint.x / im_slices.xdim(), slicePoint.y / im_slices.ydim(), slicePoint.z / im_slices.zdim()) = 102;
    }
    im_slices.writeImage("initContour.nii.gz");
    int stepNumber = 1;
    // should always be 1 contour to start, case where it cross anchored triangles, only a single contour is return from
    // get contour
    while(aurcontour<float>::getContour(all_contours, slicePoint, normal).empty()) {
        cout << "empty let's move forward alogn normal " << all_contours.size() << " "
             << aurcontour<float>::getContour(all_contours, slicePoint, normal).size() << " " << slicePoint.to_string()
             << endl;
        slicePoint = slicePoint + normal * zresBifurcation;
        all_contours = airwaySurface.cutSurface(slicePoint, normal);
        // TODO stop when no intersectionn at all, not just closed contour
        if(stepNumber > 15) // if it hasn't found it yet something is wrong
            return 1;
        ++stepNumber;
    }

    //        cout<<"done finding start point"<<endl;
    slicePoint = aurcontour<float>::getContour(all_contours, slicePoint, normal).centreOfGravity();

    float areaInit = aurcontour<float>::getContour(all_contours, slicePoint, normal).area(normal);

    vertex<float> COG_cur{ slicePoint };
    //        ,COG_prev{slicePoint};
    float z = zres;

    aurcontour<float> validContour;
    vec3<float> local_normal{ normal }, prevNormal{ normal };
    int iter = 1;
    float offset = 15;                            // need 10 mm past bifurcation
    while(zresBifurcation < (bounds[1] - offset)) // i starts at 1 to skip the open slice where the cut was
    {
        //        vertex<float> slicePoint = cutPoint + normal*( i*zresBifurcation
        //        );
        // TODO average around contour
        //                cout<<"CUT AXIS "<<all_contours.size()<<endl;
        // average over a
        vertex<float> cutAxisPoint0 = airwaySurface.getTriangleCentroid(all_contours.front().triangle(0)[0]);
        vertex<float> cutAxisPoint1 =
            airwaySurface.getTriangleCentroid(airwaySurface.getClosestTriangle(all_contours.front().triangle(0)[0]));

        vec3<float> cutAxis = cutAxisPoint1 - cutAxisPoint0;
        normalize(cutAxis);
        //            vertex<float> pivotPoint =
        //            midpoint(cutAxisPoint0,cutAxisPoint1);

        //            cout<<"cutAxis "<<cutAxis.to_string()<<endl;
        // current implementation require slciepoint to be on the surface
        //            local_normal = estimateCutPlane(airwaySurface, cutAxis,
        //            slicePoint).begin()->second;
        //            local_normal = estimateCutPlane(airwaySurface, cutAxis,
        //            pivotPoint).begin()->second;
        //            local_normal = estimateCutPlane(airwaySurface, cutAxis,
        //            pivotPoint,local_normal).begin()->second;
        // TODO use previous nromal to restrict change in nromal direction
        //            local_normal = estimateCutPlane(airwaySurface, cutAxis,
        //            pivotPoint);
        prevNormal = local_normal;
        local_normal = estimateCutPlane(airwaySurface, cutAxis, slicePoint);

        // TODO figure out why 0 closed contours and optiimize search whihtin angle
        // RANGE
        if((angle(prevNormal, local_normal) > ANGLE_RANGE) ||
            (l2norm(local_normal) == 0)) { // allows for poor normal estimation by ignoring at astep
            local_normal = prevNormal;
        }
        //            local_normal = estimateCutPlane(airwaySurface, cutAxis,
        //            pivotPoint,local_normal).begin()->second;
        //            local_normal = estimateCutPlane(airwaySurface, cutAxis,
        //            cutAxisPoint0).begin()->second;
        // TODO change to use of prevcious normal, enforces smoothness of normal
        reorientNormal(local_normal, prevNormal);

        // chnages the centre as we move own the airway
        bounds = airwaySurface.zbounds(slicePoint, local_normal); // get bounds to scans

        all_contours = airwaySurface.cutSurface(slicePoint, local_normal);
        cout << "all contours.size() " << slicePoint.to_string() << " " << all_contours.size() << " "
             << local_normal.to_string() << endl;
        //                    if ( all_contours.size() == 1 )
        //                    {"
        //                        cout<<"singlel "
        //                    }
        // safeguard against going outside of surface bounds
        if(all_contours.empty()) {
            break;
        }
        //                cout<<"Previous normal "<< all_contours.size() <<" "<<slicePoint.coord_to_string()<<"
        //                "<<prevNormal.to_string()<<endl;
        if(all_contours.size() > 1) {
            cout << "Compare all contours against previous valid contour " << all_contours.size() << " "
                 << prevNormal.to_string() << endl;
            //                             cout<<"let's make sure triangles are not neighbours "<<endl;
            // assume size is 2, maye need to incorporate the contour filter

            //                        all_contours.front().nearestVertex(all_contours.back(), bifurcationTriangles);
            // make sure that trinagle are not neghbour
            //                        for (unsigned int side = 0 ; side < 2; ++side  ){
            //                        for ( unsigned int tri = 0 ; tri < 2 ; ++tri ){
            //
            //                        }

            // exlude if shares a triangle
            //                    for (auto cont = all_contours.begin(); cont != all_contours.end(); ++cont )
            //                    {
            //                        //compare each contour with every other contour
            //                        //this is a safe guard for >2 contour due to geometry issues and distant airways
            //                        auto cont2 = cont;
            //                        cont2++;
            //
            //                        for ( ; cont2 != all_contours.end(); ++cont2 )
            //                            {
            //                                all_contours
            //                            }
            //
            //                    }

            // project points onto plane defined by previous contours
            //...or just project the COG for speed
            //                        //                    for ( auto& contour : all_contours)
            for(auto i_contour = all_contours.begin(); i_contour != all_contours.end(); ++i_contour) {
                cout << "Previous COG " << COG_cur.coord_to_string() << endl;
                cout << "AREA " << areaInit << " " << i_contour->area(local_normal) << " " << CUT_AREA_THRESH * areaInit
                     << endl;
                if(abs(i_contour->area(local_normal)) < CUT_AREA_THRESH * areaInit)
                    cout << "contourCOG " << i_contour->centreOfGravity().coord_to_string() << endl;
                // COG_cur would be old COG
                // normal is unit vector because generate with RotatePlane function
                vertex<float> cog = i_contour->centreOfGravity();
                //                                cout << "cog " << cog.coord_to_string() << endl;
                float distFromPlane = dotProduct(cog - COG_cur, prevNormal);
                cog = cog - prevNormal * distFromPlane;
                //                                cout << "cogProjected " << cog.coord_to_string() << " " <<
                //                                dotProduct(cog - COG_cur, prevNormal) << endl;
                cout << "is inside previous contour " << validContour.insideConvexPolygon(cog) << endl;
                // remove invalid contours
                // area check is used to make sure there is no spurious small contours that confound the cut
                // small contours could ressult from noise in the surface, aritfact from ventilation tube in cadavers,
                // not fully grown usfrcae
                if((!validContour.insideConvexPolygon(cog)) ||
                    (i_contour->area(local_normal) < CUT_AREA_THRESH * areaInit)) {
                    i_contour = all_contours.erase(i_contour);
                    --i_contour;
                }
            }
        }
        if(all_contours.empty()) {
            break;
        }
        cout << "grab valid contour " << all_contours.size() << endl;
        // TO DO WHAT IF SLICE POINT CROSS surface
        // this condition allows us to scan a few slices past initial cut
        if(all_contours.size() == 1) {
            validContour = aurcontour<float>::getContour(all_contours, slicePoint, local_normal);
        }
        //                validContour = all_contours.back();

        if(validContour.empty()) {
            cout << "no valid contour " << endl;
            //                validContour = all_contours.front();
            break;
        }
        //                         cout<<"allContour size "<<all_contours.size()<<" "<<validContour.size()<<endl;
        //            COG_cur = all_contours[0].centreOfGravity();
        finalVertex = slicePoint;
        COG_cur = validContour.centreOfGravity();
        validContour.drawClosedContour<short>(im_slices, iter);
        im_slices.value(slicePoint.x / im_slices.xdim(), slicePoint.y / im_slices.ydim(),
            slicePoint.z / im_slices.zdim()) = 100 + iter;
        im_cog.value(COG_cur.x / im_slices.xdim(), COG_cur.y / im_slices.ydim(), COG_cur.z / im_slices.zdim()) = iter;
        //            COG_cur=
        //            getContour(all_contours,slicePoint,local_normal).centreOfGravity();
        //            getContour(all_contours,slicePoint,local_normal).drawClosedContour<short>(im_slices,iter);

        //            slicePoint = COG_cur + local_normal * zresBifurcation;
        // TODO Check to see if the slice point cross surface
        //                slicePoint = slicePoint + local_normal * zresBifurcation;
        slicePoint = COG_cur + local_normal * zresBifurcation;
        //            cout<<"updateSlicePoint "<<slicePoint.to_string()<<"
        //            "<<local_normal.to_string()<<endl;
        // cout<<"drawcontour"<<endl;
        for(auto& c : all_contours)
            c.drawClosedContour(im_slices, short(iter));

        //                 im_slices.writeImage("imBronchusCut.nii.gz");

        // TODO add condition for more than 2 contours
        //                cout<<"bounds1 "<<bounds[1]<<endl;
        // bounds restriction forces the branch to grow Zres pas the cut
        //                if (bounds[1] > zres*2)
        if(all_contours.size() == 2) {
            cout << "found cut point at " << slicePoint.to_string() << endl;
            break;
        }

        // add to centre after cut point check to exclude at cut point
        centreLine.push_back(slicePoint);
        //                centreLine.push_back(COG_cur);
        //            cout<<"step "<<iter<<" "<<local_normal.to_string()<<"
        //            "<<slicePoint.to_string()<<endl;
        //            COG_cur.coord_to_string()<<" "<<endl;
        //            cout<<"is empty "<< validContour.empty()<<endl;
        //                drawLine(im_cutaxis, vec3<float>(cutAxisPoint0), vec3<float>(cutAxisPoint1), 0.05f,
        //                short(iter));

        iter++;
    }

    //                im_slices.writeImage("imBronchusCut.nii.gz");
    //                im_cog.writeImage("imBronchusCut_cog.nii.gz");
    //        im_cutaxis.writeImage("imBronchusCut_cutAxis.nii.gz");
    if((zresBifurcation > (bounds[1] - offset)) || all_contours.empty() || validContour.empty()) // found not cut point
    {
        //                cout << "did not find ct point " << endl;
        return 1; // these are (0,0,0)
    }

    //    cout<<"get point and triangles"<<endl;
    //        	cout<<"search for more precise bifurcation point "<<zfine<<endl;
    //				all_contours =
    // surfTrachea.cutSurface(vertex<float>(0,0,zfine), vec3<float>(0,0,1));
    //				if (all_contours.size()==2)
    {
        // all_contours outside this loop will be the intersection one
        //                        array< array<unsigned int,2> ,2 >
        //                        bifurcationTriangles;
        //                    array<vertex<float>,2> closestContourPoints ;
        //					closestContourPoints
        // should ionly be two contours
        //                                all_contours = airwaySurface.cutSurface(slicePoint, local_normal);
        cutPoints = all_contours.front().nearestVertex(all_contours.back(), bifurcationTriangles);
        cout << "cutpoints " << all_contours.size() << " , " << cutPoints[0].coord_to_string() << " "
             << cutPoints[1].coord_to_string() << endl;
        //                            cout<<"bifTriangles
        //                            "<<bifurcationTriangles[0][0]<<"
        //                            "<<bifurcationTriangles[0][1]<<"
        //                            "<<bifurcationTriangles[1][0]<<"
        //                            "<<bifurcationTriangles[1][0]<<"
        //                            "<<bifurcationTriangles[1][1]<<endl;
        vertex<float> bifurcationPoint = midpoint(cutPoints[0], cutPoints[1]);
        //					skeleton.push_back(midpoint(closestContourPoints[0],closestContourPoints[1]));
        //                    cout<<"contour 0 "<<endl;
        //                    all_contours[0].printVerticesWithTriangles();
        //                    cout<<"contour 1 "<<endl;
        //                    all_contours[1].printVerticesWithTriangles();
        setNormal(finalVertex, local_normal);
        //                finalNormal = local_normal;
        cout << "found bifurcation Point " << bifurcationPoint.x / imSpeedMap.xdim() << " "
             << bifurcationPoint.y / imSpeedMap.ydim() << " " << bifurcationPoint.z / imSpeedMap.zdim() << endl;
        //					break;
    }
    //				zfine-=zresBifurcation;
    //    im_slices.writeImage("imBronchusCut.nii.gz");
    //                im_cog.writeImage("imBronchusCut_cog.nii.gz");
    return 0;
}

array<vertex<float>, 2> scanForBifurcation(aursurface<float>& airwaySurface,
    AsrImage3D<short>& imSpeedMap,
    const vertex<float>& cutPoint,
    const vec3<float>& normal,
    array<array<unsigned int, 2>, 2>& bifurcationTriangles)
{
    AsrImage3D<short> im_slices = imSpeedMap;
    im_slices = 0;
    array<vertex<float>, 2> cutPoints;
    cout << "scan for bifurcation " << endl;
    array<float, 2> bounds = airwaySurface.zbounds(cutPoint, normal); // get bounds to scans
    cout << ":bounds " << bounds[0] << " " << bounds[1] << endl;
    int Nsteps = static_cast<int>((bounds[1] - bounds[0] + zres) / zresBifurcation);
    vertex<float> slicePoint = cutPoint + normal * zres;
    float z = zres;
    list<aurcontour<float> > all_contours;
    cout << "normal " << normal.to_string() << endl;
    for(int i = 1; i <= Nsteps; ++i) // i starts at 1 to skip the open slice where the cut was
    {
        //        vertex<float> slicePoint = cutPoint + normal*( i*zresBifurcation
        //        );
        cout << "step " << i << " " << z << " " << bounds[1] << endl;

        all_contours = airwaySurface.cutSurface(slicePoint, normal);
        for(auto& c : all_contours)
            c.drawClosedContour(im_slices, short(i));

        cout << "numebr of contorus " << all_contours.size() << endl;
        if(all_contours.size() > 1) {
            cout << "found cut point at " << slicePoint.to_string() << endl;
            break;
        }
        z += zresBifurcation;
        // ensure that it is within the surface
        if(z > bounds[1]) {
            break;
        }
        slicePoint = slicePoint + normal * zresBifurcation;
    }
    cout << "get point and triangles" << endl;
    //        	cout<<"search for more precise bifurcation point "<<zfine<<endl;
    //				all_contours =
    // surfTrachea.cutSurface(vertex<float>(0,0,zfine), vec3<float>(0,0,1));
    //				if (all_contours.size()==2)
    {
        // all_contours outside this loop will be the intersection one
        //                        array< array<unsigned int,2> ,2 >
        //                        bifurcationTriangles;
        //                    array<vertex<float>,2> closestContourPoints ;
        //					closestContourPoints
        cutPoints = all_contours.front().nearestVertex(all_contours.back(), bifurcationTriangles);
        cout << "bifTriangles " << bifurcationTriangles[0][0] << " " << bifurcationTriangles[0][1] << " "
             << bifurcationTriangles[1][0] << " " << bifurcationTriangles[1][0] << " " << bifurcationTriangles[1][1]
             << endl;
        vertex<float> bifurcationPoint = midpoint(cutPoints[0], cutPoints[1]);
        //					skeleton.push_back(midpoint(closestContourPoints[0],closestContourPoints[1]));
        //                    cout<<"contour 0 "<<endl;
        //                    all_contours[0].printVerticesWithTriangles();
        //                    cout<<"contour 1 "<<endl;
        //                    all_contours[1].printVerticesWithTriangles();

        cout << "found bifurcation Point " << bifurcationPoint.x / imSpeedMap.xdim() << " "
             << bifurcationPoint.y / imSpeedMap.ydim() << " " << bifurcationPoint.z / imSpeedMap.zdim() << endl;
        //					break;
    }
    //				zfine-=zresBifurcation;
    im_slices.writeImage("imBronchusCut.nii.gz");
    return cutPoints;
}

int readCutPlanes(const std::string& filename, const string& branchName, vertex<float>& planeVertex)
{
    ifstream fplanes(filename.c_str());
    if(!fplanes.is_open()) {
        return 1;
    }
    string line;
    while(getline(fplanes, line)) {
        stringstream ss(line);
        string name;
        ss >> name;
        if(name == branchName) {
            ss >> planeVertex.x >> planeVertex.y >> planeVertex.z >> planeVertex.nx >> planeVertex.ny >> planeVertex.nz;
            planeVertex.scalar = 0;

            fplanes.close();
            return 0;
        }
    }

    fplanes.close();
    return 1;
}

int readCutPlanes(const std::string& filename,
    const string& branchName,
    vertex<float>& planeVertex,
    vec3<float>& planeNormal)
{
    if(readCutPlanes(filename, branchName, planeVertex)) { // couldn't read file
        return 1;
    } else {
        planeNormal = vec3<float>(planeNormal.x, planeNormal.y, planeNormal.z);
    }
    return 0;
}

// utilities to write cut points to a file, overwrite ir append
int writeCutPointToFile(const string& filename, const string& branchName, const vertex<float>& planeVertex)
{
    return writeCutPointToFile(filename, branchName, planeVertex,
        vec3<float>(planeVertex.nx, planeVertex.ny, planeVertex.nz), std::ofstream::out);
}

int appendCutPointToFile(const string& filename, const string& branchName, const vertex<float>& planeVertex)
{
    return writeCutPointToFile(filename, branchName, planeVertex,
        vec3<float>(planeVertex.nx, planeVertex.ny, planeVertex.nz), (std::ofstream::out | std::ofstream::app));
}
int appendCutPointToFile(const string& filename,
    const string& branchName,
    const vertex<float>& planeVertex,
    const vec3<float>& planeNormal)
{
    return writeCutPointToFile(
        filename, branchName, planeVertex, planeNormal, (std::ofstream::out | std::ofstream::app));
}
int writeCutPointToFile(const string& filename,
    const string& branchName,
    const vertex<float>& planePoint,
    const vec3<float>& planeNormal,
    ios_base::openmode mode)
{
    ofstream fout(filename, mode);
    if(!fout.is_open())
        return 1;

    fout << branchName << " " << planePoint.x << " " << planePoint.y << " " << planePoint.z << " " << planeNormal.x
         << " " << planeNormal.y << " " << planeNormal.z << endl;

    return 0;
}
}