/*
 * run_test.cpp
 *
 *  Created on: May 11, 2016
 *      Author: brian
 */
#include "asrtreemodel/AsrTreeModel.h"
#include "aursurface/aursurface.h"
//#include <asrtreemodel/AsrAirwayGeometryModel.h>
#include "AsrImage3D/AsrImage3D.h"
#include "AsrImage3D/AsrImage3Ditk.h"
#include "airwayseg/RegionGrowingAlgos.h"
#include "aurcommon/aurstructs_functions.hpp"
#include "aurcommon/miscfunctions.h"
#include "segmentation_utils.h"
#include <glm/gtx/string_cast.hpp>
// STL includes
#include <fstream>
#include <iostream>
#include <map>
#include <vector>
// 3rd party includes
#include "utils/asr-timer.h"
#include <asrtreemodel/AsrAirwayGeometryModel.h>
#include <boost/filesystem.hpp>
// wxWidgets "Hello world" Program
// For compilers that support precompilation, includes "wx/wx.h".
//#include <wx/window.h>
//#ifndef WX_PRECOMP
//    #include <wx/wx.h>
//#endif
template <class T> using grow = auris_segtools::RegionGrowingAlgos<T>;
using namespace std;
using namespace auris_segtools;
using namespace boost::filesystem;
void Usage()
{
    cout << "\n grow_airway [-distMap ] [-fill <min,max,medium,gradient,threshold> [-mask <initial_image> ] [-thr <threshold> ] <nifit file> <ouput base> "
            "<vox_seedx> "
            "<vox_seedy> <vox_seedz>"
         << endl;
    cout << "\n This program is uses region growing in an image\n" << endl;
}

float maxNeighbour(AsrImage3D<short>& im, const vec3<int>& seed)
{
    short max{ im.value(seed.x, seed.y, seed.z) };
    array<int, 2> offsets{ -1, 1 };
    for(auto& dxyz : offsets) {
        if(im.value(seed.x + dxyz, seed.y, seed.z) > max)
            max = im.value(seed.x + dxyz, seed.y, seed.z);
        if(im.value(seed.x, seed.y + dxyz, seed.z) > max)
            max = im.value(seed.x, seed.y + dxyz, seed.z);
        if(im.value(seed.x, seed.y, seed.z + dxyz) > max)
            max = im.value(seed.x, seed.y, seed.z + dxyz);
    }
    return static_cast<float>(max);
}

int main(int argc, char* argv[])
{
    //	cout<<"\nEstimating tissue thresholds for Fat and Lung from
    // image..."<<endl;
    if(argc < 6) {
        Usage();
        return 0;
    }

    bool writeDebug{ true };
    //    bool writeDebug{ false };
    asrTimer algoTimer;
    cout << "create Tree Model " << endl;
    AsrTreeModel airwayTree;
    cout << "end tree " << endl;
    AsrAirwayGeometryModel airwayModel;
    int currentArg = 1;
    //    bool argParsing = true;
    bool useDistMap{ false }, useMask{ false };
    float airThresh{ -950 }; // taken from RSIP base on CR data and houndsfield units
    vec3<float> tracheaCOG(0, 0, 0);
    AsrImage3D<float> distMap;
    AsrImage3D<short> mask;
    FillMethod fillType{ FillMethod::MEDIAN_THRESHOLD };
    int zmax{ 0 };

    while(currentArg < argc) {
        if(string(argv[currentArg]) == "-distMap") {
            distMap.readImage(argv[++currentArg]);
            currentArg++;
            useDistMap = true;
        } else if(string(argv[currentArg]) == "-mask") {
            cout << "load mask " << endl;
            mask.readImage(argv[++currentArg]);
            useMask = true;
            ++currentArg;
        } else if(string(argv[currentArg]) == "-thr") {
            cout << "set threshold  " << endl;
            airThresh = atof(argv[++currentArg]);
            ++currentArg;
        } else if(string(argv[currentArg]) == "-zmax") {
            cout << "set z  " << endl;
            zmax = atoi(argv[++currentArg]);
            ++currentArg;
        } else if(string(argv[currentArg]) == "-fill") {
            string sfill(argv[++currentArg]);
            if(sfill == "median") {
                fillType = FillMethod::MEDIAN_THRESHOLD;
            } else if(sfill == "threshold") {
                fillType = FillMethod::THRESHOLD;
            } else if(sfill == "min") {
                fillType = FillMethod::MIN_THRESHOLD;
            } else if(sfill == "max") {
                fillType = FillMethod::MAX_THRESHOLD;
            } else if(sfill == "gradient") {
                fillType = FillMethod::GRADIENT;
            } else if(sfill == "mean") {
                fillType = FillMethod::MEAN_THRESHOLD;
            }
            //            else if ( sfill == "median"){
            //                fillType = FillMethod::MEDIAN_THRESHOLD;
            //            }else

            ++currentArg;
        } else { // break iof no recognized argument

            break;
        }
    }

    cout << (argc - currentArg) << endl;
    if((argc - currentArg) < 4) {
        Usage();
        exit(EXIT_FAILURE);
    }
    path file(argv[currentArg]);
    if(!exists(file)) {
        cerr << "\n File does not exists : " << file << "\n" << endl;
        return 1;
    } else if(!is_regular_file(file)) {
        cerr << "\n"
             << file << " is not a file"
             << "\n"
             << endl;
        return 1;
    }
    cout << "proceed " << endl;
    string filename(argv[currentArg++]);
    string filename_out(argv[currentArg++]);
    cout << "Input Image " << filename << endl;
    cout << "Output base " << filename_out << endl;

    // TODO make threshold symmetric pass , need recursion
    float distanceThreshold{ 100.0 }; // trachea uses different one
                                      //    float distanceThreshold{ 100.0 }; // trachea uses different one
    //                                      //    float distanceThreshold{ 0.0 }; // trachea uses different one
    float thresholdInit{ -1000 };
    if(fillType == FillMethod::GRADIENT) {
        thresholdInit = -100;
    }
    int connectivity{ 3 };
    float growthFactor{ 1.0 }, maxGrowthFactor{ 10.3 };
    unsigned int minSegmentSize{ 10 };
    //    unsigned int maxSegmentSize{ imAirways.GetNumberOfNonZeroVoxels() };
    unsigned int maxSegmentSize{ 100000 };
    // get trachea seed in s
    vec3<int> voxSeed;
    voxSeed.x = static_cast<int>(atof(argv[currentArg++]) + 0.5);
    voxSeed.y = static_cast<int>(atof(argv[currentArg++]) + 0.5);
    voxSeed.z = static_cast<int>(atof(argv[currentArg++]) + 0.5);
    cout << "Input Seed " << voxSeed.to_string() << endl;
    // the air image should preserve more of the wall between lungs and trachea
    AsrImage3D<short> imCT;
    AsrImage3D<float> imAirways;
    unordered_set<unsigned int> sparseImAirways;
    float voxelVolume = imCT.voxelSize();
    imCT.readImage(filename);
    cout << "done CT read " << endl;
    if(zmax > 0) {
        imCT.setValueAboveZ(zmax, 1000);
        //        imCT.writeImage("grot.nii.gz");
    }
    // TODO set up proper constructor without copying data without data type
    //    AsrImage3D<short>::copyImageInfo(imAirways, imCT)
    imAirways.copyImageInfo(imCT);
    if(!useDistMap) {
        distMap = imAirways;
    }

    ///---DONE SETTING UP DATA---////

    // LETS GET TRACHEA
    // deformation parameters for the deformable surface
    deformationParameters<float> params;
    // alternate between how far down we go 10 and 20, alpha_area scales
    params.splitThreshold = 10;
    //    params.splitThreshold = 20;
    params.max_step_size = 0.5;
    params.alpha_area = 0.01;
    //    params.alpha_area = 0.005;
    params.alpha_im = 0.4;
    //        params.alpha_sn = 0.0;
    //    params.alpha_st = 0.0;
    params.alpha_sn = 0.05;
    params.alpha_st = 0.1;

    // initial trachea Seed
    vertex<float> tracheaSeed(voxSeed.x * imCT.xdim(), voxSeed.y * imCT.ydim(), voxSeed.z * imCT.zdim(), 0, 0, -1, 0);

    // newSeed will store the topmost trache voxel after trachea segmentation
    vec3<int> newSeed;

    cout << "tracheaSeed " << tracheaSeed.coord_to_string() << endl;
    algoTimer.addTimeStamp("start Pleura");
    AsrImage3D<short> imPleura;
    imPleura = imCT;
    imPleura = 0;
    cout << "thresh " << endl;
    imCT.upperThreshold(static_cast<short>(-300), imPleura);
    imPleura.fill(voxSeed.x, voxSeed.y, voxSeed.z, static_cast<short>(100), 3);
    imPleura.lowerThreshold(static_cast<short>(50));
    algoTimer.addTimeStamp("end Pleura");
    cout << "get pleura" << endl;
    // unordered_set<unsigned int> fvox,pleura;
    //    imCT.fillDevelop(voxSeed.x,voxSeed.y,voxSeed.z,distMap,static_cast<short>(1),-600,1e10,fvox,pleura,0.0,FillMethod::MEDIAN_THRESHOLD);
    // cout<<"done pleaura "<<endl;
    // for (auto& i : pleura){
    //    imPleura.value(i)=1;
    //}
    imPleura.writeImage(filename_out + "_pleura.nii.gz");
    //    bool useSurface(false);
    bool useSurface(true);
    // Default is to use the surface to grow the trachea, an image fill option is given
    // but not recommed or widely tested
    // TODO and self-intersection test at the end of the trachea segmentation
    // TODO incorporate robust stop criteria
    if(useSurface) {

        vertex<float> tracheaCut = tracheaSeed;
        std::array<aursurface<float>, 3> surfMainBronchi;
        aursurface<float> surfTrachea;
        surfTrachea.createSphere(tracheaSeed, 1.0f, 10, 10);
        surfTrachea.InitDeform();
        //    surfTrachea.write("grot.vtk", aursurface<float>::VTK);
        // imCT.zeroAboveZ(voxSeed.z);
        cout << to_vertex(surfTrachea.centroid()).to_string() << endl;
        cout << imCT.interpolate(to_vertex(surfTrachea.centroid())) << " " << surfTrachea.centroid().to_string()
             << endl;
        surfTrachea.setReferenceIntensity(imCT.interpolate(to_vertex(surfTrachea.centroid())));
        // return 0;
        //    deformToBifurcation(surfTrachea,imCT,tracheaSeed,params,aursurface<float>::NEG_GRAD);
        // TODO ADD INTELLIGENT STOP CRITERIA
        // TODO i.e cuts into multiple slices or skeleton has multiple segment
        for(int i = 0; i < 2000; ++i) {
            // cout<<"i "<<i<<endl;
            surfTrachea.deform(imCT, 1, params.alpha_sn, params.alpha_st, params.alpha_area, params.alpha_im,
                params.max_step_size, params.splitThreshold, aursurface<float>::NEG_GRAD);
        }

        cout << "fill trachea at " << voxSeed.to_string() << endl;

        if(writeDebug) {
            surfTrachea.write(filename_out + "_trachea.stl", aursurface<float>::STL);
        }

        surfTrachea.drawSurface(imAirways, 1.0f);

        // TODO rewrite directly to sparse imairways
        imAirways.fill(voxSeed.x, voxSeed.y, voxSeed.z, 1);
        algoTimer.addTimeStamp("convert trachea to sparse image");
        auto idata = imAirways.data();
        for(int z = 0; z < imAirways.zsize(); ++z) {
            for(int y = 0; y < imAirways.ysize(); ++y) {
                for(int x = 0; x < imAirways.xsize(); ++x, ++idata) {
                    if(*idata > 0) {
                        sparseImAirways.insert(imAirways.getIndex(x, y, z));
                    }
                }
            }
        }
        algoTimer.addTimeStamp("done convert trachea to sparse image");

        // Get maximum Z voxel for creating distance map
        vertex<float> highestVertex = surfTrachea.GetMaximumZVertex();
        newSeed.x = static_cast<int>(highestVertex.x / imCT.xdim() + 0.5);
        newSeed.y = static_cast<int>(highestVertex.y / imCT.ydim() + 0.5);
        newSeed.z = static_cast<int>(highestVertex.z / imCT.zdim() + 0.5);

    } else {

        float distanceThreshold{ 100.0 };
        unordered_set<unsigned int> seg_voxels;
        grow<short>::GrowRegionIncrementalThreshold(imCT, distMap, voxSeed, minSegmentSize, maxSegmentSize,
            thresholdInit, connectivity, maxGrowthFactor, distanceThreshold, fillType, imAirways, seg_voxels);
        newSeed = imAirways.findSeedInZPlane();

    }

    if(writeDebug) {
        imAirways.writeImage(filename_out + "_trachea.nii.gz");
    }

    cout << "newSeed " << newSeed.to_string() << endl;
    distFromSeed(imAirways, sparseImAirways, distMap, newSeed);
    //        distMap.writeImage("distMap.nii.gz");

    //seg_voxels is the sparse respresention of the image
    unordered_set<unsigned int> seg_voxels;
    //create skeleton from trachea segmentation to find bronchi entry points
    airwayTree.CreateSkeleton(distMap, newSeed, seg_voxels);

    if(writeDebug) {
        airwayTree.writeTreeStructure(filename_out + "_trachea.tree.csv");
        airwayTree.writeTreePointsAsSurface(filename_out + "_trachea.skeleton.vtk", 1.0);
    }

    // trachea and two bronchi

    // initializes to main bronchi
    // get entry point
    // Assumes the 1 is trachea
    // TODO have safety check to assert / fix this 
    vector<int> bronchiID = airwayTree.GetChildren(1);
    int level = 1;
    int childCount{ 0 };

    //    float thresholdInit{ imCT.GetMaxIntensity(imAirways) };
    // Sample the intesity values on the trachea and branches skeleton
    // Find the maximum to  initiate threshold
    aurcontour<float> skelPoints = airwayTree.getSkeletonPoints();
    //       for(auto i = skelPoints.begin(); i != skelPoints.end(); ++i) {
    //            float val = imCT.interpolate(*i);
    //            if(val > thresholdInit)
    //                thresholdInit = val;
    //            //        cout<<"sample "<<i->to_string()<<imCT.interpolate(*i)<<endl;
    //        }
    //remove this was not robust

    cout << "Initial threshold " << thresholdInit << endl;
    cout << "min segment size " << minSegmentSize << endl;
    cout << "max segment size " << maxSegmentSize << endl;

    unsigned int generation{ 2 };
    // gtre the tracheas children
    list<pair<int, vec3<float> > > childrenEntryPoints{ airwayTree.GetChildrenEntryPoints(1) };
    // TODO only need to run distance map down each branch
    // Have option to jump to leaves or descend by generation 
    // Descend by generation seems to be more robust
    //    bool useLeaves{ true };
    bool useLeaves{ false };
    if(useLeaves) {
        int iter{ 0 };
        while(iter < 5) {
            while(!childrenEntryPoints.empty()) {
                auto entryPoint = childrenEntryPoints.front().second;
                childrenEntryPoints.pop_front();

                cout << "Entry Point " << entryPoint.to_string() << endl;
                cout << "Entry Point " << entryPoint.x / imCT.xdim() << " " << entryPoint.y / imCT.ydim() << " "
                     << entryPoint.z / imCT.zdim() << " " << endl;
                cout << "Generation " << generation << endl;
                cout << "initial threshold " << thresholdInit << endl;
                //                imSubAirways = 0;
                //                float threshold{ imCT.interpolate(entryPoint.x, entryPoint.y, entryPoint.z) };
                float threshold{ thresholdInit };
                unordered_set<unsigned int> seg_voxels;
                grow<short>::GrowRegionIncrementalThreshold(imCT, distMap,
                    vec3<int>(entryPoint.x / imCT.xdim(), entryPoint.y / imCT.ydim(), entryPoint.z / imCT.zdim()),
                    minSegmentSize, maxSegmentSize, threshold, connectivity, maxGrowthFactor, distanceThreshold,
                    fillType, imAirways, seg_voxels);
                
                //legacy from using images instead of sparse representation
                //                    fillType, imSubAirways, seg_voxels);
                //                imAirways += imSubAirways;
            }
            // TODO ELEIMINAte extra call to this portion at the end
            { // get next generation entry points
                // can get caught in terrible loops if leaks exist
                //                imAirways.binarize();

                distFromSeed(imAirways, distMap, newSeed);
                unordered_set<unsigned int> seg_voxels;
                AsrTreeModel airwayTree;
                airwayTree.CreateSkeleton(distMap, newSeed, seg_voxels);

                // add next generation
                for(auto& leaf : airwayTree.GetLeaves()) {
                    cout << "get new entry point for next leaf " << leaf << airwayTree.GetEntryPoint(leaf).to_string()
                         << endl;
                    childrenEntryPoints.push_back(make_pair(leaf, airwayTree.GetEntryPoint(leaf)));
                }
            }
            ++iter;
        }

    } else {//descend by generation 

        list<unordered_set<unsigned int> > l_seg_voxels;
        unsigned int iter{ 0 };
        unsigned int maxGeneration{ 5 };
        while(generation <= maxGeneration) {
            cout << "NEXT GEN " << generation << " " << childrenEntryPoints.size() << endl;
            //            list<int> branchesVisited;
            while(!childrenEntryPoints.empty()) {
                auto entryPoint = childrenEntryPoints.front().second;
                //                branchesVisited.push_back(childrenEntryPoints.front().first);
                //            auto entryPoint = childrenEntryPoints.front().first;
                //            auto generation = childrenEntryPoints.front().second;
                childrenEntryPoints.pop_front();
                cout << "GENERATION " << generation << endl;

                cout << "Entry Point " << entryPoint.to_string() << endl;
                cout << "Entry Point " << entryPoint.x / imCT.xdim() << " " << entryPoint.y / imCT.ydim() << " "
                     << entryPoint.z / imCT.zdim() << " " << endl;
                cout << "Generation " << generation << endl;
                
                
                //This code utilizaes priors for segment size threshold
                //model is in development 
                // x2 multiplicative factro because of N=1 sample model
                //                                unsigned int maxSegmentSize{
                //                                    static_cast<unsigned
                //                                    int>(airwayModel.getMaximumAirwayVolume(generation) /
                //                                    voxelVolume) * 2
                //                                };
                //                maxSegmentSize =
                //                minSegmentSize = maxSegmentSize / 2.0;
                cout << "maxsegemtsize " << maxSegmentSize << endl;

                float threshold{ thresholdInit };
                cout << "initial threshold " << threshold << endl;
                //                imSubAirways = 0;
                cout << "setzero " << threshold << endl;
                algoTimer.addTimeStamp("GrowRegion_gen" + to_string(generation));
                unordered_set<unsigned int> seg_voxels;
                grow<short>::GrowRegionIncrementalThreshold(imCT, distMap,
                    vec3<int>(entryPoint.x / imCT.xdim(), entryPoint.y / imCT.ydim(), entryPoint.z / imCT.zdim()),
                    minSegmentSize, maxSegmentSize, threshold, connectivity, maxGrowthFactor, distanceThreshold,
                    fillType, imAirways, seg_voxels);

                //sparse image dilation (with statistical criteria )
                grow<short>::FillHoles(imCT, seg_voxels);
                //                              grow<short>::FillHoles(imCT,seg_voxels);
                //can just insert voxels instead of writing to image
                sparseImAirways.insert(seg_voxels.cbegin(), seg_voxels.cend());
                l_seg_voxels.push_back(seg_voxels);
                
                //the following is used to test the sparse image representation 
                //                AsrImage3D<short> grot = imCT;
                //              imCT=0;
                //                for(auto& i : seg_voxels) {
                //                    grot.value(i) = 1;
                //                }
                //                grot.writeImage("sparseAirway_gen"+ to_string(generation)+"_iter"+to_string(iter) +
                //                ".nii.gz");
                ++iter;
                //                imSubAirways.writeImage("grot2.nii.gz");
                cout << "add subairway " << seg_voxels.size() << endl;
                // TODO remove the binarize, used for debugginh, but not necessary
                algoTimer.addTimeStamp("binarize_gen" + to_string(generation));
                //                imSubAirways.binarize();
                cout << "done binazrize" << endl;
                algoTimer.addTimeStamp("add to subairways" + to_string(generation));
                //                imAirways += imSubAirways;
                cout << "done add subairway " << endl;
                algoTimer.addTimeStamp("done add to subairways" + to_string(generation));
            }
            // TODO ELEIMINAte extra call to this portion at the end
            if(generation < maxGeneration) { // get next generation entry points

                // Be careful here the branch IDs change with every skeleton
                //no longer needed to do binarization because of sparse image representation 
                cout << "binarize " << endl;
                //                imAirways.binarize();
                cout << "dist form seed " << endl;
                //                distFromSeed(imAirways, distMap, newSeed);
                distFromSeed(imAirways, sparseImAirways, distMap, newSeed);

                cout << "create skeleton " << sparseImAirways.size() << endl;
                unordered_set<unsigned int> seg_voxels;
                AsrTreeModel airwayTree;
                airwayTree.SetMaximumGeneration(generation + 1);
                cout << "newSeed " << newSeed.to_string() << endl;
                //                distMap.writeImage("sparseAirway_gen"+ to_string(generation)+"_iter"+to_string(iter) +
                //                ".nii.gz");
                airwayTree.CreateSkeleton(distMap, newSeed, seg_voxels);
                //                                sparseImAirways.insert(seg_voxels.cbegin(),seg_voxels.cend());
                cout << "Skeleton Seg Voxels " << seg_voxels.size() << endl;
                //                airwayTree.writeSkeletonImage("skeleton_gen"+to_string(generation)+".nii.gz");
                // add next generation
                // this while loop os only used if not adding all generation branches
                while(childrenEntryPoints.empty() && (generation < maxGeneration)) {
                    // keep on incrementing generation until you've foud a leaf, avpid re-skeltomizing
                    for(auto& brVisited : airwayTree.GetGenerationBranchIDs(generation)) {
                        cout << "get new entry point for next generartion " << brVisited << " "
                             << airwayTree.GetChildrenEntryPoints(brVisited).size() << " " << generation << endl;
                        //                        if(airwayTree.IsLeaf(brVisited)) {
                        cout << "is leaf!" << endl;
                        childrenEntryPoints.push_back(make_pair(brVisited, airwayTree.GetEntryPoint(brVisited)));
                    }
                    if(childrenEntryPoints.empty()) {
                        ++generation;
                    }
                }
            }
            ++generation;
        }

        // write out evolution of the segmentation
        imAirways = 0;
        float count{ 1 };
        for(auto& roi : l_seg_voxels) {
            for(auto& i : roi) {
                imAirways.value(i) = count;
            }
            ++count;
        }
        imAirways.writeImage(filename_out + "_level1Evo.nii.gz");
    }

    //remove image processing version
    //    imAirways.binarize();
    //    imAirways=0;
    for(auto& i : sparseImAirways) {
        imAirways.value(i) = 1;
    }
    imAirways.writeImage(filename_out + "_level1.nii.gz");
    //    imAirways.writeImage(filename_out + "_level1sparse.nii.gz");
    { // create skeleton for level 1
        // seed from trachea
        distFromSeed(imAirways, distMap, newSeed);
        AsrTreeModel airwayTree;
        airwayTree.CreateSkeleton(distMap, newSeed, seg_voxels);
        if(writeDebug) {
            vec3<float> origin{ imAirways.getOrigin() };

            airwayTree.translate(vec3<float>(abs(origin.x) * -1, abs(origin.y) * -1, abs(origin.z) * -1));

            airwayTree.writeSkeletonImage(filename_out + "_level1.skeleton.nii.gz");
            airwayTree.writeTreeStructure(filename_out + "_level1.tree.csv");
            airwayTree.writeTreePointsAsSurface(filename_out + "_level1.skeleton.vtk", 1.0);
            airwayTree.writePerGenerationsStats(filename_out + "_level1.tree.GenStats.vtk");
            airwayTree.writeAirwayGeometryModel(filename_out + "_level1.geometry.csv");
        }
    }
    algoTimer.addTimeStamp("do distance  ");
    cout << "call distance from seed " << endl;
    //    distFromSeed
    algoTimer.addTimeStamp("done distance compute  ");
    if(writeDebug) {
        distMap.writeImage(filename_out + "_distFromSeed.nii.gz");
        ofstream ftime(filename_out + "_timingDebug.txt");
        if(ftime.is_open()) {
            ftime << algoTimer.to_string() << endl;
            ftime.close();
        }
    }
    cout << "Algotime " << algoTimer.to_string() << endl;
}
