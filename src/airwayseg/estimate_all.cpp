/*
 * run_test.cpp
 *
 *  Created on: May 11, 2016
 *      Author: brian
 */
#include "aursurface/aursurface.h"

#include "aurimage/AsrImage3D.h"
#include "aurcommon/miscfunctions.h"
#include "aurcommon/aurstructs_functions.hpp"
#include <glm/gtx/string_cast.hpp>
#include "segmentation_utils.h"
//STL includes
#include <iostream>
#include <vector>
#include <fstream>
#include <map>
//3rd party includes
#include <boost/filesystem.hpp>

using namespace std;
using namespace auris_segtools;
using namespace boost::filesystem;
void Usage(){
	cout<<"\n estimate_trachea <nifit file> <ouput base>"<<endl;
	cout<<"\n This program is used  to estimate the trachea \n"<<endl;

}
 
 
constexpr float areaThreshTrachea{19.625};//threshold in mm2. 3.14*r^2, where r = 2.5mm (looked at trachea stats) 
constexpr float areaThreshLungs{1256.0};//thrwshold is in mm, 3.14 * 20^2 (very low estimate of lungs, ensures further down


vector<unsigned int>  crossings( const vector<float> & histogram, const float & value  )
{
	unsigned int bin=0;
	vector<unsigned int> v_bins;
	for (auto i = histogram.begin(); i != histogram.end()-1 ; ++i,++bin)
	{
//		cout<<"i "<<*i<<endl;
		if ( ((value < *i) && (value > *(i+1))) || ((value > *i) && (value < *(i+1)))  )
			v_bins.push_back(bin);
	}
	return v_bins;
}




void addToSkeleton( const aursurface<float> & airwaySurface,  aurimage<short> & skeleton, const vertex<float> & cutPoint , const vec3<float> & cutPlane )
{
    
    
}


int main(int argc, char*argv[])
{
//	cout<<"\nEstimating tissue thresholds for Fat and Lung from image..."<<endl;
    bool useLungs {false}, cleanCT{false},userDefinedSeed{false};
    int currentArg=1;
    bool argParsing = true; 
   
	vec3<float> tracheaCOG(0,0,0);
	vertex<float> carina(0,0,0);
    

 while (argParsing)
    {
        string argname{argv[currentArg]};
        cout<<"arg "<<argname<<endl;
        if (  argname == "--useLungs" )
        {
            cout<<"Use lungs instead of air "<<endl;
            argc--;
            useLungs=true;
            currentArg++;
                cout<<"currentarg "<<currentArg<<endl;

        }else if (  argname == "--cleanCT" )
        {
            cout<<"cleanCT "<<endl;
            argc--;
            cleanCT=true;
            currentArg++;
            cout<<"currentargCT "<<currentArg<<endl;

                        
        }else if(argname == "--seed" ){
                cout<<"seed "<<endl;
            argc--;
            tracheaCOG.x = atof(argv[currentArg++]);
            argc--;
            tracheaCOG.y = atoi(argv[currentArg++]);
            argc--; 
            tracheaCOG.z = atoi(argv[currentArg++]);
            argc--;
            
            userDefinedSeed=true;
            currentArg++;
        }else{
        
            argParsing=false;
        }
    }
    cout<<"currentarg "<<currentArg<<endl;
	if (argc < 3 )
	{
		Usage();
		return 0;
	}

	path file(argv[currentArg]);
	if (!exists(file))
	{
		cerr<<"\n File does not exists : "<<file<<"\n"<<endl;
		return 1;
	}else if (!is_regular_file(file))
	{
		cerr<<"\n"<<file<<" is not a file"<<"\n"<<endl;
		return 1;
	}

	string filename(argv[currentArg]);
    currentArg++;
	string filename_out(argv[currentArg]);

//	cout<<"Read in file: "<<filename<<endl;

//	aurimage<float> im0(filename);

	//the air image should preserve more of the wall between lungs and trachea
    aurimage<short> imAir;
    if (useLungs)
    {
        imAir.readImage(filename_out + "_lungs.nii.gz");
    }else{
        imAir.readImage(filename_out + "_air.nii.gz");

    }     
  	aurimage<short> imLungs(filename_out + "_lungs.nii.gz");
    

    
	vector<int> bounds{0,0,0,imAir.xsize()-1, imAir.ysize()-1, imAir.zsize()-1};
	
		aurimage<short> imTrachea = imAir;
		imTrachea=0;
		aurimage<short> imSkeleton = imAir;
		imSkeleton=0;
		aurimage<short> imSpeedMap = imAir;
		
		aurimage<short> imCog = imAir;
	imCog=0;
	float voxel_area = imAir.xdim() *imAir.ydim();

	
	//scan through image to look for the trachea 
    //note that _air and _lungs image are same size
	for (int z = imLungs.zsize() -1 ; z>=0 ; --z  )
    {
//		cout<<"z "<<z<<endl;
		vector<int> bounds_roi{bounds[0],bounds[1],z,bounds[3],bounds[4],z};
		sizeCog l_sizes_cog = imLungs.connectedComponents(imLungs,bounds_roi,0);
//		cout<<"number of ROIs "<<l_sizes_cog.size()<<endl;
		int possibleTracheaCount{0}, possibleLungsCount{0};
		sizeCog validCogs;
//                    cout<<"-------"<<endl;

		for (auto& iter :  l_sizes_cog)
		{
//			cout<<"area "<<get<0>(iter)*voxel_area<<" "<<get<1>(iter).x<<" "<<get<1>(iter).y<<" "<<get<1>(iter).z<<endl;
			float area = get<0>(iter)*voxel_area;
			if ( area > areaThreshLungs)
				{
					validCogs.push_back(iter);
					++possibleLungsCount;
				}
			else if (  area > areaThreshTrachea)
				{
				validCogs.push_back(iter);

				++possibleTracheaCount;
				}



		}
//                    cout<<"-------"<<endl;

//		cout<<"check "<<possibleTracheaCount<<" "<<possibleLungsCount<<" "<<validCogs.size()<<endl;
		//3 and 2 because lungs will double count
        //only for more than one trachea possibility, assumption is that trachea is largest amongst noise
		if ((possibleTracheaCount >= 1) && (possibleLungsCount == 2 ))
		{
			
//			validCogs.sort();
//			auto iter = validCogs.begin();
//			//take the middle cog
//			tracheaCOG=get<1>(*(++iter));
			auto riter = validCogs.rbegin();
			array<float,3> cog_x;
			for ( int i = 0 ; i < 3 ;++i,++riter )
			{
				cog_x[i]=get<1>(*riter).x ;
				cout<<"cogx "<<i<<" "<<cog_x[i]<<" "<< imCog.xdim()<<endl;
				imCog.value(get<1>(*riter).x / imCog.xdim()+0.5,get<1>(*riter).y / imCog.ydim()+0.5,get<1>(*riter).z / imCog.zdim()+0.5 )=1;
			}


			//find the middle 
			riter = validCogs.rbegin();
			if ( ( (cog_x[0] > cog_x[1]) && (cog_x[0] < cog_x[2]) ) || 
					( (cog_x[0] < cog_x[1]) && (cog_x[0] > cog_x[2]) ))  
			{
				
				tracheaCOG=get<1>(*riter);
			}else if  ( ( (cog_x[1] > cog_x[0]) && (cog_x[1] < cog_x[2]) ) || 
					( (cog_x[1] < cog_x[0]) && (cog_x[1] > cog_x[2]) ))  
			{
				
				tracheaCOG=get<1>(*(++riter));
			}else{
				++riter;
				tracheaCOG=get<1>(*(++riter));

			}


			break;
		}
    }

	ofstream fout(filename_out + "_tracheaSeed.txt");
	if (fout.is_open())
	{
		cout<<"seed "<<tracheaCOG.x<<" "<<tracheaCOG.y<<" "<<tracheaCOG.z<<" "<<(tracheaCOG.x / imCog.xdim())<< \
				" "<<(tracheaCOG.y / imCog.ydim())<<" "<<(tracheaCOG.z / imCog.zdim())<<endl;
		fout<<tracheaCOG.x<<" "<<tracheaCOG.y<<" "<<tracheaCOG.z<<" "<<(tracheaCOG.x / imCog.xdim())<< \
				" "<<(tracheaCOG.y / imCog.ydim())<<" "<<(tracheaCOG.z / imCog.zdim())<<endl;
	fout.close(); 
	}
	imCog.writeImage(filename_out + "_lungs_COG.nii.gz");
	
	aursurface<float> surfTrachea;

	 
	
	
	surfTrachea.createSphere(tracheaCOG,1.0f,10,10);
	cout<<"Deform "<<endl;
	surfTrachea.drawSurface(imTrachea,short(1));

	imTrachea.writeImage(filename_out + "_lungs_COG_sphere.nii.gz");

	vector<  aurcontour<float> > all_contours;
    
//    void deformToBifurcation(vector<  aurcontour<float> > & all_contours)
    {
        //for (short i = 1 ; i <= Niters ; ++i)
	short i = 1; 
	do {
		cout<<"iterations "<<i*iStep<<" / "<<iStep*Niters<<endl;
		surfTrachea.deform(imSpeedMap,iStep,0.0,0.0,0.01,0.2,0.01,8.0);
cout<<"done deform "<<endl;
		array<float,6> bounds = surfTrachea.bounds();

//moving down the trachea
		float z = bounds[2] + zres ; 
//		cout<<"cut surface at z = "<<z<<endl;
		all_contours =  surfTrachea.cutSurface(vertex<float>(0,0,z), vec3<float>(0,0,1));
		cout<<"Iterations/Z-cut/Number of Contours : "<<i*iStep<<" / "<<iStep*Niters<<"    "<<z<<"    "<<all_contours.size()<<endl;
		surfTrachea.drawSurface(imTrachea,short(i-1)); //i=1 is reserve for seed 

		//take end cut unitl surfcae passes and its stable, 
		//then update cut
		//sgiould be moving along vessel direction

//		cout<<"done draw"<<endl;
		
		++i;// for moniroting iterations
	}while (all_contours.size()<=1); //less than is there for strat cases
    
    
    	imTrachea.writeImage(filename_out + "_lungs_COG_deformFront.nii.gz");
surfTrachea.InitDeform();
surfTrachea.computeRadiusAtVertex();
//surfTrachea.copyRadiusToScalars();
//surfTrachea.splitSurface(vertex<float>(carina.x,carina.y,carina.z), vec3<float>(1,0,1)); //note the caraina is defined above by the fact it does split in 2 the contour
surfTrachea.setScalars(0);
surfTrachea.write(filename_out + "_trachea.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);


    
    }
    
    
//    deformToBifurcation(all_contours);
	
    //Clean up ventilation tube, used for cadavers
  	if (cleanCT)
    {
        aurimage<short> imCT(filename);
        //create a mask of the trachea to identify the air tube
        imTrachea = 0; 
        surfTrachea.drawSurface(imTrachea,short(1)); //i=1 is reserve for seed 
        vec3<float> cog = surfTrachea.centroid(); 
        imTrachea.fill( cog.x/imTrachea.xdim() , cog.y/imTrachea.ydim()  , cog.z/imTrachea.zdim()  ,2 );    
        imTrachea.lowerThreshold(2,imTrachea);
//                imTrachea.mask()
        imTrachea.writeImage(filename_out + "_trachea.nii.gz");
        //find th eairtube
        aurimage<short> imTube(filename_out + "_chest.nii.gz");
        imTube.mask(imTrachea);
        imTube.dilate3x3();
        imCT.fillWithinMask(  imTube, imCT.value(cog.x/imTrachea.xdim() , cog.y/imTrachea.ydim()  , cog.z/imTrachea.zdim())   );

        	imTube.writeImage(filename_out + "_tube.nii.gz");
        	imCT.writeImage(filename_out + "_CTnoTube.nii.gz");

    }
	
	
	cout<<"Generating Skeleton... "<<endl;
	//create Skeleton for trachea
	aurcontour<float> skeleton;
	
	array< array<unsigned int,2> ,2 > carinaTriangles;
	array<vertex<float>,2> closestContourPoints ;
	short branchValue=1;
//start searching from tracheal seed
	float z = tracheaCOG.z / imCog.zdim();//bounds[5];
	while (z > bounds[2])
	{
		//cut with z planee
		all_contours =  surfTrachea.cutSurface(vertex<float>(0,0,z), vec3<float>(0,0,1));
		cout<<"cutting at z = "<<z<<" "<<all_contours.size()<<endl;
		if (all_contours.size()>2)
		{
			cerr<<"More than 2 contours in slice"<<endl;
						break;
		}
		else if (all_contours.size() > 1 ) 
		{
			//let's find the nearest point between the contours 
			//but let's do a finer sweep 
			float zfine=z+zres;//move back up a slice 
			while ( zfine >= z )
			{ 
				cout<<"search for more precise bifurcation point "<<zfine<<endl;
				all_contours =  surfTrachea.cutSurface(vertex<float>(0,0,zfine), vec3<float>(0,0,1));
				if (all_contours.size()==2)
				{
					//all_contours outside this loop will be the intersection one
					closestContourPoints = all_contours[0].nearestVertex(all_contours[1],  carinaTriangles);
					carina = midpoint(closestContourPoints[0],closestContourPoints[1]);
//					skeleton.push_back(midpoint(closestContourPoints[0],closestContourPoints[1]));
                    cout<<"contour 0 "<<endl;
                    all_contours[0].printVerticesWithTriangles();
                    cout<<"contour 1 "<<endl;
                    all_contours[1].printVerticesWithTriangles();


					cout<<"found Carina "<<carina.x/imAir.xdim()<<" "<<carina.y/imAir.ydim()<<" "<<carina.z/imAir.zdim()<<endl;
					break;
				}
				zfine-=zresBifurcation;

			}


			break;
		}
		
		z-=zres;
		
		if (all_contours.empty())//this should only occur at the very top of the surface 
		{
		
			continue;
		}
		
		
		skeleton.push_back(all_contours[0].centreOfGravity());
		
//		for (auto i = all_contours.begin(); i != all_contours.end(); ++i,++fillValue)
//		{
//			i->drawClosedContour(imTrachea,fillValue);
//		}
//		z-=zres;
	}

		cout<<"draw"<<endl;
	cout<<"write out wavefront image"<<endl;
ofstream fcarina(filename_out + "_carina.txt");
if (fcarina.is_open())
{
	cout<<"found Carina "<<carina.x/imAir.xdim()<<" "<<carina.y/imAir.ydim()<<" "<<carina.z/imAir.zdim()<<endl;

	fcarina<<carina.x<<" "<<carina.y<<" "<<carina.z<<" "<<carina.x/imAir.xdim()<<" "<<carina.y/imAir.ydim()<<" "<<carina.z/imAir.zdim()<<endl;
	fcarina.close();
}


for ( int i = 0 ; i < 2 ; ++i )
	for ( int j = 0 ; j < 2 ; ++j )
{cout<<carinaTriangles[i][j]<<endl;

cout<<"closest "<<surfTrachea.getClosestTriangle(carinaTriangles[i][j])<<endl;
surfTrachea.colorTriangle(carinaTriangles[i][j],i+1);
//surfTrachea.colorTriangle(surfTrachea.getClosestTriangle(carinaTriangles[i][j]),i+1);

}
surfTrachea.write(filename_out + "_tracheaCutPoints.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);

surfTrachea.setScalars(0);
for ( int i = 0 ; i < 2 ; ++i )
	for ( int j = 0 ; j < 2 ; ++j )
{cout<<carinaTriangles[i][j]<<endl;

cout<<"closest "<<surfTrachea.getClosestTriangle(carinaTriangles[i][j])<<endl;
//surfTrachea.colorTriangle(carinaTriangles[i][j],i+1);
surfTrachea.colorTriangle(surfTrachea.getClosestTriangle(carinaTriangles[i][j]),i+1);

}
surfTrachea.write(filename_out + "_tracheaCutPointsClosest.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);



//could average over 2 estimates
//surfTrachea.splitSurface(vertex<float>(carina.x,carina.y,carina.z), crossProduct(surfTrachea.getTriangleNormal(carinaTriangles[0][0]),surfTrachea.getTriangleNormal(surfTrachea.getClosestTriangle(carinaTriangles[0][0])) ) );
//		vec3<float>(1,0,1)); 


//
//surfTrachea.colorTriangle(carinaTriangles[0][1],1);
//surfTrachea.colorTriangle(carinaTriangles[1][0],2);
//surfTrachea.colorTriangle(carinaTriangles[1][1],2);
//
//
//aursurface<float> surfLeftBronchus =surfTrachea;
//surfLeftBronchus.setScalars(0);

//Continue to grow surface until cut is stable
//for now let's leave carina and closestContourPoints fixed


//initial curPoint should now always remain after the cutBronchus deformation 
//The intersected triangle are anchored prior to deforming
vec3<float> tracheaNormal( 0.0,0.0, -1.0 );
array< vertex<float>,2> cutPlanePoints;
array< vec3<float>,2> cutPlaneNormals = cutBronchus( imSpeedMap, surfTrachea, closestContourPoints,carinaTriangles,cutPlanePoints , tracheaNormal);


//for (int i = 0 ; i < 10 ; ++i)

surfTrachea.write(filename_out + "_tracheaLeftBronchusDeform.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);


//surfTrachea = surfLeftBronchus.splitSurface(carinaTriangles[0][0], vertex<float>(carina.x,carina.y,carina.z), crossProduct(vInPlane0,vInPlane1) );
cout<<"cut cog "<< cutPlanePoints[0].x<<" "<< cutPlanePoints[0].y<<" "<< cutPlanePoints[0].z<<endl;
cout<<"cut cogVox "<< cutPlanePoints[0].x / imSpeedMap.xdim()<<" "<< cutPlanePoints[0].y/imSpeedMap.ydim()<<" "<< cutPlanePoints[0].z/imSpeedMap.zdim()<<endl;
cout<<"closestContourPoints "<<closestContourPoints[0].x / imSpeedMap.xdim()<<" "<< closestContourPoints[0].y/imSpeedMap.ydim()<<" "<< closestContourPoints[0].z/imSpeedMap.zdim()<<endl;

//need to sue cut points instead of cog for proper cutting, want plane estimation to pas through closestContourPoints
//closest point ensures a proper cut without connecting left/right...maybe
//aursurface<float> surfLeftBronchus = surfTrachea.splitSurface(carinaTriangles[0][0], closestContourPoints[0], cutPlaneNormals[0]);


//aursurface<float> surfLeftBronchus = surfTrachea.splitSurface(carinaTriangles[0][0], cutPlanePoints[0], cutPlaneNormals[0],tracheaNormal);
//LETS TEST THE SPLITTING SURFACE 
//float angleIncrement = M_PI/40;
//int branch = 0 ; //left 
//vec3<float> leftCutAxis =surfTrachea.getTriangleCentroid( surfTrachea.getClosestTriangle(carinaTriangles[branch][0] ))- surfTrachea.getTriangleCentroid(carinaTriangles[branch][0]) ;
//
//for ( int i = 0; i <= 40 ; ++i )
//{
//
////    vec3<float> normal = cutPlaneNormals[0]
////cout<<"cut axis "<<leftCutAxis.x<<" "<<leftCutAxis.y<<" "<<leftCutAxis.z<<endl;
//
//vec3<float> planeNormal = rotatePlaneAboutAxis(  leftCutAxis, i*angleIncrement); 
////cout<<"Plane normal "<<planeNormal.x<<" "<<planeNormal.y<<" "<<planeNormal.z<<endl; 
//    surfLeftBronchus = surfTrachea.splitSurface(carinaTriangles[0][0], cutPlanePoints[0], planeNormal );
//    cout<<"cutplanepoint "<<cutPlanePoints[0].x<<" "<<cutPlanePoints[0].y<<" "<<cutPlanePoints[0].z<<endl;
//    float area = getContour( surfTrachea.cutSurface(cutPlanePoints[0],planeNormal),cutPlanePoints[0],planeNormal).area(planeNormal);
//     
//    vector< aurcontour<float>  > all_contours = surfTrachea.cutSurface(cutPlanePoints[0],planeNormal);
//    for (auto& ic : all_contours)
//    {  
//        cout<<"--------------------size "<<i<<" "<<ic.size()<<endl;
////        ic.printVertices();
//    }
//      
//    cout<<"area "<<i<<" "<<surfTrachea.cutSurface(cutPlanePoints[0],planeNormal).size()<<" "<<getContour( surfTrachea.cutSurface(cutPlanePoints[0],planeNormal),cutPlanePoints[0],planeNormal).size()<<" "<<area<<endl;
////        getContour( surfTrachea.cutSurface(cutPlanePoints[0],planeNormal),cutPlanePoints[0],planeNormal).printVertices();
//    
////    surfLeftBronchus = surfTrachea.splitSurface(carinaTriangles[0][0],     cutPlanePoints[0], planeNormal ,tracheaNormal);
//   surfLeftBronchus.write(filename_out + "_tracheaCutLeft_"+to_string(i)+".vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
//
//}//    surfTrachea.write(filename_out + "_trachea.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);


 
surfTrachea.colorSideOfPlane(cutPlanePoints[0], cutPlaneNormals[0]);
surfTrachea.write(filename_out + "_tracheaCutLeft.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
surfTrachea.colorSideOfPlane(cutPlanePoints[1], cutPlaneNormals[1]);
surfTrachea.write(filename_out + "_tracheaCutRight.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);

aursurface<float> surfLeftBronchus = surfTrachea.splitSurface(carinaTriangles[0][0], cutPlanePoints[0], cutPlaneNormals[0]);

//aursurface<float> surfRightBronchus = surfTrachea.splitSurface(carinaTriangles[1][0], closestContourPoints[1], cutPlaneNormals[1]);
aursurface<float> surfRightBronchus = surfTrachea.splitSurface(carinaTriangles[1][0], cutPlanePoints[1], cutPlaneNormals[1]);
//aursurface<float> surfRightBronchus = surfTrachea.splitSurface(carinaTriangles[1][0], cutPlanePoints[1], cutPlaneNormals[1],tracheaNormal);
//
surfTrachea.write(filename_out + "_tracheaNoBronchus.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);

surfLeftBronchus.copyAnchoredVerticesToScalars();
surfRightBronchus.copyAnchoredVerticesToScalars();

surfLeftBronchus.write(filename_out + "_leftBronchus.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
surfRightBronchus.write(filename_out + "_rightBronchus.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);

//surfLeftBronchus.copyAnchoredVerticesToScalars();
//surfLeftBronchus.write(filename_out + "_leftBronchusAnchored.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
	 
    cout<<"left ronchus "<<endl;

 deformToBifurcation(surfLeftBronchus, imSpeedMap,cutPlanePoints[0],  cutPlaneNormals[0]);

     cout<<"right ronchus "<<endl;

 deformToBifurcation(surfRightBronchus, imSpeedMap,cutPlanePoints[1],  cutPlaneNormals[1]);

    
surfLeftBronchus.write(filename_out + "_leftBronchusPostDeform.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
surfRightBronchus.write(filename_out + "_rightBronchusPostDeform.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);

{
    
    cout<<"findbirufrcation for left bronchus" <<endl;
    array< vertex<float>, 2>  cutPlanePoints_LBronchus = scanForBifurcation(surfLeftBronchus,imSpeedMap,cutPlanePoints[0], cutPlaneNormals[0]);
    cout<<"do split "<<endl;
    aursurface<float> surf_LeftLowerLobeBronchus = surfLeftBronchus.splitSurface(carinaTriangles[0][0], cutPlanePoints_LBronchus[0], cutPlaneNormals[0]);
    surf_LeftLowerLobeBronchus.write(filename_out + "_leftLowerLobeBronchus.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
//    cout<<"findbirufrcation forrighft bronchus" <<endl;
//    scanForBifurcation(surfRightBronchus,imSpeedMap,cutPlanePoints[1], cutPlaneNormals[1]);
//    array< vertex<float>,2> cutPlanePoints;
//    array< vec3<float>,2> cutPlaneNormals = cutBronchus( imSpeedMap, surfLeftBronchus, closestContourPoints,carinaTriangles,cutPlanePoints , tracheaNormal);
} 

    
    
    
    
    
    skeleton.drawOpenContour(imSkeleton,branchValue);

	imSkeleton.writeImage(filename_out + "_skeleton.nii.gz");



//surfLeftBronchus.splitSurface(vertex<float>(carina.x,carina.y,carina.z), crossProduct(surfLeftBronchus.getTriangleNormal(carinaTriangles[0][0]),surfLeftBronchus.getTriangleNormal(surfLeftBronchus.getClosestTriangle(carinaTriangles[0][0])) ) );
//surfLeftBronchus.InitDeform();
//surfLeftBronchus.computeRadiusAtVertex();
//
//surfTrachea.copyRadiusToScalars();
//surfTrachea.write(filename_out + "_tracheaRadius.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
//			void deform(  aurimage<T2> & image, const unsigned int & Niterations, \
					const T & alpha_sn, const T & alpha_st, const T & alpha_area, const T& alpha_im, \
					const T& max_step_size, const T& splitThreshold );
	
	//
//	//do this to ensure that trachea is disconnected lung
//	imAir.erode3x3();
//
//	imAir.writeImage(filename_out + "_air_erode3x3.nii.gz");

}

