#ifndef REGIONGROWINGALGOS_H
#define REGIONGROWINGALGOS_H

#include <AsrImage3D/AsrImage3D.h>
#include <unordered_set>
namespace auris_segtools
{

template <class T> class RegionGrowingAlgos
{
public:
    RegionGrowingAlgos();
    ~RegionGrowingAlgos();

    static void FillHoles(const AsrImage3D<T>& image, std::unordered_set<unsigned int>& seg_voxels);
    static void GrowRegionIncrementalThreshold(const AsrImage3D<T>& image,
        const AsrImage3D<float>& distanceMap,
        const vec3<int>& seed,
        const unsigned int& minSegmentSize,
        const unsigned int& maxSegmentSize,
        const float& initialThreshold,
        const int& connectivity,
        const float& maxGrowthFactor,
        const float& distanceThreshold,
//    const bool& decrementThreshold,
        const FillMethod& fill,
        AsrImage3D<float>& imAirways,
        std::unordered_set<unsigned int>& seg_voxels);
    //                AsrImage3D<float>& imAirways);
};
}

#endif // REGIONGROWINGALGOS_H
