#include <segmentation_utils.h>

#include <aurcommon/aurstructs_functions.hpp>

#include <map>

using namespace std;

namespace auris_segtools {

// enforces  a smoothness in the changing normal
// TODO
//        map<float ,vec3<float> > estimateCutPlane(aursurface<float> &
//        surfTrachea, const vec3<float> & cutAxis, vertex<float> &
//        cutPlanePoint, const vec3<float> & prevNormal )
//    {//estimate cut plane
//                map<float ,vec3<float> > areaToNormal;
//                float angleThresh = 45.0/180.0 *M_PI;
//                float angleIncrement = M_PI/40;
//                for ( int i = 0; i <= 40 ; ++i )
//                {
//                    cout<<"i ----"<<i<<endl;
//                    vec3<float> planeNormal = rotatePlaneAboutAxis(  cutAxis,
//                    i*angleIncrement);
//                    float angRads = angle(planeNormal,prevNormal) ;
//                    if (angRads < angleThresh)
//                    {
//                    float area = getContour(
//                    surfTrachea.cutSurface(cutPlanePoint,planeNormal),cutPlanePoint,planeNormal).area(planeNormal);
//                   cout<<"area "<<area<<" / "<<angRads<<" /
//                   "<<surfTrachea.cutSurface(cutPlanePoint,planeNormal).size()<<endl;
//                    if (area != 0 )//0 is the null conditions , couldn't find
//                    contour
//                        areaToNormal[ abs(getContour(
//                        surfTrachea.cutSurface(cutPlanePoint,planeNormal),cutPlanePoint,planeNormal).area(planeNormal)
//                        ) ] = planeNormal;
//
//                    }
//                }
//
//                return     areaToNormal;
//    }
bool successfulSegment(const std::array<vertex<float>, 2> &verts) {
  return (!(verts[0].isOrigin() || verts[1].isOrigin()));
}

array<vertex<float>, 2>
segmentAirwaySegment(aurimage<short> &imSpeedMap, aursurface<float> &surfAirway,
                     aursurface<float> &surfAirwayA,
                     aursurface<float> &surfAirwayB,
                     const vertex<float> &initPoint,
                     const deformationParameters<float> &params) {

  array<vertex<float>, 2> bifurcationCutPoints{vertex<float>(0, 0, 0),
                                               vertex<float>(0, 0, 0)};
  if (surfAirway.empty())
    return bifurcationCutPoints;
  cout << "segment airways " << normal(initPoint).to_string() << endl;
  // need to search for bifurcation before further deformation
  array<array<unsigned int, 2>, 2>
      bifurcationTriangles; // actually use the trinag les for cutting
  //            surfAirway.deform(imSpeedMap,100,params);
  vec3<float> normalAtBifurcation;
  int bifurcationScanResults = scanForBifurcationWithNormalTracking(
      surfAirway, imSpeedMap, initPoint, normal(initPoint),
      bifurcationTriangles, normalAtBifurcation);
  if (bifurcationScanResults > 0) {
    cout << "Deform to Bifurcation " << endl;
    // deform to Birfurcation incoroprates self-intersection test
    // return 0 if succesfull, 1 if failure
    if (deformToBifurcation(surfAirway, imSpeedMap, initPoint,
                            normal(initPoint), params) > 0)
      return bifurcationCutPoints;

    // deformToBifurcation does not update the normal estimation
    // This differ from scanForBifurcationWithNormalTracking, use deform to
    // bifurcation as initializatioon
    // Now let's deform until we find bifurcation using the scan
    //                int results{1};
    //                int iter = 0;
    //                while( results > 0 )
    //                {
    //                    cout<<"deforming until we reach a bifurcation
    //                    "<<iter<<endl;
    //                    surfAirway.deform(imSpeedMap,100,params);
    //                    if (surfAirway.selfIntersects())
    //                    {
    //                        cout<<"!!!FOUND  self intersectiion!!!!"<<endl;
    //                        return bifurcationCutPoints;
    //                    }
    //                    results = scanForBifurcationWithNormalTracking(
    //                    surfAirway, imSpeedMap,initPoint,  normal(initPoint),
    //                    bifurcationTriangles  );
    //                    ++iter;
    //                }

    //        cout<<"Find the cut point "<<endl;
    // this condition is no longer need because of while loop above
    //                if (results > 0 )
    //                    return bifurcationCutPoints;
  }

  // need to grab the proper bifurcation triangles
  // deform to bifurcation's stop criteria guarantees that it will exist
  scanForBifurcationWithNormalTracking(surfAirway, imSpeedMap, initPoint,
                                       normal(initPoint), bifurcationTriangles,
                                       normalAtBifurcation);

  cout << "found bifurcation Point " << endl;
  //                array< vec3<float>,2> cutPlaneNormals = cutAirway(
  //                imSpeedMap,
  //                surfAirway,bifurcationTriangles,bifurcationCutPoints,
  //                normal(initPoint),params );
  array<vec3<float>, 2> cutPlaneNormals =
      cutAirway(imSpeedMap, surfAirway, bifurcationTriangles,
                bifurcationCutPoints, normalAtBifurcation, params);

  cout << "Split the surfaces " << endl;
  surfAirway.splitSurface(surfAirwayA, bifurcationTriangles[0][0],
                          bifurcationCutPoints[0], cutPlaneNormals[0]);
  surfAirway.splitSurface(surfAirwayB, bifurcationTriangles[1][0],
                          bifurcationCutPoints[1], cutPlaneNormals[1]);
  setNormal(bifurcationCutPoints[0], cutPlaneNormals[0]);
  setNormal(bifurcationCutPoints[1], cutPlaneNormals[1]);

  return bifurcationCutPoints;
}

//    map<float ,vec3<float> >
vec3<float>
estimateCutPlane(aursurface<float> &surfTrachea, const vec3<float> &cutAxis,
                 const vertex<float> &cutPlanePoint) { // estimate cut plane
  map<float, pair<vec3<float>, vertex<float>>> areaToNormal;
  int Nsteps = 360;
  float angleIncrement = M_PI / Nsteps;
  for (int i = 0; i <= Nsteps; ++i) {
    //                    cout<<"i ----"<<i<<endl;
    vec3<float> planeNormal = rotatePlaneAboutAxis(cutAxis, i * angleIncrement);
    aurcontour<float> contour =
        getContour(surfTrachea.cutSurface(cutPlanePoint, planeNormal),
                   cutPlanePoint, planeNormal);
    float area = contour.area(planeNormal);
    //                  cout<<"area "<<area<<"
    //                  "<<surfTrachea.cutSurface(cutPlanePoint,planeNormal).size()<<"
    //                  "<<planeNormal.to_string()<<endl;
    //
    if (area != 0) // 0 is the null conditions , couldn't find contour
      areaToNormal[abs(
          getContour(surfTrachea.cutSurface(cutPlanePoint, planeNormal),
                     cutPlanePoint, planeNormal)
              .area(planeNormal))] =
          pair<vec3<float>, vertex<float>>(planeNormal,
                                           contour.centreOfGravity());
  }
  //                cutPlanePoint = areaToNormal.begin()->second.second;
  if (areaToNormal.empty()) // conditioon where no closed cut plane is found
    return vec3<float>(0.0, 0.0, 0.0);

  return areaToNormal.begin()->second.first;
}

//    map<float ,vec3<float> >
vec3<float>
estimateCutPlane(aursurface<float> &surfTrachea,
                 array<unsigned int, 2> &carinaTriangles,
                 const vertex<float> &cutPlanePoint) { // estimate cut plane
  map<float, vec3<float>> areaToNormal;

  //                float angleIncrement = M_PI/40;
  vec3<float> cutAxis =
      surfTrachea.getTriangleCentroid(
          surfTrachea.getClosestTriangle(carinaTriangles[0])) -
      surfTrachea.getTriangleCentroid(carinaTriangles[0]);
  return estimateCutPlane(surfTrachea, cutAxis, cutPlanePoint);
  //                for ( int i = 0; i <= 40 ; ++i )
  //                {
  //                    vec3<float> planeNormal = rotatePlaneAboutAxis(
  //                    cutAxis, i*angleIncrement);
  //                //cout<<"Plane normal "<<planeNormal.x<<"
  //                "<<planeNormal.y<<" "<<planeNormal.z<<endl;
  //    //                surfLeftBronchus =
  //    surfTrachea.splitSurface(carinaTriangles[0][0], cutPlanePoints[0],
  //    planeNormal );
  //    //                cout<<"cutplanepoint "<<cutPlanePoints[0].x<<"
  //    "<<cutPlanePoints[0].y<<" "<<cutPlanePoints[0].z<<endl;
  //                    float area = getContour(
  //                    surfTrachea.cutSurface(cutPlanePoint,planeNormal),cutPlanePoint,planeNormal).area(planeNormal);
  //                    areaToNormal[ abs(getContour(
  //                    surfTrachea.cutSurface(cutPlanePoint,planeNormal),cutPlanePoint,planeNormal).area(planeNormal)
  //                    ) ] = planeNormal;
  //    //                cout<<"area "<<i<<"
  //    "<<surfTrachea.cutSurface(cutPlanePoints[0],planeNormal).size()<<"
  //    "<<getContour(
  //    surfTrachea.cutSurface(cutPlanePoints[0],planeNormal),cutPlanePoints[0],planeNormal).size()<<"
  //    "<<area<<endl;
  //                //        getContour(
  //                surfTrachea.cutSurface(cutPlanePoints[0],planeNormal),cutPlanePoints[0],planeNormal).printVertices();
  //
  //                //    surfLeftBronchus =
  //                surfTrachea.splitSurface(carinaTriangles[0][0],
  //                cutPlanePoints[0], planeNormal ,tracheaNormal);
  //    //               surfLeftBronchus.write(filename_out +
  //    "_tracheaCutLeft_"+to_string(i)+".vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
  //
  //                }

  //                return     areaToNormal;
}

array<vec3<float>, 2>
cutAirway(aurimage<short> &imSpeedMap, aursurface<float> &surfTrachea,
          array<array<unsigned int, 2>, 2> &carinaTriangles,
          array<vertex<float>, 2> &cutPlanePoints,
          const vec3<float> &prevNormal,
          const deformationParameters<float> &params) {
  cout << "cutairway " << endl;
  // we want to deform entire surface for left and right to ensure contour
  // matching criterai
  // achor the cut trinagles so that the cut plane stays valid
  for (auto &array_tris : carinaTriangles) {
    cout << "looping over triangles  " << endl;
    for (auto &tris : array_tris) {
      cout << "tri " << tris << endl;
      surfTrachea.anchorTriangle(tris);
    }
  }
  //        cout<<"computer surface stats "<<endl;
  surfTrachea.computeMinimumNormalToTriangleIntersection();
  surfTrachea.computeRadiusAtVertex();
  //                cout<<"computer surface stats done "<<endl;

  array<vec3<float>, 2> cutPlaneNormals;
  // vInPlane0,vInPlane1,cutPlaneNormal;
  // TODO UPDATE THE PREVIOUS NORMAL
  /// normals is poiinting in the direction going deeper into the lungs
  //	vec3<float> prevNormal(0.0,0.0,-1.0);
  for (int branch = 0; branch <= 1; ++branch) {
    //            cout<<"BRANCH "<<branch<<"
    //            "<<carinaTriangles[branch][0]<<endl;
    cutPlanePoints[branch] =
        surfTrachea.getTriangleCentroid(carinaTriangles[branch][0]);
    //           .z<<")"<<endl;

    //            map<float ,vec3<float> > areaToNormal  =
    //            estimateCutPlane(surfTrachea,carinaTriangles[branch],
    //            cutPlanePoints[branch]);
    //            cout<<"minimum area "<<areaToNormal.begin()->first<<endl;
    //            cutPlaneNormals[branch] = areaToNormal.begin()->second;
    cutPlaneNormals[branch] = estimateCutPlane(
        surfTrachea, carinaTriangles[branch], cutPlanePoints[branch]);

    normalize(cutPlaneNormals[branch]);

    cout << "cutPlaneNromal " << cutPlaneNormals[branch].to_string() << " "
         << prevNormal.to_string() << endl;

    //        Need to make sure normal is in correct direction, may need
    //        something more sophisticated.
    reorientNormal(cutPlaneNormals[branch], prevNormal);
    cout << "cutPlaneNromal2  " << branch << " " << cutPlaneNormals[branch].x
         << " " << cutPlaneNormals[branch].y << " " << cutPlaneNormals[branch].z
         << endl;

    // initiial edge
    aurcontour<float> bronchusEdge_prev = getContour(
        surfTrachea.cutSurface(cutPlanePoints[branch], cutPlaneNormals[branch]),
        cutPlanePoints[branch], cutPlaneNormals[branch]);

    int iter = 0;
    bool matched{false};
    do { // deform surface until we get a stable estimate of the bronchus cut
      //        break;
      cout << "ideform " << iter << endl;
      surfTrachea.deform(imSpeedMap, iStep, params.alpha_sn, params.alpha_st,
                         params.alpha_area, params.alpha_im,
                         params.max_step_size, params.splitThreshold);
      // stop deforming if we've hit a self-intersection
      if (surfTrachea.selfIntersects())
        break;
      aurcontour<float> bronchusEdge =
          getContour(surfTrachea.cutSurface(cutPlanePoints[branch],
                                            cutPlaneNormals[branch]),
                     cutPlanePoints[branch], cutPlaneNormals[branch]);
      //            cout<<"Is cut empty ? "<<bronchusEdge.empty()<<endl;
      cout << "cutPoint ( " << cutPlanePoints[branch].x << " "
           << cutPlanePoints[branch].y << " " << cutPlanePoints[branch].z << ")"
           << endl;

      // by virtue of the method, there must be at least 1 contour
      float dist = bronchusEdge_prev.maxDistanceSquared(bronchusEdge);
      if (dist < contourThresh) {
        matched = true;
        break;
      }
      cout << "maxdist " << dist << " " << bronchusEdge.size() << endl;

      bronchusEdge_prev = bronchusEdge;
      ++iter;
    } while (!matched);

    /// a final restimation
    surfTrachea.computeMinimumNormalToTriangleIntersection();
    surfTrachea.computeRadiusAtVertex();
    //            areaToNormal  =
    //            estimateCutPlane(surfTrachea,carinaTriangles[branch],
    //            cutPlanePoints[branch]);
    //
    //            cout<<"minimum area
    //            "<<areaToNormal.begin()->first<<cutPlaneNormals[branch].to_string()<<"
    //            "<<endl;
    //            cutPlaneNormals[branch] = areaToNormal.begin()->second;
    cutPlaneNormals[branch] = estimateCutPlane(
        surfTrachea, carinaTriangles[branch], cutPlanePoints[branch]);
    normalize(cutPlaneNormals[branch]);

    reorientNormal(cutPlaneNormals[branch], prevNormal);

    cutPlanePoints[branch] =
        getContour(surfTrachea.cutSurface(cutPlanePoints[branch],
                                          cutPlaneNormals[branch]),
                   cutPlanePoints[branch], cutPlaneNormals[branch])
            .centreOfGravity();
    //            cutPlanePoints[branch] = surfTrachea.cutSurface()
    //                  if (dotProduct(prevNormal,cutPlaneNormals[branch]) < 0 )
    //             {
    //                 cout<<"FLIP NORMAL "<<cutPlaneNormals.size()<<"
    //                 "<<branch<<endl;
    //                 cutPlaneNormals[branch]  *= -1;
    //             }

    //              aursurface<float> surfLeftBronchus;
    //              surfTrachea.splitSurface(surfLeftBronchus,
    //              carinaTriangles[branch][0],     cutPlanePoints[branch],
    //              cutPlaneNormals[branch]);
    //            surfLeftBronchus.write("grot_tracheaCut_"+to_string(branch)+".vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
  }

  return cutPlaneNormals;
}

int deformToBifurcation(aursurface<float> &airwaySurface,
                        aurimage<short> &imSpeedMap,
                        const vertex<float> &cutPoint,
                        const deformationParameters<float> &params) {
  return deformToBifurcation(airwaySurface, imSpeedMap, cutPoint,
                             vec3<float>(cutPoint.nx, cutPoint.ny, cutPoint.nz),
                             params);
}
int deformToBifurcation(aursurface<float> &airwaySurface,
                        aurimage<short> &imSpeedMap,
                        const vertex<float> &cutPoint,
                        const vec3<float> &normal,
                        const deformationParameters<float> &params) {
  if (airwaySurface.empty()) {
    cout << "WARNING : surface has no vertices " << endl;
    return 1;
  }
  short i = 1;
  vec3<float> normalAtBifurcation;
  array<array<unsigned int, 2>, 2>
      bifurcationTriangles; // actually use the trinag les for cutting
  int results = scanForBifurcationWithNormalTracking(
      airwaySurface, imSpeedMap, cutPoint, normal, bifurcationTriangles,
      normalAtBifurcation);

  while (results > 0) {
    cout << "Deform to Bifurcation iterations " << i * iStep << " / "
         << iStep * Niters << endl;
    airwaySurface.deform(imSpeedMap, iStep, params.alpha_sn, params.alpha_st,
                         params.alpha_area, params.alpha_im,
                         params.max_step_size, params.splitThreshold);
    // TODO Re-estimate surface normal ?
    // moving down the trachea
    if (i % selfIntersectionInterval == 0) {
      if (airwaySurface.selfIntersects()) {
        cout << "!!!FOUND  self intersectiion!!!!" << endl;
        return 1;
        // bifurcationCutPoints;
      }
    }

    results = scanForBifurcationWithNormalTracking(
        airwaySurface, imSpeedMap, cutPoint, normal, bifurcationTriangles,
        normalAtBifurcation);
    cout << "Iterations/Z-cut/Number of Contours : " << i * iStep << " / "
         << iStep * Niters << "    "
         << "    " << endl;
    ++i; // for moniroting iterations
  }
  return 0;
}
// this is old verison that precedes the front by X=5 mm
// void deformToBifurcation( aursurface<float> & airwaySurface, aurimage<short>
// & imSpeedMap, const vertex<float> & cutPoint, const vec3<float> & normal,
// const deformationParameters<float> & params )
//    {
//        if (airwaySurface.empty())
//        {
//            cout<<"WARNING : surface has no vertices "<<endl;
//            return;
//        }
//        short i = 1;
//        vertex<float> slicePoint = cutPoint+ normal*zres ;
//        vector< aurcontour<float> > all_contours;
//        do {
//            cout<<"Deform to Bifurcation iterations "<<i*iStep<<" /
//            "<<iStep*Niters<<endl;
//            airwaySurface.deform(imSpeedMap,iStep,params.alpha_sn,
//            params.alpha_st,params.alpha_area,params.alpha_im,params.max_step_size,params.splitThreshold);
//            array<float,2> bounds = airwaySurface.zbounds(cutPoint,normal);
//            //deform until the surface spans greater than the resolution at
//            whihc we're checking for bifurcations
//            if ( (bounds[1]-bounds[0]) < zres )
//                continue;
//            //TODO Re-estimate surface normal ?
//    //moving down the trachea
//            slicePoint = cutPoint + normal*(bounds[1] - zres);
//            all_contours =  airwaySurface.cutSurface(slicePoint, normal);
//
//            if (all_contours.empty()) break;
//
//            cout<<"Iterations/Z-cut/Number of Contours : "<<i*iStep<<" /
//            "<<iStep*Niters<<"    "<<"    "<<all_contours.size()<<endl;
//            ++i;// for moniroting iterations
//        }while (all_contours.size()<2); //less than is there for strat cases
//
//    }

array<vertex<float>, 2>
scanForBifurcation(aursurface<float> &airwaySurface,
                   aurimage<short> &imSpeedMap, const vertex<float> &cutPoint,
                   array<array<unsigned int, 2>, 2> &bifurcationTriangles) {
  return scanForBifurcation(airwaySurface, imSpeedMap, cutPoint,
                            vec3<float>(cutPoint.nx, cutPoint.ny, cutPoint.nz),
                            bifurcationTriangles);
}

//     array< vertex<float>, 2>
int scanForBifurcationWithNormalTracking(
    aursurface<float> &airwaySurface, aurimage<short> &imSpeedMap,
    const vertex<float> &cutPoint, const vec3<float> &normal,
    array<array<unsigned int, 2>, 2> &bifurcationTriangles,
    vec3<float> &finalNormal) {
  airwaySurface.InitDeform();
  airwaySurface.computeMinimumNormalToTriangleIntersection();
  //        airwaySurface.computeRadiusAtVertex();
  aurimage<short> im_slices = imSpeedMap;
  im_slices = 0;
  aurimage<short> im_cog = im_slices;
  aurimage<short> im_cutaxis = im_slices;

  array<vertex<float>, 2> cutPoints{vertex<float>(0, 0, 0),
                                    vertex<float>(0, 0, 0)};
  cout << "scan for bifurcation " << endl;
  // bounds will change with changing normal
  array<float, 2> bounds =
      airwaySurface.zbounds(cutPoint, normal); // get bounds to scans
  cout << ":bounds " << bounds[0] << " " << bounds[1] << " "
       << airwaySurface.numberOfVertices() << endl;

  // the cutSurcace can pose problem if it cut through distant airway
  vertex<float> slicePoint = cutPoint + normal * zres;
  list<aurcontour<float>> all_contours =
      airwaySurface.cutSurface(slicePoint, normal);
  { // draw initial contour for debugging
    for (auto &c : all_contours)
      c.drawClosedContour(im_slices, short(1));
    im_slices.value(slicePoint.x / im_slices.xdim(),
                    slicePoint.y / im_slices.ydim(),
                    slicePoint.z / im_slices.zdim()) = 100;
  }
  //            im_slices.writeImage("initContour.nii.gz");
  int stepNumber = 1;
  while (getContour(all_contours, slicePoint, normal).empty()) {
    cout << "empty let's move forward alogn normal " << all_contours.size()
         << " " << slicePoint.to_string() << endl;
    slicePoint = slicePoint + normal * zresBifurcation;
    all_contours = airwaySurface.cutSurface(slicePoint, normal);
    // TODO stop when no intersectionn at all, not just closed contour
    if (stepNumber > 5) // if it hasn't found it yet something is wrong
      return 1;
    ++stepNumber;
  }
  slicePoint = getContour(all_contours, slicePoint, normal).centreOfGravity();
  vertex<float> COG_cur{slicePoint};
  //        ,COG_prev{slicePoint};
  float z = zres;

  aurcontour<float> validContour;
  vec3<float> local_normal{normal}, prevNormal{normal};
  int iter = 1;
  while (zresBifurcation <
         bounds[1]) // i starts at 1 to skip the open slice where the cut was
  {
    //        vertex<float> slicePoint = cutPoint + normal*( i*zresBifurcation
    //        );
    // TODO average around contour
    vertex<float> cutAxisPoint0 =
        airwaySurface.getTriangleCentroid(all_contours.front().triangle(0)[0]);
    vertex<float> cutAxisPoint1 = airwaySurface.getTriangleCentroid(
        airwaySurface.getClosestTriangle(all_contours.front().triangle(0)[0]));

    vec3<float> cutAxis = cutAxisPoint1 - cutAxisPoint0;
    normalize(cutAxis);
    //            vertex<float> pivotPoint =
    //            midpoint(cutAxisPoint0,cutAxisPoint1);

    //            cout<<"cutAxis "<<cutAxis.to_string()<<endl;
    // current implementation require slciepoint to be on the surface
    //            local_normal = estimateCutPlane(airwaySurface, cutAxis,
    //            slicePoint).begin()->second;
    //            local_normal = estimateCutPlane(airwaySurface, cutAxis,
    //            pivotPoint).begin()->second;
    //            local_normal = estimateCutPlane(airwaySurface, cutAxis,
    //            pivotPoint,local_normal).begin()->second;
    // TODO use previous nromal to restrict change in nromal direction
    //            local_normal = estimateCutPlane(airwaySurface, cutAxis,
    //            pivotPoint);
    prevNormal = local_normal;
    local_normal = estimateCutPlane(airwaySurface, cutAxis, slicePoint);

    // TODO figure out why 0 closed contours and optiimize search whihtin angle
    // RANGE
    if ((angle(prevNormal, local_normal) > ANGLE_RANGE) ||
        (l2norm(local_normal) ==
         0)) { // allows for poor normal estimation by ignoring at astep
      local_normal = prevNormal;
    }
    //            local_normal = estimateCutPlane(airwaySurface, cutAxis,
    //            pivotPoint,local_normal).begin()->second;
    //            local_normal = estimateCutPlane(airwaySurface, cutAxis,
    //            cutAxisPoint0).begin()->second;
    // TODO change to use of prevcious normal, enforces smoothness of normal
    reorientNormal(local_normal, prevNormal);
    //            cout<<"Change in Normals "<<prevNormal.to_string()<<"
    //            "<<local_normal.to_string()<<"
    //            "<<angle(prevNormal,local_normal)*180/M_PI<<endl;
    //                    cout<<"step "<<iter<<" "<<local_normal.to_string()<<"
    //                    "<<slicePoint.coord_to_string()<<" "<<endl;
    // chnages the centre as we move own the airway
    bounds =
        airwaySurface.zbounds(slicePoint, local_normal); // get bounds to scans
    //    local_normal = vec3<float>(0,0,-1);
    //            slicePoint = slicePoint + local_normal * zresBifurcation;
    //            z+=zresBifurcation;
    // ensure that it is within the surface
    //                        cout<<"bounds "<<zresBifurcation<<"
    //                        "<<bounds[0]<<" "<<bounds[1]<<endl;
    //            if (zresBifurcation > bounds[1])
    //            {
    //                break;
    //            }
    //            cout<<"cut "<<slicePoint.to_string()<<"
    //            "<<local_normal.to_string()<<endl;
    all_contours = airwaySurface.cutSurface(slicePoint, local_normal);
    if (all_contours.empty()) {
      break;
    }
    if (all_contours.size() > 1) {
      cout << "Compare all contours against previous valid contour "
           << all_contours.size() << " " << prevNormal.to_string() << endl;

      // project points onto plane defined by previous contours
      //...or just project the COG for speed
      //                    for ( auto& contour : all_contours)
      for (auto i_contour = all_contours.begin();
           i_contour != all_contours.end(); ++i_contour) {
        cout << "Previous COG " << COG_cur.coord_to_string() << endl;
        cout << "contourCOG " << i_contour->centreOfGravity().coord_to_string()
             << endl;
        // COG_cur would be old COG
        // normal is unit vector because generate with RotatePlane function
        vertex<float> cog = i_contour->centreOfGravity();
        cout << "cog " << cog.coord_to_string() << endl;
        float distFromPlane = dotProduct(cog - COG_cur, prevNormal);
        cog = cog - prevNormal * distFromPlane;
        cout << "cogProjected " << cog.coord_to_string() << " "
             << dotProduct(cog - COG_cur, prevNormal) << endl;
        cout << "is inside previous contour "
             << validContour.insideConvexPolygon(cog) << endl;
        // remove invalid contours
        if (!validContour.insideConvexPolygon(cog)) {
          i_contour = all_contours.erase(i_contour);
          --i_contour;
        }
      }
    }

    validContour = getContour(all_contours, slicePoint, local_normal);
    if (validContour.empty()) {
      cout << "no valid contour " << endl;
      //                validContour = all_contours.front();
      break;
    }
    //           cout<<"allContour size "<<all_contours.size()<<"
    //           "<<validContour.size()<<endl;
    //            COG_cur = all_contours[0].centreOfGravity();
    COG_cur = validContour.centreOfGravity();
    validContour.drawClosedContour<short>(im_slices, iter);
    im_slices.value(slicePoint.x / im_slices.xdim(),
                    slicePoint.y / im_slices.ydim(),
                    slicePoint.z / im_slices.zdim()) = 100 + iter;
    im_cog.value(COG_cur.x / im_slices.xdim(), COG_cur.y / im_slices.ydim(),
                 COG_cur.z / im_slices.zdim()) = iter;
    //            COG_cur=
    //            getContour(all_contours,slicePoint,local_normal).centreOfGravity();
    //            getContour(all_contours,slicePoint,local_normal).drawClosedContour<short>(im_slices,iter);

    //            slicePoint = COG_cur + local_normal * zresBifurcation;
    // TODO Check to see if the slice point cross surface
    slicePoint = slicePoint + local_normal * zresBifurcation;
    //            cout<<"updateSlicePoint "<<slicePoint.to_string()<<"
    //            "<<local_normal.to_string()<<endl;

    //            for ( auto & c : all_contours)
    //                c.drawClosedContour(im_slices,short(iter));

    //            im_slices.writeImage("imBronchusCut.nii.gz");

    // TODO add condition for more than 2 contours
    if (all_contours.size() == 2) {
      cout << "found cut point at " << slicePoint.to_string() << endl;
      break;
    }
    //            cout<<"step "<<iter<<" "<<local_normal.to_string()<<"
    //            "<<slicePoint.to_string()<<endl;
    //            COG_cur.coord_to_string()<<" "<<endl;
    //            cout<<"is empty "<< validContour.empty()<<endl;
    drawLine(im_cutaxis, vec3<float>(cutAxisPoint0), vec3<float>(cutAxisPoint1),
             0.05f, short(iter));

    iter++;
  }

  //        im_slices.writeImage("imBronchusCut.nii.gz");
  //        im_cog.writeImage("imBronchusCut_cog.nii.gz");
  //        im_cutaxis.writeImage("imBronchusCut_cutAxis.nii.gz");
  if ((zresBifurcation > bounds[1]) || all_contours.empty() ||
      validContour.empty()) // found not cut point
  {
    cout << "did not find ct point " << endl;
    return 1; // these are (0,0,0)
  }

  //    cout<<"get point and triangles"<<endl;
  //        	cout<<"search for more precise bifurcation point "<<zfine<<endl;
  //				all_contours =
  // surfTrachea.cutSurface(vertex<float>(0,0,zfine), vec3<float>(0,0,1));
  //				if (all_contours.size()==2)
  {
    // all_contours outside this loop will be the intersection one
    //                        array< array<unsigned int,2> ,2 >
    //                        bifurcationTriangles;
    //                    array<vertex<float>,2> closestContourPoints ;
    //					closestContourPoints
    // should ionly be two contours
    cutPoints = all_contours.front().nearestVertex(all_contours.back(),
                                                   bifurcationTriangles);
    //                            cout<<"bifTriangles
    //                            "<<bifurcationTriangles[0][0]<<"
    //                            "<<bifurcationTriangles[0][1]<<"
    //                            "<<bifurcationTriangles[1][0]<<"
    //                            "<<bifurcationTriangles[1][0]<<"
    //                            "<<bifurcationTriangles[1][1]<<endl;
    vertex<float> bifurcationPoint = midpoint(cutPoints[0], cutPoints[1]);
    //					skeleton.push_back(midpoint(closestContourPoints[0],closestContourPoints[1]));
    //                    cout<<"contour 0 "<<endl;
    //                    all_contours[0].printVerticesWithTriangles();
    //                    cout<<"contour 1 "<<endl;
    //                    all_contours[1].printVerticesWithTriangles();

    finalNormal = local_normal;
    cout << "found bifurcation Point " << bifurcationPoint.x / imSpeedMap.xdim()
         << " " << bifurcationPoint.y / imSpeedMap.ydim() << " "
         << bifurcationPoint.z / imSpeedMap.zdim() << endl;
    //					break;
  }
  //				zfine-=zresBifurcation;

  return 0;
}

array<vertex<float>, 2>
scanForBifurcation(aursurface<float> &airwaySurface,
                   aurimage<short> &imSpeedMap, const vertex<float> &cutPoint,
                   const vec3<float> &normal,
                   array<array<unsigned int, 2>, 2> &bifurcationTriangles) {
  aurimage<short> im_slices = imSpeedMap;
  im_slices = 0;
  array<vertex<float>, 2> cutPoints;
  cout << "scan for bifurcation " << endl;
  array<float, 2> bounds =
      airwaySurface.zbounds(cutPoint, normal); // get bounds to scans
  cout << ":bounds " << bounds[0] << " " << bounds[1] << endl;
  int Nsteps =
      static_cast<int>((bounds[1] - bounds[0] + zres) / zresBifurcation);
  vertex<float> slicePoint = cutPoint + normal * zres;
  float z = zres;
  list<aurcontour<float>> all_contours;
  cout << "normal " << normal.to_string() << endl;
  for (int i = 1; i <= Nsteps;
       ++i) // i starts at 1 to skip the open slice where the cut was
  {
    //        vertex<float> slicePoint = cutPoint + normal*( i*zresBifurcation
    //        );
    cout << "step " << i << " " << z << " " << bounds[1] << endl;

    all_contours = airwaySurface.cutSurface(slicePoint, normal);
    for (auto &c : all_contours)
      c.drawClosedContour(im_slices, short(i));

    cout << "numebr of contorus " << all_contours.size() << endl;
    if (all_contours.size() > 1) {
      cout << "found cut point at " << slicePoint.to_string() << endl;
      break;
    }
    z += zresBifurcation;
    // ensure that it is within the surface
    if (z > bounds[1]) {
      break;
    }
    slicePoint = slicePoint + normal * zresBifurcation;
  }
  cout << "get point and triangles" << endl;
  //        	cout<<"search for more precise bifurcation point "<<zfine<<endl;
  //				all_contours =
  // surfTrachea.cutSurface(vertex<float>(0,0,zfine), vec3<float>(0,0,1));
  //				if (all_contours.size()==2)
  {
    // all_contours outside this loop will be the intersection one
    //                        array< array<unsigned int,2> ,2 >
    //                        bifurcationTriangles;
    //                    array<vertex<float>,2> closestContourPoints ;
    //					closestContourPoints
    cutPoints = all_contours.front().nearestVertex(all_contours.back(),
                                                   bifurcationTriangles);
    cout << "bifTriangles " << bifurcationTriangles[0][0] << " "
         << bifurcationTriangles[0][1] << " " << bifurcationTriangles[1][0]
         << " " << bifurcationTriangles[1][0] << " "
         << bifurcationTriangles[1][1] << endl;
    vertex<float> bifurcationPoint = midpoint(cutPoints[0], cutPoints[1]);
    //					skeleton.push_back(midpoint(closestContourPoints[0],closestContourPoints[1]));
    //                    cout<<"contour 0 "<<endl;
    //                    all_contours[0].printVerticesWithTriangles();
    //                    cout<<"contour 1 "<<endl;
    //                    all_contours[1].printVerticesWithTriangles();

    cout << "found bifurcation Point " << bifurcationPoint.x / imSpeedMap.xdim()
         << " " << bifurcationPoint.y / imSpeedMap.ydim() << " "
         << bifurcationPoint.z / imSpeedMap.zdim() << endl;
    //					break;
  }
  //				zfine-=zresBifurcation;
  im_slices.writeImage("imBronchusCut.nii.gz");
  return cutPoints;
}

int readCutPlanes(const std::string &filename, const string &branchName,
                  vertex<float> &planeVertex) {
  ifstream fplanes(filename.c_str());
  if (!fplanes.is_open()) {
    return 1;
  }
  string line;
  while (getline(fplanes, line)) {
    stringstream ss(line);
    string name;
    ss >> name;
    if (name == branchName) {
      ss >> planeVertex.x >> planeVertex.y >> planeVertex.z >> planeVertex.nx >>
          planeVertex.ny >> planeVertex.nz;
      planeVertex.scalar = 0;

      fplanes.close();
      return 0;
    }
  }

  fplanes.close();
  return 1;
}

int readCutPlanes(const std::string &filename, const string &branchName,
                  vertex<float> &planeVertex, vec3<float> &planeNormal) {
  if (readCutPlanes(filename, branchName, planeVertex)) { // couldn't read file
    return 1;
  } else {
    planeNormal = vec3<float>(planeNormal.x, planeNormal.y, planeNormal.z);
  }
  return 0;
}

// utilities to write cut points to a file, overwrite ir append
int writeCutPointToFile(const string &filename, const string &branchName,
                        const vertex<float> &planeVertex) {
  return writeCutPointToFile(
      filename, branchName, planeVertex,
      vec3<float>(planeVertex.nx, planeVertex.ny, planeVertex.nz),
      std::ofstream::out);
}

int appendCutPointToFile(const string &filename, const string &branchName,
                         const vertex<float> &planeVertex) {
  return writeCutPointToFile(
      filename, branchName, planeVertex,
      vec3<float>(planeVertex.nx, planeVertex.ny, planeVertex.nz),
      (std::ofstream::out | std::ofstream::app));
}
int appendCutPointToFile(const string &filename, const string &branchName,
                         const vertex<float> &planeVertex,
                         const vec3<float> &planeNormal) {
  return writeCutPointToFile(filename, branchName, planeVertex, planeNormal,
                             (std::ofstream::out | std::ofstream::app));
}
int writeCutPointToFile(const string &filename, const string &branchName,
                        const vertex<float> &planePoint,
                        const vec3<float> &planeNormal,
                        ios_base::openmode mode) {
  ofstream fout(filename, mode);
  if (!fout.is_open())
    return 1;

  fout << branchName << " " << planePoint.x << " " << planePoint.y << " "
       << planePoint.z << " " << planeNormal.x << " " << planeNormal.y << " "
       << planeNormal.z << endl;

  return 0;
}
}