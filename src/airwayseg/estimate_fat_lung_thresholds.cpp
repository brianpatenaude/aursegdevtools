/*
 * run_test.cpp
 *
 *  Created on: May 11, 2016
 *      Author: brian
 */
#include "AsrImage3D/AsrImage3D.h"
#include "aurcommon/miscfunctions.h"
//STL includes
#include <iostream>
#include <fstream>
#include <vector>
//3rd party includes
#include <boost/filesystem.hpp>

using namespace std;
using namespace auris_segtools;
using namespace boost::filesystem;
void Usage(){
	cout<<"\n estimate_fat_lung_thresholds [--cadaver] [ --thresh <value_low> <value_high> ]  <nifti_file> <output_base>"<<endl;
	cout<<"\n This program is used  to estimate intensity thresholds for the RSIP software. \n"<<endl;

}
 
constexpr unsigned int NBINS{100};
constexpr unsigned int THRESH_DIST{200};
constexpr int RANGE_LOW{-1000};
constexpr int RANGE_HIGH{ 0};


vector<unsigned int>  crossings( const vector<float> & histogram, const float & value  )
{
	unsigned int bin=0;
	vector<unsigned int> v_bins;
	for (auto i = histogram.begin(); i != histogram.end()-1 ; ++i,++bin)
	{
//		cout<<"i "<<*i<<endl;
		if ( ((value < *i) && (value > *(i+1))) || ((value > *i) && (value < *(i+1)))  )
			v_bins.push_back(bin);
	}
	return v_bins;
}

void getAirAndFatThresholds(  AsrImage3D<float> & im0 , float & airThresh, float & fatThresh)
{//search histogram for threshold 

    vector<float> bin_values;
    vector<float> histogram = im0.histogram(NBINS,bin_values);

	list<float> l_hist;
	for (auto& i : histogram )
		l_hist.push_back(i);
	l_hist.sort();

	auto i_hist_next = l_hist.begin();
	++i_hist_next;
	for ( auto i_hist = l_hist.begin(); i_hist_next != l_hist.end(); ++i_hist,++i_hist_next )
	{
		if (*i_hist == *i_hist_next) continue;

		{
			vector<unsigned int> v_crossings_est = crossings(histogram,0.5*(*i_hist + *i_hist_next));
			//Restrtict search between -1000 and 0 , allow for avriability, but not totall ignore tissue properties
			vector<unsigned int> v_crossings;
			for (auto& i : v_crossings_est)
			{
				if ( (bin_values[i]>RANGE_LOW) && (bin_values[i]<RANGE_HIGH))
					v_crossings.push_back(i);
			}
			//the 2 is because we limit the range to 0
			if ((v_crossings.size()==2) && (bin_values[v_crossings[0]]<0) &&(bin_values[v_crossings[1]]<0) \
					&& ((bin_values[v_crossings[1]] - bin_values[v_crossings[0]] ) > THRESH_DIST) )
			{
//				for (auto& bin : v_crossings)
//					cout<<"bin "<<bin<<" "<<bin_values[bin]<<endl;
				airThresh=bin_values[v_crossings[0]];
				fatThresh=bin_values[v_crossings[1]];

				break;
			}
		}
	}
}

//checks along z direction to make sure is a constant value
bool checkZdirection(AsrImage3D<short> & im , const int & x, const int &y , const int & z )
{
        short initValue = im.value(x,y,z);
        for ( int znew = 0 ; znew < im.zsize(); ++znew )
        {
            if (im.value(x,y,znew)  != initValue)
                    return false;
        }
    
    return true;
}


void fillBackground( AsrImage3D<short> & im , const float & replaceValue, const float & fillValue , const bool & bySlice )
{
array<int,2> xlocs{0,im.xsize()-1};
	array<int,2> ylocs{0,im.ysize()-1};
//	array<int,2> zlocs{0,im.zsize()-1};
//    int z = 0 ; 
    for (int z = 0 ; z < im.zsize(); ++z )
    {
        for ( auto& y : ylocs)
        {
            for (int x = 0 ; x < im.xsize(); ++x )
            {
                if (im.value(x,y,z)==replaceValue)
                {
//                    if (checkZdirection(im,x,y,z))
                    {
//                        cout<<"fill at "<<x<<" "<<y<<" "<<z<<endl;
                        if (bySlice)
                        {
                            im.fillXY(x,y,z,fillValue);//fill background so that it matches fat tissue
                        }
                        else{
                            im.fill(x,y,z,fillValue);//fill background so that it matches fat tissue
                        }
                            
                            
                    }
                }
            }
        }
    }
            for (int z = 0 ; z < im.zsize(); ++z )
    {
        for ( auto& x : xlocs)
        {
            for (int y = 0 ; y < im.ysize(); ++y )
            {
                if (im.value(x,y,0)==replaceValue)
                {
//                     if (checkZdirection(im,x,y,z))
                     {
//                                            cout<<"fill at "<<x<<" "<<y<<" "<<z<<endl;
                        if (bySlice){
                            im.fillXY(x,y,z,fillValue);//fill background so that it matches fat tissue
                        }
                        else{
                            im.fill(x,y,z,fillValue);//fill background so that it matches fat tissue
                        }
                            
                
                    }
                }
            }
        }
    }
}

int main(int argc, char*argv[])
{
//	cout<<"\nEstimating tissue thresholds for Fat and Lung from image..."<<endl;
    

	if (argc < 3 )
	{
		Usage();
		return 0;
	}
    
    bool isCadaver{false};
    int argIndex{1};
    if ( string(argv[argIndex]) == "--cadaver")
    {
        isCadaver=true;
        argIndex++;
    }

    bool userDefinedThresholds{false};
    array<float,2> thresholds{-500,-500};
    if (string(argv[argIndex]) == "--thresh")
    {
        userDefinedThresholds=true;
        thresholds[0] = atof(argv[++argIndex]);
        thresholds[1] = atof(argv[++argIndex]);
        ++argIndex;
    }

    
	path file(argv[argIndex]);
	if (!exists(file))
	{
		cerr<<"\n File does not exists : "<<file<<"\n"<<endl;
		return 1;
	}else if (!is_regular_file(file))
	{
		cerr<<"\n"<<file<<" is not a file"<<"\n"<<endl;
		return 1;
	}

	string filename(argv[argIndex++]);
	string filename_out(argv[argIndex++]);

//	cout<<"Read in file: "<<filename<<endl;

	AsrImage3D<float> im0(filename);
	//should use quick sort, but for now...

	float airThresh{0},fatThresh{0};
    if ( userDefinedThresholds ){
        airThresh=thresholds[0];
        fatThresh=thresholds[1];
    }else{
        getAirAndFatThresholds(im0,airThresh,fatThresh);
    }
        
    cout<<"Thresholds "<<airThresh<<" "<<fatThresh<<endl;
    ofstream fthresholds((filename_out + "_threshold.txt").c_str());
    if (fthresholds.is_open()){
        fthresholds<<"air "<<airThresh<<endl;
        fthresholds<<"fat "<<fatThresh<<endl;
        fthresholds.close();
    }
	AsrImage3D<short> imAir,imChest,imPleura;
	im0.upperThreshold(airThresh,imAir);
    

    //Threshold with air thresh to be more inclusive of 
//	im0.lowerThreshold(fatThresh,imChest);
	im0.lowerThreshold(airThresh,imChest);

	imChest.writeImage(filename_out + "_prefillchest.nii.gz");

	//strip outside using full 3D connectedness
	imChest.connectedComponents(imChest,1);
	//scan along outside box at z=0;

    fillBackground(imChest ,0,1,isCadaver);//is cadaver flag triggers fill by slice
	

	imChest.writeImage(filename_out + "_chest.nii.gz");
	imAir.writeImage(filename_out + "_prefillair.nii.gz");

    fillBackground(imAir,1,0,isCadaver);
	//do similar for air segmentation
	//	imAir.fill(0,0,0,0);
//        array<int,2> xlocs{0,imAir.xsize()-1};
//	array<int,2> ylocs{0,imAir.ysize()-1};
//
//	for ( auto& y : ylocs)
//	{
//		for (int x = 0 ; x < imAir.xsize(); ++x )
//		{
//			if (imAir.value(x,y,0)!=0)
//			{
//				imAir.fill(x,y,0,0);//fill background so that it matches fat tissue
//			}
//		}
//	}
//
//	for ( auto& x : xlocs)
//	{
//		for (int y = 0 ; y < imAir.ysize(); ++y )
//		{
//			if (imAir.value(x,y,0)!=0)
//			{
//				imAir.fill(x,y,0,0);//fill background so that it matches fat tissue
//			}
//		}
//	}
	imAir.writeImage(filename_out + "_air.nii.gz");


	AsrImage3D<short> imChestComponents;
	imChestComponents=imChest;



	//clean up in axial directions
	vector<int> bounds{0,0,0,imChestComponents.xsize()-1, imChestComponents.ysize()-1, imChestComponents.zsize()-1};

	//TODO WRITE DEDICATED PLANAR CONNECTED COMPONENTS FOR SPEED, propdivded method proves fruitful
	//currently create a copy of image for each plane
	cout<<"Largest Connection Z "<<endl;
	for (int z = 0 ; z <imChestComponents.zsize(); ++z  )
	{
		vector<int> bounds_roi{bounds[0],bounds[1],z,bounds[3],bounds[4],z};
		imChestComponents.connectedComponents(imChestComponents,bounds_roi,1);
	}
	cout<<"Largest Connection X "<<endl;
	for (int x = 0 ; x <imChestComponents.xsize(); ++x  )
	{
		vector<int> bounds_roi{x,bounds[1],bounds[2],x,bounds[4],bounds[5]};
		imChestComponents.connectedComponents(imChestComponents,bounds_roi,1);
	}
	cout<<"Largest Connection Y "<<endl;
	for (int y = 0 ; y <imChestComponents.ysize(); ++y  )
	{
		vector<int> bounds_roi{bounds[0],y,bounds[2],bounds[3],y,bounds[5]};
		imChestComponents.connectedComponents(imChestComponents,bounds_roi,1);
	}


	cout<<"write components"<<endl;
	imChestComponents.writeImage(filename_out + "_chest_components.nii.gz");
	//do lungs
	AsrImage3D<short> lungs = imChestComponents;
	lungs.binaryInverse();
	lungs.connectedComponents(lungs,1); //lungs are connected via trachea

    //extra bit of cleaning to make nicer
//	for (int x = 0 ; x <lungs.xsize(); ++x  )
//	{
//		vector<int> bounds_roi{x,bounds[1],bounds[2],x,bounds[4],bounds[5]};
//		lungs.connectedComponents(lungs,bounds_roi,1);
//	}
    
        
	lungs.writeImage(filename_out + "_lungs.nii.gz");

//    im0.lowerUpperThreshold(airThresh,fatThresh,imPleura);
//    im0.mask(lungs);
//    im0.writeImage(filename_out + "_pleura.nii.gz");

	cout<<"copy vessels "<<endl;
	AsrImage3D<short> imVessels;
	imVessels=imChest;
	cout<<"copied "<<endl;
//	AsrImage3D<short>::copyImage(imVessels,imChest,4);
//	imVessels=imChest;
	imVessels.mask(imChestComponents,true); //invert the mask to exttract only vessels
//	imVessels.connectedComponents(imVessels,2); //grab the left and right vessels

	imVessels.writeImage(filename_out + "_vessels.nii.gz");
//	cout<<"histogram "<<endl;
//	for (auto& i : histogram )
//		cout<<i<<" ";
//	cout<<endl;

}

