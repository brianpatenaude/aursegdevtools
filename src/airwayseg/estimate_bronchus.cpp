/*
 * run_test.cpp
 *
 *  Created on: May 11, 2016
 *      Author: brian
 */
#include "aursurface/aursurface.h"

#include "aurcommon/aurstructs_functions.hpp"
#include "aurcommon/miscfunctions.h"
#include "AsrImage3D/AsrImage3D.h"
#include "segmentation_utils.h"
#include <glm/gtx/string_cast.hpp>
// STL includes
#include <fstream>
#include <iostream>
#include <map>
#include <vector>
// 3rd party includes
#include <boost/filesystem.hpp>

using namespace std;
using namespace auris_segtools;
using namespace boost::filesystem;
void Usage()
{
    cout << "\n estimate_trachea <nifit file> <ouput base>" << endl;
    cout << "\n This program is used  to estimate the trachea \n" << endl;
}

constexpr float areaThreshTrachea{19.625}; // threshold in mm2. 3.14*r^2, where
                                           // r = 2.5mm (looked at trachea
                                           // stats)
constexpr float areaThreshLungs{1256.0};   // thrwshold is in mm, 3.14 * 20^2
// (very low estimate of lungs, ensures
// further down
using plane = vertex<float>;

vector<unsigned int> crossings(const vector<float>& histogram,
                               const float& value)
{
    unsigned int bin = 0;
    vector<unsigned int> v_bins;
    for (auto i = histogram.begin(); i != histogram.end() - 1; ++i, ++bin) {
        //		cout<<"i "<<*i<<endl;
        if (((value < *i) && (value > *(i + 1)))
            || ((value > *i) && (value < *(i + 1))))
            v_bins.push_back(bin);
    }
    return v_bins;
}

void addToSkeleton(const aursurface<float>& airwaySurface,
                   AsrImage3D<short>& skeleton, const vertex<float>& cutPoint,
                   const vec3<float>& cutPlane)
{
}

int main(int argc, char* argv[])
{
    //	cout<<"\nEstimating tissue thresholds for Fat and Lung from
    // image..."<<endl;
    bool useCT{false};
    int currentArg = 1;

    if (argc < 3) {
        Usage();
        return 0;
    }
    string argname{argv[currentArg]};

    if (argname == "--useCT") {
        cout << "Use lungs instead of air " << endl;
        argc--;
        useCT = true;
        currentArg++;
        cout << "currentarg " << currentArg << endl;
    }
    path file(argv[currentArg]);
    if (!exists(file)) {
        cerr << "\n File does not exists : " << file << "\n" << endl;
        return 1;
    } else if (!is_regular_file(file)) {
        cerr << "\n"
             << file << " is not a file"
             << "\n"
             << endl;
        return 1;
    }

    string filename(argv[currentArg]);
    currentArg++;
    string filename_out(argv[currentArg]);

    // the air image should preserve more of the wall between lungs and trachea
    AsrImage3D<short> imAir;
    if (useCT) {
        imAir.readImage(filename);
    } else {
        imAir.readImage(filename_out + "_air.nii.gz");
    }


    //    imAir.readImage(filename_out + "_localMedium.nii.gz");
    AsrImage3D<short> imSurf = imAir;
    imSurf = 0;
    vector<int> bounds{
        0, 0, 0, imAir.xsize() - 1, imAir.ysize() - 1, imAir.zsize() - 1};

    AsrImage3D<short> imSpeedMap = imAir;


    {
        cout << "let's just grow the trachea " << endl;
        aursurface<float> surfAirways(filename_out + "_trachea.vtk");
        surfAirways.InitDeform();
        deformationParameters<float> airwaysParams;
        airwaysParams.splitThreshold = 0.05;
        airwaysParams.max_step_size = 0.1;
        airwaysParams.alpha_area = 0.05;
        airwaysParams.alpha_im = 0.2;
        airwaysParams.alpha_sn = 0.05;
        airwaysParams.alpha_st = 0.1;
                
//    params.splitThreshold = 0.05;
//    params.max_step_size = 0.1;
//    params.alpha_area = 0.05;
//    params.alpha_im = 0.2;
//   params.alpha_sn = 0.05;
//    params.alpha_st = 0.1;
        
        plane tracheaStart;
        if (!readCutPlanes(filename_out + "_airwayCuts.ssv", "trachea_start",tracheaStart)){

 
            surfAirways.anchorBelowPlane(tracheaStart);

            array<vertex<float>, 2> cutPoints;
            aurcontour<float> centreLine;
            // by default segmentAndCutSegment will color cut point
//            cout << "upsample trachea " << endl;
//                    surfAirways.splitTriangles(airwaysParams.splitThreshold);//resample
//            //        surface to higher before deforming
//                    surfAirways.write(filename_out +
//                    "_tracheaUpsample.vtk",aursurface<float>::VTK);
//            cout << "done upsample trrachea " << endl;
    airwaysParams.splitThreshold = 0.05;
    airwaysParams.max_step_size = 0.1;
    airwaysParams.alpha_area = 0.05;
    airwaysParams.alpha_im = 0.2;
   airwaysParams.alpha_sn = 0.05;
    airwaysParams.alpha_st = 0.1;
//
//
            surfAirways.InitDeform();
            surfAirways.copyAnchoredVerticesToScalars();
            surfAirways.setReferenceIntensity(
                imSpeedMap.interpolate(to_vertex(surfAirways.centroid())));
            cout << "reference intensity " << surfAirways.centroid().to_string()
                 << " "
                 << imSpeedMap.interpolate(to_vertex(surfAirways.centroid()))
                 << endl;
            // surfLeftBronchus.deform(imSpeedMap,1000,
            // bronchusParams,aursurface<float>::NEG_GRAD);
            deformUntilAllAnchored(surfAirways, imSpeedMap, airwaysParams,
                                   aursurface<float>::NEG_GRAD);
            surfAirways.write(filename_out + "_airways.vtk",
                              aursurface<float>::VTK);
                          
        }
    }


    aursurface<float> surfLeftBronchus(filename_out + "_leftBronchusInit.vtk");
    surfLeftBronchus.InitDeform();
    aursurface<float> surfRightBronchus(filename_out
                                        + "_rightBronchusInit.vtk");
    surfRightBronchus.InitDeform();

    // surfLeftBronchus.copyAnchoredVerticesToScalars();
    // surfLeftBronchus.write(filename_out +
    // "_leftBronchusAnchored.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);

    cout << "left ronchus " << endl;
    //    vec3<float> leftBronchusCutNormal, rightBronchusCutNormal;
    // surface, cutplane defined by vertex, genenation, side (0=left,1=right)
    list<tuple<aursurface<float>, vertex<float>, int, int> > branches_remaining;

    vertex<float> leftBronchusCut, rightBronchusCut;
    deformationParameters<float> bronchusParams;
//    bronchusParams.splitThreshold = 1;
//    bronchusParams.max_step_size = 0.1;
//    bronchusParams.alpha_area = 0.01;
    bronchusParams.alpha_im = 0.2;
//    bronchusParams.alpha_sn = 0.01;
//    bronchusParams.alpha_st = 0.1;
         bronchusParams.splitThreshold = 0.05;
                    bronchusParams.alpha_area = 0.05;
                    bronchusParams.alpha_sn = 0.05;
                    bronchusParams.alpha_st = 0.1;
                    bronchusParams.max_step_size = 0.1;
//        bronchusParams.alpha_area = 0.01;
    //    bronchusParams.alpha_sn = 0.01;
    //    bronchusParams.alpha_st = 0.01;
    //    bronchusParams.splitThreshold = 6;
    if (!readCutPlanes(filename_out + "_airwayCuts.ssv", "leftBronchus_start",
                       leftBronchusCut)) {
        cout << "-------------\n Segment Left Bronchus \n-----------------"
             << endl;
        surfLeftBronchus.InitDeform();
        surfLeftBronchus.copyAnchoredVerticesToScalars();
//        surfLeftBronchus.splitTriangles(bronchusParams.splitThreshold);//resample
//        surfLeftBronchus.write(
//            filename_out + "_leftBronchusInitUpsample.vtk",
//            auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
        array<vertex<float>, 2> cutPoints;
        aurcontour<float> centreLine;
                std::array<aursurface<float>, 3> surfLeftLobes
                    = segmentAndCutSegment(imSpeedMap, surfLeftBronchus,
                                           leftBronchusCut, bronchusParams,
                                           centreLine,
                                           cutPoints,aursurface<float>::NEG_GRAD);
        // by default segmentAndCutSegment will color cut point
        cout << "upsample left bronchus " << endl;
        //        surface to higher before deforming
        cout << "done upsample left bronchus " << endl;
//        surfLeftBronchus.setReferenceIntensity(
//            imSpeedMap.interpolate(to_vertex(surfLeftBronchus.centroid())));
//        cout << "reference intensity "
//             << surfLeftBronchus.centroid().to_string() << " "
//             << imSpeedMap.interpolate(to_vertex(surfLeftBronchus.centroid()))
//             << endl;
//        // surfLeftBronchus.deform(imSpeedMap,1000,
//        // bronchusParams,aursurface<float>::NEG_GRAD);
//        std::array<aursurface<float>, 3> surfLeftLobes;
//        deformUntilAllAnchored(surfLeftBronchus, imSpeedMap, bronchusParams,
//                               aursurface<float>::NEG_GRAD);
//        surfLeftBronchus.copyAnchoredVerticesToScalars();
//
//        //        surfLeftBronchus.setScalars(0);
//        surfLeftBronchus.computeVertexNormals();
        //        surfLeftBronchus.copyCurvatureToScalars();
        surfLeftBronchus.write(
            filename_out + "_leftBronchus.vtk",
            auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);

        aursurface<float> surfSkel = centreLine.getVerticesAsSpheres(0.25);
        surfSkel.setScalars(0);
        surfSkel.write(filename_out + "_leftBronchus.skel.balls.vtk",
                       auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
        //    return 1;
        deformUntilAllAnchored(surfLeftBronchus, imSpeedMap, bronchusParams, aursurface<float>::NEG_GRAD);
   surfLeftBronchus.write(
            filename_out + "_leftBronchusFull.vtk",
            auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);


        if (successfulSegment(cutPoints)) {
            // TODO alllow to go into only one branch
            branches_remaining.push_back(
                tuple<aursurface<float>, vertex<float>, int, int>(
                    surfLeftLobes[0], cutPoints[0], 3, 0));
            surfLeftLobes[0].write(
                filename_out + "_leftBronchusChild0.vtk",
                auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);

            branches_remaining.push_back(
                tuple<aursurface<float>, vertex<float>, int, int>(
                    surfLeftLobes[1], cutPoints[1], 3, 0));
            surfLeftLobes[1].write(
                filename_out + "_leftBronchusChild1.vtk",
                auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);

            appendCutPointToFile(filename_out + "_airwayCuts.ssv",
                                 "leftLowerLobeBronchus_start", cutPoints[0]);
            appendCutPointToFile(filename_out + "_airwayCuts.ssv",
                                 "leftUpperLobeBronchus_start", cutPoints[1]);
        }
    }
    if (!readCutPlanes(filename_out + "_airwayCuts.ssv", "rightBronchus_start",
                       rightBronchusCut)) {
        cout << "-------------\n Segment Right Bronchus \n-----------------"
             << endl;
        array<vertex<float>, 2> cutPoints;
        aurcontour<float> centreLine;
        std::array<aursurface<float>, 3> surfRightLobes = segmentAndCutSegment(
            imSpeedMap, surfRightBronchus, rightBronchusCut, bronchusParams,
            centreLine, cutPoints, aursurface<float>::NEG_GRAD);
        //                array< vertex<float>,2 > cutPoints_leftBronchus  =
        //                segmentAirwaySegment(imSpeedMap, surfLeftBronchus,
        //                surfLeftLowerLobeBronchus,surfLeftUpperLobeBronchus,leftBronchusCut,bronchusParams);
        //        surfRightBronchus.setScalars(1);
        surfRightBronchus.write(
            filename_out + "_rightBronchus.vtk",
            auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
        aursurface<float> surfSkel = centreLine.getVerticesAsSpheres(0.25);
        surfSkel.setScalars(0);
        surfSkel.write(filename_out + "_rightBronchus.skel.balls.vtk",
                       auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
        deformUntilAllAnchored(surfRightBronchus, imSpeedMap, bronchusParams, aursurface<float>::NEG_GRAD);
   surfLeftBronchus.write(
            filename_out + "_rightBronchusFull.vtk",
            auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
            
        if (successfulSegment(cutPoints)) {
            // TODO alllow to go into only one branch
            branches_remaining.push_back(
                tuple<aursurface<float>, vertex<float>, int, int>(
                    surfRightLobes[0], cutPoints[0], 3, 1));
            surfRightLobes[0].write(
                filename_out + "_rightBronchusChild0.vtk",
                auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
            branches_remaining.push_back(
                tuple<aursurface<float>, vertex<float>, int, int>(
                    surfRightLobes[1], cutPoints[1], 3, 1));
            surfRightLobes[1].write(
                filename_out + "_rightBronchusChild1.vtk",
                auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);


            appendCutPointToFile(filename_out + "_airwayCuts.ssv",
                                 "rightLowerLobeBronchus_start", cutPoints[0]);
            appendCutPointToFile(filename_out + "_airwayCuts.ssv",
                                 "rightUpperLobeBronchus_start", cutPoints[1]);
        }
    }

    // start recursive traversal of tree
    //    return 0;
    array<vertex<float>, 2> cutPoints;

    int branchIndex = 3;
    while (!branches_remaining.empty()) {
        cout << "-------------\n Segment Next Bronchus \n-----------------"
             << endl;
        std::array<aursurface<float>, 3> surfChildren;
        tuple<aursurface<float>, vertex<float>, int, int> branchCur
            = branches_remaining.front();
        branches_remaining.pop_front();
        int gen = get<2>(branchCur);
        deformationParameters<float> bronchusParams;
        if (gen == 4)
            break;
        if (branchIndex == 3)
            break;
        switch (gen) {
            case 3:
                bronchusParams.splitThreshold = 0.1;
                bronchusParams.alpha_area = 0.01;
                bronchusParams.alpha_sn = 0.0;
                bronchusParams.max_step_size = 0.1;
                bronchusParams.alpha_st = 0.1;
                break;
            case 4:
                bronchusParams.splitThreshold = 2;
                bronchusParams.alpha_area = 0.01;
                bronchusParams.alpha_sn = 0.0;
                bronchusParams.alpha_st = 0.1;
                break;
            case 5:
                bronchusParams.splitThreshold = 2;
                bronchusParams.alpha_area = 0.01;
                bronchusParams.alpha_sn = 0.0;
                bronchusParams.alpha_st = 0.1;
            default:
                bronchusParams.splitThreshold = 2;
                bronchusParams.alpha_area = 0.01;
                bronchusParams.alpha_sn = 0.2;
                bronchusParams.alpha_st = 0.1;
                //                        return 0;//return past 5th generation
                break;
        }

        // write out initial cut
        get<0>(branchCur).write(
            filename_out + "_side" + to_string(get<3>(branchCur)) + "_branch"
                + to_string(branchIndex) + "_gen" + to_string(get<2>(branchCur))
                + "_init.vtk",
            auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);

        cout << "-------------\n " << ("_side" + to_string(get<3>(branchCur))
                                       + "_branch" + to_string(branchIndex)
                                       + "_gen" + to_string(get<2>(branchCur)))
             << " \n-----------------" << endl;


        aurcontour<float> centreLine;
        surfChildren = segmentAndCutSegment(imSpeedMap, get<0>(branchCur),
                                            get<1>(branchCur), bronchusParams,
                                            centreLine, cutPoints);
        get<0>(branchCur).setScalars((get<2>(branchCur) + 1) % 2);
        // write surface with naming
        get<0>(branchCur).write(
            filename_out + "_side" + to_string(get<3>(branchCur)) + "_branch"
                + to_string(branchIndex) + "_gen" + to_string(get<2>(branchCur))
                + ".vtk",
            auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);


        aursurface<float> surfSkel = centreLine.getVerticesAsSpheres(0.25);
        surfSkel.setScalars((get<2>(branchCur))
                            % 2); // use opposite color of surface
        surfSkel.write(filename_out + "_side" + to_string(get<3>(branchCur))
                           + "_branch" + to_string(branchIndex) + "_gen"
                           + to_string(get<2>(branchCur)) + ".skel.balls.vtk",
                       auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
        get<0>(branchCur).drawSurface(imSurf, static_cast<short>(branchIndex));
        imSurf.writeImage(filename_out + "_imsurf.nii.gz");
        ++branchIndex;

        if (successfulSegment(cutPoints)) {
            branches_remaining.push_back(
                tuple<aursurface<float>, vertex<float>, int, int>(
                    surfChildren[0], cutPoints[0], get<2>(branchCur) + 1,
                    get<3>(branchCur)));
            surfChildren[0].write(
                filename_out + "_side" + to_string(get<3>(branchCur))
                    + "_branch" + to_string(branchIndex) + "_gen"
                    + to_string(get<2>(branchCur)) + "_child0.vtk",
                auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
            branches_remaining.push_back(
                tuple<aursurface<float>, vertex<float>, int, int>(
                    surfChildren[1], cutPoints[1], get<2>(branchCur) + 1,
                    get<3>(branchCur)));
            surfChildren[1].write(
                filename_out + "_side" + to_string(get<3>(branchCur))
                    + "_branch" + to_string(branchIndex) + "_gen"
                    + to_string(get<2>(branchCur)) + "_child1.vtk",
                auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
        }
    }

    //        cout<<"number of vertices in the cut
    //        "<<surfLeftLobes[0].numberOfVertices()<<"
    //        "<<surfLeftLobes[1].numberOfVertices()<<endl;

    //{
    //
    //    cout<<"findbirufrcation for left bronchus" <<endl;
    //    array< vertex<float>, 2>  cutPlanePoints_LBronchus =
    //    scanForBifurcation(surfLeftBronchus,imSpeedMap,cutPlanePoints[0],
    //    cutPlaneNormals[0]);
    //    cout<<"do split "<<endl;
    //    aursurface<float> surf_LeftLowerLobeBronchus =
    //    surfLeftBronchus.splitSurface(carinaTriangles[0][0],
    //    cutPlanePoints_LBronchus[0],
    //    cutPlaneNormals[0]);
    //    surf_LeftLowerLobeBronchus.write(filename_out +
    //    "_leftLowerLobeBronchus.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
    //    cout<<"findbirufrcation forrighft bronchus" <<endl;
    //    scanForBifurcation(surfRightBronchus,imSpeedMap,cutPlanePoints[1],
    //    cutPlaneNormals[1]);
    //    array< vertex<float>,2> cutPlanePoints;
    //    array< vec3<float>,2> cutPlaneNormals = cutBronchus( imSpeedMap,
    //    surfLeftBronchus, closestContourPoints,carinaTriangles,cutPlanePoints
    //    ,
    //    tracheaNormal);
    //}
}
