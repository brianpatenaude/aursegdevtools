//========================== ===================================================
// Auris Surgical Robotics, Inc.
// (C) Copyright 2016. All Rights Reserved.
//
//
//! \file       segmentation_utils.cpp
//! \brief
//! Segmentation specific methods
//!
//! \date       July 2016
//!
//! \author     Brian Patenaude
//!
//=============================================================================

#ifndef SEGMENTATION_UTILS_H
#define SEGMENTATION_UTILS_H

// STL includes
#include <array>
#include <fstream>
#include <iostream>
#include <map>
// auris includes
#include <aurcommon/aurstructs.h>
#include <AsrImage3D/AsrImage3D.h>
#include <aursurface/aursurface.h>
#include <aursurface/aurcontour.h>

namespace auris_segtools
{
constexpr float contourThresh{ 0.2 };    /// threshodl by whihc we assume the contours are the same
constexpr float SAME_COUNT_THRESH{ 10 }; /// threshodl by whihc we assume the contours are the same
constexpr int iStep = 1000;              // affects the slef-intersection correction
constexpr int selfIntersectionInterval = 100;
constexpr short Niters = 500;
constexpr float zres = 5; // mm
constexpr float zresBifurcation = 0.5;
constexpr float MAX_ALPHA_AREA = 0.1;
constexpr float MAX_ALPHA_ST = 0.3;
constexpr float SPLIT_THRESH_MIN = 0.5;
constexpr float CUT_AREA_THRESH = 0.25; // a valid cut contour must be a quarter of the initial
constexpr float ALPHA_ST_STEP = 0.01;
constexpr float SPLIT_THRESH_STEP = 0.1;

// 0.5;//resolution by which to discern bifurcatio, for trachea, the carina
constexpr float ANGLE_RANGE = M_PI_4 / 2.0;
/**
 * @brief Test if airway segmentation completed successfully (false if input vertices are origin)
 */
bool successfulSegment(const std::array<vertex<float>, 2>& verts);
/**
 * @brief Grows surface with an aiway segment up to point to bifurcation, and cuts childrean aiways
 * @param imSpeedMap Image specifying the outword force on vertices
 * @param surfAirway Initial and final airway (represented as a surface)
 * @param initPoint Initial point of the airway (x,y,z) location and normal direction (nx,ny,nz)
 * @param deformParams Parameters input to the deformable surface
 * @param centreLine Center line (centre of gravity estimate) along the airway (this is an output)
 * @param cutPoints Initial points for the next braches (this is an output)
 * @return returns initial surfaces to the next bracnches
 */
std::array<aursurface<float>, 3> segmentAndCutSegment(AsrImage3D<short>& imSpeedMap,
    aursurface<float>& surfAirway,
    const vertex<float>& initPoint,
    const deformationParameters<float>& deformParams,
    aurcontour<float>& centreLine,
    std::array<vertex<float>, 2>& cutPoints,
    aursurface<float>::SPEEDFUNC speedFuncType = aursurface<float>::NEG_GRAD);
/**
 * @brief
 * @param imSpeedMap Image specifying the outword force on vertices
 * @param surfAirway
 * @param surfAirwayA - branch A
 * @param surfAirwayB - brnach B
 * @param surfAirwayC uncut curfaces
 */
std::array<vertex<float>, 2> segmentAirwaySegment(AsrImage3D<short>& imSpeedMap,
    aursurface<float>& surfAirway,
    aursurface<float>& surfAirwayA,
    aursurface<float>& surfAirwayB,
    aursurface<float>& surfAirwayC,
    const vertex<float>& initPoint,
    const deformationParameters<float>& params,
    aurcontour<float>& centreLine,
    const aursurface<float>::SPEEDFUNC& speedFuncType);

//       std::map<float ,vec3<float> > estimateCutPlane(aursurface<float> & surfTrachea, std::array<unsigned int,2>  &
//       carinaTriangles, vertex<float> & cutPlanePoint) ;
vec3<float> estimateCutPlane(aursurface<float>& surfTrachea,
    std::array<unsigned int, 2>& carinaTriangles,
    const vertex<float>& cutPlanePoint);
vec3<float>
estimateCutPlane(aursurface<float>& surfTrachea, const vec3<float>& cutAxis, const vertex<float>& cutPlanePoint);

//       std::map<float ,vec3<float> > estimateCutPlane(aursurface<float> & surfTrachea, std::array<unsigned int,2>  &
//       carinaTriangles, vertex<float> & cutPlanePoint, const vec3<float> & prevNormal) ;
//       vec3<float> estimateCutPlane(aursurface<float> & surfTrachea, std::array<unsigned int,2>  & carinaTriangles,
//       vertex<float> & cutPlanePoint, const vec3<float> & prevNormal) ;

// cuts the airway given the cutpoint, returns the plane normals,
std::array<vec3<float>, 2> cutAirway(AsrImage3D<short>& imSpeedMap,
    aursurface<float>& surfTrachea,
    std::array<std::array<unsigned int, 2>, 2>& carinaTriangles,
    std::array<vertex<float>, 2>& cutPlanePoints,
    const vec3<float>& prevNormal,
    const deformationParameters<float>& params);

// deforms surface until it reaches bifurcation

int deformUntilAllAnchored(aursurface<float>& airwaySurface,
    AsrImage3D<short>& imSpeedMap,
    const deformationParameters<float>& params,
    const aursurface<float>::SPEEDFUNC& speedFuncType);

int deformToBifurcation(aursurface<float>& airwaySurface,
    AsrImage3D<short>& imSpeedMap,
    const vertex<float>& cutPoint,
    const vec3<float>& normal,
    const deformationParameters<float>& params,
    const aursurface<float>::SPEEDFUNC& speedFuncType);

int deformToBifurcation(aursurface<float>& airwaySurface,
    AsrImage3D<short>& imSpeedMap,
    const vertex<float>& cutPoint,
    const deformationParameters<float>& params,
    const aursurface<float>::SPEEDFUNC& speedFuncType);
int findTrachea( AsrImage3D<short>& imAir,
    vec3<float>& tracheaSeed,
    const float& areaThreshLungs,
    const float& areaThreshTrachea,
    const float& searchStep_dzmm);

// return in two in-plane vertcies that are closest between contours, used to cut bifurcation
std::array<vertex<float>, 2> scanForBifurcation(aursurface<float>& airwaySurface,
    AsrImage3D<short>& imSpeedMap,
    const vertex<float>& cutPoint,
    std::array<std::array<unsigned int, 2>, 2>& bifurcationTriangles);
//    std::array< vertex<float>, 2>  scanForBifurcationWithNormalTracking( aursurface<float> & airwaySurface,
//    AsrImage3D<short> & imSpeedMap, const vertex<float> & cutPoint, const vec3<float> & normal,  std::array<
//    std::array<unsigned int,2> ,2 > & bifurcationTriangles );
int scanForBifurcationWithNormalTracking(aursurface<float>& airwaySurface,
    AsrImage3D<short>& imSpeedMap,
    const vertex<float>& cutPoint,
    const vec3<float>& normal,
    std::array<std::array<unsigned int, 2>, 2>& bifurcationTriangles,
    vertex<float>& finalVertex,
    aurcontour<float>& centreLine);
std::array<vertex<float>, 2> scanForBifurcation(aursurface<float>& airwaySurface,
    AsrImage3D<short>& imSpeedMap,
    const vertex<float>& cutPoint,
    const vec3<float>& normal,
    std::array<std::array<unsigned int, 2>, 2>& bifurcationTriangles);

int readCutPlanes(const std::string& filename,
    const std::string& branchName,
    vertex<float>& planeVertex,
    vec3<float>& planeNormal);
int readCutPlanes(const std::string& filename, const std::string& branchName, vertex<float>& planeVertex);

// IO
int writeCutPointToFile(const std::string& filename, const std::string& branchName, const vertex<float>& planePoint);
int appendCutPointToFile(const std::string& filename, const std::string& branchName, const vertex<float>& planePoint);
int appendCutPointToFile(const std::string& filename,
    const std::string& branchName,
    const vertex<float>& planePoint,
    const vec3<float>& planeNormal);

int writeCutPointToFile(const std::string& filename,
    const std::string& branchName,
    const vertex<float>& planePoint,
    const vec3<float>& planeNormal,
    std::ios_base::openmode mode = (std::ofstream::out));
}

#endif