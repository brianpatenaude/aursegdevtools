/*
 * run_test.cpp
 *
 *  Created on: May 11, 2016
 *      Author: brian
 */
#include "aurcommon/miscfunctions.h"
#include "AsrImage3D/AsrImage3D.h"
#include "segmentation_utils.h"
#include <utils/asr-timer.h>
// STL includes
#include <fstream>
#include <iostream>
#include <vector>
// 3rd party includes
//#include <AlgoGlobalAirwaysSegmentationFilter.h>
//#include <Rsip/AlgoConfigurations.h>
//#include <Rsip/AlgoRegionGrowing.h>
//#include <SegmentationTypedefs.h>
#include <boost/filesystem.hpp>
#include <png.h>

//    typedef itk::Image<int16_t, 3> CTImageType;
//    typedef itk::Image<unsigned char, 3> MaskImageType;
//    typedef itk::Image<int, 3> LabeledMaskImageType;
//    typedef itk::Image<double, 3> RealImageType;

//#include "Rsip/utils/WriteNIIFTI.hpp"
//#include "Rsip/utils/ReadNIIFTI.hpp"

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkImageIterator.h"
#include "itkNiftiImageIOFactory.h"
#include <itkNiftiImageIO.h>

//#include <AlgoGlobalPhase.h>
using namespace std;
using namespace auris_segtools;
using namespace boost::filesystem;
//using namespace segm;
void Usage()
{
    cout << "\n estimate_fat_lung_thresholds [--cadaver] [ --thresh <value_low> <value_high> ]  <nifti_file> "
            "<output_base>"
         << endl;
    cout << "\n This program is used  to estimate intensity thresholds for the RSIP software. \n" << endl;
}

constexpr unsigned int NBINS{ 100 };
constexpr unsigned int THRESH_DIST{ 200 };
constexpr int RANGE_LOW{ -1000 };
constexpr int RANGE_HIGH{ 0 };

constexpr float areaThreshTrachea{ 19.625 }; // threshold in mm2. 3.14*r^2, where
                                             // r = 2.5mm (looked at trachea
                                             // stats)
constexpr float areaThreshLungs{ 1256.0 };   // thrwshold is in mm, 3.14 * 20^2
constexpr float tracheaSeed_dz{ 10.0 };      // search for trachea seed in 5mm intervals
// (very low estimate of lungs, ensures
// further down

void writePixel(vector<unsigned char>& data,
    const int& width,
    const int& height,
    const int& x,
    const int& y,
    const int& R,
    const int& G,
    const int& B)
{
    data[(y * width + x) * 3] = static_cast<int>(R);
    data[(y * width + x) * 3 + 1] = static_cast<int>(G);
    data[(y * width + x) * 3 + 2] = static_cast<int>(B);
}
void writeAxialSliceWithSeed(const string& filename, AsrImage3D<float>& im0, const vec3<float>& tracheaSeed)
{
    png_byte color_type{ PNG_COLOR_TYPE_RGB };
    png_byte bit_depth{ 8 };
    png_structp png_ptr;
    png_infop info_ptr;
    int width{ im0.xsize() };
    int height{ im0.ysize() };
    vector<unsigned char> data(width * height * 3, static_cast<unsigned char>(0));
    png_bytep row_pointers[height];
    for(int i = 0; i < height; ++i)
        row_pointers[i] = &(data[i * width * 3]);

    //   row_pointers = png_malloc(png_ptr, height*sizeof(png_bytep));

    //   row_pointers = png_malloc(png_ptr, height*sizeof(png_bytep));
    FILE* fp = fopen(filename.c_str(), "wb");

    //    vector<png_bytep> data(3 * width*height, 125);

    png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    png_init_io(png_ptr, fp);
    info_ptr = png_create_info_struct(png_ptr);

    png_set_IHDR(png_ptr, info_ptr, width, height, bit_depth, color_type, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE,
        PNG_FILTER_TYPE_BASE);
    //    int slicez = tracheaCOG.z/im0.zdim();
    int seedX{ static_cast<int>(tracheaSeed.x / im0.xdim() + 0.5) };
    int seedY{ static_cast<int>(tracheaSeed.y / im0.ydim() + 0.5) };
    int seedZ{ static_cast<int>(tracheaSeed.z / im0.zdim() + 0.5) };

    cout << "slicesz " << seedZ << endl;
    float min{ 0 }, max{ 0 };

    { // caculate late min max to map grey scale
        const float* voxel_ptr = im0.data() + width * height * seedZ;
        min = *(voxel_ptr++);
        max = *(voxel_ptr++);
        for(int i = 1; i < width * height; ++i, ++voxel_ptr) {
            if(*voxel_ptr < min)
                min = *voxel_ptr;
            if(*voxel_ptr > max)
                max = *voxel_ptr;
        }
    }
    {
        cout << "max min " << min << " " << max << endl;
        float range = max - min;
        const float* voxel_ptr = im0.data() + width * height * seedZ;
        int stride = width * height * 3;
        if(range > 0) {
            for(int i = 0; i < stride; ++i, ++voxel_ptr) {
                int val = 255 * (*voxel_ptr - min) / range;
                data[i++] = static_cast<unsigned char>(val);
                data[i++] = static_cast<unsigned char>(val);
                data[i] = static_cast<unsigned char>(val);
            }
        }
    }

    //    int seedIndex=static_cast<int>( tracheaCOG.y/im0.ydim()) *width*3 +
    //    static_cast<int>(tracheaCOG.x/im0.xdim())*3 ;
    //    data[seedIndex++] = 255;
    //    data[seedIndex++] = 0;
    //    data[seedIndex] = 0;
    int crossHeight = 5; // pixels
    for(int dx = -crossHeight; dx <= crossHeight; ++dx) {
        writePixel(data, width, height, seedX + dx, seedY, 255, 0, 0);
    }
    for(int dy = -crossHeight; dy <= crossHeight; ++dy) {
        writePixel(data, width, height, seedX, seedY + dy, 255, 0, 0);
    }

    png_write_info(png_ptr, info_ptr);

    png_write_image(png_ptr, row_pointers);
    if(setjmp(png_jmpbuf(png_ptr)))
        cout << "[write_png_file] Error during end of write" << endl;

    png_write_end(png_ptr, NULL);
}

vector<unsigned int> crossings(const vector<float>& histogram, const float& value)
{
    unsigned int bin = 0;
    vector<unsigned int> v_bins;
    for(auto i = histogram.begin(); i != histogram.end() - 1; ++i, ++bin) {
        //		cout<<"i "<<*i<<endl;
        if(((value < *i) && (value > *(i + 1))) || ((value > *i) && (value < *(i + 1))))
            v_bins.push_back(bin);
    }
    return v_bins;
}

void getAirAndFatThresholds(AsrImage3D<float>& im0, float& airThresh, float& fatThresh)
{ // search histogram for threshold

    vector<float> bin_values;
    vector<float> histogram = im0.histogram(NBINS, bin_values);

    list<float> l_hist;
    for(auto& i : histogram)
        l_hist.push_back(i);
    l_hist.sort();

    auto i_hist_next = l_hist.begin();
    ++i_hist_next;
    for(auto i_hist = l_hist.begin(); i_hist_next != l_hist.end(); ++i_hist, ++i_hist_next) {
        if(*i_hist == *i_hist_next)
            continue;

        {
            vector<unsigned int> v_crossings_est = crossings(histogram, 0.5 * (*i_hist + *i_hist_next));
            // Restrtict search between -1000 and 0 , allow for avriability, but not totall ignore tissue properties
            vector<unsigned int> v_crossings;
            for(auto& i : v_crossings_est) {
                if((bin_values[i] > RANGE_LOW) && (bin_values[i] < RANGE_HIGH))
                    v_crossings.push_back(i);
            }
            // the 2 is because we limit the range to 0
            if((v_crossings.size() == 2) && (bin_values[v_crossings[0]] < 0) && (bin_values[v_crossings[1]] < 0) &&
                ((bin_values[v_crossings[1]] - bin_values[v_crossings[0]]) > THRESH_DIST)) {
                //				for (auto& bin : v_crossings)
                //					cout<<"bin "<<bin<<" "<<bin_values[bin]<<endl;
                airThresh = bin_values[v_crossings[0]];
                fatThresh = bin_values[v_crossings[1]];
 
                break;
            }
        }
    }
}

// checks along z direction to make sure is a constant value
bool checkZdirection(AsrImage3D<short>& im, const int& x, const int& y, const int& z)
{
    short initValue = im.value(x, y, z);
    for(int znew = 0; znew < im.zsize(); ++znew) {
        if(im.value(x, y, znew) != initValue)
            return false;
    }

    return true;
}

void fillBackground(AsrImage3D<short>& im, const float& replaceValue, const float& fillValue, const bool& bySlice)
{
    array<int, 2> xlocs{ 0, im.xsize() - 1 };
    array<int, 2> ylocs{ 0, im.ysize() - 1 };
    //	array<int,2> zlocs{0,im.zsize()-1};
    //    int z = 0 ;
    for(int z = 0; z < im.zsize(); ++z) {
        for(auto& y : ylocs) {
            for(int x = 0; x < im.xsize(); ++x) {
                if(im.value(x, y, z) == replaceValue) {
                    //                    if (checkZdirection(im,x,y,z))
                    {
                        //                        cout<<"fill at "<<x<<" "<<y<<" "<<z<<endl;
                        if(bySlice) {
                            im.fillXY(x, y, z, fillValue); // fill background so that it matches fat tissue
                        } else {
                            im.fill(x, y, z, fillValue); // fill background so that it matches fat tissue
                        }
                    }
                }
            }
        }
    }
    for(int z = 0; z < im.zsize(); ++z) {
        for(auto& x : xlocs) {
            for(int y = 0; y < im.ysize(); ++y) {
                if(im.value(x, y, 0) == replaceValue) {
                    //                     if (checkZdirection(im,x,y,z))
                    {
                        //                                            cout<<"fill at "<<x<<" "<<y<<" "<<z<<endl;
                        if(bySlice) {
                            im.fillXY(x, y, z, fillValue); // fill background so that it matches fat tissue
                        } else {
                            im.fill(x, y, z, fillValue); // fill background so that it matches fat tissue
                        }
                    }
                }
            }
        }
    }
}

int main(int argc, char* argv[])
{
    //	cout<<"\nEstimating tissue thresholds for Fat and Lung from image..."<<endl;
    asrTimer appTimer;

    if(argc < 3) {
        Usage();
        return 0;
    }

    bool isCadaver{ false };
    int argIndex{ 1 };
    if(string(argv[argIndex]) == "--cadaver") {
        isCadaver = true;
        argIndex++;
    }

    bool userDefinedThresholds{ false };
    array<float, 2> thresholds{ -500, -500 };
    if(string(argv[argIndex]) == "--thresh") {
        userDefinedThresholds = true;
        thresholds[0] = atof(argv[++argIndex]);
        thresholds[1] = atof(argv[++argIndex]);
        ++argIndex;
    }

    path file(argv[argIndex]);
    if(!exists(file)) {
        cerr << "\n File does not exists : " << file << "\n" << endl;
        return 1;
    } else if(!is_regular_file(file)) {
        cerr << "\n"
             << file << " is not a file"
             << "\n"
             << endl;
        return 1;
    }

    string filename(argv[argIndex++]);
    string filename_out(argv[argIndex++]);

    ofstream ftimelog((filename_out + "_time.log").c_str());

    vec3<float> tracheaSeed(0, 0, 0);

    //	cout<<"Read in file: "<<filename<<endl;
    AsrImage3D<float> im0(filename);
    // should use quick sort, but for now...

    appTimer.addTimeStamp("Done Reading CT");

    float airThresh{ 0 }, fatThresh{ 0 };
    if(userDefinedThresholds) {
        airThresh = thresholds[0];
        fatThresh = thresholds[1];
    } else {
        getAirAndFatThresholds(im0, airThresh, fatThresh);
    }

    appTimer.addTimeStamp("Found Thesholds");

    cout << "Thresholds " << airThresh << " " << fatThresh << endl;
    ofstream fthresholds((filename_out + "_threshold.txt").c_str());
    if(fthresholds.is_open()) {
        fthresholds << "air " << airThresh << endl;
        fthresholds << "fat " << fatThresh << endl;
        fthresholds.close();
    }
    AsrImage3D<short> imAir;
    //	im0.upperThreshold(airThresh,imAir);
    // TODO COMBINE THRESHOLLD AND FILL
    im0.upperThreshold(airThresh, imAir);
    //    im0.upperThreshold(fatThresh, imAir);
    //    imAir.writeImage("air.nii.gz");
    appTimer.addTimeStamp("copied and thresholded CT");

    // TODO combine fill background in slice and trachea finding, don't need to fill whole image
    fillBackground(imAir, 1, 0, isCadaver); // is cadaver flag triggers fill by slice

    appTimer.addTimeStamp("Removed background from Air mask");

    if(findTrachea(imAir, tracheaSeed, areaThreshLungs, areaThreshTrachea, tracheaSeed_dz)) {
        cerr << "Failed to find trachea " << endl;
        return 1;
    }
    
    //Write trache seed to file
    ofstream fout(filename_out + "_tracheaSeed.txt");
    if(fout.is_open()) {
        cout << "seed " << tracheaSeed.x << " " << tracheaSeed.y << " " << tracheaSeed.z << " "
             << (tracheaSeed.x / imAir.xdim()) << " " << (tracheaSeed.y / imAir.ydim()) << " "
             << (tracheaSeed.z / imAir.zdim()) << endl;
        fout << tracheaSeed.x << " " << tracheaSeed.y << " " << tracheaSeed.z << " " << (tracheaSeed.x / imAir.xdim())
             << " " << (tracheaSeed.y / imAir.ydim()) << " " << (tracheaSeed.z / imAir.zdim()) << endl;
        fout.close();
    }

    appTimer.addTimeStamp("Found trachea seed");

    //write snapshot of result
    writeAxialSliceWithSeed(filename_out + "_tracheaSeed.png", im0, tracheaSeed);
    
    // here's a quick hack to save and read NIFTI, will write converter later
    imAir.writeImage(filename_out + "_air.nii.gz");
    appTimer.addTimeStamp("Wrote Air image");

    cout << appTimer.to_string() << endl;
    if(ftimelog.is_open()) {
        ftimelog << appTimer.to_string() << endl;
        ftimelog.close();
    }

    return 0;
}
