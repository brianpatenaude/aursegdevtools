/*
 * run_test.cpp
 *
 *  Created on: May 11, 2016
 *      Author: brian
 */
#include "aursurface/aursurface.h"
 
#include "aurimage/aurimage.h"
#include "aurcommon/miscfunctions.h"
#include "aurcommon/aurstructs_functions.hpp"
#include <glm/gtx/string_cast.hpp>
#include "segmentation_utils.h"
//STL includes
#include <iostream>
#include <vector>
#include <fstream>
#include <map>
//3rd party includes
#include <boost/filesystem.hpp>

using namespace std;
using namespace auris_segtools;
using namespace boost::filesystem;
void Usage(){
	cout<<"\n estimate_trachea <nifit file> <ouput base>"<<endl;
	cout<<"\n This program is used  to estimate the trachea \n"<<endl;

} 

 
 
constexpr float areaThreshTrachea{19.625};//threshold in mm2. 3.14*r^2, where r = 2.5mm (looked at trachea stats) 
constexpr float areaThreshLungs{1256.0};//thrwshold is in mm, 3.14 * 20^2 (very low estimate of lungs, ensures further down


vector<unsigned int>  crossings( const vector<float> & histogram, const float & value  )
{
	unsigned int bin=0;
	vector<unsigned int> v_bins;
	for (auto i = histogram.begin(); i != histogram.end()-1 ; ++i,++bin)
	{
//		cout<<"i "<<*i<<endl;
		if ( ((value < *i) && (value > *(i+1))) || ((value > *i) && (value < *(i+1)))  )
			v_bins.push_back(bin);
	}
	return v_bins;
}




void addToSkeleton( const aursurface<float> & airwaySurface,  aurimage<short> & skeleton, const vertex<float> & cutPoint , const vec3<float> & cutPlane )
{
     
    
}

  
int main(int argc, char*argv[])
{
//	cout<<"\nEstimating tissue thresholds for Fat and Lung from image..."<<endl;
    bool useLungs {false};
    int currentArg=1;
    
	if (argc < 3 )
	{
		Usage();
		return 0;
	}

	path file(argv[currentArg]);
	if (!exists(file))
	{
		cerr<<"\n File does not exists : "<<file<<"\n"<<endl;
		return 1;
	}else if (!is_regular_file(file))
	{
		cerr<<"\n"<<file<<" is not a file"<<"\n"<<endl;
		return 1;
	}
 
	string filename(argv[currentArg]);
    currentArg++;
	string filename_out(argv[currentArg]);
  
	//the air image should preserve more of the wall between lungs and trachea
    aurimage<short> imAir;
    imAir.readImage(filename_out + "_air.nii.gz");
 
    
	vector<int> bounds{0,0,0,imAir.xsize()-1, imAir.ysize()-1, imAir.zsize()-1};
    aurimage<short> imSpeedMap = imAir;

    aursurface<float> surfLeftBronchus(filename_out + "_leftBronchusInit.vtk");
    surfLeftBronchus.InitDeform();
    aursurface<float> surfRightBronchus(filename_out + "_rightBronchusInit.vtk");
    surfRightBronchus.InitDeform();
  
//surfLeftBronchus.copyAnchoredVerticesToScalars();
//surfLeftBronchus.write(filename_out + "_leftBronchusAnchored.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);

     
    cout<<"left ronchus "<<endl;
    vertex<float> leftBronchusCut, rightBronchusCut;
//    vec3<float> leftBronchusCutNormal, rightBronchusCutNormal; 
  
    if ( !readCutPlanes( filename_out + "_airwayCuts.ssv", "leftBronchus_start", leftBronchusCut )) 
//    if (false)
        {

//        readCutPlanes( filename_out + "_airwayCuts.ssv", "leftBronchus_start", leftBronchusCut );
            cout<<"left bronchus cut "<<leftBronchusCut.to_string()<<endl;
            //make sure bifurcation is not already there

            aursurface<float> surfLeftLowerLobeBronchus, surfLeftUpperLobeBronchus;
            aursurface<float> surfLeft_B910,surfLeft_B10, surfLeft_B9;
            aursurface<float> surfLeft_B9a,surfLeft_B10a, surfLeft_B9b, surfLeft_B10b;
            aursurface<float> surfLeft_B78,surfLeft_B78a, surfLeft_B78b;
            aursurface<float> surfLeft_B78ai,surfLeft_B78aii, surfLeft_B78bi,surfLeft_B78bii;
//            { 
                cout<<"-----------------------\n Segment Left Bronchus \n-----------------"<<endl;
                deformationParameters<float> bronchusParams;
                bronchusParams.alpha_area= 0.03;
                bronchusParams.splitThreshold = 3;
//                    bronchusParams.alpha_sn = 0.01;
                //surface and initial cut point 
                 std::array< pair< aursurface<float>, vertex<float>, 2 > surfLeftLobes  = segmentAndCutSegment( imSpeedMap, surfLeftBronchus, leftBronchusCut,  bronchusParams);
   array< vertex<float>,2 > cutPoints_leftBronchus;
//                array< vertex<float>,2 > cutPoints_leftBronchus  = segmentAirwaySegment(imSpeedMap, surfLeftBronchus, surfLeftLowerLobeBronchus,surfLeftUpperLobeBronchus,leftBronchusCut,bronchusParams);
                surfLeftBronchus.write(filename_out + "_leftBronchus.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
                surfLeftLobes[0].write(filename_out + "_leftLowerLobeBronchus.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
                surfLeftLobes[1].write(filename_out + "_leftUpperLobeBronchus.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
        
        cout<<"number of vertices in the cut "<<surfLeftLobes[0].numberOfVertices()<<" "<<surfLeftLobes[1].numberOfVertices()<<endl;

//                cout<<"-----------------------\nCut Points-----------------------\n "<<cutPoints_leftBronchus[0].to_string()<<cutPoints_leftBronchus[1].to_string()<<"\n--------------------------------------\n"<<endl;
//                return 0;

if (false)
//                if ( successfulSegment(cutPoints_leftBronchus))
                {
                    deformationParameters<float> bronchusParams;
                    bronchusParams.alpha_area= 0.01;
                    bronchusParams.splitThreshold = 6;
                    bronchusParams.alpha_sn = 0.01;
                
//                    setNormal( cutPoints_leftBronchus[0], cutPlaneNormals[0]);
                    cout<<"-----------------------\n Segment  Left Lower Lobe Bronchus \n-----------------"<<endl;
                    cout<<"cutpoints left bronchus  "<<cutPoints_leftBronchus[0].to_string()<<" "<<cutPoints_leftBronchus[1].to_string()<<endl;
                    array< vertex<float>,2 > cutPoints_leftLowerLobe  = segmentAirwaySegment(imSpeedMap, surfLeftLowerLobeBronchus, surfLeft_B78,surfLeft_B910,cutPoints_leftBronchus[0],bronchusParams);
//                        surfLeftLowerLobeBronchus.write(filename_out + "_leftLowerLobeBronchus.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
                    if (  successfulSegment(  cutPoints_leftLowerLobe ) )
//                        if (false)
                    {
                        cout<<"-----------------------\n Segment  B78 Bronchus \n-----------------"<<endl;
                        
                    
                        deformationParameters<float> bronchusLowerParams;
                        bronchusLowerParams.alpha_area= 0.03;
                        bronchusLowerParams.splitThreshold = 2;
//                                       surfLeft_B78.deform(imSpeedMap,5000,bronchusLowerParams);

                        array< vertex<float>,2 > cutPoints_leftB78 = segmentAirwaySegment(imSpeedMap, surfLeft_B78,surfLeft_B78a,surfLeft_B78b,cutPoints_leftBronchus[0],bronchusLowerParams);
                            surfLeft_B78.write(filename_out + "_leftB78.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
                        if ( successfulSegment(cutPoints_leftB78))
                        {
                            deformationParameters<float> params;
                            bronchusLowerParams.alpha_area= 0.02;
                            bronchusLowerParams.splitThreshold = 3;
                            array< vertex<float>,2 > cutPoints_leftB78a = segmentAirwaySegment(imSpeedMap, surfLeft_B78a,surfLeft_B78ai,surfLeft_B78aii,cutPoints_leftBronchus[0],bronchusLowerParams);
                            array< vertex<float>,2 > cutPoints_leftB78b = segmentAirwaySegment(imSpeedMap, surfLeft_B78b,surfLeft_B78bi,surfLeft_B78bii,cutPoints_leftBronchus[0],bronchusLowerParams);
                            surfLeft_B78a.write(filename_out + "_leftB78a.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
                            surfLeft_B78b.write(filename_out + "_leftB78b.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
                            
                        }
                        cout<<"-----------------------\n Segment  B910 Bronchus \n-----------------"<<endl;
                        array< vertex<float>,2 > cutPoints_left910 = segmentAirwaySegment(imSpeedMap, surfLeft_B910,surfLeft_B9,surfLeft_B10,cutPoints_leftLowerLobe[1],bronchusLowerParams);
                                surfLeft_B910.write(filename_out + "_leftB910.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
                        if (successfulSegment(cutPoints_left910) )
                            {
                               array< vertex<float>,2 > cutPoints_leftB9 = segmentAirwaySegment(imSpeedMap, surfLeft_B9,surfLeft_B9a,surfLeft_B9b,cutPoints_leftBronchus[0],bronchusLowerParams);
                            array< vertex<float>,2 > cutPoints_leftB10 = segmentAirwaySegment(imSpeedMap, surfLeft_B10,surfLeft_B10a,surfLeft_B10b,cutPoints_leftBronchus[0],bronchusLowerParams);                             
                                
                                surfLeft_B9.write(filename_out + "_leftB9.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
                                surfLeft_B10.write(filename_out + "_leftB10.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
                                surfLeft_B9a.write(filename_out + "_leftB9a.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
                                surfLeft_B9b.write(filename_out + "_leftB9b.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
                                surfLeft_B10a.write(filename_out + "_leftB10a.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
                                surfLeft_B10b.write(filename_out + "_leftB10b.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
                            }
                    }
                }
//            } 
            aursurface<float> surfLeft_B123;
            aursurface<float> surfLeft_B3,surfLeft_B45;
            if(false)
            {            
                deformationParameters<float> bronchusLowerParams; 
                bronchusLowerParams.alpha_area= 0.01;
                bronchusLowerParams.splitThreshold = 6;
                 cout<<"-----------------------\n Segment  Left Upper Lobe Bronchus \n-----------------"<<endl;
//                setNormal( cutPoints_leftBronchus[1], cutPoints_leftBronchus[1]);
                array< vertex<float>,2 > cutPoints_leftLowerLobe  = segmentAirwaySegment(imSpeedMap, surfLeftUpperLobeBronchus, surfLeft_B123,surfLeft_B45,cutPoints_leftBronchus[1],bronchusLowerParams);
//                surfLeftUpperLobeBronchus.write(filename_out + "_leftUpperLobeBronchus.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
                
                bronchusLowerParams.alpha_area= 0.01;
                bronchusLowerParams.splitThreshold = 4;
               surfLeft_B123.deform(imSpeedMap,5000,bronchusLowerParams);
               surfLeft_B45.deform(imSpeedMap,5000,bronchusLowerParams);
                surfLeft_B123.write(filename_out + "_leftB123.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
                surfLeft_B45.write(filename_out + "_leftB45.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
                
            }
            
     
   }
                cout<<"RIGHT BRONCHUS"<<endl;
//    if (!readCutPlanes( filename_out + "_airwayCuts.ssv", "rightBronchus_start", rightBronchusCut ))
    if (false)
    {
            aursurface<float> surfRightLowerLobeBronchus, surfRightUpperLobeBronchus;
        	 deformationParameters<float> bronchusParams;
     bronchusParams.alpha_area= 0.01;
        bronchusParams.splitThreshold = 6;
//        
        array< vertex<float>,2 > cutPoints_rightBronchus  = segmentAirwaySegment(imSpeedMap, surfRightBronchus, surfRightLowerLobeBronchus,surfRightUpperLobeBronchus,rightBronchusCut,bronchusParams);
//        array< vertex<float>,2 > cutPoints_rightBronchus  = segmentAirwaySegment(imSpeedMap, surfRightBronchus, surfRightUpperLobeBronchus,surfRightLowerLobeBronchus,rightBronchusCut,bronchusParams);
        surfRightBronchus.write(filename_out + "_rightBronchus.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
        surfRightLowerLobeBronchus.write(filename_out + "_rightLowerLobeBronchus.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
        surfRightUpperLobeBronchus.write(filename_out + "_rightUpperLobeBronchus.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
                 cout<<"-----------------------\n Segment  Right Bronchus \n-----------------"<<endl;

    }    

//{
//    
//    cout<<"findbirufrcation for left bronchus" <<endl;
//    array< vertex<float>, 2>  cutPlanePoints_LBronchus = scanForBifurcation(surfLeftBronchus,imSpeedMap,cutPlanePoints[0], cutPlaneNormals[0]);
//    cout<<"do split "<<endl;
//    aursurface<float> surf_LeftLowerLobeBronchus = surfLeftBronchus.splitSurface(carinaTriangles[0][0], cutPlanePoints_LBronchus[0], cutPlaneNormals[0]);
//    surf_LeftLowerLobeBronchus.write(filename_out + "_leftLowerLobeBronchus.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);
//    cout<<"findbirufrcation forrighft bronchus" <<endl;
//    scanForBifurcation(surfRightBronchus,imSpeedMap,cutPlanePoints[1], cutPlaneNormals[1]);
//    array< vertex<float>,2> cutPlanePoints;
//    array< vec3<float>,2> cutPlaneNormals = cutBronchus( imSpeedMap, surfLeftBronchus, closestContourPoints,carinaTriangles,cutPlanePoints , tracheaNormal);
//} 


}

