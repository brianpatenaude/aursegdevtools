/*
 * run_test.cpp
 *
 *  Created on: May 11, 2016
 *      Author: brian
 */
#include "aursurface/aursurface.h"

#include "AsrImage3D/AsrImage3D.h"
#include "aurcommon/miscfunctions.h"
#include "aurcommon/aurstructs_functions.hpp"
#include <glm/gtx/string_cast.hpp>
#include "segmentation_utils.h"
//STL includes
#include <iostream>
#include <vector>
#include <fstream>
#include <map>
//3rd party includes
#include <boost/filesystem.hpp>

using namespace std;
using namespace auris_segtools;
using namespace boost::filesystem;
void Usage(){
  cout<<"\n estimate_trachea <nifit file> <ouput base>"<<endl;
  cout<<"\n This program is used  to estimate the trachea \n"<<endl;

}
  
 
constexpr float areaThreshTrachea{19.625};//threshold in mm2. 3.14*r^2, where r = 2.5mm (looked at trachea stats) 
constexpr float areaThreshLungs{1256.0};//thrwshold is in mm, 3.14 * 20^2 (very low estimate of lungs, ensures further down


vector<unsigned int>  crossings( const vector<float> & histogram, const float & value  )
{
  unsigned int bin=0;
  vector<unsigned int> v_bins;
  for (auto i = histogram.begin(); i != histogram.end()-1 ; ++i,++bin)
    {
      //		cout<<"i "<<*i<<endl;
      if ( ((value < *i) && (value > *(i+1))) || ((value > *i) && (value < *(i+1)))  )
	v_bins.push_back(bin);
    }
  return v_bins;
}


 

      void removeTube( const string & filename , const string & filename_out,  AsrImage3D<short> & imTrachea,  aursurface<float> & surfTrachea)
    {
      AsrImage3D <short> imCT(filename);
      //create a mask of the trachea to identify the air tube
      imTrachea = 0; 
      surfTrachea.drawSurface(imTrachea,short(1)); //i=1 is reserve for seed 
      vec3<float> cog = surfTrachea.centroid(); 
      imTrachea.fill( cog.x/imTrachea.xdim() , cog.y/imTrachea.ydim()  , cog.z/imTrachea.zdim()  ,2 );    
      imTrachea.lowerThreshold(2,imTrachea);
      //                imTrachea.mask()
      imTrachea.writeImage(filename_out + "_trachea.nii.gz");
      //find th eairtube
      AsrImage3D<short> imTube(filename_out + "_chest.nii.gz");
      imTube.mask(imTrachea);
      imTube.dilate3x3();
      imCT.fillWithinMask(  imTube, imCT.value(cog.x/imTrachea.xdim() , cog.y/imTrachea.ydim()  , cog.z/imTrachea.zdim())   );

      imTube.writeImage(filename_out + "_tube.nii.gz");
      imCT.writeImage(filename_out + "_CTnoTube.nii.gz");

    }
    
    
void addToSkeleton( const aursurface<float> & airwaySurface,  AsrImage3D<short> & skeleton, const vertex<float> & cutPoint , const vec3<float> & cutPlane )
{
    
    
}


int main(int argc, char*argv[])
{
  //	cout<<"\nEstimating tissue thresholds for Fat and Lung from image..."<<endl;
  bool useLungs {false}, cleanCT{false},userDefinedSeed{false};
  int currentArg=1;
  bool argParsing = true; 
   
  vec3<float> tracheaCOG(0,0,0);
  vertex<float> carina(0,0,0);
     

  while (argParsing)
    {
      string argname{argv[currentArg]};
      cout<<"arg "<<argname<<endl;
      if (  argname == "--useLungs" )
        {
	  cout<<"Use lungs instead of air "<<endl;
	  argc--;
	  useLungs=true;
	  currentArg++;
	  cout<<"currentarg "<<currentArg<<endl;

        }else if (  argname == "--cleanCT" )
        {
	  cout<<"cleanCT "<<endl;
	  argc--;
	  cleanCT=true;
	  currentArg++;
	  cout<<"currentargCT "<<currentArg<<endl;

                        
        }else if(argname == "--seed" ){
	cout<<"seed "<<endl;
	argc--;
	tracheaCOG.x = atof(argv[currentArg++]);
	argc--;
	tracheaCOG.y = atoi(argv[currentArg++]);
	argc--; 
	tracheaCOG.z = atoi(argv[currentArg++]);
	argc--;
            
	userDefinedSeed=true;
	currentArg++;
      }else{
        
	argParsing=false;
      }
    }
  cout<<"currentarg "<<currentArg<<endl;
  if (argc < 3 )
    {
      Usage();
      return 0;
    } 
 
  path file(argv[currentArg]);
  if (!exists(file))
    {
      cerr<<"\n File does not exists : "<<file<<"\n"<<endl;
      return 1;
    }else if (!is_regular_file(file))
    {
      cerr<<"\n"<<file<<" is not a file"<<"\n"<<endl;
      return 1;
    }

  string filename(argv[currentArg]);
  currentArg++;
  string filename_out(argv[currentArg]);

  //	cout<<"Read in file: "<<filename<<endl;

  //	AsrImage3D<float> im0(filename);

  //the air image should preserve more of the wall between lungs and trachea
  AsrImage3D<short> imAir;
    imAir.readImage(filename);
//  if (useLungs)
//    {
//      imAir.readImage(filename_out + "_lungs.nii.gz");
//    }else{
//    imAir.readImage(filename_out + "_air.nii.gz");
//
//  }     
  AsrImage3D<short> imLungs(filename_out + "_lungs.nii.gz");
    

    
  vector<int> bounds{0,0,0,imAir.xsize()-1, imAir.ysize()-1, imAir.zsize()-1};
	
  AsrImage3D<short> imTrachea = imAir;
  imTrachea=0;
  AsrImage3D<short> imSkeleton = imAir;
  imSkeleton=0;
  AsrImage3D<short> imSpeedMap = imAir;
		
  AsrImage3D<short> imCog = imAir;
  imCog=0;
  float voxel_area = imAir.xdim() *imAir.ydim();

	
  //scan through image to look for the trachea 
  //note that _air and _lungs image are same size
  for (int z = imLungs.zsize() -1 ; z>=0 ; --z  )
    {
      //		cout<<"z "<<z<<endl;
      vector<int> bounds_roi{bounds[0],bounds[1],z,bounds[3],bounds[4],z};
      sizeCog l_sizes_cog = imLungs.connectedComponents(imLungs,bounds_roi,0);
      //		cout<<"number of ROIs "<<l_sizes_cog.size()<<endl;
      int possibleTracheaCount{0}, possibleLungsCount{0};
      sizeCog validCogs;
      //                    cout<<"-------"<<endl;

      for (auto& iter :  l_sizes_cog)
	{
	  //			cout<<"area "<<get<0>(iter)*voxel_area<<" "<<get<1>(iter).x<<" "<<get<1>(iter).y<<" "<<get<1>(iter).z<<endl;
	  float area = get<0>(iter)*voxel_area;
	  if ( area > areaThreshLungs)
	    {
	      validCogs.push_back(iter);
	      ++possibleLungsCount;
	    }
	  else if (  area > areaThreshTrachea)
	    {
	      validCogs.push_back(iter);

	      ++possibleTracheaCount;
	    }

 
 
	}
      //                    cout<<"-------"<<endl;
 
      //		cout<<"check "<<possibleTracheaCount<<" "<<possibleLungsCount<<" "<<validCogs.size()<<endl;
      //3 and 2 because lungs will double count
      //only for more than one trachea possibility, assumption is that trachea is largest amongst noise
      if ((possibleTracheaCount >= 1) && (possibleLungsCount == 2 ))
	{
			
	  //			validCogs.sort();
	  //			auto iter = validCogs.begin();
	  //			//take the middle cog
	  //			tracheaCOG=get<1>(*(++iter));
	  auto riter = validCogs.rbegin();
	  array<float,3> cog_x;
	  for ( int i = 0 ; i < 3 ;++i,++riter )
	    {
	      cog_x[i]=get<1>(*riter).x ;
	      cout<<"cogx "<<i<<" "<<cog_x[i]<<" "<< imCog.xdim()<<endl;
	      imCog.value(get<1>(*riter).x / imCog.xdim()+0.5,get<1>(*riter).y / imCog.ydim()+0.5,get<1>(*riter).z / imCog.zdim()+0.5 )=1;
	    }


	  //find the middle 
	  riter = validCogs.rbegin();
	  if ( ( (cog_x[0] > cog_x[1]) && (cog_x[0] < cog_x[2]) ) || 
	       ( (cog_x[0] < cog_x[1]) && (cog_x[0] > cog_x[2]) ))  
	    {
				
	      tracheaCOG=get<1>(*riter);
	    }else if  ( ( (cog_x[1] > cog_x[0]) && (cog_x[1] < cog_x[2]) ) || 
			( (cog_x[1] < cog_x[0]) && (cog_x[1] > cog_x[2]) ))  
	    {
				
	      tracheaCOG=get<1>(*(++riter));
	    }else{
	    ++riter;
	    tracheaCOG=get<1>(*(++riter));

	  }


	  break;
	}
    }

  ofstream fout(filename_out + "_tracheaSeed.txt");
  if (fout.is_open())
    {
      cout<<"seed "<<tracheaCOG.x<<" "<<tracheaCOG.y<<" "<<tracheaCOG.z<<" "<<(tracheaCOG.x / imCog.xdim())<< \
	" "<<(tracheaCOG.y / imCog.ydim())<<" "<<(tracheaCOG.z / imCog.zdim())<<endl;
      fout<<tracheaCOG.x<<" "<<tracheaCOG.y<<" "<<tracheaCOG.z<<" "<<(tracheaCOG.x / imCog.xdim())<< \
	" "<<(tracheaCOG.y / imCog.ydim())<<" "<<(tracheaCOG.z / imCog.zdim())<<endl;
      fout.close(); 
    }
  imCog.writeImage(filename_out + "_lungs_COG.nii.gz");
	
  aursurface<float> airwaySurface;
 
  
 
	
  airwaySurface.createSphere(tracheaCOG,1.0f,10,10); 
  cout<<"Deform "<<endl;
  airwaySurface.drawSurface(imTrachea,short(1));

    
 

  imTrachea.writeImage(filename_out + "_lungs_COG_sphere.nii.gz");
 
  vector<  aurcontour<float> > all_contours;
     
            airwaySurface.setReferenceIntensity( imSpeedMap.interpolate( to_vertex(airwaySurface.centroid()) ));

  vertex<float> tracheaCut( tracheaCOG.x, tracheaCOG.y, tracheaCOG.z, 0,0,-1,0);
    vec3<float> cutNormal(0,0,-1);
    writeCutPointToFile(filename_out + "_airwayCuts.ssv","trachea_start",tracheaCut,cutNormal);
    deformationParameters<float> params;
//    params.splitThreshold = 8.0;
             params.splitThreshold = 0.25;
                params.alpha_area = 0.01;
                params.alpha_sn = 0.0;
                params.alpha_st = 0.1;
                params.max_step_size = 0.1;
    
      unsigned int same_count{0};
        unsigned int NVertices_prev{0};
       aursurface<float>  airwaySurface_prev = airwaySurface;
       unsigned int Niteration = 10000/iStep;
       for (int i = 0 ; i  < Niteration ; ++i ) {
                cout << "Deform  " << i * iStep << " / " << iStep * Niters << " "<<airwaySurface.getNumberOfVertices()<<endl;
                cout<<params.to_string()<<endl;
                airwaySurface.deform(imSpeedMap, iStep, params.alpha_sn, params.alpha_st, params.alpha_area, params.alpha_im, params.max_step_size,
                                     params.splitThreshold, aursurface<float>::NEG_GRAD);
                        cout<<"do sefl-intersection test "<<endl;
                    if (airwaySurface.selfIntersects()) {
                                cout << "!!!FOUND  self intersectiion!!!!" << endl;

                                if (( (params.alpha_st + ALPHA_ST_STEP) > MAX_ALPHA_ST ) && ( ( params.splitThreshold  - SPLIT_THRESH_STEP )  < SPLIT_THRESH_MIN ))
                                    return 1;
    

                                if ( (params.alpha_st + ALPHA_ST_STEP ) < MAX_ALPHA_ST ) {
                                    params.alpha_st += ALPHA_ST_STEP;
                                }
                                if ( (params.splitThreshold  - SPLIT_THRESH_STEP) < SPLIT_THRESH_STEP ) {
                                    params.splitThreshold -= SPLIT_THRESH_STEP;
                                }
                                
                                airwaySurface = airwaySurface_prev;
                                continue;
//                                return 1;
                                // bifurcationCutPoints;
                        }else{
                            airwaySurface_prev = airwaySurface;
                        }
                        cout<<"done sefl-intersection test "<<endl;
//                }              
                                     
                array<float, 6> surfBounds = airwaySurface.bounds();
                cout<<"airwayBounds "<<surfBounds[0]<<" "<<surfBounds[1]<<" "<<surfBounds[2]<<" "<<surfBounds[3]<<" "<<surfBounds[4]<<" "<<surfBounds[5]<<endl;                   
                unsigned int NVertices = airwaySurface.getNumberOfVertices();
                if (NVertices_prev == NVertices)
                {
                    same_count++;
                }
                else{
                    NVertices_prev = NVertices;
                }
                if (same_count == SAME_COUNT_THRESH )
                {
                    params.splitThreshold -=0.5;
                    same_count=0;
                }
                cout << "Iterations/Z-cut/Number of Contours : " << i * iStep << " / " << iStep * Niters << "    "
                     << "    " << endl;
                ++i; // for moniroting iterations
        }
    
 
      airwaySurface.write(filename_out + "_airways.vtk",auris_segtools::aursurface<float>::WRITE_FORMAT::VTK);

}

