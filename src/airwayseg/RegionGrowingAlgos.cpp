#include "RegionGrowingAlgos.h"

#include <unordered_set>

using namespace std;

namespace auris_segtools
{
template <class T> RegionGrowingAlgos<T>::RegionGrowingAlgos()
{
}

template <class T> RegionGrowingAlgos<T>::~RegionGrowingAlgos()
{
}

template <class T>
void RegionGrowingAlgos<T>::FillHoles(const AsrImage3D<T>& image, unordered_set<unsigned int>& seg_voxels)
{
    // fill holes based on connectivity=1 is just the ortho directions
    unsigned int connectivity{ 1 };
    unsigned int conThresh{ 4 };
    if(connectivity == 3)
        conThresh = 20;
    auto neighboursOffsets = image.GetNeighbourOffsets(connectivity);
    unordered_map<unsigned int, unsigned int> index_2_neighbourCount;
    float mean{ 0 }, stdev{ 0 };
    // get nieghour count for each voxel
    for(auto& vox : seg_voxels) {
        T val = image.value(vox);
        mean += val;
        stdev += val * val;
        for(auto& neigh : neighboursOffsets) {
            index_2_neighbourCount[vox + neigh]++;
        }
    }
    // mean is Sx at this point
    // stdev is Sxx
    stdev = sqrtf((stdev - mean * mean / seg_voxels.size()) / (seg_voxels.size() - 1));
    mean /= seg_voxels.size();
    //    cout << "FillHoles " << mean << " " << stdev << endl;
    // get nieghour count for each voxel
    float upperThresh = mean + 3 * stdev;
    for(auto& vox : index_2_neighbourCount) {
        if((vox.second > conThresh) || (image.value(vox.first) < upperThresh)) {
            // sets are unqiue so doesn't matter that I repeat
            seg_voxels.insert(vox.first);
        }
    }
}

template <class T>
void RegionGrowingAlgos<T>::GrowRegionIncrementalThreshold(const AsrImage3D<T>& image,
    const AsrImage3D<float>& distanceMap,
    const vec3<int>& seed,
    const unsigned int& minSegmentSize,
    const unsigned int& maxSegmentSize,
    const float& initialThreshold,
    const int& connectivity,
    const float& maxGrowthFactor,
    const float& distanceThreshold,
//    const bool& decrementThreshold,
    const FillMethod& fill,
    AsrImage3D<float>& imAirways,
    unordered_set<unsigned int>& seg_voxels)
{

    // we care about how chnage in growth factor, leaks will exponential expand
    // may be better criteria than min segment size, or in conjunction with
    float maxGrowthFactorFactor{ 0.9 }; // ste slightly less than 1 to prevent leaks
    //    float distanceThreshold{20.0};
    float threshold{ initialThreshold };
    float fillValue(1.0);
    unsigned int sizePrev{ 0 }; // size of the current segmentation
    int count{ 0 };
    // prevGrowth factor is to look at changes in growth factor
    float prevGrowthFactor{ 1.0 };
    while(true) {
        ++count;
        unsigned int size{ 0 };
        unordered_set<unsigned int> vox_forbidden, vox_mask, vox_mask_prev; // are here for development
        //        size = image.fillDevelop(seed.x, seed.y, seed.z, distanceMap, ++fillValue, ++threshold,
        //            maxSegmentSize, vox_forbidden, vox_mask,distanceThreshold, fill, connectivity);
        unsigned int maxSize{ maxSegmentSize };

        // remove for pig
        if(sizePrev > minSegmentSize) { // should speed up algorithm by not over growing
            if(maxSegmentSize > (sizePrev * maxGrowthFactor)) {
                maxSize = (sizePrev * maxGrowthFactor) + 1;
            }
        }

//        threshold += decrementThreshold ? -1 : 1;
//        size = image.fillDevelop(seed.x, seed.y, seed.z, distanceMap, ++fillValue, threshold, maxSize, vox_forbidden,
//            vox_mask, distanceThreshold, fill, connectivity);
        size = image.fillDevelop(seed.x, seed.y, seed.z, distanceMap, ++fillValue, ++threshold, maxSize, vox_forbidden,
            vox_mask, distanceThreshold, fill, connectivity);
        // in here runs witgh every fill
        // Has advantage of incorporating in growth estimate
        // disadvatange will be that it's slower
        // cleaup inside mask
        //        FillHoles(image, vox_mask);
        //        FillHoles(image, vox_mask);
        // adjust size after cleanup
        //        size = vox_mask.size();

        float growthFactor{ static_cast<float>(size) / sizePrev };
        //        cout << "thresh " << threshold << " size " << size << " growthfactor " << growthFactor << " "
        //             << growthFactor / prevGrowthFactor << endl;
        //        cout << "segmentSize " << minSegmentSize << " " << maxSegmentSize << endl;
        // the change in growth factor means that the growfact6or must be incrasing
        if(((growthFactor > maxGrowthFactor) && (sizePrev > minSegmentSize) &&
               ((growthFactor / prevGrowthFactor) > maxGrowthFactorFactor) && (count > 1)) ||
            (size > maxSegmentSize)) {

            //            cout << "looking for leak" << endl;
            // this finds the difference between the previous and current segmenttation
            //            for(auto i = vox_mask_prev.begin(); i != vox_mask_prev.end(); ++i) {
            //                vox_mask.erase(*i);
            //            }
            cout << "growthfactors " << prevGrowthFactor << " " << growthFactor << endl;
            break;
            //            }
        }
        vox_mask_prev = vox_mask;
        sizePrev = size;
        //                    cout << "threshold  " << threshold << endl;
        //            cout << "ChidlCount " << childCount << " " << voxSeed.to_string() << endl;

        prevGrowthFactor = growthFactor;
    }

    // TODO DECREMENT DISTANCE_THRESHOLD UNTIL IS WITHIN GROWTH FACTOR
    cout << "final threshold " << (threshold - 1) << endl;
    //    imAirways=0;//clear history
    //    if (distanceThreshold == 0.0 ){
//    threshold += decrementThreshold ? -1 : 1;
    --threshold;
    //    }

    // fillDevelop applies distanceThreshold, so let leak , then limit distance
    // vox_forbideden is for development to be able to clog holes
    unordered_set<unsigned int> vox_forbidden; // are here for development
    image.fillDevelop(seed.x, seed.y, seed.z, distanceMap, 1.0f, threshold, maxSegmentSize, vox_forbidden, seg_voxels,
        distanceThreshold, fill, connectivity);
    //        FillHoles(image,seg_voxels);
    //        FillHoles(image,seg_voxels);

    //    for (auto & i_seg : seg_voxels){
    //
    //    }

    //        imAirways.writeImage("algoAirway_"+ to_string(seed.x) + "_"+ to_string(seed.y) + "_"+ to_string(seed.z)
    //        +".nii.gz");
}

template class RegionGrowingAlgos<short>;
template class RegionGrowingAlgos<float>;
}
