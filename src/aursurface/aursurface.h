/*
 * aursurface.h
 *
 *  Created on: May 13, 2016
 *      Author: brian
 */

#ifndef AURSURFACE_H_
#define AURSURFACE_H_
#include "aurcommon/aurstructs.h"
#include "aurgeometry/aurgeometry.h"
#include "AsrImage3D/AsrImage3D.h"
#include "aursurface/aurcontour.h"
// GL
#define GL_GLEXT_PROTOTYPES
#include "GL/gl.h"

// GLM library
//#include <glm/mat4x4.hpp> // glm::mat4
//#include <glm/gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective
//#include <glm/gtx/string_cast.hpp>
#include <Eigen/Core>

// STL
#include <list>
#include <set>
#include <tuple>
#include <unordered_map>
#include <unordered_set>
#include <vector>

//#include<pair>
namespace auris_segtools
{

// going to assume float or double
template <class T> class aursurface
{
    //    template <class T2> class aurcontour<T2>;
    //  template<class T2, class Allocator = std::allocator<T2>>
    //  class aurcontour;

    //	using lineSegment = std::tuple< vertex<T>, vertex<T> >;
    using lineSegment = std::tuple<unsigned int, unsigned int>;
    // use four to preserve ordering
    // the fifth element is the originating trinagle index
    using triangleSides = std::tuple<unsigned int, unsigned int, unsigned int, unsigned int, unsigned int>;

public:
    enum SPEEDFUNC { SAMPLE, NEG_GRAD };
    enum WRITE_FORMAT { VTK, STL, OBJ };
    enum SPLIT_CRITERIA { AREA, CURVATURE };
    enum ARROW_FORMAT { START_AT_CENTER, END_AT_CENTER };
    aursurface();
    aursurface(const std::string& filename);

    virtual ~aursurface();
    void InitDeform();

    //	T areaThreshold_{2};

    void write(const std::string& filename, WRITE_FORMAT format = VTK);
    void readVTK(const std::string& filename);
    void readSTL(const std::string& filename);
  
    void readSuperDMesh(const std::string& filename,  const vec3<int> imSize, const vec3<float> & imDim  );
    std::vector<vertex<T> >& getVertices()
    {
        return m_vertices;
    }
    std::vector<unsigned int>& getTriangles()
    {
        return m_triangles;
    }
    unsigned int getNumberOfVertices()
    {
        return m_vertices.size();
    }
    unsigned int getNumberOfAnchoredVertices()
    {
        return m_anchoredVertices.size();
    }
    unsigned int getNumberOfIndices()
    {
        return m_triangles.size();
    } // number of triangle indices (i.e. 3*number of triangles)

    void setReferenceIntensity(const T& ref)
    {
        m_referenceIntensity = ref;
    }
    T getReferenceIntensity()
    {
        return m_referenceIntensity;
    }

    std::array<T, 6> bounds();
    std::array<T, 2> zbounds(const vertex<T>& planePoint, const vec3<T>& planeNormal);
    bool empty() const
    {
        return (m_vertices.empty());
    };

    vec3<T> centroid();
    vertex<T> GetMaximumZVertex();
    void printTriangleVertices(const unsigned int& triangleIndex) const;
    void printVertex(const unsigned int& vertexIndex) const;
    void printVertices() const;
    void printTriangles() const;

    // create primitive meshes
    template <class T2> int createDiamondPrism(const vec3<T>& center, const T2& radius);

    // borrowed the algorithm I wrote in my personal copy of fslsurface (there is no ownshership of the fslsurface
    // edits)
    template <class T2>
    int createSphere(const vertex<T>& center, const T2& radius, const int& slices, const int& stacks);
    template <class T2> int createSphere(const vec3<T>& center, const T2& radius, const int& slices, const int& stacks);

    template <class T2>
    int createArrow(const vec3<T>& center,
        const vec3<T>& vdir,
        const T2& length,
        const T2& radius,
        const int& slices,
        const T2& tipSize,
        ARROW_FORMAT format = START_AT_CENTER);

    void append(aursurface<T>& surf);

    // GL functions
    void bufferData(const GLuint& vertexBuffer, const GLuint& vertexElementsBuffer, GLenum usage = GL_STATIC_DRAW);

    // Deformable models function
    void translate(const vec3<T>& txyz);
    void scale(const T& scale);
    void scale(const vec3<T>& scale);
    void rotateX(const T& radians);
    void rotateY(const T& radians);
    void rotateZ(const T& radians);

    bool isTriangleAnchored(const unsigned int triangleIndex);

    std::list<aurcontour<T> > cutSurface(const vertex<T> point, const vec3<T>& normal);
    std::list<aurcontour<T> > cutSurface(const vertex<T> point);
    std::list<aurcontour<T> > createContour(std::list<triangleSides> l_triangleIntersections,
        const vertex<T>& p0,
        const vec3<T>& normal,
        const bool closedContoursOnly = true);

    template <class T2>
    void deform(AsrImage3D<T2>& imagein,
        const unsigned int& Niterations,
        const deformationParameters<T>& params,
        const SPEEDFUNC& sfunc);

    template <class T2>
    void deform(AsrImage3D<T2>& image,
        const unsigned int& Niterations,
        const T& alpha_sn,
        const T& alpha_st,
        const T& alpha_area,
        const T& alpha_im,
        const T& max_step_size,
        const T& splitThreshold,
        const SPEEDFUNC& sfunc);
    void splitTriangles(const T& area_threshold);
    aursurface<T> splitSurface(aursurface<T>& trimmedSurface,
        const unsigned int& seedTriangleIndex,
        const vertex<T>& p0,
        const vec3<T>& normal);
    void trimSurface();

    //	aursurface<T> splitSurface(  aursurface<T> & trimmedSurface, const unsigned int & seedTriangleIndex, const
    //vertex<T> & p0, const vec3<T> & normal,  const vec3<T> & prevNormal  );
    void anchorTriangle(const unsigned int& triangleIndex);
    void anchorAbovePlane(const vertex<T>& plane);
    void anchorBelowPlane(const vertex<T>& plane);
    //    bool areTrianglesNeighbours( const unsigned int & triangleIndex_0 , const unsigned int & triangleIndex_1 );
    bool selfIntersects();

    template <class T2> int drawSurface(AsrImage3D<T2>& image, const T2& fillValue);

    void setGradientThresholdNormalization(const T& norm);
    void setScalars(const T& sc);
    void colorTriangle(const unsigned int& triangleIndex, const T& sc);
    void colorSideOfPlane(const vertex<T>& vPlane0, const vec3<T>& pNormal);

    void copyAreaToScalars();
    void copyRadiusToScalars();
    void copyCurvatureToScalars();
    void copyAnchoredVerticesToScalars();
    void copyDisplacementToScalars();
    void computeNeighbours();         // stores neighbouring vertcies
    void computeTriangleNeighbours(); // stores triangles that touch a given vertex
    void computeTriangleNormals();    // computes normals define by each triangle

    void computeTriangleCurvature(); // computers curvature at each triangle sum of curvature at vertices
    void computeVertexNormals();     // computer the normals at each vertex
    void computeMinimumNormalToTriangleIntersection();
    void computeRadiusAtVertex();
    unsigned int getClosestTriangle(const unsigned int& triangleIndex)
    {
        return closestTriangle_[triangleIndex];
    }
    vec3<T> getTriangleNormal(const unsigned int& triangleIndex)
    {
        return m_triangleNormals[triangleIndex];
    }

    vertex<T> getTriangleCentroid(const unsigned int& triangleIndex);

protected:
    static int32_t ReverseInt(const int32_t inInt);
    static float ReverseFloat(const float inFloat);
    static T meanDisplacement(const std::list<vertex<T> >& l_verts);
    void computeTriangleAreas(); // computers area of each triangle,uses normals, should recompute normals before
                                 // calling this
    void computeMediumNeighbour();
    void computeMaxTriangleForce();

    unsigned int addVertices(const T& areaThreshold,
        const SPLIT_CRITERIA& splitType); // add vertices when triangle are too big
    //    void swapVertices( const unsigned int & index0, const un);

    void writeVTK(std::ofstream& fout);
    void writeSTL(std::ofstream& fout);
    void writeOBJ(std::ofstream& fout);
    // geometric intersections functions
    //	static int sideOfPlane( const vertex<T> & vert0, const vertex<T> & vPlane0, const vec3<T> & pNormal );
    //	static int trianglePlaneIntersection( const vertex<T> & vTri0, const vertex<T> & vTri1, const vertex<T> & vTri2,
    //const vertex<T> & vPlane0, const vec3<T> & pNormal );
    std::unordered_map<unsigned int, T>& getData(const SPLIT_CRITERIA& splitType);

    /// store information regarding which vertices neighbour each vertex and which triangles neighbour each vertex
    std::unordered_map<unsigned int, std::unordered_set<unsigned int> > neighbours_, neighboursm_triangles;
    std::unordered_map<unsigned int, vec3<T> > m_triangleNormals, mediumNeighbours_, maxTriangle_;
    std::unordered_map<unsigned int, T> triangleAreas_, m_triangleCurvature, meanTriangleArea_, estimatedDiameter_,
        radiusAtVertex_, m_curvature;
    std::unordered_map<unsigned int, std::list<vertex<T> > > displacementMag_; // let's keep a history
    std::unordered_map<unsigned int, unsigned int> closestTriangle_;           // this is calculated when
                                                                     // computeMinimumNormalToTriangleIntersection() is
                                                                     // called,closest in terms of along normal
    std::unordered_set<unsigned int> m_anchoredVertices;
    std::set<unsigned int> m_markedForDeletion; // want it order because of the trim operation
    std::vector<vertex<T> > m_vertices;
    std::vector<unsigned int> m_triangles;
    T m_inflationGradThreshold{ 150 }, m_inflationGradNorm{ 1.0 }, m_referenceIntensity{ 0.0 };
    T m_gradStep{ 0.5 };
    T m_anchorThresh{ 0.0001 };
    T m_displacementHistory{ 10 };
    std::string m_name;
    
};

//

template <class T, class T2>
int drawLine(AsrImage3D<T2>& image, const vec3<T>& v0, const vec3<T>& v1, const float& stepValue, const T2& fillValue)
{
    // step size for scanning along triangle
    vec3<T> v_dif = v1 - v0;
    float xdim = image.xdim();
    float ydim = image.ydim();
    float zdim = image.zdim();
    // length of vector divided by step size
    // truncate so not to overshoot the vertex
    int Nsteps{ static_cast<int>(l2norm(v_dif) / stepValue) };
    if(Nsteps == 0) // just draw vertices
    {
        image.value(v0.x / xdim, v0.y / ydim, v0.z / zdim) = fillValue;
        image.value(v1.x / xdim, v1.y / ydim, v1.z / zdim) = fillValue;

        return 0;
    } else {
        // precompute step in voxels , void multiplication and division
        T stepx = v_dif.x / Nsteps / xdim;
        T stepy = v_dif.y / Nsteps / ydim;
        T stepz = v_dif.z / Nsteps / zdim;
        T x{ v0.x / xdim }, y{ v0.y / ydim }, z{ v0.z / zdim };
        for(int i = 0; i <= Nsteps; ++i, x += stepx, y += stepy, z += stepz) {
            int xw{ static_cast<int>(x + 0.5) }, yw{ static_cast<int>(y + 0.5) }, zw{ static_cast<int>(z + 0.5) };
            if((xw < 0) || (yw < 0) || (zw < 0)) // bounds check
                continue;
            image.value(xw, yw, zw) = fillValue;
        }
    }

    return 0;
}

} /* namespace auris_segtools */

#endif /* AURSURFACE_H_ */
