/*
 * run_test.cpp
 *
 *  Created on: May 11, 2016
 *      Author: brian
 */
#include "aursurface.h"

//STL includes
#include <iostream>

//3rd party includes
#include <boost/filesystem.hpp>

using namespace std;
using namespace auris_segtools;
using namespace boost::filesystem;
void Usage(){
	cout<<"\n run_test "<<endl;
	cout<<"\n This program is used to test the surface library\n"<<endl;

}

int main(int argc, char*argv[])
{
	cout<<"\nRunning aursurface library test..."<<endl;
	if (argc == 0 )//disbaled for now
	{
		Usage();
		return 0;
	}

	//create surface
	aursurface<float> surf;
	cout<<"Creating Diamond Prism..."<<endl;
	if (surf.createDiamondPrism(vec3<float>(0,0,0),1.0f))
		{
		cout<<"Failed to create Diamond Prism"<<endl;
		}
	cout<<"Diamond Prism:"<<endl;
	surf.printVertices();



}

