/*
 * run_test.cpp
 *
 *  Created on: May 11, 2016
 *      Author: brian
 */
#include "aurgeometry/aurgeometry.h"
#include "aursurface/aursurface.h"
#include "aurcommon/aurstructs_functions.hpp"
#include <Eigen/Core>

//STL includes
#include <iostream>
#include <fstream>

using namespace std;
using namespace auris_segtools;
using namespace Eigen;

void Usage(){
	cout<<"\n test_insideConvexPolygon <x> <y> <z> "<<endl;
	cout<<"\n This program tests whether a point is inside a convex polygon \n"<<endl;

}
int main(int argc, char*argv[])
{
    if (argc < 3 )
    {
        Usage();
        return 0; 
    }
    float x = atof(argv[1]);  
    float y = atof(argv[2]);   
    float z = atof(argv[3]);
 
    cout<<"Test Vertex x/y/z "<<x<<" "<<y<<" "<<z<<endl;
    
    
    
    aurcontour<float> triangle;  
    triangle.push_back(vertex<float>(0,0,0));
    triangle.push_back(vertex<float>(0,1,0));
    triangle.push_back(vertex<float>(1,0,0));

    cout<<"triangle: "<<endl;
    triangle.printVertices();
    cout<<"Winding number "<<triangle.insideConvexPolygon(vertex<float>(x,y,z))<<endl;

} 