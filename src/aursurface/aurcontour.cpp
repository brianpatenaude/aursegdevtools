/*
 * aursurface.cpp
 *
 *  Created on: May 13, 2016
 *      Author: brian
 */

#include "aurcommon/aurstructs_functions.hpp"
#include "aursurface.h"

// STL
#include <array>
#include <fstream>
#include <iostream>
#include <sstream>
#include <unordered_map>
#include <unordered_set>

using namespace std;
using namespace Eigen;
// template< class T> using aursurface<T>::contour;
namespace auris_segtools
{

template <class T> aurcontour<T>::aurcontour()
{
    m_vertices = list<vertex<T> >();
}

template <class T> aurcontour<T>::~aurcontour()
{
}

template <class T> const std::array<unsigned int, 2>& aurcontour<T>::triangle(const unsigned int& index) const
{
    auto i = triangleMembership_.begin();
    advance(i, index);
    return *i;
}

template <class T> vertex<T> aurcontour<T>::centreOfGravity()
{
    vec3<T> cog;
    for(auto& i_vert : m_vertices) {
        cog += i_vert;
    }
    cog /= m_vertices.size();

    return vertex<T>(cog.x, cog.y, cog.z);
}

template <class T> T aurcontour<T>::maxDistanceSquared(const aurcontour<T>& targetContour)
{
    T distance{ 0.0 };
    for(auto& vertTarg : targetContour.m_vertices) { // for each target vertex find the minimum distance
        //		cout<<"vertex "<<vertTarg.coord_to_string()<<endl;
        auto vert0 = m_vertices.begin();
        auto vert1 = m_vertices.begin();
        vert1++;

        T distMin = distanceToSegment(vertTarg, m_vertices.front(), m_vertices.back());

        //        cout<<"dist "<<distMin<<endl;
        for(; vert1 != m_vertices.end(); ++vert1, ++vert0) {
            T dist = distanceToSegment(vertTarg, *vert1, *vert0);
            //			cout<<"dist "<<dist<<endl;
            if(dist < distMin) {
                distMin = dist;
            }
        }
        // its calculating the maximum distance, where diostance is the minimum distance to the contour
        if(distMin > distance) {
            distance = distMin;
        }
        // old way was to look at vertices instead of lin segments
        //		auto vert = m_vertices.begin();
        //		T distMin = l2normSquared( *vert- vertTarg );
        //		for ( ;  vert != m_vertices.end(); ++vert )
        //		{
        //			T dist = l2normSquared( *vert- vertTarg );
        //
        //			if (dist < distMin ){ distMin = dist; }
        //
        //		}
        //		if (distMin > distance) { distance = distMin; }
    }
    return distance;
}

template <class T> vertex<T> aurcontour<T>::closestVertexToRay(const vertex<T>& ray_start, const vec3<T>& ray_dir) const
{
    vertex<T> vertMin = vertex<T>(0, 0, 0);
    T distMin = 1e12;
    for(auto& v : m_vertices) {
        vec3<T> ray_to_vert = v - ray_start;
        //        normalize(ray_dir);
        T dot = dotProduct(ray_to_vert, ray_dir);
        T dist = l2normSquared(ray_to_vert - ray_dir * dot);
        if(dist < distMin) {
            distMin = dist;
            vertMin = v;
        }
    }
    return vertMin;
}

template <class T> vertex<T> aurcontour<T>::closestVertex(const vertex<T>& vert) const
{
    vertex<T> vertMin = vertex<T>(0, 0, 0);
    T distMin = 1e12;
    for(auto& v : m_vertices) {
        T dist = l2normSquared(vert - v);
        if(dist < distMin) {
            distMin = dist;
            vertMin = v;
        }
    }
    return vertMin;
}

template <class T> void aurcontour<T>::trimUpper(const vertex<T>& vert)
{
    for(auto i_v = m_vertices.begin(); i_v != m_vertices.end(); ++i_v) {
        if((i_v->x == vert.x) && (i_v->y == vert.y) && (i_v->z == vert.z)) {
            cout << "found vert " << m_vertices.size() << endl;
            i_v++;
            m_vertices.erase(i_v, m_vertices.end());
            cout << "found vert2 " << m_vertices.size() << endl;
            break;
        }
    }
}

template <class T> bool aurcontour<T>::onPolygon(const vertex<T>& vert, const vec3<T>& normal) const
{

    auto v_first = m_vertices.begin();
    auto v_second = m_vertices.begin();
    ++v_second;
    for(; v_second != m_vertices.end(); ++v_second, ++v_first) {
        if(sideOfLine(*v_second - *v_first, vert - *v_first, normal) == 0)
            return true; // line on line
    }
    // if got to end then all segmentas have been check
    return false;
}
template <class T>

int aurcontour<T>::insideConvexPolygon(const vertex<T>& v0) const
{
    // TODO check for whether point is inplane
    // TODO MAKE MORE EFFICIENT IMPLEMENTATION
    double sumAngles{ 0.0 };
    // cout<<"convex "<<m_vertices.size()<<endl;
    // cout<<"vertex "<<v0.coord_to_string()<<endl;

    // printVertices();

    auto c_vert0 = m_vertices.begin();
    auto c_vert1 = m_vertices.begin();
    advance(c_vert1, 1);
    for(; c_vert1 != m_vertices.end(); ++c_vert0, ++c_vert1) {
        vec3<T> dif0{ *c_vert0 - v0 }, dif1{ *c_vert1 - v0 };
        //        cout<<"angleFull "<<angle(dif0,dif1)<<" "<<angle(dif0,dif1) *180/M_PI<<endl;
        sumAngles += angle(dif0, dif1);
    }
    vec3<T> dif0{ *c_vert0 - v0 }, dif1{ m_vertices.front() - v0 };
    sumAngles += angle(dif0, dif1);
    //    cout<<"Sum of Angles "<<sumAngles<<" "<<sumAngles*180/M_PI<<" "<< (sumAngles >= (2*M_PI))<<((sumAngles -
    //    2*M_PI) > -WINDING_TOLERANCE ) <<endl;
    //    return static_cast<int>( sumAngles / (2*M_PI));
    return static_cast<int>((sumAngles + WINDING_TOLERANCE) / (2 * M_PI));
}
template <class T> void aurcontour<T>::deform()
{
    //
    //
    //	auto v_second = m_vertices.begin();
    //    auto v_third = m_vertices.begin();
    //    advance(v_second,1);
    //    advance(v_third,2);
    //
    //	for (auto v_first = m_vertices.begin();  ; v_first!= m_vertices.end(); ++v_first )
    //	{
    //            vec3<T> normal = *v_
    //            if (side == 0 ) return true; //line on line
    //            if ( side != initSide) return false; //lies on different side than previous
    //	}
    //   //if got to end then all segmentas have been check
    //    return true;
    //
    //
}

template <class T> bool aurcontour<T>::contains(const vertex<T>& vert_in) const
{
    for(auto& vert : m_vertices) {
        if((vert.x == vert_in.x) && (vert.y == vert_in.y) && (vert.z == vert_in.z))
            return true;
    }
    return false;
}

template <class T> T aurcontour<T>::area(vec3<T> normal) // it is assumed to be a planar contour
{
    T area{ 0 };
    if(m_vertices.size() < 3)
        return area;

    vertex<T> v0 = m_vertices.front();
    auto vi = m_vertices.begin();
    auto vii = m_vertices.begin();
    advance(vi, 1);
    advance(vii, 2);
    vec3<T> vArea(0, 0, 0);
    for(; vii != m_vertices.end(); ++vii, ++vi) {
        vArea += crossProduct(*vi - v0, *vii - v0);
    }
    //    cout<<"normal "<<l2norm(normal)<<endl;
    area = abs(dotProduct(vArea, normal));
    return 0.5 * area;
}

template <class T> void aurcontour<T>::printVerticesWithTriangles()
{
    auto i_tri = triangleMembership_.begin();
    for(auto i_v = m_vertices.begin(); i_v != m_vertices.end(); ++i_v, ++i_tri) {
        cout << i_v->x << " " << i_v->y << " " << i_v->z << " " << (*i_tri)[0] << " " << (*i_tri)[1] << endl;
    }
}

template <class T> void aurcontour<T>::printVertices() const
{
    for(auto& v : m_vertices)
        cout << v.x << " " << v.y << " " << v.z << " " << endl;
}
// need to keep vertex push and tesnor push synchronous
// add some logic to ensure synchrony, push back 0 tensors so that they always match in size
template <class T> void aurcontour<T>::push_back(const Eigen::Matrix<T, 3, 3>& mat)
{
    // should only enter this loop if added a vertex without a tensor at anypoint
    while(m_tensors.size() < m_vertices.size()) {
        m_tensors.push_back(Matrix<T, 3, 3>::Zero());
    }
    m_tensors.push_back(mat);
}

template <class T> void aurcontour<T>::push_back(const vertex<T>& vert, const Eigen::Matrix<T, 3, 3>& mat)
{
    // should only enter this loop if added a vertex without a tensor at anypoint
    while(m_tensors.size() < m_vertices.size()) {
        m_tensors.push_back(Matrix<T, 3, 3>::Zero());
    }
    m_vertices.push_back(vert);
    m_tensors.push_back(mat);
};

// subindex refers to which triangle each poitn shhould have 2
template <class T>
void aurcontour<T>::addTriangleIndex(const unsigned int& index,
    const unsigned int& subindex,
    const unsigned int& triangleIndex)
{
    // using linked-list so can do lots of push back
    // thie function really only intended to do one at a time
    while(index >= triangleMembership_.size())
        triangleMembership_.push_back(array<unsigned int, 2>{ 0, 0 });
    auto i_tri = triangleMembership_.begin();
    advance(i_tri, index);
    (*i_tri)[subindex] = triangleIndex;
    //	cout<<"add trinagle index "<<index<<" "<<subindex<<" "<<triangleMembership_.size()<<endl;
}

template <class T>
array<vertex<T>, 2> aurcontour<T>::nearestVertex(const aurcontour<T>& contourTarget,
    array<array<unsigned int, 2>, 2>& triangleIndices)
{ // find the closest vertuices between the controus

    //	cout<<"trianglesSrc "<<triangleMembership_.size()<<endl;
    //	for (auto& i : triangleMembership_)
    //		cout<<i[0]<<" "<<i[1]<<endl;
    //	cout<<"trianglesTarg "<<triangleMembership_.size()<<endl;
    //	for (auto& i : contourTarget.triangleMembership_)
    //		cout<<i[0]<<" "<<i[1]<<endl;

    array<vertex<T>, 2> minVertices{ vertex<T>(0, 0, 0), vertex<T>(0, 0, 0) };

    if(m_vertices.empty() || contourTarget.m_vertices.empty())
        return minVertices;

    T minDist{ l2norm(m_vertices.front() - contourTarget.m_vertices.front()) };

    minVertices[0] = m_vertices.front();
    minVertices[1] = contourTarget.m_vertices.front();
    triangleIndices[0] = contourTarget.triangleMembership_.front();
    triangleIndices[1] = triangleMembership_.front();
    auto i_triSrc = triangleMembership_.begin();
    for(auto& vertexSrc : m_vertices) {
        //        cout<<"tris "<<(*i_triSrc)[0]<<" "<<endl;
        auto i_triTarg = contourTarget.triangleMembership_.begin();
        for(auto& vertexTarg : contourTarget.m_vertices) {
            T dist = l2normSquared(vertexSrc - vertexTarg);
            //			cout<<"l2dist "<<dist<<endl;
            if(dist < minDist) {
                minVertices[0] = vertexSrc;
                minVertices[1] = vertexTarg;

                triangleIndices[0] = (*i_triSrc);
                triangleIndices[1] = (*i_triTarg);

                //				triangleIndices[0][0] = (*i_triSrc)[0];
                //				triangleIndices[0][1] = (*i_triSrc)[1];
                //
                //				triangleIndices[1][0] = (*i_triTarg)[0];
                //				triangleIndices[1][0] = (*i_triTarg)[1];
            }
            ++i_triTarg;
        }
        ++i_triSrc;
    }

    return minVertices;
}

template <class T> void aurcontour<T>::readEMcsv(const std::string& fname)
{

    //        cout<<"readEMcsv "<<fname<<endl;
    ifstream fin(fname.c_str());
    if(fin.is_open()) {

        string line;
        getline(fin, line); // discard first line
        //            cout<<"firstline "<<line<<endl;
        while(getline(fin, line)) {
            stringstream ss(line);
            string val;
            // return because of invalid file
            if(!getline(ss, val, ','))
                return;
            T x = stof(val);
            if(!getline(ss, val, ','))
                return;
            T y = stof(val);
            if(!getline(ss, val, ','))
                return;
            T z = stof(val);
            m_vertices.push_back(vertex<T>(x, y, z));
            //                cout<<m_vertices.back().coord_to_string()<<endl;
        }

        fin.close();
    }
}

template <class T> void aurcontour<T>::translate(const vec3<T>& txyz)
{
    for(auto& v : m_vertices) {
        v.x += txyz.x;
        v.y += txyz.y;
        v.z += txyz.z;
    }
}

template <class T> void aurcontour<T>::scale(const T& sx, const T& sy, const T& sz)
{
    for(auto& v : m_vertices) {
        v.x *= sx;
        v.y *= sy;
        v.z *= sz;
    }
}

template <class T> void aurcontour<T>::writeSkeletonCSV(const string& fname)
{
    ofstream fout(fname.c_str());
    if(fout.is_open()) {
        fout << "LinkId,x,y,z" << endl;
        for(auto& i : m_vertices) {
            fout << static_cast<int>(i.scalar) << "," << i.x << "," << i.y << "," << i.z << endl;
        }

        fout.close();
    }
}

template <class T> void aurcontour<T>::writeVTK(const std::string& fname)
{

    ofstream fout(fname.c_str());
    if(fout.is_open()) {
        // Just doing ASCII for now
        // just supporting float
        fout << "# vtk DataFile Version 2.0" << endl;
        fout << "This surface was generated by Auris Surgical Robotics Libraries" << endl;
        fout << "ASCII" << endl;
        fout << "DATASET POLYDATA" << endl;
        fout << "POINTS " << m_vertices.size() << " float" << endl;
        for(auto& vert : m_vertices)
            fout << vert.x << " " << vert.y << " " << vert.z << endl;

        fout << "LINES " << (m_vertices.size() - 1) << " " << (m_vertices.size() - 1) * 3 << endl;
        for(unsigned int i = 0; i < (m_vertices.size() - 1); ++i) {
            fout << 2 << " " << i << " " << (i + 1) << endl;
            ;
        }

        fout << "POINT_DATA " << m_vertices.size() << endl;
        fout << "SCALARS vertexScalars float 1" << endl;
        fout << "LOOKUP_TABLE default" << endl;
        for(auto& vert : m_vertices)
            fout << vert.scalar << endl;

        fout.close();
    }
}

template <class T> template <class T2> int aurcontour<T>::drawClosedContour(AsrImage3D<T2>& image, const T2& fillValue)
{
    if(m_vertices.size() < 2)
        return 1;

    float stepValue{ ((image.xdim() < image.ydim()) ? image.xdim() : image.ydim()) };
    stepValue = (image.zdim() < stepValue) ? image.zdim() : stepValue;
    stepValue *= 0.5f;

    drawLine(image, vec3<T>(m_vertices.back()), vec3<T>(m_vertices.front()), stepValue, fillValue);
    return drawOpenContour(image, fillValue);
}

template <class T> template <class T2> int aurcontour<T>::drawOpenContour(AsrImage3D<T2>& image, const T2& fillValue)
{
    if(m_vertices.size() < 2)
        return 1;

    float stepValue{ ((image.xdim() < image.ydim()) ? image.xdim() : image.ydim()) };
    stepValue = (image.zdim() < stepValue) ? image.zdim() : stepValue;

    stepValue *= 0.5f;

    auto v_first = m_vertices.begin();
    auto v_second = m_vertices.begin();
    ++v_second;
    //	advance(v_second,1);
    for(; v_second != m_vertices.end(); ++v_second, ++v_first) {
        drawLine(image, vec3<T>(*v_first), vec3<T>(*v_second), stepValue, fillValue);
    }

    return 0;
}

template <class T> aursurface<T> aurcontour<T>::getVerticesAsSpheres(const T& radius)
{
    aursurface<float> surfAllBalls;
    for(auto& vert : m_vertices) {
        aursurface<float> surfBall;
        surfBall.createSphere(vert, radius, 5, 5);
        surfAllBalls.append(surfBall);
    }
    cout << "return balls " << endl;
    return surfAllBalls;
}

// finds the desired  contour  based on inital point, point shold be in contour or contained within
// assumes no shares point between contoours and that they don't cross
// will find first contour that satifies search condition
// If none foune, returns empty contour
// only check if on polygon, which is case when using to cut
// assuems point is in plane
template <class T>
aurcontour<T>
aurcontour<T>::getContour(const std::list<aurcontour<T> >& all_contours, const vertex<T>& p0, const vec3<T>& normal)
{
    //    std::cout<<"getcontour "<<all_contours.size()<<std::endl;
    //    int cindex{0};
    for(auto& contour : all_contours) {
        //        std::cout<<"Checking contour "<<cindex<<" : ";
        if(contour.onPolygon(p0, normal)) {
            //                std::cout<<"onPolygon : yes  "<<std::endl;
            return contour;
        } else if(contour.insideConvexPolygon(p0) > 0) {
            //                std::cout<<"insideConvexPolygon : yes  "<<std::endl;
            return contour;
        }
        //            std::cout<<"no  "<<std::endl;
        //        ++cindex;
    }
    return aurcontour<T>();
}

template class aurcontour<float>;
template int aurcontour<float>::drawClosedContour<float>(AsrImage3D<float>& image, const float& fillValue);
template int aurcontour<float>::drawClosedContour<short>(AsrImage3D<short>& image, const short& fillValue);
template int aurcontour<float>::drawOpenContour<float>(AsrImage3D<float>& image, const float& fillValue);
template int aurcontour<float>::drawOpenContour<short>(AsrImage3D<short>& image, const short& fillValue);

} /* namespace auris_segtools */
