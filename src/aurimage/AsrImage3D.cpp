#include <AsrImage3D.h>

#include <array>
#include <aurcommon/aurstructs_functions.hpp>
#include <iostream>
#include <list>
#include <map>
#include <tuple>
#include <typeindex>
#include <typeinfo>
#include <unordered_map>
#include <unordered_set>

using namespace std;

namespace auris_segtools
{

// default constrcutro

template <class T>
AsrImage3D<T>::AsrImage3D()
    : m_v_size(3, 0)
    , m_v_dim{ 1, 1, 1 }
    , m_ndims{ 3 }
    , m_stride_y{ 2 }
    , m_stride_z{ 3 }
{
    unordered_map<std::type_index, int> type_names;

    type_names[std::type_index(typeid(uint8_t))] = DT_UINT8;
    type_names[std::type_index(typeid(int16_t))] = DT_INT16;
    type_names[std::type_index(typeid(int32_t))] = DT_INT32;
    type_names[std::type_index(typeid(float))] = DT_FLOAT32;
    cout << "Image type " << type_names[std::type_index(typeid(T))] << endl;
    m_nifti_datatype = type_names[std::type_index(typeid(T))];
    cout << "done constructor " << endl;
}

// allows to change data type
template <class T> void AsrImage3D<T>::initialize(const int& nifti_type, nifti_1_header* header)
{
    // if you initialize an existing image with itself, it will remain unchanged
    m_ndims = header->dim[0];
    int nvoxels = 0;
    m_v_size.resize(m_ndims);
    m_v_dim.resize(m_ndims);
    if(m_ndims > 0)
        nvoxels = 1;
    auto i_size = m_v_size.begin();
    auto i_dim = m_v_dim.begin();
    for(int i = 1; i <= m_ndims; ++i, ++i_size, ++i_dim) {
        *i_size = header->dim[i];
        *i_dim = header->pixdim[i];
        nvoxels *= *i_size;
    }

    updateStrides();
    m_data.resize(nvoxels, 0);

    if(m_nifti_header_ptr == nullptr)
        m_nifti_header_ptr = new nifti_1_header();
    *m_nifti_header_ptr = *header;
    m_nifti_header_ptr->datatype = nifti_type;
    memcpy(m_nifti_header_ptr->data_type, nifti_datatype_to_string(nifti_type), 10 * sizeof(char));
    m_nifti_datatype = nifti_type;
}

// assumes same data type
template <class T> void AsrImage3D<T>::initialize(nifti_1_header* header)
{
    // if you initialize an existing image with itself, it will remain unchanged
    m_ndims = header->dim[0];
    int nvoxels = 0;
    m_v_size.resize(m_ndims);
    m_v_dim.resize(m_ndims);
    if(m_ndims > 0)
        nvoxels = 1;
    auto i_size = m_v_size.begin();
    auto i_dim = m_v_dim.begin();
    for(int i = 1; i <= m_ndims; ++i, ++i_size, ++i_dim) {
        *i_size = header->dim[i];
        *i_dim = header->pixdim[i];
        nvoxels *= *i_size;
    }

    updateStrides();
    m_data.resize(nvoxels, 0);

    if(m_nifti_header_ptr == nullptr)
        m_nifti_header_ptr = new nifti_1_header();
    *m_nifti_header_ptr = *header;
    memcpy(m_nifti_header_ptr->data_type, nifti_datatype_to_string(m_nifti_header_ptr->datatype), 10 * sizeof(char));
    m_nifti_header_ptr->datatype = m_nifti_header_ptr->datatype;
    m_nifti_datatype = m_nifti_header_ptr->datatype;
}

template <class T> AsrImage3D<T>::AsrImage3D(const string& filename)
{

    readImage(filename);
    //	info();
}
template <class T> AsrImage3D<T>::AsrImage3D(const AsrImage3D<T>& image)
{
    //	cout<<"copy constrcutor"<<endl;
    initialize(image.m_nifti_header_ptr);
    m_data = image.m_data;
}

template <class T> AsrImage3D<T>::~AsrImage3D()
{
    //	cout<<"AsrImage3D destrcutor "<<endl;
    if(m_nifti_header_ptr != nullptr)
        delete m_nifti_header_ptr;

    //	cout<<"AsrImage3D destrcutor end"<<endl;
}

template <class T> AsrImage3D<T>& AsrImage3D<T>::operator+=(const AsrImage3D<T>& image)
{

    auto ii = image.data();
    for(auto i = m_data.begin(); i != m_data.end(); ++i, ++ii) {
        *i += *ii;
    }
    return *this;
}

template <class T> AsrImage3D<T>& AsrImage3D<T>::operator=(const AsrImage3D<T>& image)
{
    initialize(image.m_nifti_header_ptr);
    m_data = image.m_data;
    return *this;
}

template <class T> AsrImage3D<T>& AsrImage3D<T>::operator=(const T& value)
{
    for(auto& i : m_data)
        i = value;
    return *this;
}

template <class T> void AsrImage3D<T>::copyImage(AsrImage3D<T>& dest, const AsrImage3D<T>& src)
{
    dest.initialize(src.m_nifti_datatype, src.m_nifti_header_ptr);
    dest.m_data = src.m_data;
}

template <class T>
template <class T2>
void AsrImage3D<T>::copyImage(AsrImage3D<T2>& dest, const AsrImage3D<T>& src, const int& nifiti_datatype)
{
    dest.initialize(nifiti_datatype, src.m_nifti_header_ptr);
    //  auto i_dest = dest.data_vector().begin();
    const T* i_src = &(src.m_data[0]);
    // = src.data();
    for(auto i_dest = dest.data_vector().begin(); i_dest != dest.data_vector().end(); ++i_dest, ++i_src) {
        *i_dest = static_cast<T2>(*i_src);
    }
}
template <class T>
template <class T2>
void AsrImage3D<T>::copyImageInfo(AsrImage3D<T2>& dest, const AsrImage3D<T>& src, const int& nifiti_datatype)
{
    dest.initialize(nifiti_datatype, src.m_nifti_header_ptr);
}
template <class T> void AsrImage3D<T>::zeroAboveZ(const int& zmax)
{
    auto d_iter = m_data.begin() + m_v_size[0] * m_v_size[1] * zmax;
    for(int z = zmax; z < m_v_size[2]; ++z) {
        for(int y = 0; y < m_v_size[1]; ++y) {
            for(int x = 0; x < m_v_size[0]; ++x, ++d_iter) {
                *d_iter = 0;
            }
        }
    }
}
template <class T> void AsrImage3D<T>::setValueAboveZ(const int& zmax, const T& val)
{
    auto d_iter = m_data.begin() + m_v_size[0] * m_v_size[1] * zmax;
    for(int z = zmax; z < m_v_size[2]; ++z) {
        for(int y = 0; y < m_v_size[1]; ++y) {
            for(int x = 0; x < m_v_size[0]; ++x, ++d_iter) {
                *d_iter = val;
            }
        }
    }
}
// also acts on the image rather then store in the reference
// differs from lowerThreshold in the > vs >= 0
template <class T> void AsrImage3D<T>::binarize()
{
    for(auto i = m_data.begin(); i != m_data.end(); ++i) {
        *i = (*i > 0) ? 1 : 0;
    }
}
template <class T> void AsrImage3D<T>::lowerThreshold(const T& threshold, AsrImage3D<short>& bin_im)
{
    bin_im.initialize(4, m_nifti_header_ptr);

    auto i_bin = bin_im.data_vector().begin();
    for(auto i = m_data.begin(); i != m_data.end(); ++i, ++i_bin) {
        if(*i != 0) {
            if(*i >= threshold)
                *i_bin = 1;
            else
                *i_bin = 0;
        }
    }
}

template <class T>
void AsrImage3D<T>::lowerUpperThreshold(const T& thresholdLower, const T& thresholdUpper, AsrImage3D<short>& bin_im)
{
    bin_im.initialize(4, m_nifti_header_ptr);
    auto i_bin = bin_im.data_vector().begin();
    for(auto i = m_data.begin(); i != m_data.end(); ++i, ++i_bin) {
        if((*i >= thresholdLower) && (*i <= thresholdUpper))
            *i_bin = 1;
        else
            *i_bin = 0;
    }
}

template <class T> void AsrImage3D<T>::upperThreshold(const T& threshold, AsrImage3D<short>& bin_im)
{
    bin_im.initialize(4, m_nifti_header_ptr);
    auto i_bin = bin_im.data_vector().begin();
    for(auto i = m_data.begin(); i != m_data.end(); ++i, ++i_bin) {
        if(*i <= threshold)
            *i_bin = 1;
        else
            *i_bin = 0;
    }
}

template <class T> template <class T2> void AsrImage3D<T>::mask(AsrImage3D<T2>& immask, bool invert)
{
    auto i_mask_data = immask.data_vector().begin();
    // some duplicate code, but only does 1 test for invert instead of number of voxels
    if(invert) {
        for(auto i = m_data.begin(); i != m_data.end(); ++i, ++i_mask_data) {
            if(*i_mask_data != 0)
                *i = 0;
        }
    } else {
        for(auto i = m_data.begin(); i != m_data.end(); ++i, ++i_mask_data) {
            if(*i_mask_data == 0)
                *i = 0;
        }
    }
}
template <class T> float AsrImage3D<T>::getQfac()
{
    return ((m_nifti_header_ptr->pixdim[0] == 0) ? 1 : m_nifti_header_ptr->pixdim[0]);
}
template <class T> vec3<float> AsrImage3D<T>::getPixDim()
{
    return vec3<float>(m_nifti_header_ptr->pixdim[1], m_nifti_header_ptr->pixdim[2], m_nifti_header_ptr->pixdim[3]);
}
template <class T> std::array<float, 9> AsrImage3D<T>::getR()
{
    // m_nifti_header_ptr->
    array<float, 9> R;
    float b = m_nifti_header_ptr->quatern_b;
    float c = m_nifti_header_ptr->quatern_c;
    float d = m_nifti_header_ptr->quatern_d;
    float a = sqrt(1.0 - (b * b + c * c + d * d));
    R[0] = a * a + b * b - c * c - d * d;
    R[1] = 2 * b * c - 2 * a * d;
    R[2] = 2 * b * d + 2 * a * c;
    R[3] = 2 * b * c + 2 * a * d;
    R[4] = a * a + c * c - b * b - d * d;
    R[5] = 2 * c * d - 2 * a * b;
    R[6] = 2 * b * d - 2 * a * c;
    R[7] = 2 * c * d + 2 * a * b;
    R[8] = a * a + d * d - c * c - b * b;

    cout << "R:" << endl;
    cout << R[0] << " " << R[1] << " " << R[2] << endl;
    cout << R[3] << " " << R[4] << " " << R[5] << endl;
    cout << R[6] << " " << R[7] << " " << R[8] << endl;
    return R;
}
template <class T> vec3<float> AsrImage3D<T>::getOrigin()
{
    return vec3<float>(m_nifti_header_ptr->qoffset_x, m_nifti_header_ptr->qoffset_y, m_nifti_header_ptr->qoffset_z);
}
// assumtpions of no rotations or shear
template <class T> vec3<float> AsrImage3D<T>::getVox2mmShift()
{
    //    vec3<float>
    //    affShift(m_nifti_header_ptr->srow_x[3],m_nifti_header_ptr->srow_y[3],m_nifti_header_ptr->srow_z[3]);
    vec3<float> affShift(m_nifti_header_ptr->qoffset_x, m_nifti_header_ptr->qoffset_y, m_nifti_header_ptr->qoffset_z);
    return affShift;
}
template <class T> vec3<float> AsrImage3D<T>::getVox2mmScale()
{
    vec3<float> affShift(m_nifti_header_ptr->srow_x[0], m_nifti_header_ptr->srow_y[1], m_nifti_header_ptr->srow_z[2]);
    return affShift;
}
template <class T> void AsrImage3D<T>::getBounds(std::array<int, 6>& bounds)
{
    bounds[0] = m_v_size[3];
    bounds[1] = m_v_size[4];
    bounds[2] = m_v_size[5];
    bounds[3] = 0;
    bounds[4] = 0;
    bounds[5] = 0;

    auto d_iter = m_data.begin();
    for(int z = 0; z < m_v_size[2]; ++z) {
        for(int y = 0; y < m_v_size[1]; ++y) {
            for(int x = 0; x < m_v_size[0]; ++x, ++d_iter) {
                if(*d_iter != 0) {
                    if(x < bounds[0])
                        bounds[0] = x;
                    if(y < bounds[1])
                        bounds[1] = y;
                    if(z < bounds[2])
                        bounds[2] = z;
                    if(x > bounds[3])
                        bounds[3] = x;
                    if(y > bounds[4])
                        bounds[4] = y;
                    if(z > bounds[5])
                        bounds[5] = z;
                }
            }
        }
    }
}

template <class T> vec3<int> AsrImage3D<T>::findSeedInZPLane() const
{

    int z{ zsize() };
    unsigned int N{ 0 };
    int cogx{ 0 }, cogy{ 0 };
    pair<int, int> lastVert{ make_pair<int, int>(0, 0) }; // if cog is nto with mask default to last vertex found
    while(z >= 0) {                                       // cycle down slices until a voxel is found
        z--;                                              // going S->I in the image, assume [RL][AP]S storage
        // reset for slice
        N = 0;
        cogx = cogy = 0;
        lastVert.first = lastVert.second = 0;
        auto d_iter = m_data.begin() + z * m_stride_z;

        for(int y = 0; y < ysize(); ++y) {
            for(int x = 0; x < xsize(); ++x, ++d_iter) {
                if(*d_iter > 0) {
                    cogx += x;
                    cogy += y;
                    lastVert.first = x;
                    lastVert.second = y;
                    ++N;
                }
            }
        }
        if(N > 0) {
            break;
        }
    }

    // need to derement z by one, because of last increment
    vec3<int> cog(
        static_cast<int>(static_cast<float>(cogx) / N + 0.5), static_cast<int>(static_cast<float>(cogy) / N + 0.5), z);
    if(value(cog.x, cog.y, cog.z) == 0)
        return vec3<int>(lastVert.first, lastVert.second, z);

    return cog;
}
template <class T>
void AsrImage3D<T>::fillXY(const int& x, const int& y, const int& z, const T& fillValue, int connectivity)
{

    array<int, 6> bounds{ 0, 0, z, m_v_size[0] - 1, m_v_size[1] - 1, z };
    fill(x, y, z, fillValue, bounds, connectivity);
}

template <class T> void AsrImage3D<T>::fillWithinMask(AsrImage3D<T>& mask, const T& fillValue)
{
    auto i_mask = mask.data_vector().begin();
    for(auto i = m_data.begin(); i != m_data.end(); ++i, ++i_mask) {
        if(*i_mask > 0)
            *i = fillValue;
    }
}

template <class T>
void AsrImage3D<T>::fill(const int& x, const int& y, const int& z, const T& fillValue, int connectivity)
{
    array<int, 6> bounds{ 0, 0, 0, m_v_size[0] - 1, m_v_size[1] - 1, m_v_size[2] - 1 };
    fill(x, y, z, fillValue, bounds, connectivity);
}

template <class T>
template <class T2>
unsigned int AsrImage3D<T>::fill(AsrImage3D<T2>& image,
    const int& x,
    const int& y,
    const int& z,
    const T2& fillValue,
    const float& threshold,
    int connectivity)
{
    array<int, 6> bounds{ 0, 0, 0, m_v_size[0] - 1, m_v_size[1] - 1, m_v_size[2] - 1 };
    return fill(image, x, y, z, fillValue, bounds, threshold, connectivity);
}

template <class T>
template <class T2>
unsigned int AsrImage3D<T>::fillDevelop(AsrImage3D<T2>& image,
    //    AsrImage3D<T2>& mask,
    list<vec3<int> > voxel_list,
    AsrImage3D<float>& distFromTrachea,
    const T2& fillValue,
    const float& threshold,
    const unsigned int maxSize,
    unordered_set<unsigned int>& vox_forbidden, unordered_set<unsigned int> & vox_mask,
    int connectivity)
{
    array<int, 6> bounds{ 0, 0, 0, m_v_size[0] - 1, m_v_size[1] - 1, m_v_size[2] - 1 };
    //    return fillDevelop(image, mask, distFromTrachea, fillValue, bounds, threshold, maxSize, connectivity);
    return fillDevelop(image, voxel_list, distFromTrachea, fillValue, bounds, threshold, maxSize, vox_forbidden, vox_mask,  connectivity);
}

template <class T>
template <class T2>
unsigned int AsrImage3D<T>::fillDevelop(AsrImage3D<T2>& image,
    const int& x,
    const int& y,
    const int& z,
    AsrImage3D<float>& distFromTrachea,
    const T2& fillValue,
    const float& threshold,
    const unsigned int maxSize,
    unordered_set<unsigned int>& vox_forbidden, unordered_set<unsigned int> & vox_mask,
    int connectivity)
{
    array<int, 6> bounds{ 0, 0, 0, m_v_size[0] - 1, m_v_size[1] - 1, m_v_size[2] - 1 };
    return fillDevelop(image, x, y, z, distFromTrachea, fillValue, bounds, threshold, maxSize, vox_forbidden, vox_mask, connectivity);
}

template <class T>
template <class T2>
unsigned int AsrImage3D<T>::fill(AsrImage3D<T2>& image,
    const int& x,
    const int& y,
    const int& z,
    const T2& fillValue,
    const array<int, 6>& bounds,
    const float& threshold,
    int connectivity)
{
    cout << "fill " << fillValue << endl;
    //	array<int,6> bounds{0,0,0,m_v_size[0]-1,m_v_size[1]-1,m_v_size[2]-1};
    //  T initValue = value(x, y, z);
    list<vec3<int> > voxel_list{ vec3<int>(x, y, z) };
    unsigned int size{ 1 };
    image.value(x, y, z) = fillValue;
    cout << "start " << x << " " << y << " " << z << endl;
    while(!voxel_list.empty()) {
        //      if(voxel_list.size() > 1000) break;
        //           cout<<"voxelListSize "<<voxel_list.size()<<endl;
        vec3<int> voxel_current = voxel_list.front();
        voxel_list.pop_front();
        //    cout<<"curvox "<<voxel_current.to_string()<<endl;
        // look at neighbours
        // can hard code away loops later for speed? if need
        // TODO precalculate offset
        for(int dx = -1; dx <= 1; ++dx)
            for(int dy = -1; dy <= 1; ++dy)
                for(int dz = -1; dz <= 1; ++dz) {
                    int sum = (abs(dx) + abs(dy) + abs(dz));
                    if((sum < 1) || (sum > connectivity))
                        continue;
                    int x_neighbour = voxel_current.x + dx;
                    int y_neighbour = voxel_current.y + dy;
                    int z_neighbour = voxel_current.z + dz;

                    // bounds check
                    if((x_neighbour < bounds[0]) || (y_neighbour < bounds[1]) || (z_neighbour < bounds[2]) ||
                        (x_neighbour > bounds[3]) || (y_neighbour > bounds[4]) || (z_neighbour > bounds[5])) {
                        continue;
                    }

                    //									cout<<"neighbour val
                    //"<<imcpy.value(x_neighbour,y_neighbour,z_neighbour)<<endl;
                    if(image.value(x_neighbour, y_neighbour, z_neighbour) != fillValue) {
                        if(value(x_neighbour, y_neighbour, z_neighbour) < threshold) {
                            //           cout<<"push back "<<value(x_neighbour, y_neighbour, z_neighbour)<<"
                            //           "<<threshold<<endl;
                            voxel_list.push_back(vec3<int>(x_neighbour, y_neighbour, z_neighbour));
                            image.value(x_neighbour, y_neighbour, z_neighbour) = fillValue;
                            ++size;
                        }
                    }
                }
    }
    return size;
}

template <class T>
template <class T2>
unsigned int AsrImage3D<T>::fillDevelop(AsrImage3D<T2>& image,
    const int& x,
    const int& y,
    const int& z,
    AsrImage3D<float>& distFromTrachea,
    const T2& fillValue,
    const array<int, 6>& bounds,
    const float& threshold,
    const unsigned int maxSize,
 unordered_set<unsigned int>& vox_forbidden,
    unordered_set<unsigned int>& vox_mask,   
 int connectivity)
{

    list<vec3<int> > seed{ vec3<int>(x, y, z) };
    return fillDevelop(image, seed, distFromTrachea, fillValue, bounds, threshold, maxSize,vox_forbidden, vox_mask, connectivity);

    //    cout << "fill " << fillValue << endl;
    //    //	array<int,6> bounds{0,0,0,m_v_size[0]-1,m_v_size[1]-1,m_v_size[2]-1};
    //    //  T initValue = value(x, y, z);
    //    list<vec3<int> > voxel_list{ vec3<int>(x, y, z) };
    //    unsigned int size{ 1 };
    //    image.value(x, y, z) = fillValue;
    //    cout << "start " << x << " " << y << " " << z << endl;
    //    AsrImage3D<T2> image_visited = image;
    //
    //    while(!voxel_list.empty()) {
    //        //      if(voxel_list.size() > 1000) break;
    //        if(maxSize > 0) {
    //            if(size > maxSize) {
    //                cout << "return " << size << "/" << maxSize << endl;
    //                return size;
    //            }
    //        }
    //        // if( (size % 1000) ==0)
    //        //                   cout<<"voxelListSize "<<size<< "/" << maxSize << endl;
    //        //                   cout<<"done copy  "<<endl;
    //        vec3<int> voxel_current = voxel_list.front();
    //        voxel_list.pop_front();
    //        //        T currentValue = value(voxel_current.x, voxel_current.y, voxel_current.z);
    //        //    cout<<"curvox "<<voxel_current.to_string()<<endl;
    //        // look at neighbours
    //        // can hard code away loops later for speed? if need
    //        // TODO precalculate offset
    //        for(int dx = -1; dx <= 1; ++dx)
    //            for(int dy = -1; dy <= 1; ++dy)
    //                for(int dz = -1; dz <= 1; ++dz) {
    //                    int sum = (abs(dx) + abs(dy) + abs(dz));
    //                    if((sum < 1) || (sum > connectivity))
    //                        continue;
    //                    int x_neighbour = voxel_current.x + dx;
    //                    int y_neighbour = voxel_current.y + dy;
    //                    int z_neighbour = voxel_current.z + dz;
    //
    //                    // bounds check
    //                    if((x_neighbour < bounds[0]) || (y_neighbour < bounds[1]) || (z_neighbour < bounds[2]) ||
    //                        (x_neighbour > bounds[3]) || (y_neighbour > bounds[4]) || (z_neighbour > bounds[5])) {
    //                        continue;
    //                    }
    //
    //                    // let's test out some history
    //                    //
    //                    //          cout<<"history "<<fillValue<<" "<<image.value(x_neighbour, y_neighbour,
    //                    z_neighbour)
    //                    //          <<endl;
    //                    //									cout<<"neighbour
    //                    val
    //                    //"<<imcpy.value(x_neighbour,y_neighbour,z_neighbour)<<endl;
    //                    // Gradient or nto seems to produce similar stuff
    //                    //                    cout<<"Imsample " << image_visited.value(x_neighbour, y_neighbour,
    //                    //                    z_neighbour)<<endl;
    //                    if(image_visited.value(x_neighbour, y_neighbour, z_neighbour) != fillValue) {
    //                        //                    if(image.value(x_neighbour, y_neighbour, z_neighbour) < 1) {
    //                        //              cout<<"diff "<<(value(x_neighbour, y_neighbour, z_neighbour) -
    //                        //              currentValue)<<endl;
    //                        // believe absolute value shoudl reduce speckle
    //                        //                        if(fabs(value(x_neighbour, y_neighbour, z_neighbour) -
    //                        currentValue) <
    //                        //                        threshold) {
    //                        //                        if((value(x_neighbour, y_neighbour, z_neighbour) -
    //                        currentValue)
    //                        <
    //                        //                        threshold) {
    //                        if(value(x_neighbour, y_neighbour, z_neighbour) < threshold) {
    //                            //           cout<<"push back "<<value(x_neighbour, y_neighbour, z_neighbour)<<"
    //                            //           "<<threshold<<endl;
    //                            voxel_list.push_back(vec3<int>(x_neighbour, y_neighbour, z_neighbour));
    //                            image_visited.value(x_neighbour, y_neighbour, z_neighbour) = fillValue;
    //                            image.value(x_neighbour, y_neighbour, z_neighbour) = fillValue;
    //
    //                            // this is to show iterations
    //                            //                            if (image.value(x_neighbour, y_neighbour,
    //                            z_neighbour)
    //                            == 0 )
    //                            //                                image.value(x_neighbour, y_neighbour,
    //                            z_neighbour)++;
    //                            ++size;
    //                        }
    //                    }
    //                }
    //    }
    //    return size;
}

template <class T>
void AsrImage3D<T>::fill(const int& x,
    const int& y,
    const int& z,
    const T& fillValue,
    const array<int, 6>& bounds,
    int connectivity)
{

    //	array<int,6> bounds{0,0,0,m_v_size[0]-1,m_v_size[1]-1,m_v_size[2]-1};
    T initValue = value(x, y, z);
    list<vec3<int> > voxel_list{ vec3<int>(x, y, z) };
    value(x, y, z) = fillValue;

    while(!voxel_list.empty()) {
        //        cout<<"voxelListSize "<<voxel_list.size()<<endl;
        vec3<int> voxel_current = voxel_list.front();
        voxel_list.pop_front();
        // look at neighbours
        // can hard code away loops later for speed? if need
        for(int dx = -1; dx <= 1; ++dx)
            for(int dy = -1; dy <= 1; ++dy)
                for(int dz = -1; dz <= 1; ++dz) {
                    int sum = (abs(dx) + abs(dy) + abs(dz));
                    if((sum < 1) || (sum > connectivity))
                        continue;
                    int x_neighbour = voxel_current.x + dx;
                    int y_neighbour = voxel_current.y + dy;
                    int z_neighbour = voxel_current.z + dz;

                    // bounds check
                    if((x_neighbour < bounds[0]) || (y_neighbour < bounds[1]) || (z_neighbour < bounds[2]) ||
                        (x_neighbour > bounds[3]) || (y_neighbour > bounds[4]) || (z_neighbour > bounds[5])) {
                        continue;
                    }
                    //									cout<<"neighbour val
                    //"<<imcpy.value(x_neighbour,y_neighbour,z_neighbour)<<endl;
                    if(value(x_neighbour, y_neighbour, z_neighbour) == initValue) {
                        voxel_list.push_back(vec3<int>(x_neighbour, y_neighbour, z_neighbour));
                        value(x_neighbour, y_neighbour, z_neighbour) = fillValue;
                    }
                }
    }
}
template <class T>
template <class T2>
unsigned int AsrImage3D<T>::fillDevelop(AsrImage3D<T2>& image,
    //    AsrImage3D<T2>& mask,
    list<vec3<int> > voxel_list,
    AsrImage3D<float>& distFromTrachea,
    const T2& fillValue,
    const array<int, 6>& bounds,
    const float& threshold,
    const unsigned int maxSize,
    unordered_set<unsigned int>& vox_forbidden,
    unordered_set<unsigned int>& vox_mask,
    int connectivity)
{
    cout << "fill " << fillValue << endl;
    //	array<int,6> bounds{0,0,0,m_v_size[0]-1,m_v_size[1]-1,m_v_size[2]-1};
    //  T initValue = value(x, y, z);
    // TODO REMOVE FROM LOOP
    // THIS IS CALLED FOR EVERY NEW THRESHOLD
    //    list<vec3<int> > voxel_list; //{ vec3<int>(x, y, z) };
    // keep track of minimum in mask, set init threshold to that if less than
    //    float maskMin{ mask.begin() };
    //    unsigned int index{ 0 };
    //    for(auto& i : mask.data_vector()) {
    //        if(i > 0) {
    //            vec3<int> coord = getCoord(index);
    //            voxel_list.push_back(coord);
    //            image.value(coord.x, coord.y, coord.z) = 1;
    //        }
    //        index++;
    //    }
    for(auto& coord : voxel_list) {
        image.value(coord.x, coord.y, coord.z) = fillValue;
    }

    unsigned int size{ static_cast<unsigned int>(voxel_list.size()) };
    //    for (auto& i : voxel_list)
    //        cout<<i.to_string()<<endl;
    cout << "initial size of mask " << size << endl;

    //    image.value(x, y, z) = fillValue;
    //    cout << "start " << x << " " << y << " " << z << endl;

    // increment seem to be by ones for threshodl
    //    float nextThresh {INFINITY};
    //    float xdim{image.xdim()};
    //    float ydim{image.ydim()};
    //    float zdim{image.zdim()};
    //
    // TODO REMOVE THE IMAGE_VISITED - REALLY SLOWS THINGS DOWN
    //    AsrImage3D<T2> image_visited = image;
    float curDist = distFromTrachea.value(voxel_list.front().x, voxel_list.front().y, voxel_list.front().z);
    cout << "curDirst " << curDist << endl;
    while(!voxel_list.empty()) {
        //      if(voxel_list.size() > 1000) break;
        if(maxSize > 0) {
            if(size > maxSize) {
                cout << "return " << size << "/" << maxSize << endl;
                return size;
            }
        }
        // if( (size % 1000) ==0)
        //                   cout<<"voxelListSize "<<size<< "/" << maxSize << endl;
        //                   cout<<"done copy  "<<endl;
        vec3<int> voxel_current = voxel_list.front();
        voxel_list.pop_front();
        //                T currentValue = value(voxel_current.x, voxel_current.y, voxel_current.z);
        //        float curDist = distFromTrachea.value(voxel_current.x, voxel_current.y, voxel_current.z);
        //        cout << "current distance " << curDist << endl;

        //            cout<<"CURvox "<<voxel_current.to_string()<<endl;
        // look at neighbours
        // can hard code away loops later for speed? if need
        // TODO precalculate offset
        for(int dx = -1; dx <= 1; ++dx)
            for(int dy = -1; dy <= 1; ++dy)
                for(int dz = -1; dz <= 1; ++dz) {
                    int sum = (abs(dx) + abs(dy) + abs(dz));
                    //                    cout<<"dxyz "<<dx<<" "<<dy<<" "<<dz<<" "<<sum<<" "<<connectivity<<endl;
                    if((sum < 1) || (sum > connectivity))
                        continue;
                    //                    cout<<"pass "<<endl;
                    int x_neighbour = voxel_current.x + dx;
                    int y_neighbour = voxel_current.y + dy;
                    int z_neighbour = voxel_current.z + dz;

                    // bounds check
                    if((x_neighbour < bounds[0]) || (y_neighbour < bounds[1]) || (z_neighbour < bounds[2]) ||
                        (x_neighbour > bounds[3]) || (y_neighbour > bounds[4]) || (z_neighbour > bounds[5])) {
                        continue;
                    }

                    // let's test out some history
                    //
                    //          cout<<"history "<<fillValue<<" "<<image.value(x_neighbour, y_neighbour, z_neighbour)
                    //          <<endl;
                    //									cout<<"neighbour val
                    //"<<imcpy.value(x_neighbour,y_neighbour,z_neighbour)<<endl;
                    // Gradient or nto seems to produce similar stuff
                    //                   cout<<"Imsample " << image_visited.value(x_neighbour, y_neighbour,
                    //                   z_neighbour)<<endl;
                    //                    if(image_visited.value(x_neighbour, y_neighbour, z_neighbour) != fillValue) {
                    if(image.value(x_neighbour, y_neighbour, z_neighbour) != fillValue) {
                        if(value(x_neighbour, y_neighbour, z_neighbour) < threshold) {
                            float dist{ distFromTrachea.value(x_neighbour, y_neighbour, z_neighbour) };
                            //                                                        cout<<"dist "<<curDist<<"
                            //                                                        "<<dist<<endl;
                            if((dist > curDist) || (dist == 0)) { // only grow down tree
                                voxel_list.push_back(vec3<int>(x_neighbour, y_neighbour, z_neighbour));
                                vox_mask.insert(getIndex(x_neighbour, y_neighbour, z_neighbour));
                                //                                image_visited.value(x_neighbour, y_neighbour,
                                //                                z_neighbour) = fillValue;
                                //                                image_visited.value(x_neighbour, y_neighbour,
                                //                                z_neighbour) = fillValue;
                                //                                image.value(x_neighbour, y_neighbour, z_neighbour)++;
                                image.value(x_neighbour, y_neighbour, z_neighbour) = fillValue;
                                ++size;
                            }
                        }
                    }
                }
    }
    //    cout<<"Next Threshold "<<nextThresh<<endl;
    return size;
}

template <class T> void AsrImage3D<T>::binaryInverse()
{
    for(auto i = m_data.begin(); i != m_data.end(); ++i) {
        if(*i == 0) {
            *i = 1;
        } else {
            *i = 0;
        }
    }
}

template <class T> void AsrImage3D<T>::dilate3x3()
{
    AsrImage3D<T> imcpy;
    copyImage(imcpy, *this);
    array<int, 6> bounds{ 0, 0, 0, m_v_size[0] - 1, m_v_size[1] - 1, m_v_size[2] - 1 };
    array<int, 2> dx{ -1, 1 }, dy{ -1, 1 }, dz{ -1, 1 };

    auto d_iter = imcpy.data_vector().begin(); // + bounds[1]*m_stride_y+bounds[0];
    for(int z = bounds[2]; z <= bounds[5]; ++z) {
        for(int y = bounds[1]; y <= bounds[4]; ++y) {
            for(int x = bounds[0]; x <= bounds[3]; ++x, ++d_iter) {
                if(*d_iter > 0) {
                    for(auto& xdif : dx)
                        for(auto& ydif : dy)
                            for(auto& zdif : dz) {
                                this->value(x + xdif, y + ydif, z + zdif) = *d_iter;
                            }
                }
            }
        }
    }
}
template <class T> void AsrImage3D<T>::erode3x3()
{
    cout << "erode3x3 " << endl;
    AsrImage3D<T> imcpy;
    copyImage(imcpy, *this);

    array<int, 6> bounds{ 0, 0, 0, m_v_size[0] - 1, m_v_size[1] - 1, m_v_size[2] - 1 };
    getBounds(bounds);
    cout << "boudns " << bounds[0] << " " << bounds[1] << " " << bounds[2] << " " << bounds[3] << " " << bounds[4]
         << " " << bounds[5] << " " << endl;
    // basic implementation, need to add bounds option
    bounds[3] = m_v_size[0] - 1;
    bounds[4] = m_v_size[1] - 1;
    bounds[5] = m_v_size[2] - 1;

    auto d_iter = m_data.begin() + bounds[2] * m_stride_z;
    for(int z = bounds[2]; z <= bounds[5]; ++z) {
        cout << "z " << z << endl;
        d_iter += bounds[1] * m_stride_y;
        for(int y = bounds[1]; y <= bounds[4]; ++y) {
            d_iter += bounds[0];
            for(int x = bounds[0]; x <= bounds[3]; ++x, ++d_iter) {
                if(*d_iter > 0) // check original image, will ignore voxels alreayd eroded
                {
                    //					bool erode=false;
                    for(int x_neighbour = x - 1; x_neighbour <= x + 1; ++x_neighbour)
                        for(int y_neighbour = y - 1; y_neighbour <= y + 1; ++y_neighbour)
                            for(int z_neighbour = x - 1; z_neighbour <= z + 1; ++z_neighbour) {
                                if((x_neighbour < bounds[0]) || (y_neighbour < bounds[1]) ||
                                    (z_neighbour < bounds[2]) || (x_neighbour > bounds[3]) ||
                                    (y_neighbour > bounds[4]) || (z_neighbour > bounds[5])) {
                                    continue;
                                }
                                if(imcpy.value(x_neighbour, y_neighbour, z_neighbour) == 0) {
                                    //									erode =
                                    // true;
                                    y_neighbour = y + 2;
                                    x_neighbour = x + 2; // kick out of loop outer loops
                                    //																	break;
                                    *d_iter = 0;
                                    break;
                                }
                            }

                    //					//erode everything in the original image
                    //					if (erode)
                    //					{
                    //						for (int x_neighbour= x-1 ;x_neighbour  <= x+1 ;
                    //++x_neighbour)
                    //							for (int y_neighbour= y-1 ;y_neighbour  <=
                    // y+1
                    //;
                    //++y_neighbour)
                    //								for (int z_neighbour= x-1
                    //;z_neighbour
                    //<=
                    // z+1
                    //;
                    //++z_neighbour)
                    //								{
                    //									if ((x_neighbour< bounds[0]) || (y_neighbour<bounds[1]) || (z_neighbour<bounds[2]) || \
//																						(x_neighbour>bounds[3]) || (y_neighbour > bounds[4]) || (z_neighbour > bounds[5]))
                    //																				{
                    //																					continue;
                    //																				}
                    //
                    //									value(x_neighbour,y_neighbour,z_neighbour)
                    //=
                    // 0;
                    //								}
                    //					}
                }
                // for (int dx = -1 ; dx <=1 ; ++dx)
                //					for (int dy = -1 ; dy <=1 ; ++dy)
                //						for (int dz = -1 ; dz <=1 ; ++dz)
                //						{
                //							if ( (abs(dx) + abs(dy) + abs(dz)) < 1 )
                // continue;
                //							int x_neighbour = x+dx;
                //							int y_neighbour = x+dy;
                //							int z_neighbour = z+dz;
                //
                //							//bounds check
                //							if ((x_neighbour< bounds[0]) || (y_neighbour<bounds[1]) || (z_neighbour<bounds[2]) || \
//									(x_neighbour>bounds[3]) || (y_neighbour > bounds[4]) || (z_neighbour > bounds[5]))
                //							{
                //								continue;
                //							}
                //							if
                //(imcpy.value(x_neighbour,y_neighbour,z_neighbour)
                //==
                // 0
                //)
                //							{
                ////								value(x,y,z) = 0;
                //								dy=dx=2;//kick out of loop outer loops
                //								break;
                //							}
                //						}
                ////				}
            }
        }
    }
}

// assumes distance to interior voxel, not
// template <class T> void AsrImage3D<T>::distanceMap(AsrImage3D<float> &imDist) {
//  cout << "calculatre distance map " << endl;
//  // for use with l2norm function
//  list<vec3<int>> boundaryVoxels;
//    imDist.initialize(DT_FLOAT, m_nifti_header_ptr);
//
//  copyImage(imDist,*this, DT_FLOAT);
//  array<int, 6> bounds{0, 0, 0, m_v_size[0] - 1, m_v_size[1] - 1, m_v_size[2] - 1};
//  getBounds(bounds);
//  cout << "boudns " << bounds[0] << " " << bounds[1] << " " << bounds[2] << " " << bounds[3] << " " << bounds[4]
//  << "
//  " << bounds[5] << " " << endl;
//  // basic implementation, need to add bounds option
//  bounds[3] = m_v_size[0] - 1;
//  bounds[4] = m_v_size[1] - 1;
//  bounds[5] = m_v_size[2] - 1;
//
//  array<int, 2> offsetX{-1, 1};
//  array<int, 2> offsetY{-1, 1};
//  array<int, 2> offsetZ{-1, 1};
//  { // find boudnary voxels
//    auto d_iter = m_data.begin() + bounds[2] * m_stride_z;
//    for (int z = bounds[2]; z <= bounds[5]; ++z) {
//      d_iter += bounds[1] * m_stride_y;
//      for (int y = bounds[1]; y <= bounds[4]; ++y) {
//        d_iter += bounds[0];
//        for (int x = bounds[0]; x <= bounds[3]; ++x, ++d_iter) {
//
//          // interior voxel
//          if (*d_iter == 0) // check original image, will ignore voxels alreayd eroded
//          {
//              bool found = false;
//            // check to see if its is a boundary voxel
//            for (auto &dx : offsetX) {
//              for (auto &dy : offsetY) {
//                for (auto &dz : offsetZ) {
//                  if (value(x + dx, y + dy, z + dz) == 1) {
//                    boundaryVoxels.push_back(vec3<int>(x, y , z));
//                    found=true;
//                    break;
//                  }
//                }
//                if (found) break;
//              }
//              if (found) break;
//            }
//          }
//        }
//      }
//    }
//  }
//
//    auto i_bvox = boundaryVoxels.begin();
//    imDist.distanceFromVoxel(*(i_bvox++),3,1);
//    int count=0;
//
//    for ( ; i_bvox != boundaryVoxels.end(); ++i_bvox , ++count)
//    {
//        cout<<"nbext bvoxel "<<count<<" / "<<boundaryVoxels.size()<<" "<<i_bvox->to_string()<<endl;
////      ////  imDist.value(i_bvox->x,i_bvox->y,i_bvox->z)=100;
//        imDist.distanceFromVoxel(*(i_bvox++),3,0);
////  ////      imDist.writeImage("imDist"+to_string(count)+".nii.gz");
//    }
//
//
////
////
//  { // find distance to boudnary voxels
//    auto d_iter = m_data.begin() + bounds[2] * m_stride_z;
//    for (int z = bounds[2]; z <= bounds[5]; ++z) {
//      cout << "z " << z << endl;
//      d_iter += bounds[1] * m_stride_y;
//      for (int y = bounds[1]; y <= bounds[4]; ++y) {
//        d_iter += bounds[0];
//        for (int x = bounds[0]; x <= bounds[3]; ++x, ++d_iter) {
//
//          // interior voxel
//          if (*d_iter > 0) // check original image, will ignore voxels alreayd eroded
//          {
//              cout<<"calculate distance at "<<x<<" "<<y<<" "<<z<<" "<<*d_iter<<endl;
//            imDist.distanceFromVoxel(x,y,z,3,1);
//            return;
//            float distMin = l2normSquared(vec3<int>(x, y, z) - boundaryVoxels.front());
//            for (auto &bVoxel : boundaryVoxels) {
//              float dist = l2normSquared(vec3<int>(x, y, z) - bVoxel);
//              if (distMin > dist)
//                distMin = dist;
//              if (dist == 0)
//                break;
//            }
//            distMin = sqrt(distMin);
//          }
//        }
//      }
//    }
//  }
//}

template <class T>
template <class T2>
sizeCog AsrImage3D<T>::connectedComponents(AsrImage3D<T2>& image_mask, const unsigned int& largestOnly)
{
    vector<int> bounds{ 0, 0, 0, m_v_size[0] - 1, m_v_size[1] - 1, m_v_size[2] - 1 };
    return connectedComponents(image_mask, bounds, largestOnly);
}

template <class T>
template <class T2>
sizeCog AsrImage3D<T>::connectedComponents(AsrImage3D<T2>& image_mask,
    const vector<int>& bounds,
    const unsigned int& numberOfLargest)
{
    //	cout<<"connected components "<<numberOfLargest<<endl;
    if((bounds[0] < 0) || (bounds[0] > m_v_size[0]) || (bounds[1] < 0) || (bounds[1] > m_v_size[1]) ||
        (bounds[2] < 0) || (bounds[2] > m_v_size[2]) || (bounds[0] < 0) || (bounds[0] > m_v_size[0]) ||
        (bounds[1] < 0) || (bounds[1] > m_v_size[1]) || (bounds[2] < 0) || (bounds[2] > m_v_size[2])) {
        cerr << "Invalid image bounds. " << endl;
        return sizeCog();
    }

    // initialize with itself should leave unchanged
    //	int Ncomponents{0};
    image_mask.initialize(4, m_nifti_header_ptr);
    //
    AsrImage3D<T2> imcpy = image_mask;
    //	copyImage(imcpy,*this,4);

    sizeCog l_sizes_cog;

    float voxel_size{ m_v_dim[0] * m_v_dim[1] * m_v_dim[2] };
    map<int, tuple<int, vec3<float> > > map_size_2_index_cog;

    T2 component_index = 1;
    // for speed up can change the bounds check and embed in for loop
    // with appropriate strdies	[x + y*m_stride_y +z*m_stride_z];
    // cout<<"CC strideyY "<<m_stride_y<<endl;
    int iterBeginIncrementY{ bounds[1] * m_stride_y }, iterEndIncrementY{ (m_v_size[1] - 1 - bounds[4]) * m_stride_y };
    int iterBeginIncrementX{ bounds[0] }, iterEndIncrementX{ (m_v_size[0] - 1 - bounds[3]) };
    // cout<<"bounds "<<bounds[0]<<" "<<bounds[1]<<" "<<bounds[2]<<" "<<bounds[3]<<" "<<bounds[4]<<"
    // "<<bounds[5]<<endl;
    //	cout<<"incrememts "<<iterBeginIncrementX<<" "<<iterEndIncrementX<<" "<<iterBeginIncrementY<<"
    //"<<iterEndIncrementY<<endl;
    auto d_iter = imcpy.data_vector().begin() + bounds[2] * m_stride_z; // + bounds[1]*m_stride_y+bounds[0];
    for(int z = bounds[2]; z <= bounds[5]; ++z) {
        d_iter += iterBeginIncrementY; // bounds[1]*m_stride_y;
        for(int y = bounds[1]; y <= bounds[4]; ++y) {
            d_iter += iterBeginIncrementX; // bounds[0];
            for(int x = bounds[0]; x <= bounds[3]; ++x, ++d_iter) {
                //				{//no longer need bounds check because of limited loop

                if(*d_iter > 0) {

                    int size{ 0 };

                    //						cout<<"seed "<<x<<" "<<y<<" "<<z<<endl;

                    list<vec3<int> > voxel_list{ vec3<int>(x, y, z) };
                    imcpy.value(x, y, z) = 0;
                    image_mask.value(x, y, z) = component_index;
                    vec3<float> cog(x * m_v_dim[0], y * m_v_dim[1], z * m_v_dim[2]);
                    //						++size;

                    while(!voxel_list.empty()) {
                        vec3<int> voxel_current = voxel_list.front();
                        voxel_list.pop_front();
                        ++size;
                        // look at neighbours
                        // can hard code away loops later for speed? if need
                        for(int dx = -1; dx <= 1; ++dx)
                            for(int dy = -1; dy <= 1; ++dy)
                                for(int dz = -1; dz <= 1; ++dz) {
                                    if((abs(dx) + abs(dy) + abs(dz)) < 1)
                                        continue;
                                    int x_neighbour = voxel_current.x + dx;
                                    int y_neighbour = voxel_current.y + dy;
                                    int z_neighbour = voxel_current.z + dz;

                                    // bounds check
                                    if((x_neighbour < bounds[0]) || (y_neighbour < bounds[1]) ||
                                        (z_neighbour < bounds[2]) || (x_neighbour > bounds[3]) ||
                                        (y_neighbour > bounds[4]) || (z_neighbour > bounds[5])) {
                                        continue;
                                    }
                                    //									cout<<"neighbour
                                    // val
                                    //"<<imcpy.value(x_neighbour,y_neighbour,z_neighbour)<<endl;
                                    if(imcpy.value(x_neighbour, y_neighbour, z_neighbour) > 0) {
                                        voxel_list.push_back(vec3<int>(x_neighbour, y_neighbour, z_neighbour));
                                        imcpy.value(x_neighbour, y_neighbour, z_neighbour) = 0;

                                        image_mask.value(x_neighbour, y_neighbour, z_neighbour) = component_index;
                                        cog.x += x_neighbour * m_v_dim[0];
                                        cog.y += y_neighbour * m_v_dim[1];
                                        cog.z += z_neighbour * m_v_dim[2];
                                    }
                                }

                        //							++size;
                    }
                    cog /= size;
                    map_size_2_index_cog[size] =
                        make_tuple(component_index++, cog); // static_cast<float>(component_index++);
                }
                //				}
            }
            d_iter += iterEndIncrementX;
        }
        d_iter += iterEndIncrementY;
    }

    //	Ncomponents = map_size_2_index_cog.size(); //map_value_2_size.size();

    //	if (!map_value_2_size.empty())
    //	{
    //		for (auto& i : map_value_2_size )
    //		{
    //			cout<<"componentn "<<i.first<<" "<<i.second<<"
    //"<<map_value_2_size.crbegin()->second<<endl;
    //		}
    //
    //	}

    //	cout<<"N "<<numberOfLargest<<" "<<map_value_2_size.size()<<endl;
    // remove smaller regions
    if((numberOfLargest > 0) && (!map_size_2_index_cog.empty())) {
        // if there does not exist the number of desired regions, return those available
        unsigned int Ndesired{ (numberOfLargest < map_size_2_index_cog.size()) ?
                numberOfLargest :
                static_cast<unsigned int>(map_size_2_index_cog.size()) };
        unordered_set<int> maxIndices;
        //		cout<<"Nsedired "<<Ndesired<<endl;
        auto iter_maxIndex = map_size_2_index_cog.crbegin();
        for(unsigned int region = 0; region < Ndesired; ++region, ++iter_maxIndex) {
            //			cout<<"insert max index "<<iter_maxIndex->second<<"
            //"<<map_value_2_size.size()<<endl;
            // index of largest ROI
            maxIndices.insert(get<0>(iter_maxIndex->second));
            l_sizes_cog.push_back(make_tuple(iter_maxIndex->first * voxel_size, get<1>(iter_maxIndex->second)));
        }
        auto d_iter = image_mask.data_vector().begin() + bounds[2] * m_stride_z; // + bounds[1]*m_stride_y+bounds[0];
        for(int z = bounds[2]; z <= bounds[5]; ++z) {

            d_iter += iterBeginIncrementY; // bounds[1]*m_stride_y;
            for(int y = bounds[1]; y <= bounds[4]; ++y) {
                d_iter += iterBeginIncrementX; // bounds[0];
                for(int x = bounds[0]; x <= bounds[3]; ++x, ++d_iter) {
                    // will be one if exists, 0 if not
                    *d_iter = maxIndices.count(*d_iter);
                    //						if ((*d_iter) != maxIndex )
                    //						{
                    //							*d_iter = 0 ;
                    //						}
                    //						else
                    //						{
                    //							*d_iter = 1 ;
                    //						}
                }
                d_iter += iterEndIncrementX;
            }
            d_iter += iterEndIncrementY;
        }
    }

    for(auto& i : map_size_2_index_cog) {
        //		cout<<"add size "<<i.first<<" "<<voxel_size<<endl;
        // size in voxels
        l_sizes_cog.push_back(make_tuple(i.first, get<1>(i.second)));
    }
    return l_sizes_cog;
}

template <class T> void AsrImage3D<T>::normalize()
{
    m_min = m_max = m_data[0];
    for(auto& i_data : m_data) {
        if(i_data < m_min)
            m_min = i_data;
        if(i_data > m_max)
            m_max = i_data;
    }
    for(auto& i_data : m_data) {
        i_data = (i_data - m_min) / (m_max - m_min);
    }
}
//----------------------IO------------------------------//
template <class T> int AsrImage3D<T>::readImage(const string& filename)
{

    // read and store header. Going to use for writing
    int swapped = 0;
    m_nifti_header_ptr = nifti_read_header(filename.c_str(), &swapped, 0);

    nifti_image* imread = nifti_image_read(filename.c_str(), 1);
    m_ndims = imread->dim[0];
    int nvoxels = 0;
    // alocate vector to dimensionality of the image
    m_v_size.resize(m_ndims);
    m_v_dim.resize(m_ndims);
    if(m_ndims > 0)
        nvoxels = 1;
    auto i_size = m_v_size.begin();
    auto i_dim = m_v_dim.begin();
    for(int i = 1; i <= m_ndims; ++i, ++i_size, ++i_dim) {
        *i_size = imread->dim[i];
        *i_dim = imread->pixdim[i];
        nvoxels *= *i_size;
    }

    // updated dimensions, now update stride
    updateStrides();

    //    nifti_datatype_ = static_cast<int>(imread->datatype);

    // let's allocate and copy/cast data
    m_data.resize(nvoxels);
    cout << "datatype " << imread->datatype << endl;
    if((imread->datatype == 4)) {
        // NIFTI_INT16 -> signed short
        signed short* imdata = static_cast<signed short*>(imread->data);
        for(auto& voxel : m_data) {
            voxel = static_cast<T>(*imdata);
            ++imdata;
        }

    } else if(imread->datatype == 2) {
        // NIFTI_INT16 -> signed short

        unsigned char* imdata = static_cast<unsigned char*>(imread->data);
        for(auto& voxel : m_data) {
            voxel = static_cast<T>(*imdata);
            ++imdata;
        }

    } else if(imread->datatype == 16) {
        // NIFTI_INT16 -> signed short

        float* imdata = static_cast<float*>(imread->data);
        for(auto& voxel : m_data) {
            voxel = static_cast<T>(*imdata);
            ++imdata;
        }

    } else {
        cerr << "Input type not supported" << endl;
    }
    m_nifti_header_ptr->datatype = m_nifti_datatype;
    memcpy(m_nifti_header_ptr->data_type, nifti_datatype_to_string(m_nifti_datatype), 10 * sizeof(char));

    return 0;
}

template <class T> int AsrImage3D<T>::writeImage(const string& filename) const
{
    cout << "write image " << filename << endl;

    //	nifti_1_header* 	nifti_make_new_header (const int arg_dims[],int arg_dtype)
    //	nifti_1_header* 	nifti_read_header (const char *hname,int *swapped,int check)
    //	nifti_image* 	nifti_copy_nim_info (const nifti_image *src)
    //	nifti_image* 	nifti_make_new_nim (const int dims[],int datatype,int m_datafill)
    //	nifti_image* 	nifti_simple_init_nim (void)
    //	nifti_image* 	nifti_convert_nhdr2nim (struct nifti_1_header nhdr,const char *fname)

    nifti_image* image_to_write;
    if(m_nifti_header_ptr != nullptr) {
        image_to_write = nifti_convert_nhdr2nim(*m_nifti_header_ptr, filename.c_str());
        if(image_to_write != NULL) {
            if(image_to_write->data == NULL) {
                // dont need to copy
                image_to_write->data = const_cast<T*>(&(m_data[0]));
            } else {
                cout<<"copy in data "<<endl;
                // copy in data
                memcpy(
                    image_to_write->data, static_cast<void*>(const_cast<T*>(&(m_data[0]))), m_data.size() * sizeof(T));
                cout<<"done copy"<<endl;
            }
            nifti_image_write(image_to_write);
            image_to_write->data = NULL;
            nifti_image_free(image_to_write);
        } else {
            cout << "Failed to create valid nifti_image for " << filename << endl;
        }
    }

    return 0;
}

//-----------------------IO-END------------------------------//

template <class T> T& AsrImage3D<T>::value(const int& x, const int& y, const int& z)
{
    return m_data[x + y * m_stride_y + z * m_stride_z];
}

template <class T> const T& AsrImage3D<T>::value(const int& x, const int& y, const int& z) const
{
    return m_data[x + y * m_stride_y + z * m_stride_z];
}
template <class T> unsigned int AsrImage3D<T>::getIndex(const int& x, const int& y, const int& z)
{
    return (x + y * m_stride_y + z * m_stride_z);
}
template <class T> vec3<int> AsrImage3D<T>::getCoord(const unsigned int& index)
{
    unsigned int indexTotal{ index };
    int z = static_cast<int>(indexTotal / m_stride_z);
    indexTotal -= z * m_stride_z;
    int y = static_cast<int>(indexTotal / m_stride_y);
    return vec3<int>(indexTotal - y * m_stride_y, y, z);
}

template <class T> void AsrImage3D<T>::info()
{
    cout << "Image Info" << endl;

    cout << "Dimensionality : " << m_v_size.size() << endl;

    cout << "Dimensions : ";
    int index = 0;
    for(auto size : m_v_size) {
        if(index > 0)
            cout << " X ";
        cout << size;
        ++index;
    }
    cout << endl;
}

template <class T> T AsrImage3D<T>::getMinIntensity() const
{
    return m_min;
}
template <class T> T AsrImage3D<T>::getMaxIntensity() const
{
    return m_max;
}

template <class T> vector<T> AsrImage3D<T>::range() const
{
    if(m_data.empty())
        return vector<T>{ 0, 0 };

    T min{ m_data[0] }, max{ m_data[0] };
    for(auto& i_data : m_data) {
        if(min > i_data)
            min = i_data;
        if(max < i_data)
            max = i_data;
    }
    return vector<T>{ min, max };
}

template <class T> vector<float> AsrImage3D<T>::histogram(const unsigned int& Nbins, vector<float>& bin_values) const
{
    // get minimum and maximum of image
    bin_values.resize(Nbins);
    vector<T> m_minmax = range();
    float min = m_minmax[0];
    float range = m_minmax[1] - min;
    float spacing = range / Nbins;
    for(unsigned int bin = 0; bin < Nbins; ++bin) {
        bin_values[bin] = min + spacing / 2.0 + bin * spacing;
    }

    int nsamples = nvoxels();
    vector<float> hist(Nbins, 0);
    for(auto& i_data : m_data) {
        hist[static_cast<unsigned int>(static_cast<float>(i_data) - min) / range * Nbins]++;
    }

    for(auto& i_hist : hist) {
        i_hist /= nsamples;
        //		cout<<i_hist<<" ";
    }
    //	cout<<endl;
    //	cout<<"minmax "<<m_minmax[0]<<" "<<m_minmax[1]<<endl;
    return hist;
}

template <class T> void AsrImage3D<T>::updateStrides()
{
    if(m_ndims > 1)
        m_stride_y = m_v_size[0];
    if(m_ndims > 2)
        m_stride_z = m_v_size[0] * m_v_size[1];
}

template <class T> template <class T2> T2 AsrImage3D<T>::interpolate(const T2& x_mm, const T2& y_mm, const T2& z_mm) const
{
    return interpolate(vertex<T2>(x_mm, y_mm, z_mm));
}

template <class T> template <class T2> T2 AsrImage3D<T>::interpolate(const vertex<T2>& v) const
{ // bounds are currently ignored
    // assume T2 is float or double

    // coord = { xmin,xmax,ymin,ymax,zmin,zmax }
    // interpolate greycales along left edge (along y)
    // interpolate greycales along right edge (along y)
    // pass in first 4 coordinate, ignore z until the end
    // pass in front face and vback face grey scale value respectively
    // std::vector<T2>
    array<T2, 3> voxel_query{ v.x / m_v_dim[0], v.y / m_v_dim[1], v.z / m_v_dim[2] };

    array<int, 6> coord{ static_cast<int>(voxel_query[0]), static_cast<int>(voxel_query[0]) + 1,
        static_cast<int>(voxel_query[1]), static_cast<int>(voxel_query[1]) + 1, static_cast<int>(voxel_query[2]),
        static_cast<int>(voxel_query[2]) + 1 };
    // grab greyscale values

    array<T2, 8> gscale{ static_cast<T2>(value(coord[0], coord[2], coord[4])),
        static_cast<T2>(value(coord[0], coord[3], coord[4])), static_cast<T2>(value(coord[1], coord[3], coord[4])),
        static_cast<T2>(value(coord[1], coord[2], coord[4])), static_cast<T2>(value(coord[0], coord[2], coord[5])),
        static_cast<T2>(value(coord[0], coord[3], coord[5])), static_cast<T2>(value(coord[1], coord[3], coord[5])),
        static_cast<T2>(value(coord[1], coord[2], coord[5])) };
    // convert from scaled voxel to voxel
    // beware if T2 is an integer tyoe
    //	array<T2,3> voxel_query{ v.x , v.y , v.z  };

    //	 cout<<"voxel "<<(v.x / m_v_dim[0])<<" , "<<(v.y / m_v_dim[1])<<" , "<<(v.z / m_v_dim[2])<<endl;
    //	 cout<<"dims "<<m_v_dim[0]<<" "<<m_v_dim[1]<<" "<<m_v_dim[2]<<endl;
    return voxelTrilinearInterpolate(coord.data(), gscale.data(), voxel_query.data());
}

template class AsrImage3D<float>;
template class AsrImage3D<short>;

// template float AsrImage3D<float>::voxelLinearInterpolate<float>(const int & coord_x0, const int& coord_x1, const
// float&
// gscale0, const float& gscale1, const float& val );
// template float AsrImage3D<float>::voxelLinearInterpolate<float>(const int* coord,const float* gscale, const float&
// val
// );
// template float AsrImage3D<float>::voxelBilinearInterpolate<float>(const int* coord,const float* gscale, const
// float*
// val );
template float AsrImage3D<float>::interpolate(const vertex<float>& v) const;
template float AsrImage3D<short>::interpolate(const vertex<float>& v) const;

template float AsrImage3D<float>::interpolate(const float& x_mm, const float& y_mm, const float& z_mm) const;
template float AsrImage3D<short>::interpolate(const float& x_mm, const float& y_mm, const float& z_mm) const;

template void AsrImage3D<short>::copyImage(AsrImage3D<float>& dest, const AsrImage3D<short>& src, const int& nifiti_datatype);
template void AsrImage3D<float>::copyImage(AsrImage3D<short>& dest, const AsrImage3D<float>& src, const int& nifiti_datatype);
template void
AsrImage3D<short>::copyImageInfo(AsrImage3D<float>& dest, const AsrImage3D<short>& src, const int& nifiti_datatype);

template sizeCog AsrImage3D<float>::connectedComponents(AsrImage3D<short>& image_mask, const unsigned int& largestOnly);
template sizeCog AsrImage3D<float>::connectedComponents(AsrImage3D<float>& image_mask, const unsigned int& largestOnly);
template sizeCog AsrImage3D<short>::connectedComponents(AsrImage3D<short>& image_mask, const unsigned int& largestOnly);

template sizeCog AsrImage3D<float>::connectedComponents(AsrImage3D<float>& image_mask,
    const vector<int>& bounds,
    const unsigned int& numberOfLargest);
template sizeCog AsrImage3D<float>::connectedComponents(AsrImage3D<short>& image_mask,
    const vector<int>& bounds,
    const unsigned int& numberOfLargest);
template sizeCog AsrImage3D<short>::connectedComponents(AsrImage3D<short>& image_mask,
    const vector<int>& bounds,
    const unsigned int& numberOfLargest);

//	cout<<"connected compo
template void AsrImage3D<float>::mask<short>(AsrImage3D<short>& immask, bool invert);
template void AsrImage3D<short>::mask<short>(AsrImage3D<short>& immask, bool invert);

template unsigned int AsrImage3D<short>::fill<short>(AsrImage3D<short>& image,
    const int& x,
    const int& y,
    const int& z,
    const short& fillValue,
    const float& threshold,
    int connectivity);
template unsigned int AsrImage3D<float>::fill<short>(AsrImage3D<short>& image,
    const int& x,
    const int& y,
    const int& z,
    const short& fillValue,
    const float& threshold,
    int connectivity);
template unsigned int AsrImage3D<short>::fill<float>(AsrImage3D<float>& image,
    const int& x,
    const int& y,
    const int& z,
    const float& fillValue,
    const std::array<int, 6>& bounds,
    const float& threshold,
    int connectivity);
template unsigned int AsrImage3D<float>::fill<float>(AsrImage3D<float>& image,
    const int& x,
    const int& y,
    const int& z,
    const float& fillValue,
    const std::array<int, 6>& bounds,
    const float& threshold,
    int connectivity);

template unsigned int AsrImage3D<short>::fillDevelop<short>(AsrImage3D<short>& image,
    const int& x,
    const int& y,
    const int& z,
    AsrImage3D<float>& distFromTrachea,
    const short& fillValue,
    const float& threshold,
    const unsigned int maxSize,
    unordered_set<unsigned int>& vox_forbidden, unordered_set<unsigned int> & vox_mask,
    int connectivity);
template unsigned int AsrImage3D<float>::fillDevelop<short>(AsrImage3D<short>& image,
    const int& x,
    const int& y,
    const int& z,
    AsrImage3D<float>& distFromTrachea,
    const short& fillValue,
    const float& threshold,
    const unsigned int maxSize,
    unordered_set<unsigned int>& vox_forbidden, unordered_set<unsigned int> & vox_mask,
    int connectivity);
template unsigned int AsrImage3D<short>::fillDevelop<float>(AsrImage3D<float>& image,
    const int& x,
    const int& y,
    const int& z,
    AsrImage3D<float>& distFromTrachea,
    const float& fillValue,
    const std::array<int, 6>& bounds,
    const float& threshold,
    const unsigned int maxSize,
    unordered_set<unsigned int>& vox_forbidden, unordered_set<unsigned int> & vox_mask,
    int connectivity);
template unsigned int AsrImage3D<short>::fillDevelop<float>(AsrImage3D<float>& image,
    const int& x,
    const int& y,
    const int& z,
    AsrImage3D<float>& distFromTrachea,
    const float& fillValue,
    const float& threshold,
    const unsigned int maxSize,
    unordered_set<unsigned int>& vox_forbidden, unordered_set<unsigned int> & vox_mask,
    int connectivity);
template unsigned int AsrImage3D<float>::fillDevelop<float>(AsrImage3D<float>& image,
    const int& x,
    const int& y,
    const int& z,
    AsrImage3D<float>& distFromTrachea,
    const float& fillValue,
    const std::array<int, 6>& bounds,
    const float& threshold,
    const unsigned int maxSize,
    unordered_set<unsigned int>& vox_forbidden, unordered_set<unsigned int> & vox_mask,
    int connectivity);
template unsigned int AsrImage3D<float>::fillDevelop<float>(AsrImage3D<float>& image,
    const int& x,
    const int& y,
    const int& z,
    AsrImage3D<float>& distFromTrachea,
    const float& fillValue,
    const float& threshold,
    const unsigned int maxSize,
    unordered_set<unsigned int>& vox_forbidden, unordered_set<unsigned int> & vox_mask,
    int connectivity);

template unsigned int AsrImage3D<short>::fillDevelop<short>(AsrImage3D<short>& image,
    //    AsrImage3D<short>& mask,
    list<vec3<int> > voxel_list,
    AsrImage3D<float>& distFromTrachea,
    const short& fillValue,
    const float& threshold,
    const unsigned int maxSize,
    unordered_set<unsigned int>& vox_forbidden, unordered_set<unsigned int> & vox_mask,
    int connectivity);
template unsigned int AsrImage3D<float>::fillDevelop<short>(AsrImage3D<short>& image,
    //    AsrImage3D<short>& mask,
    list<vec3<int> > voxel_list,
    AsrImage3D<float>& distFromTrachea,
    const short& fillValue,
    const float& threshold,
    const unsigned int maxSize,
    unordered_set<unsigned int>& vox_forbidden, unordered_set<unsigned int> & vox_mask,
    int connectivity);
template unsigned int AsrImage3D<short>::fillDevelop<float>(AsrImage3D<float>& image,
    //    AsrImage3D<float>& mask,
    list<vec3<int> > voxel_list,
    AsrImage3D<float>& distFromTrachea,
    const float& fillValue,
    const std::array<int, 6>& bounds,
    const float& threshold,
    const unsigned int maxSize,
    unordered_set<unsigned int>& vox_forbidden, unordered_set<unsigned int> & vox_mask,
    int connectivity);
template unsigned int AsrImage3D<short>::fillDevelop<float>(AsrImage3D<float>& image,
    //    AsrImage3D<float>& mask,
    list<vec3<int> > voxel_list,
    AsrImage3D<float>& distFromTrachea,
    const float& fillValue,
    const float& threshold,
    const unsigned int maxSize,
    unordered_set<unsigned int>& vox_forbidden, unordered_set<unsigned int> & vox_mask,
    int connectivity);
template unsigned int AsrImage3D<float>::fillDevelop<float>(AsrImage3D<float>& image,
    //    AsrImage3D<float>& mask,
    list<vec3<int> > voxel_list,
    AsrImage3D<float>& distFromTrachea,
    const float& fillValue,
    const std::array<int, 6>& bounds,
    const float& threshold,
    const unsigned int maxSize,
    unordered_set<unsigned int>& vox_forbidden, unordered_set<unsigned int> & vox_mask,
    int connectivity);
}
