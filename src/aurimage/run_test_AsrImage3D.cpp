/*
 * run_test.cpp
 *
 *  Created on: May 11, 2016
 *      Author: brian
 */
#include "AsrImage3D.h"

//STL includes
#include <iostream>

//3rd party includes
#include <boost/filesystem.hpp>

using namespace std;
using namespace auris_segtools;
using namespace boost::filesystem;
void Usage(){
	cout<<"\n run_test <nifti_file>"<<endl;
	cout<<"\n This program is used to test the image library\n"<<endl;

}

int main(int argc, char*argv[])
{
	cout<<"\nRunning aurimage library test..."<<endl;
	if (argc == 1 )
	{
		Usage();
		return 0;
	}

	path file(argv[1]);
	if (!exists(file))
	{
		cerr<<"\n File does not exists : "<<file<<"\n"<<endl;
		return 1;
	}else if (!is_regular_file(file))
	{
		cerr<<"\n"<<file<<" is not a file"<<"\n"<<endl;
		return 1;
	}

	string filename(argv[1]);
	string filename_out(argv[2]);

	cout<<"Read in file: "<<filename<<endl;

	aurimage<float> im0(filename);

	//test image write
	im0.writeImage(filename_out);


	//---------------let's try changing a value----------------------------//
	auto v_prev = im0.value(30,45,10);
	float newval{3.14};
	 im0.value(30,45,10)=newval;
	cout<<"\nTrying to set voxel "<<30<<" "<<45<<" "<<10<<" to 3"<<newval<<" from "<<v_prev<<"..."<<endl;

	if ( newval ==  im0.value(30,45,10) ) cout<<"Passed."<<endl;
	else cout<<"Failed :  "<<newval<<" "<< im0.value(30,45,10)<<endl;

	//-----------------Testing interpolation--------------------//
	cout<<"\n Testing interpolation \n"<<endl;
	{
		cout<<"Testing linear interpolation..."<<endl;
		vector<int> x_bound{10,11};
		vector<float> gs_scale{1.34,2.5};
		cout<<"Interpolate between "<<x_bound[0]<<" and "<<x_bound[1]<<" with values "<<gs_scale[0]<<" and "<<gs_scale[1]<<endl;
		vector<float> inter_values{-1,0,5,10, 10.5,11 ,14};
		for ( auto v : inter_values)
		{
			//	cout<<"Value "<<v<<" -> "<<aurimage<float>::voxelLinearInterpolate(x_bound[0],x_bound[1],gs_scale[0],gs_scale[1],v)<<endl;
			cout<<"Value-ptr "<<v<<" -> "<<aurimage<float>::voxelLinearInterpolate(&(x_bound[0]),&(gs_scale[0]),v)<<endl;
		}
	}
	{
		cout<<"\n Testing Bilinear interpolation..."<<endl;
		vector<int> box_bound{10,13,3,5};
		vector<float> gs_scale_bi{1.34,2.5,3.7,0.4};
		cout<<"Box defined by : ";
		auto bi_gs = gs_scale_bi.begin();
		for ( int x=0; x < 2; ++x )
			for ( int y=0; y<2; ++y,++bi_gs)
			{
				cout<<" ("<<box_bound[x]<<","<<box_bound[2+y]<<","<<*bi_gs<<") ";
			}
		cout<<endl;
		cout<<"Do bilinear interpolation..."<<endl;
		vector<float> bi_x{static_cast<float>(box_bound[0]),static_cast<float>(box_bound[0]),\
			0.5f*(box_bound[0]+box_bound[1]), \
			static_cast<float>(box_bound[1]),static_cast<float>(box_bound[1])};

		vector<float> bi_y{static_cast<float>(box_bound[2]),static_cast<float>(box_bound[3]),\
			0.5f*(box_bound[2]+box_bound[3]),\
			static_cast<float>(box_bound[2]),static_cast<float>(box_bound[3])};
		for (int i =0 ; i < 5; ++i )
		{
			vector<float> v{ bi_x[i],bi_y[i] };
			cout<<"("<<v[0]<<","<<v[1]<<") -> "<<aurimage<float>::voxelBilinearInterpolate(&(box_bound[0]),&(gs_scale_bi[0]),&(v[0]))<<endl;
		}
	}

	cout<<"\n Testing Trilinear interpolation..."<<endl;
		vector<int> cube_bound{10,13,3,5,103,105};
		vector<float> gs_scale_tri{1.34,2.5,3.7,0.4,3.34,4.5,5.7,2.4 };
		cout<<"Box defined by : ";
		auto tri_gs = gs_scale_tri.begin();
		for ( int z=0; z<2; ++z)
		for ( int x=0; x < 2; ++x )
		for ( int y=0; y<2; ++y,++tri_gs)
		{
			cout<<" ("<<cube_bound[x]<<","<<cube_bound[2+y]<<","<<cube_bound[4+z]<<","<<*tri_gs<<") ";
		}
		cout<<endl;
		cout<<"Do trilinear interpolation..."<<endl;
		vector<float> tri_x{static_cast<float>(cube_bound[0]),static_cast<float>(cube_bound[0]),\
			static_cast<float>(cube_bound[1]),static_cast<float>(cube_bound[1]), \
			0.5f*(cube_bound[0]+cube_bound[1]), \
			static_cast<float>(cube_bound[0]),static_cast<float>(cube_bound[0]),\
			static_cast<float>(cube_bound[1]),static_cast<float>(cube_bound[1])
		};

		vector<float> tri_y{static_cast<float>(cube_bound[2]),static_cast<float>(cube_bound[3]),\
			static_cast<float>(cube_bound[2]),static_cast<float>(cube_bound[3]),\
			0.5f*(cube_bound[2]+cube_bound[3]),\
			static_cast<float>(cube_bound[2]),static_cast<float>(cube_bound[3]),\
			static_cast<float>(cube_bound[2]),static_cast<float>(cube_bound[3])};
		vector<float> tri_z{static_cast<float>(cube_bound[4]),static_cast<float>(cube_bound[4]),\
			static_cast<float>(cube_bound[4]),static_cast<float>(cube_bound[4]),\
			0.5f*(cube_bound[4]+cube_bound[5]),\
			static_cast<float>(cube_bound[5]),static_cast<float>(cube_bound[5]),\
			static_cast<float>(cube_bound[5]),static_cast<float>(cube_bound[5])};


		for (int i =0 ; i < 9; ++i )
		{
			vector<float> v{ tri_x[i],tri_y[i],tri_z[i] };
			cout<<"trilinear : ("<<v[0]<<","<<v[1]<<","<<v[2]<<") -> ";
			cout<<aurimage<float>::voxelTrilinearInterpolate(&(cube_bound[0]),&(gs_scale_tri[0]),&(v[0]))<<endl;

		}

}

