#ifndef AsrImage3D_H
#define AsrImage3D_H

// STL includes
#include <array>
#include <string>
#include <vector>
//#include <map>
#include <iostream>
#include <list>
#include <tuple>
#include <unordered_set>
// auris headers
#include <aurcommon/aurstructs.h>

// other headers
#include <nifti/nifti1_io.h>

namespace auris_segtools
{
using sizeCog = std::list<std::tuple<int, auris_segtools::vec3<float> > >;

template <class T> class AsrImage3D
{

    std::vector<int> m_v_size;
    std::vector<T> m_data;
    std::vector<float> m_v_dim;

    // assume stride x is always 1
    // not embedding stride_x_ in to value because of extra operation
    int m_ndims;
    int m_stride_y, m_stride_z;
    nifti_1_header* m_nifti_header_ptr{ nullptr };
    int m_nifti_datatype;
    T m_min, m_max;

public:
    AsrImage3D();
    AsrImage3D(const AsrImage3D<T>& image);
    AsrImage3D(const std::string& filename);
    virtual ~AsrImage3D();

    void initialize(nifti_1_header* header);
    // allows to change data type
    void initialize(const int& nifti_dtype, nifti_1_header* header);
    AsrImage3D<T>& operator=(const AsrImage3D<T>& image);
    AsrImage3D<T>& operator=(const T& value);
    AsrImage3D<T>& operator+=(const AsrImage3D<T>& image);

    // statistics
    T getMinIntensity() const;
    T getMaxIntensity() const;
    std::vector<float> histogram(const unsigned int& Nbins, std::vector<float>& bin_values) const;
    std::vector<T> range() const;
    void getBounds(std::array<int, 6>& bounds);

    vec3<float> getOrigin();
    vec3<float> getPixDim();
    std::array<float, 9> getR();
    float getQfac();

    vec3<float> getVox2mmShift();
    vec3<float> getmm2mmShift();
    vec3<float> getVox2mmScale();

    // image processing
    void lowerThreshold(const T& threshold, AsrImage3D<short>& bin_im);
    void upperThreshold(const T& threshold, AsrImage3D<short>& bin_im);
    void lowerUpperThreshold(const T& thresholdLower, const T& thresholdUpper, AsrImage3D<short>& bin_im);

    // zeors out image above spatial location identified by zmax (voxel)
    void zeroAboveZ(const int& zmax);
    void setValueAboveZ(const int& zmax, const T& value);

    void erode3x3(); // TODO FIX ERODDE
    void dilate3x3();

    void fillWithinMask(AsrImage3D<T>& mask, const T& fillValue);

    vec3<int> findSeedInZPLane() const;

    void fillXY(const int& x, const int& y, const int& z, const T& fillValue, int connectivity = 1);
    void fill(const int& x, const int& y, const int& z, const T& fillValue, int connectivity = 1);
    // connectiveness criteria 0,1,2
    void fill(const int& x,
        const int& y,
        const int& z,
        const T& fillValue,
        const std::array<int, 6>& bounds,
        int connectivity = 1);

    template <class T2>
    unsigned int fill(AsrImage3D<T2>& image,
        const int& x,
        const int& y,
        const int& z,
        const T2& fillValue,
        const std::array<int, 6>& bounds,
        const float& threshold,
        int connectivity = 1);
    template <class T2>
    unsigned int fill(AsrImage3D<T2>& image,
        const int& x,
        const int& y,
        const int& z,
        const T2& fillValue,
        const float& threshold,
        int connectivity = 1);
    template <class T2>
    unsigned int fillDevelop(AsrImage3D<T2>& image,
        const int& x,
        const int& y,
        const int& z,
        AsrImage3D<float>& distFromTrachea,
        const T2& fillValue,
        const std::array<int, 6>& bounds,
        const float& threshold,
        const unsigned int maxSize,
        std::unordered_set<unsigned int>& vox_forbidden,
        std::unordered_set<unsigned int>& vox_mask,
        int connectivity = 1);
    template <class T2>
    unsigned int fillDevelop(AsrImage3D<T2>& image,
        std::list<vec3<int> > voxel_list,
        AsrImage3D<float>& distFromTrachea,
        const T2& fillValue,
        const std::array<int, 6>& bounds,
        const float& threshold,
        const unsigned int maxSize,
        std::unordered_set<unsigned int>& vox_forbidden,
        std::unordered_set<unsigned int>& vox_mask,
        int connectivity = 1);
    template <class T2>
    unsigned int fillDevelop(AsrImage3D<T2>& image,
        const int& x,
        const int& y,
        const int& z,
        AsrImage3D<float>& distFromTrachea,
        const T2& fillValue,
        const float& threshold,
        const unsigned int maxSize,
        std::unordered_set<unsigned int>& vox_forbidden,
        std::unordered_set<unsigned int>& vox_mask,
        int connectivity = 1);
    template <class T2>
    unsigned int fillDevelop(AsrImage3D<T2>& image,
        std::list<vec3<int> > voxel_list,
        AsrImage3D<float>& distFromTrachea,
        const T2& fillValue,
        const float& threshold,
        const unsigned int maxSize,
        std::unordered_set<unsigned int>& vox_forbidden,
        std::unordered_set<unsigned int>& vox_mask,
        int connectivity = 1);
    //  unsigned int fillDevelop( AsrImage3D<T2> & image,   AsrImage3D<T2> & mask, AsrImage3D<float> & distFromTrachea, const
    //  T2 & fillValue, const float & threshold, const unsigned int maxSize, int connectivity = 1);

    // binarise and invert ==0
    void binaryInverse();
    void binarize();

    // option to do opposite masking
    template <class T2> void mask(AsrImage3D<T2>& immask, bool invert = false);

    // the output mask is image_mask, can input self
    template <class T2> sizeCog connectedComponents(AsrImage3D<T2>& image_mask, const unsigned int& largestOnly);
    template <class T2>
    sizeCog
    connectedComponents(AsrImage3D<T2>& image_mask, const std::vector<int>& bounds, const unsigned int& numberOfLargest);

    //    template<class T2>
    //    void distanceMap(AsrImage3D<float> & imDist );
    //    void distanceFromVoxel(const int &x, const int &y, const int &z, int connectivity, const bool & overwrite);
    //    void distanceFromVoxel(const vec3<int> & voxel, int connectivity, const bool & overwrite);
    //
    void normalize();
    // print image info to cout

    // input in mm
    template <class T2> T2 interpolate(const T2& x_mm, const T2& y_mm, const T2& z_mm) const;

    // input is in mm
    template <class T2> T2 interpolate(const vertex<T2>& v) const;
    T& value(const int& x, const int& y, const int& z);
    const T& value(const int& x, const int& y, const int& z) const;

    unsigned int getIndex(const int& x, const int& y, const int& z);
    vec3<int> getCoord(const unsigned int& index);
    void info();
    // int readVolume( const std::string & filename );

    vec3<int> GetSize() const 
    {
        return vec3<int>( m_v_size[0], m_v_size[1], m_v_size[2]);
    }
    int xsize() const
    {
        return m_v_size[0];
    }
    int ysize() const
    {
        return m_v_size[1];
    }
    int zsize() const
    {
        return m_v_size[2];
    }
    int nvoxels() const
    {
        return m_v_size[0] * m_v_size[1] * m_v_size[2];
    }
    float xdim() const
    {
        return m_v_dim[0];
    }
    float ydim() const
    {
        return m_v_dim[1];  
    }
    float zdim() const
    {
        return m_v_dim[2];
    }
    float voxelSize() const
    {
        return m_v_dim[0] * m_v_dim[1] * m_v_dim[2];
    }
    const T* data() const
    {
        return &(m_data[0]);
    }
    std::vector<T>& data_vector()
    {
        return m_data;
    }

    // IO
    int readImage(const std::string& filename);
    int writeImage(const std::string& filename) const;
    nifti_1_header getHeader()
    {
        return *m_nifti_header_ptr;
    }

    void setHeader(nifti_1_header* header)
    {
        if(header != nullptr)
            *m_nifti_header_ptr = *header;
    }

    static void copyImage(AsrImage3D<T>& dest, const AsrImage3D<T>& src);
    template <class T2> static void copyImage(AsrImage3D<T2>& dest, const AsrImage3D<T>& src, const int& nifiti_datatype);
    template <class T2>
    static void copyImageInfo(AsrImage3D<T2>& dest, const AsrImage3D<T>& src, const int& nifiti_datatype);

    //  template<class T2>
    //  inline static T2 voxelLinearInterpolate(const int & coord_x0, const int& coord_x1,const T& gscale0, const T&
    //  gscale1, const T2& val );

    // coord and gscale should be arrays of size 2nifti_1_header * header
    template <class T2> inline T2 voxelLinearInterpolate(const int* coord, const T2* gscale, const T2& val) const;

    // coord should be arrays of size 4
    // coord = { xmin,xmax,ymin,ymax }
    // gscale = { (xmin,ymin),(xmin,ymax),(xmax,ymax),(xmax,ymin)}
    // val is (x,y) coordinate
    template <class T2> inline T2 voxelBilinearInterpolate(const int* coord, const T2* gscale, const T2* val) const;

    // coord should be arrays of size 6
    // coord = { xmin,xmax,ymin,ymax,zmin,zmax }
    // size of 8
    // gscale = {
    // (xmin,ymin,zmin),(xmin,ymax,zmin),(xmax,ymax,zmin),(xmax,ymin,zmin),(xmin,ymin,zmax),(xmin,ymax,zmax),(xmax,ymax,zmax),(xmax,ymin,zmax)}
    // val is (x,y) coordinate
    // do bilnear on two faces of cube, then lkiner along line
    template <class T2> inline T2 voxelTrilinearInterpolate(const int* coord, const T2* gscale, const T2* val) const;

private:
    void updateStrides();
};

template <class T>
template <class T2>
T2 AsrImage3D<T>::voxelLinearInterpolate(const int* coord, const T2* gscale, const T2& val) const
{
    //	std::cout<<"linear "<<gscale[0]<<" "<<gscale[1]<<std::endl;
    //	std::cout<<"coord "<<coord[0]<<" "<<coord[1]<<std::endl;

    return static_cast<T2>(gscale[0]) +
        static_cast<T2>(gscale[1] - gscale[0]) *
        ((val - static_cast<T2>(coord[0])) / static_cast<T2>(coord[1] - coord[0]));
}

template <class T>
template <class T2>
T2 AsrImage3D<T>::voxelBilinearInterpolate(const int* coord, const T2* gscale, const T2* val) const
{

    // interpolate greycales along left edge (along y)
    // interpolate greycales along right edge (along y)
    std::vector<T2> interp_gscale{ AsrImage3D<T>::voxelLinearInterpolate(&(coord[2]), &(gscale[0]), val[1]),
        AsrImage3D<T>::voxelLinearInterpolate(&(coord[2]), &(gscale[2]), val[1]) };
    // std::cout<<"bilinear : interp inter "<<interp_gscale[0]<<" "<<interp_gscale[1]<<std::endl;
    // interpolate x
    return AsrImage3D<T>::voxelLinearInterpolate(&(coord[0]), &(interp_gscale[0]), val[0]);
}

template <class T>
template <class T2>
T2 AsrImage3D<T>::voxelTrilinearInterpolate(const int* coord, const T2* gscale, const T2* val) const
{
    // coord = { xmin,xmax,ymin,ymax,zmin,zmax }
    // interpolate greycales along left edge (along y)
    // interpolate greycales along right edge (along y)
    // pass in first 4 coordinate, ignore z until the end
    // pass in front face and vback face grey scale value respectively
    // std::vector<T2>
    std::vector<T2> interp_gscale{ AsrImage3D<T>::voxelBilinearInterpolate(&(coord[0]), &(gscale[0]), &(val[0])),
        AsrImage3D<T>::voxelBilinearInterpolate(&(coord[0]), &(gscale[4]), &(val[0])) };
    // std::cout<<"trilinear : interp inter "<<interp_gscale[0]<<" "<<interp_gscale[1]<<std::endl;
    // interpolate x
    return AsrImage3D<T>::voxelLinearInterpolate(&(coord[4]), &(interp_gscale[0]), val[2]);
}
}
#endif
