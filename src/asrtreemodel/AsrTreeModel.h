/*
 * aurTreeModel.h
 *
 *  Created on: May 13, 2016
 *      Author: brian
 */

#ifndef ASRTREEMODEL_H_
#define ASRTREEMODEL_H_
#include "aursurface/aursurface.h"
// need aurface to preceed  contour because of circular dependencies
#include "aursurface/aurcontour.h"
//#include "AsrImage3D/AsrImage3D.h"
#include "aurcommon/aurstructs.h"
//#include "aurgeometry/aurgeometry.h"
// GL
//#define GL_GLEXT_PROTOTYPES
//#include "GL/gl.h"

// GLM library
//#include <glm/mat4x4.hpp> // glm::mat4
//#include <glm/gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective
//#include <glm/gtx/string_cast.hpp>
//#include <Eigen/Core>
#include <asrtreemodel/AsrAirwayGeometryModel.h>
// STL
#include <list>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <vector>

//#include<set>
#include <tuple>
//#include<pair>

namespace auris_segtools
{

//    class AsrAirwayGeometryModel;
class AsrTreeModel
{
    
    using voxel = vec3<int>;
    using waveFront = std::unordered_set<unsigned int>;
    using skeletonSegment = std::tuple<int,
        int,
        unsigned int,
        float,
        unsigned int,
        float,
        float,
        float,
        vec3<float>,
        vec3<float>,
        std::string,
        vec3<float>,
        vec3<float>,
        float,
        float,
        float,
        float,
        vec3<float>,
        vec3<float>, float>;
    using vertexData = std::tuple<vec3<float>, vec3<float>, vec3<float>,float,float,float >;
    std::unordered_map<int, std::list<vec3<float> > > m_id_to_branch;
    //    std::unordered_map<unsigned int, std::list<int> > m_gen_to_id;
    std::unordered_map<int, int> m_id_to_parent;
    std::unordered_map<int, unsigned int> m_id_to_index;
    std::unordered_map<int, unsigned int> m_id_to_generation;
    std::unordered_set<int> m_leaves;
    std::vector<skeletonSegment> m_segments;
    std::vector<unsigned int> m_segmentation;
    aurcontour<float> m_skeleton_pts;
    AsrImage3D<float> m_skeleton;
    AsrAirwayGeometryModel m_airwayModel;
    std::list<vertexData> m_skeleton_pts_data;
    unsigned int m_maxGeneration{ 0 };
    float m_frontWidth{ 1.5 };
    unsigned int m_vdir_navg{ 3 };
    unsigned int m_maxGenerationCleanUp{ 5 };
    // criteria for accepting new branch, fraction of primary airway
    constexpr static float m_roithresh{ 0.0000001 };

    // number of points to average when calculating direction vector
    enum VERTEX_ELEMENT { AIRWAY_DIR, MAJOR_AXIS, MINOR_AXIS, MINOR_INNER_DIAMETER, MAJOR_INNER_DIAMETER, INNER_CROSS_SEC_AREA };
    enum SKEL_ELEMENT {
        BRANCH_ID,
        PARENT_ID,
        GENERATION,
        LENGTH,
        NPTS,
        AVG_MINOR_INNER_DIAMETER,
        AVG_MAJOR_INNER_DIAMETER,
        AVG_INNER_AREA,
        START_POINT,
        END_POINT,
        LABEL,
        ENTRY_POINT,
        BRANCH_POINT,
        EDIST_END_TO_BRANCH,
        ANGLE_WITH_PARENT,
        ENTRY_MAJOR_AXIS,
        ENTRY_MINOR_AXIS,
        START_VDIR,
        END_VDIR, 
        VOLUME
        //    PDIST_END_TO_BRANCH, remove due to innacuracies of the branch point, i.e. path does not necessarily extend
        //    to branch point
    };
    //    std::unordered_map< int, int> m_label_to_id;
public:
    AsrTreeModel();
    ~AsrTreeModel();
    void readTreePoints(const std::string& filename);
    void writeTreePoints(const std::string& filename) ;
    void writeTreePointsAsSurface(const std::string& filename, const float& radius);
    void readTreeStructure(const std::string& filename);
    void writeTreeStructure(const std::string& filename);
    void writeSegmentVectorsAsSurface(const std::string& filename);
    void writePointTensorsAsSurface(const std::string& filename);
    void writePerGenerationsStats(const std::string& filename);
    void writeAirwayGeometryModel(const std::string& filename);
    void writeSkeletonImage(const std::string& filename);
    const aurcontour<float>& getSkeletonPoints() const { return m_skeleton_pts; } 
    void scale(const float & sx, const float & sy, const float & sz );
    void scale(const vec3<float> & sxyz );
    void translate(const float & sx, const float & sy, const float & sz );
    void translate(const vec3<float>& sxyz );
    
    void leafStats();

    void CreateSkeleton( const AsrImage3D<float>& distanceFromSeed, const vec3<int>& seed, std::unordered_set<unsigned int> & seg_voxels);
//    std::list< vec3<float> > GetChildrenEntryPoints( const int & branchIndex );
   std::list< std::pair<int, vec3<float> > > GetChildrenEntryPoints( const int & branchIndex );
  static std::list< std::pair<int, vec3<float> > > GetChildrenEntryPoints( const std::vector<skeletonSegment> & segment, const int & branchIndex );
    std::vector< int > GetChildren( const int & branchID );
    const std::list<vertexData>& GetPointData() const { return m_skeleton_pts_data; };
    
    std::unordered_set< int > GetLeaves();
    bool IsLeaf( const int & branchID );
    std::unordered_set<int> GetGenerationBranchIDs(const unsigned int & generation );
    vec3< float > GetEntryPoint( const int & branchID) { return std::get<ENTRY_POINT>(m_segments[m_id_to_index[branchID]]); }
    unsigned int GetGeneration( const int & branchID) { return std::get<GENERATION>(m_segments[m_id_to_index[branchID]]); }
    void SetMaximumGeneration(const unsigned int & gen ) { m_maxGeneration = gen; } 
    unsigned int GetSegmentSizeInVoxels( const unsigned int & branchID);
    template <class T> static void printPerGenerationStats( std::ofstream& fout, const std::vector<T>& data, const std::string& name);
    template <class T> static void printPerGenerationStats(const std::vector<T>& data, const std::string& name);
    private:
    
    void updateTreeInfo();
    void computeAngles();

    static void EstimateCarinaPoints(const AsrImage3D<float>& distanceFromSeed, std::vector<skeletonSegment> & skelSegments, const float & scanResolution );
    static Eigen::Matrix<float, 3, 3> calculateLocalCoordinateFrame(AsrImage3D<float>& imFront,
        const waveFront& front,
        const vec3<float>& v_direction,
        const vec3<float>& cog);

    static void CreateSkeleton(  AsrImage3D<float> & distanceFromSeed,
        const vec3<int>& seed,
        const float& width,
        const int& maxGen,
        aurcontour<float>& skeleton,
        std::list<vertexData>& skeleton_pts_data,
        std::vector<skeletonSegment>& skelSegments,
        const unsigned int& vdir_navg,
        const unsigned int& maxGenCleanUp,
        const std::unordered_map<unsigned int, float>& gen2length, std::unordered_set<unsigned int> & seg_voxels);

    // -------Skeletonization code ------//
    static void PropagateWaveFrontUntilBifurcation(aurcontour<float>& skeleton,
        std::list<vertexData>& skeleton_pts_data,
        AsrImage3D<float>& imFront,
        const std::multimap<float, unsigned int>& distance2index,
        const std::unordered_map<unsigned int, float>& index2distance,
        std::unordered_map<unsigned int, std::vector<unsigned int> >& connectivity,
        float threshold,
        const float& thresholdStep,
        int& branchIndex,
        const int& parentIndex,
        const unsigned int& generationIndex,
        std::vector<skeletonSegment>& skelSegments,
        const vertex<float>& parentEndVertex,
        const unsigned int& maxGeneration,
        const unsigned int& vdir_navg,std::unordered_set<unsigned int> & seg_voxels);

    // this method splits a wavefront into connected components
    static unsigned int GetWaveFronts(AsrImage3D<float>& im,
        const waveFront& front,
        std::list<waveFront>& newFronts,
        std::list<voxel>& cogs,
        unsigned int& prevSize,
        const unsigned int& branchIndex,std::unordered_set<unsigned int> & seg_voxels);

    static void SplitWaveFront(const std::multimap<float, unsigned int>& distance2index,
        const std::unordered_map<unsigned int, float>& index2distance,
        const std::list<waveFront>& newFronts,
        std::vector<std::multimap<float, unsigned int> >& distance2indexParts,
        //    multimap<float, unsigned int>& distance2indexA,
        //    multimap<float, unsigned int>& distance2indexB,
        std::unordered_map<unsigned int, std::vector<unsigned int> >& connectivity);
    static void CleanUpSkeleton(std::vector<skeletonSegment>& skelSegments,
        aurcontour<float>& skeleton_pts,
        const int& maxBranchIndex,
        const unsigned int& maxGen,
        const std::unordered_map<unsigned int, float>& gen2length);
    static void LabelSkeletonBranches(std::vector<skeletonSegment>& skelSegments);
    static void GenerateSkeletonMaps(const std::vector<skeletonSegment>& skelSegments,
        std::unordered_multimap<int, int>& branch2child,
        std::unordered_map<int, unsigned int>& branch2index);
    static bool segmentCompareParent(skeletonSegment i, skeletonSegment j);

    // calculates direction at begining and end of segment
    // the end direction takes in the skeleton with the segment being the last entry
    static vec3<float> calculateStartDirection(const std::list<vertex<float> >& segment, const unsigned int& vdir_navg);
    static vec3<float> calculateEndDirection(aurcontour<float>& skeleton, const unsigned int& vdir_navg);

    // it assumes parentage has been set and that the direction vectors at begining and end are set
    static void computeAngles(std::vector<skeletonSegment>& segments);
    static void moveUp(std::vector<skeletonSegment>& segments,
        const std::unordered_multimap<int, int>& branch2child,
        const std::unordered_map<int, unsigned int>& branch2index,
        const int& pindex,
        const int& cindex);

    static void removeBranch(std::vector<skeletonSegment>& segments, const int& index);
static bool tupleCompare( std::tuple<float, vec3<float>, vec3<float> > i,std::tuple<float, vec3<float>, vec3<float> > j) { return ( std::get<0>(i) < std::get<0>(j) ); }

};
}
#endif