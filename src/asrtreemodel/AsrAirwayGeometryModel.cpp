#include <AsrAirwayGeometryModel.h>
#include <iostream>
#include <math.h>
using namespace std;

namespace auris_segtools
{

AsrAirwayGeometryModel::AsrAirwayGeometryModel()
{
//    m_generation2maximumSize = computeMaxAirwayVolumes(m_weibelSym_Gen2meanDiameter, m_weibelSym_Gen2meanLength);
    m_generation2maximumSize = computeMaxAirwayVolumes(m_105_volume);
    
}

AsrAirwayGeometryModel::~AsrAirwayGeometryModel()
{
}
std::unordered_map<unsigned int, float> AsrAirwayGeometryModel::GetGeneration2Length()
{
    return m_generation2length;
}

std::unordered_map<unsigned int, float>
AsrAirwayGeometryModel::computeMaxAirwayVolumes(const std::vector<float>& gen2volume){
    
    vector<float> gen2maxVolumes(gen2volume.size());

   // given binary branching structure
    
    unsigned int generation{ 1 };
    
    // loop over volumes per generation
    // Accumulate down tree structure
    auto i_maxVol = gen2maxVolumes.begin();
    for(auto i_volume = gen2volume.cbegin(); i_volume != gen2volume.cend(); ++i_volume, ++generation, ++i_maxVol) {
        // initialize to volume of a single branch at the current generation
        *i_maxVol = *i_volume;
        // start at begining
        // loop
        // add the current generation to previous generation multiplied by 2^(number of generation down from target)
        // only visits those prior to the current generation
        auto ii_volume = gen2maxVolumes.begin();
        for(unsigned int generationPrev = 1; generationPrev < generation; ++generationPrev, ++ii_volume) {
            *ii_volume += pow(2, generation - generationPrev) * (*i_volume); // accumulate generations
        }
    }

    // format outout
    std::unordered_map<unsigned int, float> map_generation2maxVolumes;
    { // pou
        unsigned int gen{ 1 };
        for(auto& vol : gen2maxVolumes) {
            map_generation2maxVolumes[gen++] = vol;
        }
    }
    return map_generation2maxVolumes;
}
std::unordered_map<unsigned int, float>
AsrAirwayGeometryModel::computeMaxAirwayVolumes(const std::vector<float>& diameters, const std::vector<float>& length)
{
    vector<float> gen2maxVolumes(diameters.size());
    vector<float> gen2volume(diameters.size());
    //        auto i_l = length.begin();
    // calculate volumes for a given generation branch (assumes cylinder)

    auto i_volume = gen2volume.begin();
    auto i_length = length.begin();
    for(auto i_d = diameters.begin(); i_d != diameters.end(); ++i_d, ++i_length, ++i_volume) {
        float radius{ (*i_d) * 0.5f };
        *i_volume = M_PI * radius * radius * (*i_length);
    }
    //
    // given binary branching structure
    unsigned int generation{ 1 };
    // loop over volumes per generation
    // Accumulate down tree structure
    auto i_maxVol = gen2maxVolumes.begin();
    for(auto i_volume = gen2volume.begin(); i_volume != gen2volume.end(); ++i_volume, ++generation, ++i_maxVol) {
        // initialize to volume of a single branch at the current generation
        *i_maxVol = *i_volume;
        // start at begining
        // loop
        // add the current generation to previous generation multiplied by 2^(number of generation down from target)
        // only visits those prior to the current generation
        auto ii_volume = gen2maxVolumes.begin();
        for(unsigned int generationPrev = 1; generationPrev < generation; ++generationPrev, ++ii_volume) {
            *ii_volume += pow(2, generation - generationPrev) * (*i_volume); // accumulate generations
        }
    }

    // format outout
    std::unordered_map<unsigned int, float> map_generation2maxVolumes;
    { // pou
        unsigned int gen{ 1 };
        for(auto& vol : gen2maxVolumes) {
            map_generation2maxVolumes[gen++] = vol;
        }
    }
    return map_generation2maxVolumes;
}
}