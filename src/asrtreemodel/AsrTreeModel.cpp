/*
 * AsrTreeModel.cpp
 *
 *  Created on: May 13, 2016
 *      Author: brian
 */

#include "AsrTreeModel.h"
#include "aurcommon/aurstructs_functions.hpp"
#include "aurcommon/miscfunctions.h"
#include <Eigen/LU>
#include <Eigen/SVD>
// STL
#include <fstream>
#include <iostream>
#include <queue>
#include <sstream>
#include <vector>
//#include <array>
//#include <unordered_set>
//#include <unordered_map>
//#include <map>
#include <nifti/nifti1_io.h>

using namespace std;
using namespace Eigen;
using branch = std::list<auris_segtools::vec3<float> >;

// template< class T> using AsrTreeModel::contour;
namespace auris_segtools
{

AsrTreeModel::AsrTreeModel()
    : m_frontWidth(1.5)
    , m_vdir_navg(3)
    , m_maxGenerationCleanUp(5)
{
}

AsrTreeModel::~AsrTreeModel()
{
}

void AsrTreeModel::writeSkeletonImage(const std::string& filename)
{
    m_skeleton.writeImage(filename);
}
void AsrTreeModel::scale(const float& sx, const float& sy, const float& sz)
{
    m_skeleton_pts.scale(sx, sy, sz);
}
void AsrTreeModel::scale(const vec3<float>& sxyz)
{
    m_skeleton_pts.scale(sxyz.x, sxyz.y, sxyz.z);
}
void AsrTreeModel::translate(const float& dx, const float& dy, const float& dz)
{
    m_skeleton_pts.translate(vec3<float>(dx, dy, dz));
}
void AsrTreeModel::translate(const vec3<float>& dxyz)
{
    m_skeleton_pts.translate(dxyz);
}

void AsrTreeModel::writePerGenerationsStats(const std::string& filename)
{

    ofstream fout(filename.c_str());
    if(fout.is_open()) {

        vector<unsigned int> leafCountPerGen(m_maxGeneration, 0);
        vector<unsigned int> countPerGen(m_maxGeneration, 0);

        // leafCountPerGen
        for(auto& leaf : m_leaves) {
            leafCountPerGen[m_id_to_generation[leaf] - 1]++;
        }
        for(auto& br : m_id_to_parent) {
            countPerGen[m_id_to_generation[br.first] - 1]++;
        }
        //        cout << "Total number of branches: " << m_id_to_generation.size() << endl;
        printPerGenerationStats(fout, leafCountPerGen, "Number of leave in each generation");
        printPerGenerationStats(fout, countPerGen, "Number of branches at each generation");
    }
}
template <class T>
void AsrTreeModel::printPerGenerationStats(ofstream& fout, const std::vector<T>& data, const string& name)
{

    fout << name << ":" << endl;
    if(data.empty())
        return;

    fout << "1";
    for(unsigned int gen = 2; gen <= data.size(); ++gen) {
        if(gen < 10) {
            fout << " ";
        }
        fout << " " << gen;
    }
    fout << endl;

    auto i = data.cbegin();
    fout << (*(i++));
    for(; i < data.cend(); ++i) {
        if(*i < 10) {
            fout << " ";
        }
        fout << " " << *i;
    }
    fout << endl;
}
template <class T> void AsrTreeModel::printPerGenerationStats(const std::vector<T>& data, const string& name)
{

    cout << name << ":" << endl;
    if(data.empty())
        return;

    cout << "1";
    for(unsigned int gen = 2; gen <= data.size(); ++gen) {
        if(gen < 10) {
            cout << " ";
        }
        cout << " " << gen;
    }
    cout << endl;

    auto i = data.cbegin();
    cout << (*(i++));
    for(; i < data.cend(); ++i) {
        if(*i < 10) {
            cout << " ";
        }
        cout << " " << *i;
    }
    cout << endl;
}
void AsrTreeModel::leafStats()
{
    vector<unsigned int> leafCountPerGen(m_maxGeneration, 0);
    vector<unsigned int> countPerGen(m_maxGeneration, 0);

    // leafCountPerGen
    for(auto& leaf : m_leaves) {
        leafCountPerGen[m_id_to_generation[leaf] - 1]++;
    }
    for(auto& br : m_id_to_parent) {
        countPerGen[m_id_to_generation[br.first] - 1]++;
    }
    cout << "Total number of branches: " << m_id_to_generation.size() << endl;
    printPerGenerationStats(leafCountPerGen, "Number of leave in each generation");
    printPerGenerationStats(countPerGen, "Number of branches at each generation");
}
void AsrTreeModel::readTreeStructure(const string& filename)
{
    // assumes binary tree
    m_maxGeneration = 0;
    ifstream fin(filename.c_str());
    if(fin.is_open()) {
        unordered_set<int> all_parents; // used to find leaves

        string line;
        getline(fin, line); // throw away header

        while(getline(fin, line)) {
            stringstream ss(line);
            // label
            string val;
            if(!getline(ss, val, ','))
                return;
            // branchID
            if(!getline(ss, val, ','))
                return;
            int branchID{ stoi(val) };

            // parent
            if(!getline(ss, val, ','))
                return;
            int parent{ stoi(val) };

            // generation
            if(!getline(ss, val, ','))
                return;
            unsigned int generation{ static_cast<unsigned int>(stoi(val)) };

            if(m_maxGeneration < generation)
                m_maxGeneration = generation;
            m_id_to_generation[branchID] = generation;
            m_id_to_parent[branchID] = parent;
            // list of parents is used to find the terminal branches
            all_parents.insert(parent);
        }
        fin.close();

        // find all leaves (terminal branches)
        for(auto& itree : m_id_to_parent) {
            if(all_parents.count(itree.first) == 0) {
                m_leaves.insert(itree.first);
            }
        }
    }
}

void AsrTreeModel::writeAirwayGeometryModel(const string& filename)
{
    ofstream fout(filename.c_str());

    if(fout.is_open()) {

        fout << "Generation NumberOfBranches maxVolume meanVolume varVolume maxLength meanLength varLength" << endl;
        unordered_map<unsigned int, float> gen2maxVolume, gen2meanVolume, gen2varVolume, gen2sxxVolume; //,gen2sxVolume;
        unordered_map<unsigned int, float> gen2maxLength, gen2meanLength, gen2varLength, gen2sxxLength; //,gen2sxLength;
        unordered_map<unsigned int, float> gen2N;
        for(auto& seg : m_segments) {
            unsigned int generation{ get<GENERATION>(seg) };
            if(gen2maxVolume.count(generation) == 0) { // first time we've seen the generation
                gen2N[generation] = 1;

                gen2maxVolume[generation] = get<VOLUME>(seg);
                gen2meanVolume[generation] = get<VOLUME>(seg);
                //                        gen2sxVolume[generation]=get<VOLUME>(seg);
                gen2sxxVolume[generation] = get<VOLUME>(seg) * get<VOLUME>(seg);

                gen2maxLength[generation] = get<LENGTH>(seg);
                gen2meanLength[generation] = get<LENGTH>(seg);
                //                        gen2sxLength[generation]=get<LENGTH>(seg);
                gen2sxxLength[generation] = get<LENGTH>(seg) * get<LENGTH>(seg);

            } else {
                gen2N[generation]++;

                if(gen2maxVolume[generation] < get<VOLUME>(seg))
                    gen2maxVolume[generation] = get<VOLUME>(seg);
                gen2meanVolume[generation] += get<VOLUME>(seg);
                //                        gen2sxVolume[generation]+=get<VOLUME>(seg);
                gen2sxxVolume[generation] += get<VOLUME>(seg) * get<VOLUME>(seg);

                if(gen2maxLength[generation] < get<LENGTH>(seg))
                    gen2maxLength[generation] = get<LENGTH>(seg);
                gen2meanLength[generation] += get<LENGTH>(seg);
                //                        gen2sxLength[generation]+=get<LENGTH>(seg);
                gen2sxxLength[generation] += get<LENGTH>(seg) * get<LENGTH>(seg);
            }
        }
        for(auto& gen : gen2N) {
            float varVolumes{ 0.0 };
            float varLengths{ 0.0 };
            if(gen.second > 1) {
                // mean is actually just sum at this point
                varVolumes = (gen2sxxVolume[gen.first] -
                                 gen2meanVolume[gen.first] * gen2meanVolume[gen.first] / (gen.second * gen.second)) /
                    (gen.second - 1);
                varLengths = (gen2sxxLength[gen.first] -
                                 gen2meanLength[gen.first] * gen2meanLength[gen.first] / (gen.second * gen.second)) /
                    (gen.second - 1);
            }
            fout << gen.first << " " << gen.second;
            fout << " " << gen2maxVolume[gen.first] << " " << gen2meanVolume[gen.first] / gen.second << " "
                 << varVolumes;
            fout << " " << gen2maxLength[gen.first] << " " << gen2meanLength[gen.first] / gen.second << " "
                 << varLengths;
            fout << endl;
        }
    }
}

void AsrTreeModel::readTreePoints(const string& filename)
{
    // assumptions
    // all segments are conitnues and ordered in the file
    cout << "read tree points" << endl;
    ifstream fin(filename.c_str());
    if(fin.is_open()) {
        string line;
        getline(fin, line); // throw away header

        branch singleBranch;
        while(getline(fin, line)) {
            // each line contains an ID and x,y,z
            stringstream ss(line);
            int branchID;
            if(m_id_to_branch.count(branchID) == 0) {
                // New branch, add the empty branch
                m_id_to_branch[branchID] = branch();
            }
            vec3<float> pt;
            vertexData pt_data(vec3<float>(0, 0, 0), vec3<float>(0, 0, 0), vec3<float>(0, 0, 0), 0, 0, 0);
            string val;
            // return because of invalid file
            if(!getline(ss, val, ','))
                return;
            branchID = stoi(val);

            if(!getline(ss, val, ','))
                return;
            pt.x = stof(val);

            if(!getline(ss, val, ','))
                return;
            pt.y = stof(val);

            if(!getline(ss, val, ','))
                return;
            pt.z = stof(val);

            if(!getline(ss, val, ','))
                return;

            if(val != "NA") {
                get<MINOR_INNER_DIAMETER>(pt_data) = stof(val);
            }
            if(!getline(ss, val, ','))
                return;
            if(val != "NA") {
                get<MAJOR_INNER_DIAMETER>(pt_data) = stof(val);
            }

            if(!getline(ss, val, ','))
                return;
            // substring because the endol
            if(val.substr(0, 2) != "NA") {
                get<INNER_CROSS_SEC_AREA>(pt_data) = stof(val);
            }

            m_skeleton_pts.push_back(vertex<float>(pt.x, pt.y, pt.z, 0, 0, 0, branchID));
            m_skeleton_pts_data.push_back(pt_data);
            //            ss >> branchID >> pt.x >> pt.y >> pt.z;
            m_id_to_branch[branchID].push_back(pt);
        }
        fin.close();
    }

    //    for(auto& id : m_id_to_branch) {
    //        cout << "new branch" << endl;
    //        for(auto& pt : id.second) {
    //            cout << id.first << " " << pt.to_string() << endl;
    //        }
    //    }
}
void AsrTreeModel::writePointTensorsAsSurface(const std::string& filename)
{

    float length{ 2.0f };
    float radius{ 0.5f };
    auto i_tensor = m_skeleton_pts.beginT();

    aursurface<float> allTensors;
    for(auto i_vert = m_skeleton_pts.cbegin(); i_vert != m_skeleton_pts.cend(); ++i_vert, ++i_tensor) {
        //        for(int t = 0; t < 1; ++t) {
        for(int t = 0; t < 3; ++t) {
            aursurface<float> tensor, tensorRev;
            //            cout << "tensor  " << vec3<float>((*i_tensor)(0, t), (*i_tensor)(1, t), (*i_tensor)(2,
            //            t)).to_string()
            //                 << endl;
            vec3<float> tens = vec3<float>((*i_tensor)(0, t), (*i_tensor)(1, t), (*i_tensor)(2, t));
            if(l2norm(tens) > 0) {
                //                cout << "create Arrow " << t << endl;
                //                cout << "tneg " << (tens * -1).to_string() << endl;
                {
                    tensor.createArrow(
                        vec3<float>(*i_vert), tens, l2norm(tens), radius, 10, 0.5f, aursurface<float>::START_AT_CENTER);
                    tensor.setScalars(t);
                    if(!tensor.empty())
                        allTensors.append(tensor);
                }
                {

                    tensorRev.createArrow(vec3<float>(*i_vert), tens * -1.0f, l2norm(tens), radius, 10, 0.5f,
                        aursurface<float>::START_AT_CENTER);
                    tensorRev.setScalars(t);
                    if(!tensorRev.empty())
                        allTensors.append(tensorRev);
                }
            }
        }
    }

    allTensors.computeVertexNormals();
    allTensors.write(filename, aursurface<float>::VTK);
}

void AsrTreeModel::writeSegmentVectorsAsSurface(const std::string& filename)
{
    aursurface<float> arrows;
    for(auto& seg : m_segments) {
        aursurface<float> start_arrow, end_arrow;
        start_arrow.createArrow(get<START_POINT>(seg), get<START_VDIR>(seg), 0.25 * get<LENGTH>(seg), 2.5, 10, 0.5,
            aursurface<float>::START_AT_CENTER);
        //        cout << "start vdir " << get<START_VDIR>(seg).to_string() << endl;
        //        cout << "end vdir " << get<END_VDIR>(seg).to_string() << endl;
        //        cout << "angle " << (M_PI - abs(angle(get<START_VDIR>(seg), get<END_VDIR>(seg)))) * 180 / M_PI <<
        //        endl;

        end_arrow.createArrow(get<END_POINT>(seg), get<END_VDIR>(seg), get<LENGTH>(seg), 2.5f, 10, 0.5f,
            aursurface<float>::END_AT_CENTER);
        //        start_arrow.createArrow(get<START_POINT>(seg),vec3<float>(1,0,0), 0.5 * get<LENGTH>(seg),5.0,10);
        if(!start_arrow.empty())
            arrows.append(start_arrow);
        if(!end_arrow.empty())
            arrows.append(end_arrow);
    }
    arrows.computeVertexNormals();
    arrows.write(filename, aursurface<float>::VTK);
}

void AsrTreeModel::writeTreePoints(const std::string& filename)
{
    //    m_skeleton_pts.writeSkeletonCSV(filename);
    ofstream fout(filename.c_str());
    if(fout.is_open()) {
        // write header
        fout << "LinkId,x,y,z,MinorInnerDiameter,MajorInnerDiameter,InnerCrossSecArea" << endl;
        auto i_pt_data = m_skeleton_pts_data.begin();
        for(auto i_vert = m_skeleton_pts.cbegin(); i_vert != m_skeleton_pts.cend(); ++i_vert, ++i_pt_data) {
            fout << i_vert->scalar << "," << i_vert->x << "," << i_vert->y << "," << i_vert->z << ","
                 << get<MINOR_INNER_DIAMETER>(*i_pt_data) << "," << get<MAJOR_INNER_DIAMETER>(*i_pt_data) << ","
                 << get<INNER_CROSS_SEC_AREA>(*i_pt_data) << endl;
        }
        fout.close();
    }
}
void AsrTreeModel::writeTreePointsAsSurface(const std::string& filename, const float& radius)
{
    m_skeleton_pts.getVerticesAsSpheres(radius).write(filename, aursurface<float>::VTK);
}

void AsrTreeModel::writeTreeStructure(const std::string& filename)
{
    ofstream fskelTree(filename);
    if(fskelTree.is_open()) {
        fskelTree << "label,id,parentId,generation,centerLineLength,NumberOfSamples,avgMinorInnerDiam,avgMajorInnerDiam"
                  << ",avgInnerArea,startPoint_x,startPoint_y,startPoint_z,endPoint_x,endPoint_y,endPoint_z"
                  << ",childEntryPoint_x,childEntryPoint_y,childEntryPoint_z,branchPoint_x,branchPoint_y,branchPoint_z"
                  << ",EuclideanDistanceFromEndToBranchPoint"
                  << ",startAirwayDirection_x,startAirwayDirection_y,startAirwayDirection_z"
                  << ",endAirwayDirection_x,endAirwayDirection_y,endAirwayDirection_z"
                  << ",AngleFromParent,MajorAxisLengthAtEntry,MinorAxisLengthAtEntry,Volume(mm^3)  " << endl;
        for(auto& i : m_segments) {
            fskelTree << get<LABEL>(i) << "," << get<BRANCH_ID>(i) << "," << get<PARENT_ID>(i) << ","
                      << get<GENERATION>(i) << "," << get<LENGTH>(i) << "," << get<NPTS>(i) << ","
                      << get<AVG_MINOR_INNER_DIAMETER>(i) << "," << get<AVG_MAJOR_INNER_DIAMETER>(i) << ","
                      << get<AVG_INNER_AREA>(i) << "," << get<START_POINT>(i).to_stringCSV() << ","
                      << get<END_POINT>(i).to_stringCSV() << "," << get<ENTRY_POINT>(i).to_stringCSV() << ","
                      << get<BRANCH_POINT>(i).to_stringCSV() << "," << get<EDIST_END_TO_BRANCH>(i) << ","
                      << get<START_VDIR>(i).to_stringCSV() << "," << get<END_VDIR>(i).to_stringCSV() << ","
                      << get<ANGLE_WITH_PARENT>(i) * 180.0 / M_PI << "," << get<ENTRY_MAJOR_AXIS>(i) << ","
                      << get<ENTRY_MINOR_AXIS>(i) << "," << get<VOLUME>(i) << endl;
        }
        fskelTree.close();
    }
}
Eigen::Matrix<float, 3, 3> AsrTreeModel::calculateLocalCoordinateFrame(AsrImage3D<float>& imFront,
    const waveFront& front,
    const vec3<float>& v_direction,
    const vec3<float>& cog)
{
    // will be ill conditioned
    if(front.size() < 3) {
        return Matrix<float, 3, 3>::Zero();
    }

    MatrixXf mpoints(3, front.size());
    int col = 0;
    float dx{ imFront.xdim() }, dy{ imFront.ydim() }, dz{ imFront.zdim() };
    for(auto& i : front) {
        vec3<int> coord = imFront.GetCoord(i);
        vec3<float>((coord.x * dx - cog.x), (coord.y * dy - cog.y), (coord.z * dz - cog.z));
        mpoints(0, col) = coord.x * dx - cog.x;
        mpoints(1, col) = coord.y * dy - cog.y;
        mpoints(2, col) = coord.z * dz - cog.z;
        ++col;
    }
    //    mpoints.colwise() -= Vector3f(cog.x,cog.y,cog.z);
    // each column of U is an axis
    // U compose major/minor axes of the airway cross-section
    // and the direction of the airway
    JacobiSVD<MatrixXf> svd(mpoints, ComputeThinU | ComputeThinV);
    // cout<<"frontsize and eigens "<<front.size()<<" "<<svd.nonzeroSingularValues()<<endl;
    // rank deficient, ill-conditioned problem
    if(svd.nonzeroSingularValues() < 3) {
        return Matrix<float, 3, 3>::Zero();
    }
    RowVector3f Mdirection;
    Mdirection << v_direction.x, v_direction.y, v_direction.z;
    //    cout << "current direction " << endl << Mdirection << endl;
    //    cout << "dotProduict " << endl << Mdirection * svd.matrixU() << endl;
    //    RowVector3f innerProd =  (Mdirection * svd.matrixU()).abs();
    Array3f innerProd = (Mdirection * svd.matrixU()).array();
    Array3f innerProdAbs = innerProd.abs();
    //    int maxIndex = (innerProdAbs(0) > innerProdAbs(1)) ? (innerProdAbs(0) > innerProdAbs(2) ? 0 : 2) :
    //                                                         (innerProdAbs(1) > innerProdAbs(2) ? 1 : 2);
    // choose airway direction as smallest sinfular value
    // this should be true in large airways
    // using cog estimated direction is noisy in large toruous airways
    int airDirIndex = (svd.singularValues()(0) < svd.singularValues()(1)) ?
        (svd.singularValues()(0) < svd.singularValues()(2) ? 0 : 2) :
        (svd.singularValues()(1) < svd.singularValues()(2) ? 1 : 2);
    // TODO add logic for smaller airways

    //    cout << "maxIndex " << airDirIndex << endl;
    //    cout << "coord system " << endl << svd.matrixU() << endl;
    //    cout << "eigenvalue " << svd.singularValues() << endl;
    //    cout << "direction and dot " << v_direction.to_string() << endl;
    // goign to swap out columsn, such that airway direction is always first
    Matrix3f coordSystem = svd.matrixU();

    if(airDirIndex > 0) {
        coordSystem.col(0) = coordSystem.col(airDirIndex);
        coordSystem.col(airDirIndex) = svd.matrixU().col(0);
    }
    // reorient the vector into teh actual direction
    if(innerProd(airDirIndex, 0) < 0) {
        // The column should already have been swapped into zero place
        coordSystem.col(0) *= -1.0;
    }
    // Project points
    // demean points

    MatrixXf ptsProjected = coordSystem.transpose() * mpoints;
    Vector3f max = ptsProjected.rowwise().maxCoeff();
    //    float x = ptsProjected.row(0).maxCoeff();
    //    float y = ptsProjected.row(1).array().max();
    //    float z = ptsProjected.row(2).array().max();
    //    cout<<"max "<<x<<" "<<y<<" "<<z<<endl;
    // not doing scaling in airway direction yet
    for(int i = 0; i < 3; ++i) {
        coordSystem.col(i) *= max(i);
        //        cout << "max " << i << " " << max(i) << endl;
        if(max(i) == 0) {
            return Matrix<float, 3, 3>::Zero();
        }
    }
    // sort the major and minor axes
    int majorIndex = (max(1) > max(2)) ? 1 : 2;
    if(majorIndex == 2) {
        Vector3f tmpCol = coordSystem.col(1);
        coordSystem.col(1) = coordSystem.col(2);
        coordSystem.col(2) = tmpCol;
    }

    // get the magintudes of the tesnors such that they indicate the radius

    //    cout << "coordSyst final " << endl << coordSystem << endl;
    //    return svd.matrixU();
    return coordSystem;
}

void AsrTreeModel::PropagateWaveFrontUntilBifurcation(aurcontour<float>& skeleton,
    list<vertexData>& skeleton_data,
    AsrImage3D<float>& imFront,
    const multimap<float, unsigned int>& distance2index,
    const unordered_map<unsigned int, float>& index2distance,
    unordered_map<unsigned int, vector<unsigned int> >& connectivity,
    float threshold,
    const float& thresholdStep,
    int& branchIndex,
    const int& parentIndex,
    const unsigned int& generationIndex,
    vector<skeletonSegment>& skelSegments,
    const vertex<float>& parentEndVertex,
    const unsigned int& maxGeneration,
    const unsigned int& vdir_navg,
    unordered_set<unsigned int>& seg_voxels)
{
    //        if (branchIndex> 10) return;
    if(maxGeneration > 0)
        if(generationIndex > maxGeneration)
            return;

    //    cout << "prop wave " << parentEndVertex.coord_to_string() << endl;
    //    aurcontour<float> skeleton;
    if(distance2index.empty())
        return;
    float voxelVolume{ imFront.GetVoxelVolume() };
    branchIndex++;
    skelSegments.push_back(skeletonSegment(branchIndex, parentIndex, generationIndex, 0, 0, 0, 0, 0, vec3<float>(),
        vec3<float>(), "unknown", vec3<float>(), vec3<float>(), 0, 0, 0, 0, vec3<float>(), vec3<float>(), 0));
    float threshMax{ distance2index.rbegin()->first };
    //    cout << "initial threshold " << branchIndex << " " << threshold << " / " << threshMax << endl;
    auto i_dist2index = distance2index.begin();
    //    int frontIndex{ 1 };
    unsigned int prevSize(0);
    //    unsigned int Nbranch{0};count of number of vertices added to branch
    list<vertex<float> > l_branch_verts;
    list<vertexData> l_branch_vertsData;
    list<Matrix3f> l_branch_tensors;
    //    cout << "newhile " << branchIndex << endl;
    unsigned int Ncentres{ 0 };
    while(threshold < threshMax) {
        //            imFront=0;
        //        cout << "thrshold " << threshold << " / " << threshMax << endl;
        waveFront front;
        while(i_dist2index->first < (threshold - thresholdStep))
            ++i_dist2index; // advance to start

        while(i_dist2index->first <= threshold) {
            front.insert(i_dist2index->second);
            ++i_dist2index;
        }

        list<voxel> cogs;
        list<waveFront> newFronts;
        unsigned int N = GetWaveFronts(imFront, front, newFronts, cogs, prevSize, branchIndex, seg_voxels);
        //        cout << "Fronts : " << branchIndex << " " << N;
        //        for(auto& f : newFronts)
        //            cout << " " << f.size();
        //        cout << endl;
        // Need to handle the case where there is no longer any front
        // allows for emtpy fronts, rely on threshold loop to end
        // seem to be necessary, think due to sampling thresholds
        if(N == 0) {
            //            cout << "increment threshold " << threshold << " " << thresholdStep << endl;
            threshold += thresholdStep;
            //            cout << "increment threshold " << threshold << " "<<threshMax<<endl;
            if(threshold < threshMax)
                break;

            continue;
            //            return;
        }

        // The number of connected wavefronts need to be calculate prior the following conditionals
        // in the case there is an immediate bifurcation

        // Interpolate between the bifurcation point of the parent and the start point of the child
        // This is simple a linear interpolation
        // Only perform if it's not the trachea (i.e. branchIndex==1) not vertices have been added (i.e. first
        // vertex in
        // child)

        // storing start of segment prior to adding vertices of interpolation
        bool isStartOfSegment{ false };
        if((branchIndex > 1) && (l_branch_verts.empty()) && (N > 0)) {
            isStartOfSegment = true;
            // Centre of gravity of first child point
            // It will use ther first one, in corner case whether it's > 2
            vertex<float> cog0(cogs.front().x * imFront.xdim(), cogs.front().y * imFront.ydim(),
                cogs.front().z * imFront.zdim(), branchIndex);

            // update the branch information
            get<ENTRY_POINT>(skelSegments.back()) = cog0;
            // line paramteretization
            //            cout << "start of child : " << cog0.coord_to_string() << endl;
            vec3<float> vAB = cog0 - parentEndVertex;
            // calculate number of steps between parent and child
            unsigned int Nsteps = l2norm(vAB) / thresholdStep;
            normalize(vAB); // make unit vector

            vertex<float> startVertex = parentEndVertex;
            startVertex.scalar = branchIndex;
            // Create vertcies and add to the branch.
            // omit beginning and end vertex because laready handled
            // less than N step since the COG will be added in the next loop
            for(unsigned int i = 1; i < Nsteps; ++i) {
                startVertex = startVertex + vAB * thresholdStep;
                l_branch_verts.push_back(startVertex);
                l_branch_tensors.push_back(Matrix3f::Zero());
                // add interpolated vertex
                l_branch_vertsData.push_back(vertexData(
                    vec3<float>(0.0, 0.0, 0.0), vec3<float>(0.0, 0.0, 0.0), vec3<float>(0.0, 0.0, 0.0), 0, 0, 0));
            }

            // adjust coordinated system for final point, which is the entry point
            // direction vector is vAB which is intepolated vector

            //            if(!l_branch_verts.empty()) {
            //                auto matCoord = calculateLocalCoordinateFrame(imFront, front, vAB,
            //                l_branch_verts.back());
            //                l_branch_tensors.back() = matCoord;
            //                vec3<float> majoraxis(matCoord(0, 1), matCoord(1, 1), matCoord(2, 1));
            //                vec3<float> minoraxis(matCoord(0, 2), matCoord(1, 2), matCoord(2, 2));
            //                l_branch_vertsData.back() =
            //                    vertexData(vec3<float>(matCoord(0, 0), matCoord(1, 0), matCoord(2, 0)), majoraxis,
            //                    minoraxis);
            //                // factor of two because coord vector is half the axis (i.e. raidus)
            //                get<ENTRY_MAJOR_AXIS>(skelSegments.back()) = 2.0 * l2norm(majoraxis);
            //                get<ENTRY_MINOR_AXIS>(skelSegments.back()) = 2.0 * l2norm(minoraxis);
            //            }
        }
        if(N == 1) {

            // Continuing along path, add the vertex to the skeleton
            // because N==1, should only be one vertex
            for(auto& i : cogs) {
                l_branch_verts.push_back(
                    vertex<float>(i.x * imFront.xdim(), i.y * imFront.ydim(), i.z * imFront.zdim(), branchIndex));
                //            }
                // let's try to calculate the major and minor axis
                vec3<float> vdirPrev;
                if(l_branch_verts.size() > 1) {

                    auto iprev = l_branch_verts.crbegin();
                    ++iprev;
                    vdirPrev = l_branch_verts.back() - *iprev;
                } else {
                    vdirPrev = l_branch_verts.back() - parentEndVertex;
                }
                // provide the image (to convert index to coordinates, the front (indices), and the current
                // direction of the skeleton. The latter is to determine which eigen vector is the directional
                // vector
                //                    cout << "cogsPrev " << (*iprev).to_string() << endl;
                //                    cout << "cogs " << l_branch_verts.back().to_string() << endl;
                //                    Matrix<float, 3, 3> skelTensor =
                //                        calculateLocalCoordinateFrame(imFront, front, l_branch_verts.back() -
                //                        *iprev);
                auto matCoord = calculateLocalCoordinateFrame(imFront, front, vdirPrev, l_branch_verts.back());
                //                        cout<<"matCoord "<<matCoord<<endl;
                l_branch_tensors.push_back(matCoord);
                l_branch_vertsData.push_back(vertexData(vec3<float>(matCoord(0, 0), matCoord(1, 0), matCoord(2, 0)),
                    vec3<float>(matCoord(0, 1), matCoord(1, 1), matCoord(2, 1)),
                    vec3<float>(matCoord(0, 2), matCoord(1, 2), matCoord(2, 2)), 0, 0, 0));
                //                } else {
                // default, because not using previous branch
                // Therefore, is undefined at first point
                //                    l_branch_tensors.push_back(Matrix3f::Zero());
                //                    l_branch_vertsData.push_back(
                //                        vertexData(vec3<float>(0.0, 0.0, 0.0), vec3<float>(0.0, 0.0, 0.0),
                //                        vec3<float>(0.0, 0.0, 0.0)));
                //                }

                float majoraxis = 2.0 * l2norm(get<MAJOR_AXIS>(l_branch_vertsData.back()));
                float minoraxis = 2.0 * l2norm(get<MINOR_AXIS>(l_branch_vertsData.back()));

                ++Ncentres;
                get<VOLUME>(skelSegments.back()) += front.size() * voxelVolume;
                get<AVG_MAJOR_INNER_DIAMETER>(skelSegments.back()) += majoraxis;
                get<AVG_MINOR_INNER_DIAMETER>(skelSegments.back()) += minoraxis;
                get<AVG_INNER_AREA>(skelSegments.back()) += 0.25 * M_PI * majoraxis * minoraxis;

                if(isStartOfSegment) {
                    //                    cout << "Set major/minor axes " << get<BRANCH_ID>(skelSegments.back()) <<
                    //                    endl;
                    get<ENTRY_MAJOR_AXIS>(skelSegments.back()) = majoraxis;
                    get<ENTRY_MINOR_AXIS>(skelSegments.back()) = minoraxis;
                }
            }
        } else if(N >= 2) {
            // This portion handles the bifurcation

            // store the anatomical branch point
            // TODO add in better estimation of branch point
            get<BRANCH_POINT>(skelSegments.back()) = l_branch_verts.back();

            // This next code segment deals with how it decides to handles the bifurcation
            // The algorithm retracts along parent branch such the squared distance of the segments connecting the
            // parent branch with each child is as closed to half the distance between child start points
            // dist(o->a)^2 = dist(o->b)^2 = 0.5*dist(a->b)
            // TO DO ALTER TO ACCOMODATE TRIFURCATION
            auto iv_branch = l_branch_verts.rbegin();
            //            auto it_branch = l_branch_tensors.rbegin();
            {
                vertex<float> vA = vertex<float>(
                    cogs.front().x * imFront.xdim(), cogs.front().y * imFront.ydim(), cogs.front().z * imFront.zdim());
                vertex<float> vB = vertex<float>(
                    cogs.back().x * imFront.xdim(), cogs.back().y * imFront.ydim(), cogs.back().z * imFront.zdim());

                float distAB = l2normSquared(vB - vA) / 2.0;
                float costPrev = abs(distAB + distAB - l2normSquared(vB - *iv_branch) - l2normSquared(vA - *iv_branch));
                for(; iv_branch != l_branch_verts.rend(); ++iv_branch) {
                    float cost = abs(distAB + distAB - l2normSquared(vB - *iv_branch) - l2normSquared(vA - *iv_branch));
                    if(cost > costPrev) {
                        break;
                    }
                }
                iv_branch--; // need to go back one
                             //                it_branch--; // need to go back one
            }
            // Done choosing the end point for the parent

            // Let's only add the vertices upto and including the bifiurcation point
            // make sure to atleast add one vertex
            // base() used to compare forward and reverse iterator

            // keep track of segment metrics as we add vertices to the skeleton
            float length{ 0.0 };
            unsigned int Nadded{ 1 }; // this will be set to zero if there are no vertices

            // make sure there are vertices to add
            if(!l_branch_verts.empty()) {
                // uses two iterator so that distance can be calculated
                get<START_POINT>(skelSegments.back()) = vec3<float>(l_branch_verts.front());
                auto ifv_branch_prev = l_branch_verts.begin();
                auto ifv_branch = l_branch_verts.begin();
                auto ift_branch = l_branch_tensors.begin();           // tracking tesnors to add as well
                auto ifvdata_branchData = l_branch_vertsData.begin(); // tracking tesnors to add as well

                // include distance from branch point if not first branch (i.e. trachea)
                if(branchIndex > 1) {
                    length = l2norm(*ifv_branch - parentEndVertex);
                }
                // add first point, then increment
                //                skeleton.push_back(*(ifv_branch++));
                skeleton.push_back(*(ifv_branch++), *(ift_branch++));
                skeleton_data.push_back(*(ifvdata_branchData++));
                // add all vertices and accumulate the length
                for(; ifv_branch != iv_branch.base();
                    ++ifv_branch, ++ifv_branch_prev, ++ift_branch, ++ifvdata_branchData) {
                    //                for(; ifv_branch != iv_branch.base(); ++ifv_branch, ++ifv_branch_prev) {
                    // ensure there are no duplicate vertices, this will break treeModel
                    if(!compareCoord(*ifv_branch_prev, *ifv_branch)) {
                        length += l2norm(*ifv_branch - *ifv_branch_prev);
                        //                        skeleton.push_back(*ifv_branch);
                        //                        skeleton.push_back(*ift_branch);
                        skeleton.push_back(*ifv_branch, *ift_branch);
                        skeleton_data.push_back(*ifvdata_branchData);
                        ++Nadded;
                    }
                }
            } else {
                Nadded = 0;
            }

            // store metrics
            if(Nadded > 0) {
                get<END_POINT>(skelSegments.back()) = vec3<float>(skeleton.back());
                get<EDIST_END_TO_BRANCH>(skelSegments.back()) =
                    l2norm(get<END_POINT>(skelSegments.back()) - get<BRANCH_POINT>(skelSegments.back()));
                get<LENGTH>(skelSegments.back()) = length;
                get<NPTS>(skelSegments.back()) = Nadded;
                // directional vector at start of segment
                get<START_VDIR>(skelSegments.back()) = calculateStartDirection(l_branch_verts, vdir_navg);
                // directional vector at end of segment
                get<END_VDIR>(skelSegments.back()) = calculateEndDirection(skeleton, vdir_navg);
                if(Ncentres > 0) {
                    get<AVG_MAJOR_INNER_DIAMETER>(skelSegments.back()) /= Ncentres;
                    get<AVG_MINOR_INNER_DIAMETER>(skelSegments.back()) /= Ncentres;
                    get<AVG_INNER_AREA>(skelSegments.back()) /= Ncentres;
                }
            }

            // grab the last vertex to pass on to the next branch
            vertex<float> endVert = skeleton.back();

            // Need to split distance2index into 2. This is what is bifurcating the skeleton
            // Each partition only contains it's decendants
            //            multimap<float, unsigned int> distance2indexA, distance2indexB;
            //            splitWaveFront(distance2index, index2distance, newFronts, distance2indexA,
            //            distance2indexB,
            //            connectivity);
            vector<multimap<float, unsigned int> > distance2indexParts(newFronts.size());
            SplitWaveFront(distance2index, index2distance, newFronts, distance2indexParts, connectivity);
            //            cout << "branch " << distance2indexParts.size() << endl;
            // store parent so that same parent is passed to each branch, propogateWaveFrontUntilBifurcation will
            // increment the branchIndex
            int parent = branchIndex;
            for(auto& front : distance2indexParts) {

                PropagateWaveFrontUntilBifurcation(skeleton, skeleton_data, imFront, front, index2distance,
                    connectivity, threshold, thresholdStep, branchIndex, parent, generationIndex + 1, skelSegments,
                    endVert, maxGeneration, vdir_navg, seg_voxels);
            }
            return;
        }
        threshold += thresholdStep;
    }
    // In the case of terminal segments where there is no branching at the end
    // i.e. the the wavefront threshold exceeds the max
    // We need to add the vertices of the final segment
    // this mimic that found in the N==2 case
    //    if(0 == 1) {
    if(!l_branch_verts.empty()) {
        //        cout << "add verts to end iof branch" << endl;
        // grab metrics
        float length{ 0.0 };
        unsigned int Nadded{ 1 };
        auto i_vert_prev = l_branch_verts.begin();
        auto i_vert = l_branch_verts.begin();
        auto i_tensor = l_branch_tensors.begin();
        auto i_vertData = l_branch_vertsData.begin();
        //        ADD TENSORS AND DATA HERE
        // Don't include length from parent for the trachea
        if(branchIndex > 1) {
            length = l2norm(*i_vert - parentEndVertex);
        }

        // add first vertex and increment
        skeleton.push_back(*(i_vert++), *(i_tensor++));
        skeleton_data.push_back(*(i_vertData++));

        //                     auto matCoord = calculateLocalCoordinateFrame(imFront, front, vAB,
        //                     l_branch_verts.back());
        //                l_branch_tensors.back() = matCoord;
        //        vec3<float> majoraxis((*i_tensor)(0, 1), (*i_tensor)(1, 1), (*i_tensor)(2, 1));
        //        vec3<float> minoraxis((*i_tensor)(0, 2), (*i_tensor)(1, 2), (*i_tensor)(2, 2));
        //                l_branch_vertsData.back() =
        //                    vertexData(vec3<float>(matCoord(0, 0), matCoord(1, 0), matCoord(2, 0)), majoraxis,
        //                    minoraxis);
        // factor of two because coord vector is half the axis (i.e. raidus)
        //        get<ENTRY_MAJOR_AXIS>(skelSegments.back()) = 2.0 * l2norm(majoraxis);
        //        get<ENTRY_MINOR_AXIS>(skelSegments.back()) = 2.0 * l2norm(minoraxis);

        for(; i_vert != l_branch_verts.end(); ++i_vert, ++i_vert_prev, ++i_vertData, ++i_tensor) {
            // exclude duplicate vertices in succession
            if(!compareCoord(*i_vert, *i_vert_prev)) {
                skeleton.push_back(*i_vert, *i_tensor);
                skeleton_data.push_back(*i_vertData);
                length += l2norm(*i_vert - *i_vert_prev);
            }
        }
        // store the segment data
        get<3>(skelSegments.back()) = length;
        get<4>(skelSegments.back()) = l_branch_verts.size();
        get<START_POINT>(skelSegments.back()) = vec3<float>(l_branch_verts.front());

        get<END_POINT>(skelSegments.back()) = vec3<float>(skeleton.back());
        get<START_VDIR>(skelSegments.back()) = calculateStartDirection(l_branch_verts, vdir_navg);
        get<END_VDIR>(skelSegments.back()) = calculateEndDirection(skeleton, vdir_navg);
        if(Ncentres > 0) {
            get<AVG_MAJOR_INNER_DIAMETER>(skelSegments.back()) /= Ncentres;
            get<AVG_MINOR_INNER_DIAMETER>(skelSegments.back()) /= Ncentres;
            get<AVG_INNER_AREA>(skelSegments.back()) /= Ncentres;
        }
    } else {
        // else condition is ending branch that is empty
        // the pop_back exludes the empty segment in for terminal branches
        skelSegments.pop_back();
    }
}
void AsrTreeModel::computeAngles()
{
    computeAngles(m_segments);
}

void AsrTreeModel::computeAngles(std::vector<skeletonSegment>& segments)
{
    //    cout<<"compAngles "<<endl;
    // decided to generate local maps, instead of passing in.
    // all can be derived from segments quickly, and computeAngles should not need
    // many repeated calls
    unordered_multimap<int, int> branch2child;
    unordered_map<int, unsigned int> branch2index; // mapping of branch number to index of where it's stored
    GenerateSkeletonMaps(segments, branch2child, branch2index);

    for(auto& seg : segments) {
        //        cout << "compAngles " << get<PARENT_ID>(seg) << endl;
        // we don't use a 0 segment, -1 is used for no parent
        if(get<PARENT_ID>(seg) > 0) {
            //            cout << "angle " << angle(get<END_VDIR>(segments[get<PARENT_ID>(seg)]), get<START_VDIR>(seg))
            //            <<
            //            endl;
            // get angle between parent vector and the current vector
            //            cout << "dirs " << get<END_VDIR>(segments[branch2index[get<PARENT_ID>(seg)]]).to_string() <<
            //            " "
            //                 << get<START_VDIR>(seg).to_string() << endl;
            get<ANGLE_WITH_PARENT>(seg) =
                M_PI - abs(angle(get<END_VDIR>(segments[branch2index[get<PARENT_ID>(seg)]]), get<START_VDIR>(seg)));
        }
    }
}

// Calculates the directional vector at the beginning of a segment
// Averages over the first min(vdir_navg, number fo vertices)
vec3<float> AsrTreeModel::calculateStartDirection(const std::list<vertex<float> >& segment,
    const unsigned int& vdir_navg)
{
    // vdir start
    // this is written such that it can handle segment with less than
    // m_vdir_navg points
    // for start we used l_branch_verts
    // for end we use skeleton, this accounts for trimming
    vec3<float> start_vdir(0.0, 0.0, 0.0);
    unsigned int count{ 0 };
    auto ivert = segment.cbegin();
    ++ivert;
    for(; ivert != segment.cend(); ++ivert) {
        start_vdir += vec3<float>(*ivert);
        ++count;
        if(count == vdir_navg) {
            break;
        }
    }
    if(count > 0)
        start_vdir /= count;
    // vector is the difference between the target point (averaged) and start point
    start_vdir = start_vdir - vec3<float>(segment.front());
    // make unit vector
    normalize(start_vdir);
    return start_vdir;
}

// Calculates the directional vector at the beginning of a segment
// Averages over the first min(vdir_navg, number fo vertices)
vec3<float> AsrTreeModel::calculateEndDirection(aurcontour<float>& skeleton, const unsigned int& vdir_navg)
{
    // vdir start
    // this is written such that it can handle segment with less than
    // m_vdir_navg points
    // for start we used l_branch_verts
    // for end we use skeleton, this accounts for trimming
    // vdir end
    vec3<float> end_vdir(0.0, 0.0, 0.0);
    unsigned int count{ 0 };
    auto ivert = skeleton.crbegin();
    ivert++;
    for(; ivert != skeleton.crend(); ++ivert) {
        end_vdir += vec3<float>(*ivert);
        ++count;
        if(count == vdir_navg) {
            break;
        }
    }
    if(count > 0)
        end_vdir /= count;

    end_vdir = vec3<float>(skeleton.back()) - end_vdir;

    normalize(end_vdir);
    return end_vdir;
}

// this method splits a wavefront into connected components
unsigned int AsrTreeModel::GetWaveFronts(AsrImage3D<float>& im,
    const waveFront& front,
    list<waveFront>& newFronts,
    list<voxel>& cogs,
    unsigned int& prevSize,
    const unsigned int& branchIndex,
    unordered_set<unsigned int>& seg_voxels)
{
    //    cout << "frontsize " << front.size() << endl;
    if(front.empty())
        return 0;
    //    unsigned int Ncomponents{ 0 };
    cogs.clear();

    list<unsigned int> l_sizes;
    waveFront frontCopy = front;
    while(!frontCopy.empty()) {
        //        cogs.push_back(,0,0));
        unsigned int roiSize{ 1 };
        list<voxel> toVisit{ im.GetCoord(*frontCopy.begin()) };
        frontCopy.erase(frontCopy.begin());
        // cout<<"firstVoxel "<<toVisit.front().to_string()<<endl;
        voxel frontCOG = toVisit.front();
        waveFront oneFront{ im.getIndex(frontCOG.x, frontCOG.y, frontCOG.z) };
        //        oneFront.insert(im.getIndex(frontCOG.x, frontCOG.y,frontCOG.z));
        //        waveFront.pushim.getIndex(_back(toVisit.front());
        // overite with branch index

        im.value(frontCOG.x, frontCOG.y, frontCOG.z) = branchIndex;
        while(!toVisit.empty()) {
            //            cout<<"tovisiti size "<<toVisit.size()<<endl;
            voxel seed = toVisit.front();
            toVisit.pop_front();
            im.value(seed.x, seed.y, seed.z) = branchIndex;

            // erase connected components
            array<int, 3> offsets{ -1, 0, 1 };
            // get neighbour statifying threshold
            for(auto& dx : offsets) {
                for(auto& dy : offsets) {
                    for(auto& dz : offsets) {
                        // will visit the same seed but already erase so only the cost of single erae from hash
                        // TODO pretabulate neighbours
                        voxel v_neigh(seed.x + dx, seed.y + dy, seed.z + dz);
                        int N = frontCopy.erase(im.getIndex(v_neigh.x, v_neigh.y, v_neigh.z));
                        //                        cout<<"Nerase "<<N<<" "<<im.getIndex(v_neigh.x, v_neigh.y,
                        //                        v_neigh.z)<<" "<<v_neigh.to_string()<<endl;
                        if(N == 1) {
                            oneFront.insert(im.getIndex(v_neigh.x, v_neigh.y, v_neigh.z));
                            toVisit.push_back(v_neigh);
                            frontCOG += v_neigh;
                            roiSize++;
                        }
                    }
                }
            }
        }
        //        frontCOG /= roiSize;
        //        cogs.push_back(frontCOG / roiSize);
        // centre of gravity of the front should be the skeleton, also helps visualize for development
        if(static_cast<float>(roiSize) > m_roithresh * static_cast<float>(prevSize)) {
            //{
            newFronts.push_back(oneFront);

            cogs.push_back(voxel(static_cast<int>(static_cast<float>(frontCOG.x) / roiSize + 0.5),
                static_cast<int>(static_cast<float>(frontCOG.y) / roiSize + 0.5),
                static_cast<int>(static_cast<float>(frontCOG.z) / roiSize + 0.5)));
            l_sizes.push_back(roiSize);
            //            Ncomponents++;
        }
    }

    //    cout << "sizes ";
    prevSize = l_sizes.front();
    for(auto& i : l_sizes) {
        //        cout << i << " ";
        prevSize = (prevSize > i) ? prevSize : i;
    }

    //    cout << endl;
    return newFronts.size();
}
void AsrTreeModel::SplitWaveFront(const multimap<float, unsigned int>& distance2index,
    const unordered_map<unsigned int, float>& index2distance,
    const list<waveFront>& newFronts,
    vector<multimap<float, unsigned int> >& distance2indexParts,
    //    multimap<float, unsigned int>& distance2indexA,
    //    multimap<float, unsigned int>& distance2indexB,
    unordered_map<unsigned int, vector<unsigned int> >& connectivity)
{
    // assumes 2 from present in NewFronts
    // let's do A
    // connectivity is only tiowards greater distances
    //    for(auto& wf : newFronts) { //for each front
    unsigned int wfCount{ 0 };
    for(auto& wf : newFronts) {                 // for each front
                                                //        cout << "front " << endl;
        unordered_set<unsigned int> hasVisited; // this need to perisist across all voxels in wave front
        for(auto& i_vox : wf) {                 // loop to make sure that we get all voxel connected

            if(hasVisited.count(i_vox) == 0) { // ensure has not been visited
                list<unsigned int> toVisit{ i_vox };
                hasVisited.insert(i_vox);
                while(!toVisit.empty()) {
                    unsigned int currentVoxel = toVisit.front();
                    toVisit.pop_front();
                    //                    cout<<"index "<<i_vox<<" "<<index2distance.at(i_vox)<<endl;
                    distance2indexParts[wfCount].insert(
                        pair<float, unsigned int>(index2distance.at(currentVoxel), currentVoxel));
                    //                    if(wfCount == 0) {
                    //                        distance2indexA.insert(
                    //                            pair<float, unsigned int>(index2distance.at(currentVoxel),
                    //                            currentVoxel));
                    //                    } else {
                    //                        distance2indexB.insert(
                    //                            pair<float, unsigned int>(index2distance.at(currentVoxel),
                    //                            currentVoxel));
                    //                    }
                    for(auto& neighbour : connectivity[currentVoxel]) {
                        //                    for(vector<unsigned int>::const_iterator neighbour =
                        //                    connectivity[currentVoxel].cbegin();
                        //                    neighbour != connectivity[currentVoxel].cend(); ++neighbour) {
                        if(hasVisited.count(neighbour) == 0) { // check if neighbour has not been visited
                            toVisit.push_back(neighbour);
                            hasVisited.insert(neighbour);
                        }
                    }
                }
            }
        }
        wfCount++;
    }
}

void AsrTreeModel::CreateSkeleton(const AsrImage3D<float>& distanceFromSeed,
    const vec3<int>& seed,
    unordered_set<unsigned int>& seg_voxels)
{
    cout << "CREATE SKELETON " << m_frontWidth << endl;
    //    AsrImage3D<float>::copyImageInfo( m_skeleton , distanceFromSeed);
    m_skeleton.copyImage(distanceFromSeed);
    cout << "done copying image" << endl;
    CreateSkeleton(m_skeleton, seed, m_frontWidth, m_maxGeneration, m_skeleton_pts, m_skeleton_pts_data, m_segments,
        m_vdir_navg, m_maxGenerationCleanUp, m_airwayModel.GetGeneration2Length(), seg_voxels);
    updateTreeInfo();
}
void AsrTreeModel::CreateSkeleton(AsrImage3D<float>& distanceFromSeed,
    const vec3<int>& seed,
    const float& width,
    const int& maxGen,
    aurcontour<float>& skeleton,
    list<vertexData>& skeleton_data,
    vector<skeletonSegment>& skelSegments,
    const unsigned int& vdir_navg,
    const unsigned int& maxGenCleanUp,
    const unordered_map<unsigned int, float>& gen2length,
    unordered_set<unsigned int>& seg_voxels)
{
    // clear orginal
    skeleton.clear();
    skeleton_data.clear();
    skelSegments.clear();
    //    int maxx{ distanceFromSeed.xsize() - 2 };
    //    int maxy{ distanceFromSeed.ysize() - 2 };
    //    int maxz{ distanceFromSeed.zsize() - 2 };

    distanceFromSeed.info();
    //    distanceFromSeed.zeroAboveZ(seed.z);
    cout << "create skeleton " << endl;
    //    distanceFromSeed.writeImage("distMap.nii.gz");
    // pre-calculate connectivity of voxels, index2distance from seed, and distance2index
    // goingto use a sparse representation of the distance image
    // this loop loads all non-zero voxels distances into memory
    unordered_map<unsigned int, vector<unsigned int> > connectivity;
    unordered_map<unsigned int, float> index2distance;
    multimap<float, unsigned int> distance2index;
    unsigned int index{ 0 };
    // TODO SPEED UP THESE
    for(auto& dist : distanceFromSeed.data_vector()) {
        if(dist > 0) {
            voxel v = distanceFromSeed.GetCoord(index);
            // add bounds check
            // just omits band around image instead of checking at each neighbour check
            // limiting addition here will limited search space
            //            if((v.x < 1) || (v.y < 1) || (v.z < 1 ) || (v.x > maxx) || (v.y > maxy) || (v.z > maxz)) {
            //                index++;
            //                continue;
            //            }
            //            if((v.x > 0) && (v.y > 0) && (v.z > 0) && (v.x <= maxx) && (v.y <= maxy) && (v.z <= maxz))
            {
                distance2index.insert(pair<float, unsigned int>(dist, index));
                //            index2distance[index]=dist;
                index2distance.insert(pair<unsigned int, float>(index, dist));
                // find connected neighbours, but only with increased distance
                array<int, 3> offsets{ -1, 0, 1 };
                //            // get neighbour statifying threshold
                //                cout << "vox " << v.to_string() << endl;
                for(auto& dx : offsets) {
                    for(auto& dy : offsets) {
                        for(auto& dz : offsets) {
                            // TODO REPLACE WITH ITERATION OVER OFFSETS

                            //                if((v.x > 0) || (v.y >0 ) || (v.z >0  ) || (v.x < maxx) || (v.y < maxy) ||
                            //                (v.z < maxz)) {
                            if(!((dx == 0) && (dy == 0) && (dz == 0))) {
                                if(distanceFromSeed.value(v.x + dx, v.y + dy, v.z + dz) > dist) {
                                    connectivity[index].push_back(
                                        distanceFromSeed.getIndex(v.x + dx, v.y + dy, v.z + dz));
                                }
                            }
                            //                }
                        }
                    }
                }
            }
        }
        ++index;
    }
    //    cout << "Prop wave front" << endl;
    float threshold{ 1.0 };
    int maxBranchIndex{ 0 }; // propogateWaveFrontUntilBifurcation will recursively increment this
    unsigned int generationIndex{ 1 };
    //    cout << "width of propagating front " << width << endl;
    PropagateWaveFrontUntilBifurcation(skeleton, skeleton_data, distanceFromSeed, distance2index, index2distance,
        connectivity, threshold, width, maxBranchIndex, -1, generationIndex, skelSegments, vertex<float>(0, 0, 0),
        maxGen, vdir_navg, seg_voxels); // threshold of 1mm, and step of 2mm
    //    distanceFromSeed.writeImage("skeleton.nii.gz");
    //    cout << "done wavefront " << endl;
    CleanUpSkeleton(skelSegments, skeleton, maxBranchIndex, maxGenCleanUp, gen2length);
    cout << "fix branchPoints" << endl;

    EstimateCarinaPoints(distanceFromSeed, skelSegments, 0.1);

    cout << "start label<" << endl;
    LabelSkeletonBranches(skelSegments);
    cout << "done label<" << endl;
    computeAngles(skelSegments);
    cout << "done skeleton<" << endl;
}
void AsrTreeModel::EstimateCarinaPoints(const AsrImage3D<float>& distanceFromSeed,
    std::vector<skeletonSegment>& skelSegments,
    const float& scanResolution)
{
    AsrImage3D<short> imAirway;
    imAirway.copyImage(distanceFromSeed);
    // binarize so that when interpolating, it's interpolating between 1s and 0s
    imAirway.binarize();
    //    imAirway.writeImage("estCarinaTest");

    // cleanup trifurcations
    unordered_multimap<int, int> branch2child;
    unordered_map<int, unsigned int> branch2index; // mapping of branch number to index of where it's stored
    GenerateSkeletonMaps(skelSegments, branch2child, branch2index);

    aurcontour<float> estCarinaPoints;
    for(auto& seg : skelSegments) {
        auto parentPoint = get<END_POINT>(seg);
        auto childEntryPoints = GetChildrenEntryPoints(skelSegments, get<BRANCH_ID>(seg));
        //        estCarinaPoints.push_back(vertex<float>(parentPoint.x, parentPoint.y, parentPoint.z, 1.0));
        //        //child is pair of <BRANCH_ID,ENTRY_POINT>
        //        for(auto& child : childEntryPoints) {
        //            estCarinaPoints.push_back(vertex<float>(child.second.x, child.second.y, child.second.z, 2.0));
        //        }
        // for now ignore trifurcation
        if(childEntryPoints.size() == 2) {
            cout << "parent " << parentPoint.to_string() << endl;
            vector<tuple<float, vec3<float>, vec3<float> > > pairWisePointDists;
            for(auto i_child = childEntryPoints.begin(); i_child != childEntryPoints.end(); ++i_child) {
                auto ii_child = i_child;
                advance(ii_child, 1);
                for(; ii_child != childEntryPoints.end(); ++ii_child) {
                    pairWisePointDists.push_back(tuple<float, vec3<float>, vec3<float> >(
                        l2normSquared(i_child->second - ii_child->second), i_child->second, ii_child->second));
                }
            }
            sort(pairWisePointDists.begin(), pairWisePointDists.end(), tupleCompare);
            // TODO TEST THIS ON ACTUAL TRIFURCATON OR GREATER
            cout << "pairs " << endl;
            for(auto& i : pairWisePointDists) {
                cout << get<0>(i) << " " << get<1>(i).to_string() << " " << get<2>(i).to_string() << endl;
            }

            auto i_entries = pairWisePointDists.cbegin();
            // will be 1 less then N-th furcation
            for(int entryCount = 1; entryCount < childEntryPoints.size(); ++entryCount, ++i_entries) {
                //            priority_queue<tuple<float, vec3<float>, vec3<float> >, vector<tuple<float, vec3<float>,
                //            vec3<float> > >,
                //                std::greater<tuple<float, vec3<float>, vec3<float> > > > min_heap;
                //            auto entry0 = childEntryPoints.front().second;
                //            auto entry1 = childEntryPoints.back().second;
                auto entry0 = get<1>(*i_entries);
                auto entry1 = get<2>(*i_entries);

                // let's check distance between all pairs
                //            set < pair<vec3<float>, vec3<float> > endPointsToCheck;

                cout << "entry0 " << entry0.to_string() << endl;
                cout << "entry1 " << entry1.to_string() << endl;
                vertex<float> endPoint0(to_vertex(entry0));
                vertex<float> endPoint1(to_vertex(entry1));
                vec3<float> dispEnd = endPoint1 - endPoint0;
                float dist{ l2norm(dispEnd) };
                normalize(dispEnd);
                cout << "dist " << dist << " " << scanResolution << endl;
                int N{ static_cast<int>(dist / scanResolution) };
                vertex<float> endPoint = endPoint0;

                //                dispEnd = dispEnd * scanResolution;
                dispEnd *= scanResolution;
                // note if not starting at zero need to adjust endPoint
                //            bool foundSegmentationExit{ false };
                float minDistToWall{ imAirway.zsize() * imAirway.zdim() };
                vec3<float> carina{ vertex<float>(0, 0, 0) };
                for(int step = 0; step < N; ++step) {
                    cout << "start to end " << parentPoint.to_string() << " -> " << endPoint.coord_to_string() << endl;
                    //                estCarinaPoints.push_back(endPoint);
                    endPoint = endPoint + dispEnd;
                    // traverse from parent to end look for leaving segmentation
                    // could shoot ray with end as an arc, straight line does not guarantee leaving the segmentation (i
                    // think?)
                    vec3<float> v_ray = endPoint - parentPoint;
                    float distRay = l2norm(v_ray);
                    normalize(v_ray);
                    v_ray = v_ray * scanResolution;
                    //                int Nray{ static_cast<int>(distRay / scanResolution) };

                    auto vertRay = parentPoint;
                    //                for(int stepRay = 1; stepRay <= Nray; ++stepRay)
                    // 0.5 threshold is based on half the voxel contains the tissue
                    float distToWall{ 0.0 };
                    while(imAirway.interpolate(vertRay.x, vertRay.y, vertRay.z) > 0.5) {
                        distToWall += scanResolution;
                        vertRay = vertRay + v_ray;

                        //                    if(imAirway.interpolate(vertRay.x, vertRay.y, vertRay.z) < 0.5) {
                        //
                        //                        estCarinaPoints.push_back(to_vertex(vertRay - v_ray * 0.5));
                        //
                        //                        cout << imAirway.interpolate(vertRay.x, vertRay.y, vertRay.z) << endl;
                        //                        break;
                        //                    }
                    }
                    distToWall -= 0.5 * scanResolution;
                    //                estCarinaPoints.push_back(to_vertex(vertRay - v_ray * 0.5));
                    if(distToWall < minDistToWall) {
                        minDistToWall = distToWall;
                        carina = (vertRay - v_ray * 0.5);
                        //                    carina = to_vertex(vertRay - v_ray * 0.5);
                        //                    carina.scalar = 1;
                    }
                }
                get<BRANCH_POINT>(seg) = carina;
                get<EDIST_END_TO_BRANCH>(seg) = l2norm(carina - parentPoint);
                estCarinaPoints.push_back(to_vertex(carina));
            }
        }
    }

    //    estCarinaPoints.getVerticesAsSpheres(2.0).write("estCarinas.stl", aursurface<float>::WRITE_FORMAT::STL);
    vec3<float> origin{ imAirway.getOrigin() };

    estCarinaPoints.translate(vec3<float>(abs(origin.x) * -1, abs(origin.y) * -1, abs(origin.z) * -1));
    estCarinaPoints.getVerticesAsSpheres(2.0).write("estCarinas.vtk", aursurface<float>::WRITE_FORMAT::VTK);
}

//    unsigned int AsrTreeModel::GetNumberOfDescedants( const int & branchID ){
//        unsigned int Ndesc=0;
//        for_each ( m_)
//        return 1 + GetNumberOfDescedants()
//    }

// Cleans up the tree structure of the skeleton
// Makes into a binary tree, i.e. convert trifurcation to multiple bifuration
void AsrTreeModel::CleanUpSkeleton(vector<skeletonSegment>& skelSegments,
    aurcontour<float>& skeleton_pts,
    const int& maxBranchIndex,
    const unsigned int& maxGen,
    const std::unordered_map<unsigned int, float>& gen2length)
{
    // maxGen indicates how many genertation down to attempt cleanup on
    // skeletonSegment data
    // branchIndex, parentIndex, generation index, length , number of
    // points,avgMinorInnerDiam,avgMajorInnerDiam,avgInnerArea

    // cleanup trifurcations
    unordered_multimap<int, int> branch2child;
    unordered_map<int, unsigned int> branch2index; // mapping of branch number to index of where it's stored
    GenerateSkeletonMaps(skelSegments, branch2child, branch2index);

    //--------------TRIM BASED ON LENGTH VS GENERATION0-------------//
    // lets remove branches in the first X generations
    std::unordered_map<unsigned int, std::list<int> > gen_to_id;
    for(auto& seg : skelSegments) {
        gen_to_id[get<GENERATION>(seg)].push_back(get<BRANCH_ID>(seg));
        //        cout<<"genID "<<get<BRANCH_ID>(seg)<<" "<<get<GENERATION>(seg)<<"
        //        "<<gen_to_id[get<BRANCH_ID>(seg)].size()<<endl;
    }

    // TODO FINSIH CLEANUP
    // let's look for some short segment
    // start cleanup up at second generation. This will actually cleanup
    // the trachea if there is false branches. Just not applying distance threshold
    //    float thresh = 0.1;
    //    for(unsigned int gen = 2; gen < 3; ++gen) {
    //        for(unsigned int gen = 2; gen < maxGen; ++gen) {
    //            //            cout << "CLEANUP GEN " << gen << endl;
    //            for(auto& branch : gen_to_id[gen]) {
    //                //               if(get<LENGTH>(skelSegments[branch2index[branch]]) * thresh <
    //                //               gen2length.find(gen)->second) {
    //                //                cout << "branch " << branch << " " <<
    //                get<LENGTH>(skelSegments[branch2index[branch]])
    //                //                << endl;
    //                //                removeBranch(skelSegments, branch);
    //            }
    //        }
    //    }

    // prune short branches

    //    pruneBranches()

    // we now have a binary tree assuming no branching greater than 3
    // sort tree and retabulate indices
    std::sort(skelSegments.begin(), skelSegments.end(), segmentCompareParent);
    GenerateSkeletonMaps(skelSegments, branch2child, branch2index);

    // orient the left/right bronchi
    // trachea is assumed to be 1
    // range will correpsonde to left/rightbronchus
    auto range_bronchi = branch2child.equal_range(1).first;
    int index0{ (range_bronchi++)->second };
    int index1{ (range_bronchi)->second };

    // Change DO NOT SWAP INDICES just swap order
    // Need to swap the index value if right has greater index
    float x0 = get<START_POINT>(skelSegments[branch2index[index0]]).x;
    float x1 = get<START_POINT>(skelSegments[branch2index[index1]]).x;
    if(((index0 > index1) && (x0 < x1)) || ((index1 > index0) && (x1 < x0))) {
        // First lets swap the branch index of the left/right bronchi
        cout << "swap " << endl;
        cout << index0 << " " << index1 << endl;
        cout << branch2index[index0] << endl;
        cout << branch2index[index1] << endl;
        cout << skelSegments.size() << endl;
        swap(skelSegments[branch2index[index0]], skelSegments[branch2index[index1]]);
    }
    cout << "done clean up " << endl;
}

std::unordered_set<int> AsrTreeModel::GetGenerationBranchIDs(const unsigned int& generation)
{
    unordered_set<int> branchIDs;
    for(auto& branch : m_id_to_generation) {
        if(branch.second == generation) {
            branchIDs.insert(branch.first);
        }
    }
    return branchIDs;
}

std::unordered_set<int> AsrTreeModel::GetLeaves()
{
    return m_leaves;
}
bool AsrTreeModel::IsLeaf(const int& branchID)
{
    // count will only ever return 1 for unordered_set
    return (m_leaves.count(branchID) > 0);
}

std::vector<int> AsrTreeModel::GetChildren(const int& branchID)
{
    vector<int> children;
    for(auto& seg : m_segments) {
        if(get<PARENT_ID>(seg) == branchID) {
            children.push_back(get<BRANCH_ID>(seg));
        }
    }
    return children;
}

std::list<pair<int, vec3<float> > > AsrTreeModel::GetChildrenEntryPoints(const int& branchID)
{
    //    list<pair<int, vec3<float> > > entryPoints;
    //    for(auto& seg : m_segments) {
    //        //        cout<<"tree seg "<<get<BRANCH_ID>(seg)<<" "<<get<PARENT_ID>(seg)<<endl;
    //        if(get<PARENT_ID>(seg) == branchID) {
    //            entryPoints.push_back(make_pair(get<BRANCH_ID>(seg), get<ENTRY_POINT>(seg)));
    //        }
    //    }
    //    return entryPoints;
    return GetChildrenEntryPoints(m_segments, branchID);
}

std::list<pair<int, vec3<float> > > AsrTreeModel::GetChildrenEntryPoints(const vector<skeletonSegment>& segments,
    const int& branchID)
{
    list<pair<int, vec3<float> > > entryPoints;
    for(auto& seg : segments) {
        //        cout<<"tree seg "<<get<BRANCH_ID>(seg)<<" "<<get<PARENT_ID>(seg)<<endl;
        if(get<PARENT_ID>(seg) == branchID) {
            entryPoints.push_back(make_pair(get<BRANCH_ID>(seg), get<ENTRY_POINT>(seg)));
        }
    }
    return entryPoints;
}

unsigned int AsrTreeModel::GetSegmentSizeInVoxels(const unsigned int& branchID)
{
    return m_skeleton.GetNumberOfVoxels(static_cast<float>(branchID));
}

void AsrTreeModel::moveUp(std::vector<skeletonSegment>& segments,
    const unordered_multimap<int, int>& branch2child,
    const unordered_map<int, unsigned int>& branch2index,
    const int& pBranch,
    const int& currentBranch)
{
    cout << "moveup " << endl;
    get<GENERATION>(segments[branch2index.find(currentBranch)->second])--;
    get<PARENT_ID>(segments[branch2index.find(currentBranch)->second])--;
    //    const auto& range = branch2child.equal_range(currentBranch);
    //    for(auto i_cindex = range.first; i_cindex != range.second; ++i_cindex) {
    //        cout << "moveUp-cindex " << i_cindex->second << endl;
    //        moveUp(segments, branch2child, branch2index, currentBranch, i_cindex->second);
    //    }
}
void AsrTreeModel::removeBranch(std::vector<skeletonSegment>& segments, const int& branch)
{
    // avoid recalculation of this each time, but for now let's do this
    // TODO optimize
    unordered_multimap<int, int> branch2child;
    unordered_map<int, unsigned int> branch2index; // mapping of branch number to index of where it's stored
    GenerateSkeletonMaps(segments, branch2child, branch2index);
    const auto& range = branch2child.equal_range(branch);
    for(auto i_cindex = range.first; i_cindex != range.second; ++i_cindex) {
        //        cout << "cindex " << i_cindex->second << endl;
        moveUp(segments, branch2child, branch2index, branch, i_cindex->second);
    }
    segments.erase(segments.begin() + branch2index[branch]);
    //    for_each(range.first,range.second,[]( unordered_multimap<int,int>::iterator & cindex){cout<<"cindex
    //    "<<cindex->second<<endl;});
    //    {
    //        moveUp(segments,index,cindex);
    //    }
}

bool AsrTreeModel::segmentCompareParent(skeletonSegment i, skeletonSegment j)
{
    return (get<PARENT_ID>(i) < get<PARENT_ID>(j));
}
void AsrTreeModel::LabelSkeletonBranches(vector<skeletonSegment>& skelSegments)
{
    // single pass to get children and index mapping
    // this is repeated from the cleanup in order to make modular
    // not viable to pass from cleanup since altering the tree structure
    unordered_multimap<int, int> branch2child;
    unordered_map<int, unsigned int> branch2index; // mapping of branch number to index of where it's stored
    GenerateSkeletonMaps(skelSegments, branch2child, branch2index);
    //    unsigned int index{ 0 };
    //    for(auto& branch : skelSegments) {
    //        cout << "skeleton parent " << get<1>(branch) << " " << get<0>(branch) << endl;
    //        // insert parent/current index
    //        branch2child.insert(make_pair(get<PARENT_ID>(branch), get<BRANCH_ID>(branch)));
    //        branch2index[get<BRANCH_ID>(branch)] = index;
    //        ++index;
    //    }

    // label trachea
    get<LABEL>(skelSegments[branch2index[1]]) = "Trachea";

    int Nchildren{ 0 };
    for(auto i = branch2child.equal_range(1).first; i != branch2child.equal_range(1).second; ++i) {
        ++Nchildren;
    }
    // label left/right main bronchus
    // only do if trachea has to children
    if(Nchildren == 2) {
        auto range_bronchi = branch2child.equal_range(1).first;
        //    cout << "trachea child szie " << branch2child.equal_range(1).size() << endl;
        int index0{ (range_bronchi++)->second };
        int index1{ (range_bronchi)->second };
        //    get<LABEL>(skelSegments[branch2index[(index0 < index1) ? index0 : index1]]) = "RightMainBronchus";
        //    get<LABEL>(skelSegments[branch2index[(index0 < index1) ? index1 : index0]]) = "LeftMainBronchus";
        float x0 = get<START_POINT>(skelSegments[branch2index[index0]]).x;
        float x1 = get<START_POINT>(skelSegments[branch2index[index1]]).x;
        get<LABEL>(skelSegments[branch2index[(x0 < x1) ? index0 : index1]]) = "RightMainBronchus";
        get<LABEL>(skelSegments[branch2index[(x0 < x1) ? index1 : index0]]) = "LeftMainBronchus";
    }
}

void AsrTreeModel::updateTreeInfo()
{
    // clear existing data
    m_maxGeneration = 0;
    unordered_set<int> all_parents; // used to find leaves
    m_id_to_index.clear();
    unsigned int index{ 0 };
    for(auto& seg : m_segments) {
        unsigned int generation{ get<GENERATION>(seg) };
        if(m_maxGeneration < generation)
            m_maxGeneration = generation;
        m_id_to_index[get<BRANCH_ID>(seg)] = index;
        m_id_to_generation[get<BRANCH_ID>(seg)] = generation;
        m_id_to_parent[get<BRANCH_ID>(seg)] = get<PARENT_ID>(seg);
        // list of parents is used to find the terminal branches
        all_parents.insert(get<PARENT_ID>(seg));
        ++index;
    }

    // find all leaves (terminal branches)
    for(auto& itree : m_id_to_parent) {
        if(all_parents.count(itree.first) == 0) {
            m_leaves.insert(itree.first);
        }
    }
}
void AsrTreeModel::GenerateSkeletonMaps(const vector<skeletonSegment>& skelSegments,
    unordered_multimap<int, int>& branch2child,
    unordered_map<int, unsigned int>& branch2index)
{
    branch2child.clear();
    branch2index.clear();
    unsigned int index{ 0 };
    for(auto& branch : skelSegments) {
        // insert parent/current index
        branch2child.insert(make_pair(get<PARENT_ID>(branch), get<BRANCH_ID>(branch)));
        branch2index[get<BRANCH_ID>(branch)] = index;
        ++index;
    }
}
} /* namespace auris_segtools */
