#ifndef ASRAIRWAYGEOMETRYMODEL_H_
#define ASRAIRWAYGEOMETRYMODEL_H_

#include <vector>
#include <unordered_map>
namespace auris_segtools
{

class AsrAirwayGeometryModel
{

public:
    AsrAirwayGeometryModel();
    ~AsrAirwayGeometryModel();
    float getMaximumAirwayVolume(const unsigned int& generation) { return m_generation2maximumSize[generation]; };
    std::unordered_map<unsigned int, float> GetGeneration2Length();

private:
    static std::unordered_map<unsigned int, float> computeMaxAirwayVolumes(
        const std::vector<float>& diameters,
        const std::vector<float>& length);
    static std::unordered_map<unsigned int, float> computeMaxAirwayVolumes(
        const std::vector<float>& volumes);
        
        
        const std::vector<float> m_weibelSym_Gen2meanDiameter{ 18 , 12.2 , 8.3, 5.6, 4.5 , 3.5 , 2.8 ,2.3 , 1.86,1.54 , 1.3 };
        const std::vector<float> m_weibelSym_Gen2meanLength{ 120,47.6, 19 ,7.6 , 12.7 ,10.7 , 9.0, 7.6 ,6.4 , 5.4 , 4.6 };
        const std::vector<float> m_105_volume{ 15113.7 , 5111.5 , 1253.33 , 694.257 , 380.925 , 547.386 , 189.26 , 173.221 , 217.099 , 184.104 , 33.6818 };
//, 85.3501 , 110.44 , 31.6196 , 5.84276 , 5.3845 , 12.0292 , 10.8836 , 6.07189 , 6.53014 , 10.0816 , 22.1108 , 15.6953 , 2.8641 , 14.2059 , 4.46799 , 9.27967 , 9.39424 , 13.8622 , 13.8622 , 15.237 , 15.237 , 15.237 , 6.75927 , 6.41558 , 1.71846 }

        std::unordered_map<unsigned int, float> m_generation2length; //maximum length of airways
        std::unordered_map<unsigned int, float> m_generation2maximumSize; //maixum size of all descendents 
        
        //    const std::map<unsigned int, float> m_weibelSym_Gen2meanDiameter{ { 1, 18 }, { 2, 12.2 }, { 3, 8.3 },
        //        { 4, 5.6 }, { 5, 4.5 }, { 6, 3.5 } , { 7, 2.8 }, { 8, 2.3 }, { 9, 1.86 }, { 10, 1.54 }, { 11, 1.3 }};
        //    const std::map<unsigned int, float> m_weibelSym_Gen2meanLength{ { 1, 120 }, { 2, 47.6 }, { 3, 19 },
        //        { 4, 7.6 }, { 5, 12.7 }, { 6, 10.7 }, { 7, 9.0 }, { 8, 7.6 }, { 9, 6.4 }, { 10, 5.4 }, { 11, 4.6 } };
};
}

#endif