#!/bin/sh
DIR=/home/brian/data/Segmentation/PreOpPlanning/
ODIR=/home/brian/data/Segmentation/PreOpPlanning/brian_seg/
for i in ${DIR}/002-${1}*/*.nii.gz  ; do 
    name=`basename $i .nii.gz` 
    if [ ! -d ${ODIR}/$name ] ; then
	mkdir -p ${ODIR}/$name
    fi

    echo  ./estimate_fat_lung_thresholds $i  ${ODIR}/${name}/${name}
     ./estimate_fat_lung_thresholds $i  ${ODIR}/${name}/${name}
done
