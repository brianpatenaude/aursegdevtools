#!/bin/sh
DIR=/home/brian/data/ATS/niftis/
ODIR=/home/brian/data/ATS/brian_seg_mod
for i in ${DIR}/ats_${1}.nii.gz  ; do 
    name=`basename $i .nii.gz` 
    if [ ! -d ${ODIR}/$name ] ; then
	mkdir -p ${ODIR}/$name
    fi

    echo  ./estimate_trachea $i  ${ODIR}/${name}/${name}
    ./estimate_trachea $i  ${ODIR}/${name}/${name}
done
