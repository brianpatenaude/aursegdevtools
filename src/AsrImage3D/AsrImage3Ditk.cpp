#include <AsrImage3D.h>

#include "aursurface/aursurface.h"
#include <array>
#include <aurcommon/aurstructs_functions.hpp>
#include <iostream>
#include <list>
#include <map>
#include <tuple>
#include <unordered_set>
// itk
#include "itkImage.h"
#include "itkMaskImageFilter.h"
#include <itkFastMarchingImageFilter.h>

typedef itk::Image<short, 3> ShortImageType;
typedef itk::Image<float, 3> FloatImageType;
typedef itk::FastMarchingImageFilter<FloatImageType, FloatImageType> fastMarchingFilterType;

using namespace std;

namespace auris_segtools
{
void distFromSeed( const AsrImage3D<float>& imageMask, const std::unordered_set<unsigned int>& sparseIm, AsrImage3D<float>& distMap, vec3<int>& voxel)
{
    cout<<"start distance map "<<endl;
    // assumed both input and output have been appropritaeyl allcoated
    FloatImageType::Pointer itkImageMask = FloatImageType::New();

    typedef fastMarchingFilterType::NodeContainer NodeContainer;
    NodeContainer::Pointer seeds = NodeContainer::New();
    fastMarchingFilterType::NodeType node;
    FloatImageType::IndexType seedPosition;
    seedPosition[0] = voxel.x; // atoi(argv[argIndex++]);
    seedPosition[1] = voxel.y; // atoi(argv[argIndex++]);
    seedPosition[2] = voxel.z; // atoi(argv[argIndex++]);

    node.SetValue(-1.0);
    node.SetIndex(seedPosition);

    seeds->Initialize();
    seeds->InsertElement(0, node);

    itk::Size<3> imSize;
    imSize[0] = imageMask.xsize();
    imSize[1] = imageMask.ysize();
    imSize[2] = imageMask.zsize();

    itk::Index<3> index;
    index.Fill(0);
    array<float, 3> spacing{ imageMask.xdim(), imageMask.ydim(), imageMask.zdim() };
    ShortImageType::RegionType imRegion(index, imSize);
    //    //    itkImageMask->SetSpacing(imageMask.xdim(), imageMask.ydim(), imageMask.zdim());
    itkImageMask->SetSpacing(&spacing[0]);
    itkImageMask->SetRegions(imRegion);

    // TODO DON't copy data but just adjust pointers
    //    float* data_ptr = itkImageMask->GetBufferPointer();
    //    itkImageMask->GetBufferPointer() =  &(imageMask.data_vector()[0]);
    // imageMask.data();
    // copying data between different image data structures
    itkImageMask->Allocate();
    for (auto& i : sparseIm ){
//        cout<<"i "<<i<<endl;
        *(itkImageMask->GetBufferPointer() + i) = 1;
    }
    
//    memcpy(itkImageMask->GetBufferPointer(), &(imageMask.data_vector()[0]), sizeof(float) * imageMask.nvoxels());
//    cout << "done init memcpy " << endl;
    fastMarchingFilterType::Pointer fmFilter = fastMarchingFilterType::New();
    fmFilter->SetInput(itkImageMask);
    //  fmFilter->SetAlivePoints( seeds );
    fmFilter->SetSpeedConstant(1.0);
    fmFilter->SetTrialPoints(seeds);
    fmFilter->Update();

    // mask out voxels outside the the mask
//    typedef itk::MaskImageFilter<FloatImageType, FloatImageType> MaskFilterType;
//    MaskFilterType::Pointer maskFilter = MaskFilterType::New();
//    maskFilter->SetInput(fmFilter->GetOutput());
//    maskFilter->SetMaskImage(itkImageMask);
//    maskFilter->Update();
    cout << "memcpy " << endl;
    //distance map will be restricted to only those voxels within the mask
    //because of this I know longer need to use image mask
            const auto ptr_dist = fmFilter->GetOutput()->GetBufferPointer();        
            for (auto& i : sparseIm ){
//        cout<<"i "<<i<<endl;
        distMap.value(i) = *( ptr_dist + i) ;
    }
    cout<<"done distance map"<<endl;
//    
//    memcpy(
//        &(distMap.data_vector()[0]), maskFilter->GetOutput()->GetBufferPointer(), sizeof(float) * imageMask.nvoxels());
    //    memcpy(
    //        &(distMap.data_vector()[0]), fmFilter->GetOutput()->GetBufferPointer(), sizeof(float) *
    //        imageMask.nvoxels());
    //  distMap.writeImage("distMap.nii.gz");
}

void distFromSeed(AsrImage3D<float>& imageMask, AsrImage3D<float>& distMap, vec3<int>& voxel)
{
    // assumed both input and output have been appropritaeyl allcoated
    FloatImageType::Pointer itkImageMask = FloatImageType::New();

    typedef fastMarchingFilterType::NodeContainer NodeContainer;
    NodeContainer::Pointer seeds = NodeContainer::New();
    fastMarchingFilterType::NodeType node;
    FloatImageType::IndexType seedPosition;
    seedPosition[0] = voxel.x; // atoi(argv[argIndex++]);
    seedPosition[1] = voxel.y; // atoi(argv[argIndex++]);
    seedPosition[2] = voxel.z; // atoi(argv[argIndex++]);

    node.SetValue(-1.0);
    node.SetIndex(seedPosition);

    seeds->Initialize();
    seeds->InsertElement(0, node);

    itk::Size<3> imSize;
    imSize[0] = imageMask.xsize();
    imSize[1] = imageMask.ysize();
    imSize[2] = imageMask.zsize();

    itk::Index<3> index;
    index.Fill(0);
    array<float, 3> spacing{ imageMask.xdim(), imageMask.ydim(), imageMask.zdim() };
    ShortImageType::RegionType imRegion(index, imSize);
    //    //    itkImageMask->SetSpacing(imageMask.xdim(), imageMask.ydim(), imageMask.zdim());
    itkImageMask->SetSpacing(&spacing[0]);
    itkImageMask->SetRegions(imRegion);

    // TODO DON't copy data but just adjust pointers
    //    float* data_ptr = itkImageMask->GetBufferPointer();
    //    itkImageMask->GetBufferPointer() =  &(imageMask.data_vector()[0]);
    // imageMask.data();
    // copying data between different image data structures
    itkImageMask->Allocate();
    memcpy(itkImageMask->GetBufferPointer(), &(imageMask.data_vector()[0]), sizeof(float) * imageMask.nvoxels());
    cout << "done init memcpy " << endl;
    fastMarchingFilterType::Pointer fmFilter = fastMarchingFilterType::New();
    fmFilter->SetInput(itkImageMask);
    //  fmFilter->SetAlivePoints( seeds );
    fmFilter->SetSpeedConstant(1.0);
    fmFilter->SetTrialPoints(seeds);
    fmFilter->Update();

    // mask out voxels outside the the mask
    typedef itk::MaskImageFilter<FloatImageType, FloatImageType> MaskFilterType;
    MaskFilterType::Pointer maskFilter = MaskFilterType::New();
    maskFilter->SetInput(fmFilter->GetOutput());
    maskFilter->SetMaskImage(itkImageMask);
    maskFilter->Update();
    cout << "memcpy " << endl;
    memcpy(
        &(distMap.data_vector()[0]), maskFilter->GetOutput()->GetBufferPointer(), sizeof(float) * imageMask.nvoxels());
    //    memcpy(
    //        &(distMap.data_vector()[0]), fmFilter->GetOutput()->GetBufferPointer(), sizeof(float) *
    //        imageMask.nvoxels());
    //  distMap.writeImage("distMap.nii.gz");
}
}
