#ifndef ASRIMAGE3DITK_H
#define ASRIMAGE3DITK_H

//STL includes
#include<vector>
#include<array>
#include<string>
//#include <map>
#include <list>
#include <tuple>
#include <unordered_set>
#include<iostream>
//auris headers
#include <aurcommon/aurstructs.h>
#include <AsrImage3D/AsrImage3D.h>

namespace auris_segtools{
    
    void distFromSeed( AsrImage3D<float> & imageMask, AsrImage3D<float> &  distMap, vec3<int> & voxel );
    void distFromSeed( const AsrImage3D<float> & imageMask,const std::unordered_set<unsigned int> & sparseIm, AsrImage3D<float> &  distMap, vec3<int> & voxel );
    
}
    
#endif
