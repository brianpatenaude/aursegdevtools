#include "evaluationMetrics.h"

#include <sstream>

using namespace std;

namespace auris_segtools
{

asrEvaluationMetrics::asrEvaluationMetrics()
{
}
asrEvaluationMetrics::asrEvaluationMetrics( const string & subjectName )
{
    m_subjectName = subjectName;
}

asrEvaluationMetrics::~asrEvaluationMetrics()
{
}

    void asrEvaluationMetrics::setSpacing( const std::array<float,3> & spacing )
    {
        m_spacing = spacing;
    }
    
    void asrEvaluationMetrics::setSpacing( const float& dx, const float & dy, const float & dz ){
        m_spacing[0] = dx;
        m_spacing[1] = dy;
        m_spacing[2] = dz;
    }
        void asrEvaluationMetrics::setVoxelCount( const std::string & segLevel, const float & value ){
            m_voxelCount[ segLevelToIndex[segLevel] ] = value;
        }

    void  asrEvaluationMetrics::setVolume( const string & segLevel, const float & value ){
        m_volume[ segLevelToIndex[segLevel] ] = value;
        
    }
    void asrEvaluationMetrics::setNumberOfConnectedCompenents( const unsigned int & N )
    {
        m_numberOfCC = N; 
    }

    string asrEvaluationMetrics::to_string()
    {
        stringstream ss;
        ss<<std::fixed;
        ss<<m_subjectName;
        //resolution of image
        for (auto& spacing : m_spacing)
        {
            ss<<","<<spacing;
        }
        
        //volume of segmentation (voxels*voxelvolume)
        for (auto& vol : m_volume)
        {
            ss<<","<<vol;
        }
        
        //number of voxels 
        for(auto& voxCount : m_voxelCount)
        {
            ss<<","<<voxCount;
        }
        //numbver of connected components
        ss<<","<<m_numberOfCC;
        
        return ss.str();
        
    }




}