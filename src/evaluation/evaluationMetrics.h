#ifndef EVALUATIONMETRICS_H
#define EVALUATIONMETRICS_H

#include <array>
#include <unordered_map>

namespace auris_segtools
{

class asrEvaluationMetrics
{
    std::array<unsigned int, 4> m_voxelCount{ {0,0,0,0 }};
    std::array<float,4> m_volume{ {0,0,0,0} };
    std::array<float,3> m_spacing{{1.0,1.0,1.0}};
    std::string m_subjectName{"subjectX"};
    unsigned int m_numberOfCC{0};
    std::unordered_map<std::string, int> segLevelToIndex { {"global",0},{"medium",1}, {"localGlobal",2}, {"localMedium",3} };

public:
    asrEvaluationMetrics();
    asrEvaluationMetrics(const std::string & subjectName);
    ~asrEvaluationMetrics();
    
    
    std::string to_string();
    void setSpacing( const float& dx, const float & dy, const float & dz ); 
    void setSpacing( const std::array<float,3> & spacing ); 
    void setVolume( const std::string & segLevel, const float & value );
    void setVoxelCount( const std::string & segLevel, const float & value );
    void setNumberOfConnectedCompenents( const unsigned int & N );
};

}

#endif // EVALUATIONMETRICS_H
