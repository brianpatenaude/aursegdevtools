/*
 * run_test.cpp
 *
 *  Created on: May 11, 2016
 *      Author: brian
 */
#include "aursurface/aursurface.h"

#include "aurimage/AsrImage3D.h"
#include "aurcommon/miscfunctions.h"
#include "aurcommon/aurstructs_functions.hpp"
#include "evaluationMetrics.h"
#include "utils/asr-filehandling.hpp"
//itk 
#include <itkSignedMaurerDistanceMapImageFilter.h>
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkImage.h"
#include "itkNiftiImageIOFactory.h"
#include "itkMetaImageIOFactory.h"
#include <itkNiftiImageIO.h>
#include <itkMetaImageIO.h>
#include <itkStatisticsImageFilter.h>
//#include <itkGaussianDerivativeOperator.h>
//#include <itkSobelEdgeDetectionImageFilter.h>
#include <itkConnectedComponentImageFilter.h>
//STL includes
#include <iostream>
#include <vector>
#include <fstream>
#include <map>
//3rd party includes
#include <boost/filesystem.hpp>
#include <png.h>

using namespace std;
using namespace auris_segtools;
using namespace boost::filesystem;
void Usage(){
  cout<<"\n asr-seg-evaluate <nifti file> <ouput base>"<<endl;
  cout<<"\n This program is used to evaluate segmentation \n"<<endl;

}
   
typedef itk::Image<short,3> ShortImageType;
typedef itk::Image<float,3> FloatImageType;
typedef itk::ImageFileReader<ShortImageType> ReaderType;
typedef itk::ConnectedComponentImageFilter< ShortImageType, ShortImageType > ccFilter;
 

int main(int argc, char*argv[])
{
    cout<<"evalue"<<endl;
    if ( argc != 4 )
    {
        Usage();
        return 0; 
    }
    
    path inputImage = argv[1];
    //check to see if image exists 
    if (! checkFileExists(inputImage))
    {
        return 1; 
    }
    path obase(argv[2]);
    cout<<"read path "<<obase<<endl;
//    cout<<"root_path() "<<" "<<obase.root_path()<<endl;
//    path file_rsipGlobal{obase};
    
    string imageExtension{".mhd"};
    vector<string> segExtensions{"global","medium","localGlobal","localMedium"};
    vector<int> volumes(segExtensions.size(),0);
    
    //SETUP itk STUFF
    itk::NiftiImageIOFactory::RegisterOneFactory();
    itk::MetaImageIOFactory::RegisterOneFactory();

    asrEvaluationMetrics subjectMetrics(obase.string());

    ReaderType::Pointer reader = ReaderType::New();
    reader->SetFileName(inputImage.string());
    reader->Update();
    cout<<"spacing "<<reader->GetOutput()->GetSpacing()[0]<<endl;
    array<float,3> spacing{reader->GetOutput()->GetSpacing()[0],reader->GetOutput()->GetSpacing()[1],reader->GetOutput()->GetSpacing()[2]};
        
    subjectMetrics.setSpacing(spacing);
    
    float voxelVolume = spacing[0]*spacing[1]*spacing[2];
    
    
    ShortImageType::Pointer image; 
    auto i_volumes = volumes.begin();
    for ( auto& iSeg : segExtensions ){
        
       path imageName{ obase.string() + "_" + iSeg  + imageExtension };
       cout<<"Reading "<<imageName<<endl;
       if (!checkFileExists(imageName)) 
           {
               cout<<"could not find "<<imageName<<endl;
               continue;
           }

        ReaderType::Pointer reader = ReaderType::New();
        reader->SetFileName(imageName.string());
        reader->Update();
        image = reader->GetOutput();
        {
        typedef itk::StatisticsImageFilter<ShortImageType> StatisticsImageFilterType;
        StatisticsImageFilterType::Pointer statisticsImageFilter
          = StatisticsImageFilterType::New ();
        statisticsImageFilter->SetInput(image);
        statisticsImageFilter->Update();
        subjectMetrics.setVolume(iSeg,statisticsImageFilter->GetSum()*voxelVolume);
        subjectMetrics.setVoxelCount(iSeg,statisticsImageFilter->GetSum());
        cout<<"statisticsImageFilter "<<statisticsImageFilter->GetSum()<<endl;
        }
        //find number of connect components 
        {
            ccFilter::Pointer p_ccFilter = ccFilter::New();
            p_ccFilter->SetInput(image);
            p_ccFilter->Update();
            subjectMetrics.setNumberOfConnectedCompenents(p_ccFilter->GetObjectCount());
        }
        
        

        ++i_volumes;
    }
  cout<<subjectMetrics.to_string()<<endl;
//    typedef itk::SobelEdgeDetectionImageFilter< ImageType,FloatImageType > edgeDetector ;
//    edgeDetector::Pointer sobelEdgeDector = edgeDetector::New();
//    sobelEdgeDector->SetInput(image);
//    sobelEdgeDector->Update();
//    
//    typedef  itk::ImageFileWriter< FloatImageType  > WriterType;
//    itk::NiftiImageIO::Pointer nifti_io = itk::NiftiImageIO::New();
//
//    WriterType::Pointer writer = WriterType::New();
//    writer->SetImageIO(nifti_io);
//    writer->SetFileName("edgeSobel.nii.gz");
//    writer->SetInput(sobelEdgeDector->GetOutput());
//    writer->Update();

    
}

