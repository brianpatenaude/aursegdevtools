/*
 * run_test.cpp
 *
 *  Created on: May 11, 2016
 *      Author: brian
 */
#include "aurgeometry/aurgeometry.h"
#include "aurcommon/aurstructs_functions.hpp"
#include <Eigen/Core>

//STL includes
#include <iostream>
#include <fstream>

using namespace std;
using namespace auris_segtools;
using namespace Eigen;

void Usage(){
	cout<<"\n test_rotation <x> <y> <z> "<<endl;
	cout<<"\n This program is estimate rotation matrix \n"<<endl;

}
int main(int argc, char*argv[])
{
    
    float x = atof(argv[1]);  
    float y = atof(argv[2]);   
    float z = atof(argv[3]);

    cout<<"Rotate x/y/z "<<x<<" "<<y<<" "<<z<<endl;
    
    
   Matrix<float,4,4> Rxy = rotateToZaxis( vec3<float>(x,y,z)); 
   printRotationMatrix(Rxy,"Rxy");  
    
    float angleIncrement = M_PI_2 /2;
    for ( int i = -2; i <= 2 ; ++i )
    {   
        vec3<float> cutAxis(x,y,z);
        vec3<float> planeNormal = rotatePlaneAboutAxis(  cutAxis, i*angleIncrement);
        cout<<"Plane normal "<<planeNormal.x<<" "<<planeNormal.y<<" "<<planeNormal.z<<endl; 
    }
    
    
               
} 