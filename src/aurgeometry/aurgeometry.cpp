/*
 * aursurface.cpp
 *
 *  Created on: May 13, 2016
 *      Author: brian
 */

#include "aurgeometry.h"
#include "aurcommon/aurstructs_functions.hpp"
#include <Eigen/LU>

//STL
#include <iostream>
//#include <sstream>
//#include <fstream>
//#include <array>
//#include <unordered_set>
//#include <unordered_map>

using namespace std;
using namespace Eigen;
//template< class T> using aursurface<T>::contour;
namespace auris_segtools {
//    
//    T sumOfSubtendedAngles( const aurcontour<T> & contour, const vertex<T> & point  )
//    {
//        T sum{0.0};
//        
//    }

template<class T>
int sideOfPlane( const vertex<T> & vert0, const vertex<T> & vPlane0 )
{
	//test what side of plane the point lies (-1 , 0, 1)
//	vec3<T> vPlaneToPoint = vert0 - vPlane0;
	float dot = dotProduct(vert0 - vPlane0,normal(vPlane0));
	int side { (dot == 0) ? 0 : ( (dot>0) ? 1 : -1) };

	return side;

}
template int sideOfPlane( const vertex<float> & vert0, const vertex<float> & vPlane0 );


template<class T>
int sideOfPlane( const vertex<T> & vert0, const vertex<T> & vPlane0, const vec3<T> & pNormal )
{
	//test what side of plane the point lies (-1 , 0, 1)
//	vec3<T> vPlaneToPoint = vert0 - vPlane0;
	float dot = dotProduct(vert0 - vPlane0,pNormal);
	int side { (dot == 0) ? 0 : ( (dot>0) ? 1 : -1) };

	return side;

}
template int sideOfPlane( const vertex<float> & vert0, const vertex<float> & vPlane0, const vec3<float> & pNormal );

template<class T> 
T distanceToSegment( const vertex<T> & v0, const vertex<T> & line_p0, const vertex<T> & line_p1 )
{
//    cout<<"distance To Segment "<<endl;
//    cout<<v0.coord_to_string()<<line_p0.coord_to_string()<<line_p1.coord_to_string()<<endl;
    vec3<T> lineSeg = line_p1 - line_p0;
    vec3<T> p0_to_v0 = v0 - line_p0;
    T projection  = dotProduct(p0_to_v0,lineSeg);
    //bounding points
    if (projection < 0 ) 
    {
//        cout<<"Answer : "<< l2norm(p0_to_v0)<<endl;
        return l2norm(p0_to_v0);
    }  
    if (projection > 1 )
    {
//        cout<<"Answer : "<< l2norm(v0 - line_p1)<<endl;
        return l2norm(v0 - line_p1);
    }
        
    vertex<T> p_intersect = line_p0 + lineSeg * projection;
//    cout<<"Answer : "<< l2norm(p_intersect-v0)<<endl;
    return l2norm(p_intersect-v0);
    
    
}
template float distanceToSegment( const vertex<float> & v0, const vertex<float> & line_p0, const vertex<float> & line_p1 );



template<class T>
bool triangleTriangleIntersection( const vertex<T> & v0_0, const vertex<T> & v1_0, const vertex<T> & v2_0, const vertex<T> & v0_1, const vertex<T> & v1_1, const vertex<T> & v2_1  )
{   
    //triangle - trinagle intersection 
    //http://web.stanford.edu/class/cs277/resources/papers/Moller1997b.pdf
//    cout<<"verts "<<v0_1.coord_to_string()<<" "<<v1_1.coord_to_string()<<" "<<v2_1.coord_to_string()<<endl;
//    cout<<"verts "<<v0_0.coord_to_string()<<" "<<v1_0.coord_to_string()<<" "<<v2_0.coord_to_string()<<endl;
    
    vec3<T> normal_1 = crossProduct(v1_1 - v0_1 , v2_1 -v0_1 );//normal iof triangle 2
//    cout<<"nromal1 "<<normal_1.to_string()<<endl;
    T d_1 = dotProduct(vec3<T>(v0_1),normal_1)*-1; //plane parameter
    //calculate distabnces of vertice to plane
    T dist01 = dotProduct(normal_1,vec3<T>(v0_0)) + d_1;
    T dist11 = dotProduct(normal_1,vec3<T>(v1_0)) + d_1;
    T dist21 = dotProduct(normal_1,vec3<T>(v2_0)) + d_1;
//        cout<<"dists1 "<<dist01<<" "<<dist11<<" "<<dist21<<endl;
    //check to see if all  lie on one side of plabe and none are zero
    if (  (dist01>0) + (dist11>0) + (dist21>0) == 3 ) return false;
    if (  (dist01<0) + (dist11<0) + (dist21<0) == 3 ) return false;

//cout<<"tri-tri next step "<<endl;
    if ( (dist01==0) && (dist11==0) &&(dist21==0))
    {//coplnanar case
        //TODO implement coplananr case 
//        cout<<"coplanar IMPLEMENT THIS "<<endl;
        //in our case should have this condition to check , just dont do else
    }else{
            vec3<T> normal_0 = crossProduct(v1_0 - v0_0 , v2_0 -v0_0 );//normal iof triangle 2
//    cout<<"nromal0 "<<normal_0.to_string()<<endl;
//            normalize(normal_0);
            T d_0 = dotProduct(vec3<T>(v0_0),normal_0)*-1; //plane parameter
        
            T dist00 = dotProduct(normal_0,vec3<T>(v0_1)) + d_0;
            T dist10 = dotProduct(normal_0,vec3<T>(v1_1)) + d_0;
            T dist20 = dotProduct(normal_0,vec3<T>(v2_1)) + d_0;
//        cout<<"dists0 "<<dist00<<" "<<dist10<<" "<<dist20<<endl;
    
            //check to see if all  lie on one side of plabe and none are zero
            if (  (dist00>0) + (dist10>0) + (dist20>0) == 3 ) return false;
            if (  (dist00<0) + (dist10<0) + (dist20<0) == 3 ) return false;
        
            vec3<T> Dinter = crossProduct(normal_0,normal_1);//direction of line (L) of intersection
            //project vertices onto line L 
            //TODO use optimzes from articles above, max direction of D  
            T p_v00  = dotProduct(Dinter, vec3<T>(v0_0));
            T p_v10  = dotProduct(Dinter, vec3<T>(v1_0));
            T p_v20  = dotProduct(Dinter, vec3<T>(v2_0));
//            cout<<"p0 "<<p_v00<<" "<<p_v10<<" "<<p_v20<<endl;
            T t10{0},t20{0};
            //TODO CASE OF triangle line in plane
            if  ( ( ( dist01 > 0 ) && (dist21 >0) ) || ( ( dist01 < 0 ) && (dist21 < 0 ) )  )
            {//v0 and v2 on same side
                t10 = p_v00 + (p_v10 - p_v00) * dist01 / (dist01-dist11);
                t20 = p_v20 + (p_v10 - p_v20) * dist21 / (dist21-dist11);
            }else if( ( ( dist01 > 0 ) && (dist11 >0) ) || ( ( dist01 < 0 ) && (dist11 <0 ) )  ) {//v0 and v1 on same side
                t10 = p_v00 + (p_v20 - p_v00) * dist01 / (dist01-dist21);
                t20 = p_v10 + (p_v20 - p_v10) * dist11 / (dist11-dist21);
            }else{//v1 and v2 on same side
                t10 = p_v10 + (p_v00 - p_v10) * dist11 / (dist11-dist01);
                t20 = p_v20 + (p_v00 - p_v20) * dist21 / (dist21-dist01);
            }
            
            //order 
            if (t10 > t20)
                swap(t10,t20);
//            cout<<"D "<<Dinter.to_string()<<endl;
            //calculate t1 and t2 for plane containing second triangle, the triangle is already in plane 
            T p_v01  = dotProduct(Dinter, vec3<T>(v0_1));
            T p_v11  = dotProduct(Dinter, vec3<T>(v1_1));
            T p_v21 = dotProduct(Dinter, vec3<T>(v2_1));
//            cout<<"p1 "<<p_v01<<" "<<p_v11<<" "<<p_v21<<endl;
            T t11{0},t21{0};
            //TODO CASE OF triangle line in plane
            if  ( ( ( dist00 > 0 ) && (dist20 >0) ) || ( ( dist00 < 0 ) && (dist20 <0 ) )  )
            {//v0 and v2 on same side
                t11 = p_v01 + (p_v11 - p_v01) * dist00 / (dist00-dist10);
                t21 = p_v21 + (p_v11 - p_v21) * dist20 / (dist20-dist10);
            }else if( ( ( dist00 > 0 ) && (dist10 >0) ) || ( ( dist00 < 0 ) && (dist10 <0 ) )  ) {//v0 and v1 on same side
                t11 = p_v01 + (p_v21 - p_v01) * dist00 / (dist00-dist20);
                t21 = p_v11 + (p_v21 - p_v11) * dist10 / (dist10-dist20);
            }else{//v1 and v2 on same side
                t11 = p_v11 + (p_v01 - p_v11) * dist10 / (dist10-dist00);
                t21 = p_v21 + (p_v01- p_v21) * dist20 / (dist20-dist00);
            }
            
            if (t11 > t21)
                swap(t11,t21);
            
            if   (  (( t11 > t10 ) && (t11 < t20)) ||
                    (( t21 > t10 ) && (t21 < t20)) )
                    {
//                        cout<<"tri-tri intersect "<<t10<<" "<<t20<<" "<<t11<<" "<<t21<<" "<<t11-t10<<" "<<t11 - t20<<endl;
                        return true;
                    }
            
    }

    return false;
}
template bool triangleTriangleIntersection( const vertex<float> & v0_0, const vertex<float> & v1_0, const vertex<float> & v2_0, const vertex<float> & v0_1, const vertex<float> & v1_1, const vertex<float> & v2_1  );

template<class T>
int trianglePlaneIntersection( const vertex<T> & vTri0, const vertex<T> & vTri1, const vertex<T> & vTri2, const vertex<T> & vPlane0, const vec3<T> & pNormal )
{
	//configuration index

	//1 : intersect with v0-v1 and v0-v2
	//2 : intersect with v1-v2 and v1-v0
	//3 : intersect with v2-v0 and v2-v1

	//first test sided-ness of each point
	int side_v0{ sideOfPlane(vTri0,vPlane0,pNormal) };
	int side_v1{ sideOfPlane(vTri1,vPlane0,pNormal)};
	int side_v2{ sideOfPlane(vTri2,vPlane0,pNormal)};

	if ( abs(side_v0 + side_v1 + side_v2) == 3 ) return 0; //case where all points lie on same point of plane

	//TODO add ability tto work with the following condition
	if ( ( side_v0==0 ) || (side_v1==0) || (side_v2==0) ) return 0; //ignore case where it intersect exactly

	if  ( ( (side_v0 *side_v1) < 0 ) && ( (side_v0 *side_v2) < 0 ) ) return 1;
	if  ( ( (side_v1 *side_v0) < 0 ) && ( (side_v1 *side_v2) < 0 ) ) return 2;
	if  ( ( (side_v2 *side_v0) < 0 ) && ( (side_v2 *side_v1) < 0 ) ) return 3;


	return 0;
}
template int trianglePlaneIntersection( const vertex<float> & vTri0, const vertex<float> & vTri1, const vertex<float> & vTri2, const vertex<float> & vPlane0, const vec3<float> & pNormal );




    template<class T> 
    T angle( const vec3<T> & v0, const vec3<T> &v1 )
    {
        T denom  =  (l2norm(v0) * l2norm(v1));
        if (denom == 0 )
            return 0;
        
//        cout<<l2norm(crossProduct(v0,v1)) /denom<<" "<<(l2norm(crossProduct(v0,v1)) /denom == 1) <<" "<<(l2norm(crossProduct(v0,v1)) /denom > 1)<<endl;
        T asinArg = l2norm(crossProduct(v0,v1)) /denom;
        //correct for numerical inaccuracies
        if (asinArg > 1 ) asinArg=1;
        else if (asinArg < -1 ) asinArg = -1 ; 
        T angRads = asin(  asinArg );

        if (dotProduct(v0,v1) < 0 )
        {
            bool quad34 = angRads < 0 ; 
            angRads = M_PI - abs(angRads);
            if (quad34)
                angRads *=-1;
        }
        return angRads;
    }

    template float angle( const vec3<float> & v0, const vec3<float> &v1 );

    template<class T> 
    void reorientNormal( vec3<T> & normal, const vec3<T> & normal_prev )
    {
        if (dotProduct(normal,normal_prev) < 0 )
        {
            cout<<"do reorient"<<endl;
                normal *=  -1;
        }
    }
    template void reorientNormal<float>( vec3<float> & normal, const vec3<float> & normal_prev );

    
    template<class T>
    vertex<T> linePlaneIntersection(const vertex<T> & line_p0,const vertex<T> & line_p1, const vertex<T> & planePoint, const vec3<T> & planeNormal)
    {
       
        //estimate r, in parametric equation of line P(r) = P0 + r(P1-P0)
        vec3<T> vec_p10 =line_p1- line_p0;
        T r = dotProduct(planeNormal,planePoint - line_p0)/dotProduct(planeNormal,line_p1- line_p0);

        return line_p0 + r*vec_p10 ;


    }
    template vertex<float> linePlaneIntersection(const vertex<float> & line_p0,const vertex<float> & line_p1, const vertex<float> & planePoint, const vec3<float> & planeNormal);
    
    
    template <class T> 
    T rayRriangleIntersection( const vertex<T> & ray_p0,  const vec3<T> & ray_dir, const vertex<T> & vTri0, const vertex<T> & vTri1, const vertex<T> & vTri2 )
    {
        
    }





    template <class T> 
    int sideOfLine( const vec3<T> & lineSegment, const vec3<T> & lineToVertex, const vec3<T> & normal)
    {   //+1 - left , 0 on , -1 right 
//           cout<<"sideofliABe "<<dotProduct(lineSegment,lineToVertex)<<" "<<l2norm(lineSegment)<<" "<<l2norm(lineToVertex)<<" "\ 
//           <<dotProduct(lineSegment,lineToVertex)*dotProduct(lineSegment,lineToVertex) - l2normSquared(lineSegment)* l2normSquared(lineToVertex)<<  endl;
            vec3<T> sideVector = crossProduct(lineSegment,lineToVertex);
            if (  l2normSquared(sideVector)  < SIDEOFLINE_TOLERANCE )
                    return 0; 
//                  cout<<"sideoflinBe "<<" "<<dotProduct(sideVector,normal)<<endl;
            return  ( dotProduct(sideVector,normal) > 0 ) ? 1 : -1;  
        
            return 0; 
    }
    template  int sideOfLine( const vec3<float> & lineSegment, const vec3<float> & lineToVertex, const vec3<float> & normal);
    
    template<class T> 
    Eigen::Matrix<T,4,4>    rotateToZaxis( const vec3<T> & in_vec )
    {
        //TODO optimize this
        //assumes start of vector is at origin
        //folllows https://www.siggraph.org/education/materials/HyperGraph/modeling/mod_tran/3drota.htm
        //note the webpage has some typos
             
            vec3<T> U = in_vec;
            normalize(U);
            T a{U.x}, b{U.y},c{U.z}; //these stray the sma e
            
            T d{ static_cast<T>(sqrt(U.y * U.y + U.z*U.z) )};
            
            //calculate Rx 
            Eigen::Matrix<T,4,4>  Rx;// =Eigen::FixedXD::Identity() ;//create identity
            Rx.setIdentity();
            if (d  > 0 ){
                Rx(1,1) = Rx(2,2)= c/d;
                Rx(1,2)  = -b/d; 
                Rx(2,1)  = b/d;
           
                //used for calculating RYUcalculate Ry
                U = in_vec; 
                U.y = 0;//(c*in_vec.y - b*in_vec.z)/d; //y should be 0
                U.z = (b*in_vec.y + c*in_vec.z)/d;
            
//cout<<"Ry "<<U.x<<" "<<U.z<<endl;
                normalize(U);
                a=U.x; 
                b=U.y;
                c=U.z;
            }
                            d = sqrt(U.x*U.x + U.z*U.z);

            Eigen::Matrix<T,4,4>  Ry;// =FixedXD::Identity() ;//create identity
            Ry.setIdentity();
            if (d > 0 ){
                Ry(0,0) = Ry(2,2)= c/d;
                Ry(0,2)  = -a/d; 
                Ry(2,0)  = a/d;
            }
            
//           printRotationMatrix(Ry,"Ry");
            
            return  Ry*Rx ; 
    }

    template  Matrix<float,4,4>   rotateToZaxis( const vec3<float> & in_vec );
    
    template<class T> 
    vec3<T> rotatePlaneAboutAxis(  const vec3<T> & axis,  const T & angle )
    {
        Matrix<T,4,4>   RaxisToZ =  rotateToZaxis( axis ); //going to rotate to align axis the (0,0,1)

        //construct roattion matrix about z-axis 
        Matrix<T,4,4>   Rz =  rotateToZaxis( axis );//roatet about z 
        Rz.setIdentity();
        Rz(0.0)=Rz(1,1) = cos(angle);
        Rz(1.0)=Rz(0,1) = sin(angle);
        Rz(1,0)*=-1;
//        printRotationMatrix(Rz);
        //assuming normal relative transformed axis 
        Matrix<T,4,1>  rotatedNormal(1,0,0,1.0);//initalizing to input 
//        rotatedNormal =   RaxisToZ.inverse() * Rz * RaxisToZ * rotatedNormal;
        rotatedNormal =   RaxisToZ.inverse() * Rz * rotatedNormal;
        return vec3<T>(rotatedNormal(0,0) , rotatedNormal(1,0) , rotatedNormal(2,0)  );
    }
    template vec3<float>  rotatePlaneAboutAxis(  const vec3<float> & axis,  const float & angle );

    
    template< class T> 
    void printRotationMatrix(  const Eigen::Matrix<T,4,4> & mat , string name)
    {
        cout<<name<<" = [ "<<endl;
        for (int i  = 0 ; i < 4 ; ++i)
        {
            for (int j = 0 ; j < 4 ; ++j )
            { 
                cout<<mat(i,j)<<" ";   
            }
            cout<<";"<<endl; 
        }    
        cout<<"]"<<endl; 
    }
    template  void printRotationMatrix(  const Eigen::Matrix<float,4,4> & mat , string name);


} /* namespace auris_segtools */
