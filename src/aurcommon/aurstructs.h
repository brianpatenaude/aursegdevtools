/*
 * aurstructs.h
 *
 *  Created on: May 12, 2016
 *      Author: brian
 */
#ifndef AURSTRUCTS_H_
#define AURSTRUCTS_H_

#include <iostream>

namespace auris_segtools
{
template <class T> struct deformationParameters {
    deformationParameters()
    //            : {0,0,0.01,0.2,0.01,8}
    {
    }
    std::string to_string() const
    {
        return ("sn:(" + std::to_string(alpha_sn) + "), st(" + std::to_string(alpha_st) + "), area(" +
            std::to_string(alpha_area) + "), im(" + std::to_string(alpha_im) + "),  maxStep(" +
            std::to_string(max_step_size) + "), splitTh(" + std::to_string(splitThreshold) + ")");
    }

    deformationParameters(const T& alpha_sn_in,
        const T& alpha_st_in,
        const T& alpha_area_in,
        const T& alpha_im_in,
        const T& max_step_size_in,
        const T& splitThreshold_in)
    {
        alpha_sn = alpha_sn_in;
        alpha_st = alpha_st_in;
        alpha_area = alpha_area_in;
        alpha_im = alpha_im_in;
        max_step_size = max_step_size_in;
        splitThreshold = splitThreshold_in;
    }
    T alpha_sn{ 0 }, alpha_st{ 0 }, alpha_area{ 0.01 }, alpha_im{ 0.2 }, max_step_size{ 0.01 }, splitThreshold{ 8 };
};

template <class T> struct vertex {

    vertex()
        : x{ 0 }
        , y{ 0 }
        , z{ 0 }
        , nx{ 0 }
        , ny{ 0 }
        , nz{ 0 }
        , scalar{ 0 }
    {
    }
    vertex(const T& xi, const T& yi, const T& zi)
    {
        x = xi;
        y = yi;
        z = zi;
        nx = 0;
        ny = 0;
        nz = 1;
        scalar = 0;
    }
    vertex(const T& xi, const T& yi, const T& zi, const T& sci)
    {
        x = xi;
        y = yi;
        z = zi;
        scalar = sci;
    }
    vertex(const T& xi, const T& yi, const T& zi, const T& nxi, const T& nyi, const T& nzi, const T& sci)
    {
        x = xi;
        y = yi;
        z = zi;
        nx = nxi;
        ny = nyi;
        nz = nzi;
        scalar = sci;
    }
    vertex& operator+=(const vertex& rhs)
    { // compound assignment (does not need to be a member,

        x += rhs.x;
        y += rhs.y;
        z += rhs.z;
        nx += rhs.x;
        ny += rhs.y;
        nz += rhs.z;
        scalar += rhs.scalar;

        return *this;
    }

    vertex& operator/=(const T& rhs)
    { // compound assignment (does not need to be a member,

        x /= rhs;
        y /= rhs;
        z /= rhs;
        nx /= rhs;
        ny /= rhs;
        nz /= rhs;
        scalar /= rhs;

        return *this;
    }
    vertex operator+(const vertex& rhs) const
    {
        return vertex(x + rhs.x, y + rhs.y, z + rhs.z, nx + rhs.x, ny + rhs.y, nz + rhs.z, scalar + rhs.scalar);
    }
    vertex operator/(const T& rhs) const
    {
        return vertex(x / rhs, y / rhs, z / rhs, nx / rhs, ny / rhs, nz / rhs, scalar / rhs);
    }
    std::string to_string() const
    {
        return ("( " + std::to_string(x) + " , " + std::to_string(y) + " , " + std::to_string(z) + " , " +
            std::to_string(nx) + " , " + std::to_string(ny) + " , " + std::to_string(nz) + " , " +
            std::to_string(scalar) + " )");
    }
    std::string coord_to_string() const
    {
        return ("( " + std::to_string(x) + " , " + std::to_string(y) + " , " + std::to_string(z) + " )");
    }
    std::string coord_to_stringSSV() const
    {
        return (std::to_string(x) + " " + std::to_string(y) + " " + std::to_string(z));
    }
    std::string normal_to_stringSSV() const
    {
        return (std::to_string(nx) + " " + std::to_string(ny) + " " + std::to_string(nz));
    }
    bool isOrigin() const
    {
        return ((x == 0) && (y == 0) && (z == 0));
    }

    T x, y, z, nx, ny, nz;
    T scalar;
};

template <class T> struct vec3 {
    vec3()
        : x{ 0 }
        , y{ 0 }
        , z{ 0 }
    {
    }
    vec3(const T& xi, const T& yi, const T& zi)
    {
        x = xi;
        y = yi;
        z = zi;
    }
    vec3(const vertex<T>& vert)
    {
        x = vert.x;
        y = vert.y;
        z = vert.z;
    }

    std::string to_string() const
    {
        return ("( " + std::to_string(x) + " , " + std::to_string(y) + " , " + std::to_string(z) + " )");
    }
    std::string to_stringCSV() const
    {
        return (std::to_string(x) + "," + std::to_string(y) + "," + std::to_string(z));
    }
    std::string to_stringSSV() const
    {
        return (std::to_string(x) + " " + std::to_string(y) + " " + std::to_string(z));
    }
    struct vec3& operator+=(const vec3& rhs)
    {
        x += rhs.x;
        y += rhs.y;
        z += rhs.z;
        return *this;
    }

    template <class T2> struct vec3& operator/=(const T2& rhs)
    {
        x /= rhs;
        y /= rhs;
        z /= rhs;
        return *this;
    }

    template <class T2> struct vec3& operator*=(const T2& rhs)
    {
        x *= rhs;
        y *= rhs;
        z *= rhs;
        return *this;
    }
    template <class T2> struct vec3 operator*(const T2& rhs)
    {
        return vec3<T>(x * rhs, y * rhs, z * rhs);
    }

    T x, y, z;
};
}
#endif /* AURSTRUCTS_H_ */
