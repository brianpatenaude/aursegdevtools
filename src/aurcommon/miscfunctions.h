/*
 * miscfunction.h
 *
 *  Created on: May 25, 2016
 *      Author: brian
 */

#ifndef MISCFUNCTIONS_H_
#define MISCFUNCTIONS_H_

#include <vector>
#include <list>
namespace auris_segtools{

template <class T>
T variance(const std::vector<T> & data );


//linear but historgram always small
template <class T>
T min(const std::vector<T> & data );


template <class T>
T mean(const std::list<T> & data );



template <class T>
T mean(const std::vector<T> & data );



template <class T>
void mixtureOfTwoGaussians(const std::vector<T> & histogram );


}

#endif /* MISCFUNCTIONS_H_ */
