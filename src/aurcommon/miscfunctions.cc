/*
 * miscfunctions.cc

 *
 *  Created on: May 25, 2016
 *      Author: brian
 */
#include <miscfunctions.h>

#include <iostream>

using namespace std;
namespace auris_segtools{

template <class T>
T variance(const std::vector<T> & data )
{
	T sx{0},sxx{0};
	auto N = data.size();
	for (auto& i : data)
		{
			sx += i;
			sxx += i*i;
		}

	return (sxx - sx*sx/N)/(N-1);

}
template float variance<float>(const std::vector<float> & data );

//linear but historgram always small
template <class T>
T min(const std::vector<T> & data )
{
	if (data.empty()) return 0;
	T min=data[0];
	for (auto& i : data)
	{
		if (i < min ) min = i;
	}
	return min;
}
template float min<float>(const std::vector<float> & data );

template <class T>
T mean(const std::list<T> & data )
{
    if (data.empty()) return 0; 
    
	T mean=0;
	for (auto& i : data)
	{
		mean += i;
	}
	return mean/data.size();
}
template float mean<float>(const std::list<float> & data );

template <class T>
T mean(const std::vector<T> & data )
{
    if (data.empty()) return 0;
	T mean=0;
	for (auto& i : data)
	{
		mean += i;
	}
	return mean/data.size();
}
template float mean<float>(const std::vector<float> & data );
//
//


//
//template < class T >
//void mixtureOfTwoGaussians(const std::vector<T> & histogram )
//{
//	cout<<"run mixture of Gaussians "<<endl;
//	cout<<"mean "<<mean(histogram)<<endl;
//	cout<<"variance "<<variance(histogram)<<endl;
//
//}
//
//template void mixtureOfTwoGaussians<float>(const std::vector<float> & histogram );

}




