/*
 * aurstructs_functions.hpp
 *
 *  Created on: May 13, 2016
 *      Author: brian
 */

#ifndef AURCOMMON_AURSTRUCTS_FUNCTIONS_HPP_
#define AURCOMMON_AURSTRUCTS_FUNCTIONS_HPP_
#include <aurcommon/aurstructs.h>
#include <math.h>

namespace auris_segtools{

//-------------------operators---------------------//


template<class T>
vec3<T> operator-(const vec3<T>& lhs, const vec3<T>& rhs) { return vec3<T>(lhs.x-rhs.x, lhs.y - rhs.y, lhs.z - rhs.z); }

//template<class T> 
//    std::ostream& operator<<(std::ostream& os , const vertex<float>& vert)  
//    {
////        return os<<vert.x<<" "<<vert.y<<" "<<vert.z<<" "<<vert.nx<<" "<<vert.ny<<" "<<vert.nz<<" "<<vert.scalar;
//        return os;
//    }
//    

template<class T>
inline vec3<T> operator-( const vertex<T> & vert1, const vertex<T> vert0 )
{
	return vec3<T>(vert1.x- vert0.x,vert1.y - vert0.y, vert1.z - vert0.z );
}

template<class T>
inline vec3<T> operator-( const vec3<T>& lhs, const vertex<T>& rhs)
{
	return vec3<T>(lhs.x -rhs.x, lhs.y-rhs.y, lhs.z-rhs.z );
};
template<class T>
inline vec3<T> operator+( const vec3<T>& lhs, const vertex<T>& rhs)
{
	return vec3<T>(lhs.x +rhs.x, lhs.y+rhs.y, lhs.z+rhs.z );
};

template<class T>
inline vec3<T> operator+( const vec3<T>& lhs, const vec3<T>& rhs)
{
	return vec3<T>(lhs.x +rhs.x, lhs.y+rhs.y,lhs.z+rhs.z );
};

template<class T>
inline vertex<T> operator+( const vertex<T>& lhs, const vec3<T>& rhs)
{
	return vertex<T>(lhs.x +rhs.x, lhs.y+rhs.y,lhs.z+rhs.z,lhs.nx,lhs.ny,lhs.nz, lhs.scalar );
};

template<class T>
inline vertex<T> operator-( const vertex<T>& lhs, const vec3<T>& rhs)
{
	return vertex<T>(lhs.x -rhs.x, lhs.y-rhs.y,lhs.z-rhs.z,lhs.nx,lhs.ny,lhs.nz, lhs.scalar );
};

template<class T>
inline vec3<T> operator*( const vec3<T>& lhs, const T&  rhs)
{
	return vec3<T>(lhs.x*rhs, lhs.y*rhs, lhs.z*rhs );
};
template<class T>
inline vec3<T> operator*( const T&  lhs,  const vec3<T>& rhs)
{
	return vec3<T>(rhs.x*lhs, rhs.y*lhs, rhs.z*lhs );
};

template<class T,class T2>
inline  vec3<T> operator/(const vec3<T>& lhs,const T2& rhs)
{
	return vec3<T>( lhs.x/rhs,lhs.y / rhs, lhs.z / rhs);
}

template<class T>
inline vertex<T> to_vertex( const vec3<T>& vec){
    return vertex<T>(vec.x,vec.y,vec.z); 
}
template<class T>
inline vec3<T> normal( const vertex<T>& vert)
{
	return vec3<T>(vert.nx,vert.ny,vert.nz );
};
template<class T>
inline void setNormal( vertex<T>& vert, const vec3<T>& v)
{
    vert.nx = v.x;
    vert.ny = v.y;
    vert.nz = v.z;
};


template<class T>
inline vertex<T> midpoint( const vertex<T> & vert0, const vertex<T> vert1 )
{
	return vertex<T>( (vert0.x + vert1.x)/2.0f ,(vert0.y + vert1.y)/2.0f , (vert0.z + vert1.z)/2.0f );
}

template<class T>
inline vertex<T> centroid( const vertex<T> & vert0, const vertex<T> vert1, const vertex<T> vert2 )
{
	return vertex<T>( (vert0.x + vert1.x+vert2.x)/3.0f ,(vert0.y + vert1.y+vert2.y)/3.0f , (vert0.z + vert1.z+vert2.z)/3.0f );
}

template<class T>
inline void translate( vertex<T> & vert1, const vec3<T> txyz )
{
	//cout<<"tx "<<vert1.x<<" "<<txyz.x<<endl;

	vert1.x+=txyz.x;
	vert1.y+=txyz.y;
	vert1.z+=txyz.z;
	//cout<<"tx "<<vert1.x<<" "<<txyz.x<<endl;
}

template<class T>
inline void add(  vec3<T> & sum , const vertex<T> & vert1)
{
	//cout<<"tx "<<vert1.x<<" "<<txyz.x<<endl;

	sum.x+=vert1.x;
	sum.y+=vert1.y;
	sum.z+=vert1.z;
	//cout<<"tx "<<vert1.x<<" "<<txyz.x<<endl;
}


///Pure vec operations
template<class T> T angle(const vec3<T> & vecA, const vec3<T> & vecB ){
    //atan2(|cross(AxB)|
//    T magAxB = l2norm(crossProduct(vecA,vecB));
//    T dotAB = dotProduct(vecA,vecB);
    return atan2(l2norm(crossProduct(vecA,vecB)),dotProduct(vecA,vecB));
    return atan2(l2norm(crossProduct(vecA,vecB)),dotProduct(vecA,vecB));
}
template<class T>
inline vec3<T> crossProduct( const vec3<T> & vecA, const vec3<T> & vecB )
{
	return vec3<T>(vecA.y*vecB.z - vecA.z*vecB.y,\
			vecA.z*vecB.x - vecA.x*vecB.z,\
			vecA.x*vecB.y - vecA.y*vecB.x );

}
template<class T>
inline T dotProduct( const vec3<T> & vecA, const vec3<T> & vecB )
{
	return (vecA.x*vecB.x + vecA.y*vecB.y + vecA.z*vecB.z);

}

template<class T>
inline void normalize( vec3<T> & vecA)
{
    float length2 = (vecA.x*vecA.x+vecA.y*vecA.y+vecA.z*vecA.z);
	if (length2 > 0 )
    vecA /= sqrt(length2);
}
template<class T>
inline T l2norm( const vec3<T> & vecA)
{
//	T val = sqrt(vecA.x*vecA.x+vecA.y*vecA.y+vecA.z*vecA.z);
//    return (val < 0) ? 0 : val;
    return sqrt(vecA.x*vecA.x+vecA.y*vecA.y+vecA.z*vecA.z);
}


template<class T>
inline T l2normSquared( const vec3<T> & vecA)
{
	return (vecA.x*vecA.x+vecA.y*vecA.y+vecA.z*vecA.z);
}

template<class T>
inline bool compareCoord( const vertex<T> & v0, const vertex<T> & v1 ){
    return ( (v0.x == v1.x) && (v0.y == v1.y) && (v0.z == v1.z) );
}

}
#endif /* AURCOMMON_AURSTRUCTS_FUNCTIONS_HPP_ */
