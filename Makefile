
MAKE_DIR = $(PWD)

LIB_SRCH_PATH :=
LIB_SRCH_PATH += -L$(MAKE_DIR)/libs
CFLAGS += $(INC_SRCH_PATH) $(LIB_SRCH_PATH) 

DESTDIR=./

all:
	@$(MAKE) -C src/aurcommon
	@$(MAKE) -C src/asrtreemodel
	@$(MAKE) -C src/aurgeometry    
	@$(MAKE) -C src/AsrImage3D
	@$(MAKE) -C src/aursurface
	@$(MAKE) -C src/utils
	@$(MAKE) -C src/airwayseg
	@$(MAKE) -C src/evaluation


install_local: 
	mkdir -p ${DESTDIR}/include/aurcommon
	mkdir -p ${DESTDIR}/include/asrtreemodel
	mkdir -p ${DESTDIR}/include/aurgeometry
	mkdir -p ${DESTDIR}/include/aursurface
	mkdir -p ${DESTDIR}/include/AsrImage3D
	mkdir -p ${DESTDIR}/include/utils
	mkdir -p ${DESTDIR}/lib
	mkdir -p ${DESTDIR}/bin
	cp src/aurcommon/*.h src/aurcommon/*.hpp  include/aurcommon/
	cp src/asrtreemodel/*.h include/arstreemodel/
	cp src/aurgeometry/*.h include/aurgeometry/
	cp src/aursurface/*.h include/aursurface/
	cp src/AsrImage3D/*.h include/AsrImage3D/
	cp src/utils/*.hpp src/utils/*h include/utils/
